<?php

namespace shirtplatform\constants;

/**
 * Description of WsConstants
 *
 * @author Jan Maslik
 */
class WsConstants {

	static $WS_TIME_OUT = 60; //60sec
	
        static $eventHandler = null;

	const WS_DEFAULT_PAGE_SIZE = 10000001; //using size=1000 in query makes GuzzleHttp ignore content-type (som bug)

        static $DISABLE_ASYNC_CALLS = false;
        
        static $webservicesURL = 'http://api.shirtplatform.com/webservices/rest/';
        
        static $webservicesPROXY; //optional, use only if you know what you do
        
        static $DEVELOPER_MODE = false;
	
	const IMG_REPLACEMENT = '.shirtplatform.com';
  }