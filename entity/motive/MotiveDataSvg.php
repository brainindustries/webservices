<?php
namespace shirtplatform\entity\motive;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 */
class MotiveDataSvg extends \shirtplatform\entity\abstraction\OneToOneParentDao {
    
    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/motives/{parentId}/svgData';
    const VAR_NAME = 'motiveDataSvg';
    
    public static $classMap = array(
        
    );
    
    public $version;
    public $name;
    public $size;
    public $image;
   
    public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
    {
        parent::__construct($data,$parents,$foreignKeyOnly);
    }
    
}
