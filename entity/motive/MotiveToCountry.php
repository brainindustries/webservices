<?php

namespace shirtplatform\entity\motive;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MotiveToCountry
 *
 * @author Jan Maslik
 */
use \shirtplatform\rest\REST;
use \User;
use shirtplatform\parser\WsParse;

class MotiveToCountry extends \shirtplatform\entity\abstraction\JsonEntity
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/motives/countries';
    const VAR_NAME = 'motiveToCountry';

    private $dao_shopId = null;
    public static $classMap = array(
    );
    public static $classArrayMap = array(
        'countries' => '\shirtplatform\entity\account\Country'
    );
    public $countries = null;

    public function getShopId()
    {
        return $this->dao_shopId;
    }

    /**
     * Set shop Id.
     * 
     * @param int $shopId
     */
    public function setShopId($shopId)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $this->dao_shopId = $shopId;
    }

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get url path.
     * 
     * @param int $shopId
     * @return string
     */
    public static function getUrl(&$shopId = null)
    {
        $className = get_called_class();
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);

        return $url;
    }

    /**
     * Get all enabled motive countries.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return MotiveToCountry[]
     */
    public static function &findAll(\shirtplatform\filter\WsParameters $wsParameters = null, $shopId = null, $isAsync = false)
    {

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $uri = self::getUrl($shopId);
        
        $wsParameters->setRootEntityName('\shirtplatform\entity\motive\Motive');
        
        return self::findAllFromUrl($uri, $wsParameters, $shopId, [] , $isAsync);
    }

}
