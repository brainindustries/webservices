<?php

namespace shirtplatform\entity\motive;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class MotiveDataSwfLayer extends \shirtplatform\entity\abstraction\ParentOwnedDao {
    
    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/motives/{parentId}/layers';
    const VAR_NAME = 'motiveSwfLayer';
    
    public static $classMap = array(
        
    );
    
    public $version;
    public $name;
    public $orderIndex;
    public $defaultColor;
    
    
    public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
        if($this->defaultColor == NULL) {
            $this->defaultColor = '000000';
        }
	}
    
}
