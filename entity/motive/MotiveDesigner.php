<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\motive;

/**
 * Description of MotiveDesigner
 *
 * @author Jan Maslik
 */
class MotiveDesigner extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/motiveDesigners';
	const VAR_NAME = 'motiveDesigner';
	
	public static $classMap = array(
	);
	
	public $name;
	public $version;
	public $deleted;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}
