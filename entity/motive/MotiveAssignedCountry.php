<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\motive;

/**
 * Description of MotiveAssignedCountry
 *
 * @author Jan Maslik
 */
class MotiveAssignedCountry extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/motives/{parentId}/countries';
	const VAR_NAME = 'country';
	
	public static $classMap = array(
		'standardVat' => '\shirtplatform\entity\account\Vat',
		'reducedVat' => '\shirtplatform\entity\account\Vat',
		'currency' => '\shirtplatform\entity\account\Currency',
	);
	
	public $name;
	public $deleted;
	public $version;
	public $code;
	public $standardVat;
	public $reducedVat;
	public $currency;
	
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}