<?php
namespace shirtplatform\entity\motive;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MotiveDataBitmap
 *
 * @author Jan Maslik
 */
class MotiveDataSwf extends \shirtplatform\entity\abstraction\OneToOneParentDao {
    
    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/motives/{parentId}/swfData';
    const VAR_NAME = 'motiveDataSwf';
    
    public static $classMap = array(
        
    );
    
    public $version;
    public $name;
    public $size;
    
    
    public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
    {
        parent::__construct($data,$parents,$foreignKeyOnly);
    }
    
}