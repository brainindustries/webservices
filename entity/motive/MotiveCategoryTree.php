<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\motive;

/**
 * Description of MotiveCategoryTree
 *
 * @author Jan Maslik
 */
use \User;
use \shirtplatform\rest\REST;
use shirtplatform\parser\WsParse;
use shirtplatform\parser\WsModel;
use shirtplatform\filter\WsParameters;

class MotiveCategoryTree extends \shirtplatform\entity\abstraction\JsonEntity
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/motiveCategories/trees';
    const VAR_NAME = 'motiveCategoryTree';

    public static $classMap = array(
        'category' => '\shirtplatform\entity\motive\MotiveCategory'
    );
    public $category = null;
    public $parentId;
    public $childs = array();

    /**
     * Get parent tree.
     * 
     * @param int $categoryId
     * @param WsParameters|null $wsParameters
     * @param int|null $shopId
     * @return MotiveCategoryTree
     */
    public static function findParentTree($categoryId, $wsParameters = null, $shopId = NULL)
    {
        $entities = self::findParents($categoryId, $wsParameters, $shopId);

        if (empty($entities))
        {
            return null;
        }

        $rootEntities = array();

        foreach ($entities as $entity)
        {
            if ($entity->parentId == null)
            {
                $rootEntities[$entity->id] = $entity;
            } else
            {
                $entities[$entity->parentId]->childs[$entity->id] = $entity;
            }
        }

        return ($rootEntities === NULL) ? NULL : reset($rootEntities);
    }

    /**
     * Get child tree.
     * 
     * @param int $categoryId
     * @param WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return MotiveCategoryTree
     */
    public static function &findChildTree($categoryId, $wsParameters = null, $shopId = NULL, $isAsync =false)
    {
        $entities = self::findChilds($categoryId, $wsParameters, $shopId, $isAsync);

        $rootEntity = array();

        foreach ($entities as $entity)
        {
            if ($entity->parentId == null)
            {
                $rootEntity[$entity->id] = $entity;
            } else
            {
                $entities[$entity->parentId]->childs[$entity->id] = $entity;
            }
        }

        return $rootEntity;
    }

    /**
     * Get url path.
     * 
     * @param int $shopId
     * @return string
     */
    public static function getUrl(&$shopId = null)
    {
        $className = get_called_class();
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);

        return $url;
    }

    /**
     * Get parent categories.
     * 
     * @param int $categoryId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return MotiveCategory
     */
    public static function &findParents($categoryId, $wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\motive\MotiveCategory');

        $url = self::getUrl($shopId) . '/' . $categoryId . '/parents';

        return self::findAllFromUrl($url, $wsParameters,$shopId,[],$isAsync);
    }

    /**
     * Get child categories.
     * 
     * @param int $categoryId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return MotiveCategory
     */
    public static function &findChilds($categoryId, $wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\motive\MotiveCategory');

        $url = self::getUrl($shopId) . '/' . $categoryId . '/childs';

        return self::findAllFromUrl($url, $wsParameters,$shopId ,[] , $isAsync);
    }

}
