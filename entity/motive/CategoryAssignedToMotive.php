<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\motive;

/**
 * Description of CategoryAssignedToMotive
 *
 * @author Jan Maslik
 */
use shirtplatform\filter\WsParameters;

class CategoryAssignedToMotive extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/motives/{parentId}/assignedCategories';
    const VAR_NAME = 'categoryAssignedToMotive';
    const PATH_ASSIGN_ALL = 'accounts/{accountId}/shops/{shopId}/motives/assignedCategories';

    public static $classMap = array(
        'category' => '\shirtplatform\entity\motive\MotiveCategory',
    );
    public $active;
    public $orderIndex;
    public $category;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * 
     * @param CategoryAssignedToMotive $newCategory
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @return void
     */
    public static function assignAll($newCategory, $wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\motive\Motive');

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_ASSIGN_ALL);
        $url = str_replace('{shopId}', $shopId, $url);
        $options = [
            'json' => array($newCategory::VAR_NAME => $newCategory->getWebserviceAttributes())
        ];
        
        $loadAll=false;
        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\JsonPromise('POST', $url, $wsParameters->buildParams(), $options, false);
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);

        $result = intval($promise->message);

        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::deleteCategoryAll($wsParameters, $shopId);
        }
        return $result;
    }

}
