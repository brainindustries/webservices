<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\motive;

/**
 * Description of Motive
 *
 * @author Jan Maslik
 */
use \shirtplatform\rest\REST;
use \User;
use \shirtplatform\filter\WsParameters;
use \shirtplatform\filter\Filter;
use \shirtplatform\parser\WsParse;
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\parser\WsModel;
use \shirtplatform\entity\motive\MotiveAssignedTag;
use \shirtplatform\entity\motive\MotiveAssignedCountry;

class Motive extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/motives';
    const PATH_ALL = 'accounts/{accountId}/motives';
    const PATH_CATEGORY_ASSIGN_ALL = 'accounts/{accountId}/shops/{shopId}/motives/assignedCategories';
    const VAR_NAME = 'motive';

    public static $classMap = array(
        'localizations' => '\shirtplatform\entity\localization\MotiveLocalized',
        'assignedCategories' => '\shirtplatform\entity\motive\CategoryAssignedToMotive',
        'tags' => '\shirtplatform\entity\motive\MotiveAssignedTag',
        'countries' => '\shirtplatform\entity\account\Country',
        'pool' => '\shirtplatform\entity\abstraction\Entity',
        'originalMotive' => '\shirtplatform\entity\abstraction\Entity'
    );
    public $name;
    public $type;
    public $priceMultiplier;
    public $deleted;
    public $active;
    public $version;
    public $designerId;
    public $authorProvision;
    public $motiveProfit;
    public $overridePrice;
    public $exactPrice;
    public $swfId;
    public $svgId;
    public $bitmapId;
    public $pool;
    public $orderIndex;
    public $uuid;
    protected $localizations = null;
    protected $assignedCategories = null;
    protected $tags = null;
    protected $countries = null;
    public $originalMotive = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get motive preview url.
     * 
     * @return string
     */
    public function getImageUrl()
    {
        return $this->getAtomLink('image');
    }

    /**
     * Get clean motive preview url.
     * 
     * @return string
     */
    public function getImageCleanUrl()
    {
        return $this->getAtomLink('cleanImage');
    }

    /**
     * Get SWF motive url.
     * 
     * @return string
     */
    public function getSwfUrl()
    {
        return $this->getAtomLink('swf');
    }

    /**
     * Get SVG motive url.
     * 
     * @return string
     */
    public function getSvgUrl()
    {
        return $this->getAtomLink('svg');
    }

    /**
     * Get localizations.
     * 
     * @return \shirtplatform\entity\localization\MotiveLocalized[]
     */
    public function getLocalizations()
    {
        return $this->getAtomLinkValue('localizations');
    }

    /**
     * Get assigned categories.
     * 
     * @return \shirtplatform\entity\motive\CategoryAssignedToMotive[]
     */
    public function getAssignedCategories()
    {
        return $this->getAtomLinkValue('assignedCategories');
    }

    /**
     * Get assigned tags.
     * 
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @param WsParameters|null $wsParameters
     * @return \shirtplatform\entity\motive\MotiveAssignedTag[]
     */
    public function getTags($isAsync = false, $forceLoad = false, $wsParameters = null)
    {
        return $this->getAtomLinkValue('tags', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Assign tag to motive.
     * 
     * @param int $tagId
     */
    public function addTag($tagId)
    {
        $tag = new MotiveAssignedTag(array('id' => $tagId));
        $tag->setParents($this->getParentList());
        $tag->setShopId($this->getShopId());
        $tag->__update();
    }

    /**
     * Remove tag assignment.
     * 
     * @param int $tagId
     */
    public function removeTag($tagId)
    {
        $tag = new MotiveAssignedTag(array('id' => $tagId));
        $tag->setParents($this->getParentList());
        $tag->setShopId($this->getShopId());
        $tag->__delete();
    }

    /**
     * Get enabled countries.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\account\Country[]
     */
    public function getCountries($isAsync = false)
    {
        return $this->getAtomLinkValue('countries', null, $isAsync);
    }

    /**
     * Create new country motive assignment.
     * 
     * @param int $countryId
     */
    public function addCountry($countryId)
    {
        $tag = new MotiveAssignedCountry(array('id' => $countryId));
        $tag->setParents($this->getParentList());
        $tag->setShopId($this->getShopId());
        $tag->__update();
    }

    /**
     * Remove country assignment.
     * 
     * @param int $countryId
     */
    public function removeCountry($countryId)
    {
        $tag = new MotiveAssignedCountry(array('id' => $countryId));
        $tag->setParents($this->getParentList());
        $tag->setShopId($this->getShopId());
        $tag->__delete();
    }

    /**
     * Set motive bitmap.
     * 
     * @param string $sourcePath
     * @param string $name
     * @return void
     */
    public function setBitmap($sourcePath, $name)
    {
        return \shirtplatform\resource\UploadResource::uploadMotiveImage($this->id, $sourcePath, $name, $this->getShopId());
    }

    /**
     * Set motive vector.
     * 
     * @param string $sourcePath
     * @param string $name
     * @return void
     */
    public function setVector($sourcePath, $name)
    {
        return \shirtplatform\resource\UploadResource::uploadMotiveVector($this->id, $sourcePath, $name, $this->getShopId());
    }

    /**
     * Duplicate an motive.
     * 
     * @return Motive
     */
    public function __duplicate()
    {
        return self::duplicate($this->id, $this->getShopId());
    }

    /**
     * Find all in account.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameter
     * @param boolean $isAsync
     * @return Motive[]
     */
    public static function &findAllByAccount(\shirtplatform\filter\WsParameters $wsParameter = null, $isAsync = false)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_ALL);
        return self::findAllFromUrlPost($url, $wsParameter, null, null, $isAsync);
    }

    /**
     * Count all in account.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @return int
     */
    public static function &countByAccount($wsParameters = null, $isAsync = false)
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->page = 0;
        $wsParameters->size = 1;
        $wsParameters->setRootEntityName('\shirtplatform\entity\motive\Motive');
        $rest = \shirtplatform\rest\REST::getInstance();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_ALL);

        $promise = new \shirtplatform\rest\promise\CountPromise('GET', $url, $wsParameters->buildParams() , [] , $isAsync);
        $promise->filterPromise = $wsParameters->getPromise(false);
        return $rest->_call($promise);
    }

    /**
     * Move motive to desired shop.
     * 
     * @param int $motiveId
     * @param int $dstShopId
     * @param boolean $isAsync
     * @return void
     */
    public static function &move($motiveId, $dstShopId = null ,$isAsync = false)
    {
        if ($dstShopId == null)
        {
            $dstShopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = self::getUrl($dstShopId);
        $url .= '/move/' . $motiveId;
        $rest = \shirtplatform\rest\REST::getInstance();

         
        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], [] , $isAsync);
        $promise->shopId = $dstShopId;
        return $rest->_call($promise);
    }

    /**
     * Duplicate an motive.
     * 
     * @param int $motiveId
     * @param int $shopId
     * @return Motive
     */
    public static function duplicate($motiveId, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = self::getUrl($shopId);
        $url .= '/duplicate/' . $motiveId;
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [], false);
        $promise->shopId = $shopId;
        
        return $rest->_call($promise);
    }

    /**
     * Set active status.
     * 
     * @param boolean $isActive
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @return void
     */
    public static function setActive($isActive, $wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $loadAll = false;

        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        $wsParameters->setRootEntityName(get_called_class());

        $url = self::getUrl($shopId);
        $url .= '/active/' . ($isActive ? 'true' : 'false');
        $rest = \shirtplatform\rest\REST::getInstance();
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url ,  $wsParameters->buildParams(), array(), false );
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);
        
        $result = intval( $promise->message );

        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::setActive($isActive, $wsParameters, $shopId);
        }
        return $result;
    }

    /**
     * Move multiple motives.
     * 
     * @param int $newShopId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @return void
     */
    public static function moveAll($newShopId, $wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $loadAll = false;

        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        $wsParameters->setRootEntityName(get_called_class());

        $url = self::getUrl($shopId);
        $url .= '/moveAll/' . $newShopId;
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url ,  $wsParameters->buildParams(), array(), false );
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);
        
        $result = intval( $promise->message );

        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::moveAll($newShopId, $wsParameters, $shopId);
        }
        return $result;
    }
    
    public static function &getOriginalSourceCheckSum($id, $shopId = null , $isAsync = false)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        $url = self::getUrl($shopId);
        $url .= '/'.$id.'/originalSourceCheckSum';
        
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, "\shirtplatform\\entity\abstraction\StringDataStructure", [], [], $isAsync);
        $promise->shopId = $shopId;
        return $rest->_call($promise);
    }
    
    /**
     * Add country to multiple motives.
     * 
     * @param int $countryId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @return void
     */
    public static function addCountryAll($countryId, $wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $loadAll = false;

        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        $wsParameters->setRootEntityName(get_called_class());

        $url = self::getUrl($shopId);
        $url .= '/countries/' . $countryId;
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url ,  $wsParameters->buildParams(), array(), false );
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);
        
        $result = intval( $promise->message );


        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::addCountryAll($countryId, $wsParameters, $shopId);
        }
        return $result;
    }

    /**
     * Remove country on multiple motives.
     * 
     * @param int $countryId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @return void
     */
    public static function removeCountryAll($countryId, $wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $loadAll = false;

        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\motive\Motive');

        $url = self::getUrl($shopId);
        $url .= '/countries/' . $countryId;
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $url ,  $wsParameters->buildParams(), array(), false );
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);
        
        $result = intval( $promise->message );


        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::removeCountryAll($countryId, $wsParameters, $shopId);
        }
        return $result;
    }

    /**
     * Set designer on multiple motives.
     * 
     * @param int $designerId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @return void
     */
    public static function setDesignerAll($designerId, $wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\motive\Motive');

        $url = self::getUrl($shopId);
        $url .= '/designer/' . $designerId;
        $rest = \shirtplatform\rest\REST::getInstance();

        $loadAll=false;
        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('POST', $url ,  $wsParameters->buildParams(), array(), false );
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);
        
         $result = intval($promise->message);
        
        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::deleteCategoryAll($wsParameters, $shopId);
        }
        
        return $result;
    }

    /**
     * Assign category on multiple motives.
     * 
     * @param int $newCategory
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @return void
     */
    public static function assignCategoryAll($newCategory, $wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            throw new \Exception("Empty filter, please use filter.");
        }

        $wsParameters->setRootEntityName(get_called_class());

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_CATEGORY_ASSIGN_ALL);
        $url = str_replace('{shopId}', $shopId, $url);

        $rest = \shirtplatform\rest\REST::getInstance();
        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
        }
        
        $options = [
            'json' => [$newCategory::VAR_NAME => $newCategory->getWebserviceAttributes()]
        ];
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('POST', $url ,  $wsParameters->buildParams(), $options , false );
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);
        
        $result = intval($promise->message);
        return $result;
    }

    /**
     * Delete category assignment on multiple motives.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @return void
     */
    public static function deleteCategoryAll($wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName(get_called_class());

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_CATEGORY_ASSIGN_ALL);
        $url = str_replace('{shopId}', $shopId, $url);
        $rest = \shirtplatform\rest\REST::getInstance();
        
        $loadAll=false;
        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $url ,  $wsParameters->buildParams(), [] , false );
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);
        
        $result = intval($promise->message);
        
        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::deleteCategoryAll($wsParameters, $shopId);
        }
        
        return $result;
    }

    /**
     * Set pool.
     * 
     * @param int $poolId
     * @return void
     */
    public function __setPool($poolId)
    {
        return self::setPool($this->id, $poolId, $this->getShopId());
    }

    /**
     * Set pool.
     * 
     * @param int $motiveId
     * @param int $poolId
     * @param int|null $shopId
     * @param boolean $isAsync
     */
    public static function setPool($motiveId, $poolId, $shopId = null,$isAsync = false )
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = self::getUrl($shopId);
        $url .= '/' . $motiveId . '/pools/' . $poolId;
        $rest = \shirtplatform\rest\REST::getInstance();
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url,[], [] , $isAsync);
        $rest->_call($promise);
    }
    
    
    /**
     * Synchronize product.
     * 
     * @param int $productId
     * @param \shirtplatform\entity\sharing\synchronize\SynchronizeProduct $syncStuct
     * @param boolean $shopId
     * @param boolean $isAsync
     * @return void
     */
    public static function &synchronize($motiveId, \shirtplatform\entity\sharing\synchronize\SynchronizeMotive $syncStuct = null, $shopId = null, $isAsync = false)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        if ($syncStuct == null)
        {
            $syncStuct = new \shirtplatform\entity\sharing\synchronize\SynchronizeMotive();
        }
        $url = self::getUrl($shopId);
        $url .= '/' . $motiveId . '/synchronize';
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], array( \shirtplatform\entity\sharing\synchronize\SynchronizeMotive::VAR_NAME => $syncStuct), $isAsync);
        $promise->shopId = $shopId;
        return $rest->_call($promise);
    }

}
