<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\motive;

/**
 * Description of MotivePrintTechnology
 *
 * @author Jan Maslik
 */
class MotivePrintTechnology extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/motives/{parentId}/technologies';
	const VAR_NAME = 'motivePrintTechnology';
	
	public static $classMap = array(
		'printTechnology' => '\shirtplatform\entity\technology\PrintTechnology'
	);
	
	public $printTechnology;
	public $active = true;
	public $minWidth = 0;
	public $minHeight = 0;
	public $maxWidth = 0;
	public $maxHeight = 0;
	public $allowScale = true;
	public $keepProportions = true;
	public $allowRotation = true;
	public $only90DegreesRotation = true;
	public $allowMirror = true;
	public $allowColorChange = true;
	public $reduceColors = false;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}
