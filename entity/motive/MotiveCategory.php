<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\motive;

/**
 * Description of MotiveCategory
 *
 * @author Jan Maslik
 */
use \User;
use \shirtplatform\rest\REST;
use shirtplatform\parser\WsModel;
use shirtplatform\parser\WsParse;
use shirtplatform\parser\ArrayUtil;
use shirtplatform\filter\Filter;
use shirtplatform\filter\WsParameters;
use \DebugException;

class MotiveCategory extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/motiveCategories';
    const VAR_NAME = 'motiveCategory';

    public static $classMap = array(
        'localizations' => '\shirtplatform\entity\localization\MotiveCategoryLocalized',
        'assignedMotives' => '\shirtplatform\entity\motive\MotiveAssignedToCategory'
    );
    public $name;
    public $orderIndex;
    public $active;
    public $deleted;
    public $version;
    protected $localizations = null;
    protected $assignedMotives = null;
    protected $countAssignedProducts = null;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get assigned motives.
     * 
     * @param WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\motive\MotiveAssignedToCategory[]
     */
    public function getAssignedMotives($wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('assignedMotives', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Assign parent category.
     * 
     * @param int $parentCategoryId
     */
    public function __assignParentCategory($parentCategoryId = '')
    {
        self::assignParentCategory($this->id, $parentCategoryId);
    }

    /**
     * Get child categories.
     * 
     * @return MotiveCategory[]
     */
    public function getChildCategories()
    {
        return self::findChildCategories($this->id, null);
    }

    /**
     * Assign parent category.
     * 
     * @param int $categoryId
     * @param int $parentCategoryId
     * @param int|null $shopId
     * @param boolean $isAsync
     */
    public static function assignParentCategory($categoryId, $parentCategoryId = '', $shopId = null , $isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();

        $url = self::getUrl($shopId) . '/' . $parentCategoryId . '/ownedCategory/' . $categoryId;
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url ,[], [] , $isAsync);
        $rest->_call($promise);
    }

    /**
     * Find threaded categories.
     * 
     * @param int $categoryId
     * @param int|null $shopId
     * @return MotiveCategory[]
     */
    public static function findThreadCategories($categoryId, $shopId = null)
    {
        $url = self::getUrl($shopId) . '/' . $categoryId . '/thread';

        return self::findAllFromUrl($url, null);
    }

    /**
     * Find child categories.
     * 
     * @param int $parentCategoryId
     * @param WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @return MotiveCategory[]
     */
    public static function &findChildCategories($parentCategoryId = '', $wsParameters = null, $isAsync = false )
    {
        $url = self::getUrl() . '/' . $parentCategoryId . '/ownedCategory';
        
        $result = self::findAllFromUrl($url, $wsParameters, null, null, $isAsync);
       
        if( isset($result[$parentCategoryId]) )
        {
            unset($result[$parentCategoryId]);
        }
        
        return $result;
    }

    /**
     * Get assigned motives scount.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceReload
     * @return int
     */
    public function getAssignedMotivesCount(\shirtplatform\filter\WsParameters $wsParameters = null ,$isAsync = false , $forceReload = false )
    {
        if( $this->countAssignedProducts == null || $forceReload )
        {
            $this->countAssignedProducts = MotiveAssignedToCategory::count( $this->getParentList() , $wsParameters, $this->getShopId() , $isAsync);
        }
        
        return $this->countAssignedProducts;
    }

    /**
     * Get localizations.
     * 
     * @return \shirtplatform\entity\localization\MotiveCategoryLocalized[]
     */
    public function getLocalizations(WsParameters $wsParams = NULL, $isAsync = false)
    {
        return $this->getAtomLinkValue('localizations', $wsParams, $isAsync);
    }

    /**
     * Duplicate an category.
     * 
     * @param int $categoryId
     * @param int|null $shopId
     * @return MotiveCategory
     */
    public static function &duplicate($categoryId, $shopId = null)
    {
        $url = self::getUrl($shopId);
        $url .= '/duplicate/' . $categoryId;
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [], false);
        $promise->shopId = $shopId;
        
        return $rest->_call($promise);
    }

    /**
     * Import own category.
     * 
     * @param int $categoryId
     * @param int $dstShopId
     * @return MotiveCategory
     */
    public static function importOwn($categoryId, $dstShopId = null)
    {
        $url = self::getUrl($dstShopId);
        $url .= '/importOwn/' . $categoryId;
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [], false);
        $promise->shopId = $dstShopId;
        
        return $rest->_call($promise);
    }

    /**
     * Find category roots.
     * 
     * @param WsParameters|null $wsParameters
     * @param int $shopId
     * @param boolean $isAsync
     * @return MotiveCategory[]
     */
    public static function &findRoots($wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        $url = self::getUrl($shopId);
        $url .= '/roots';
        return self::findAllFromUrl($url, $wsParameters, $shopId, [], $isAsync);
    }

}
