<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductionFileProperties
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class ProductionFileProperties {
    
    const NAME_PATTERN = 'NAME_PATTERN';
    const GROUP_PAGED_OBJECTS = 'GROUP_PAGED_OBJECTS';
    const PAGE_SORTER = 'PAGE_SORTER';
    const IGNORE_AMOUNT = 'IGNORE_AMOUNT';
    const INCLUDE_EMPTY_TEMPLATES = 'INCLUDE_EMPTY_TEMPLATES';
    
}