<?php

namespace shirtplatform\entity\enumerator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class UserAuthority
{

    const ROLE_AUTHORIZED_USER = 'ROLE_AUTHORIZED_USER';
    const ROLE_ACCOUNT_ADMIN = 'ROLE_ACCOUNT_ADMIN';
    const ROLE_SHOP_ADMIN = 'ROLE_SHOP_ADMIN';
    const ROLE_PRODUCTION_GUEST = 'ROLE_PRODUCTION_GUEST';
    const ROLE_PRODUCTION_GROUP = 'ROLE_PRODUCTION_GROUP';
    const ROLE_PRODUCTION_SUPERADMIN = 'ROLE_PRODUCTION_SUPERADMIN';
    const ROLE_STOCK_ADMIN = 'ROLE_STOCK_ADMIN';
    const ROLE_PRODUCTION_SHOP_ACCESS = 'ROLE_PRODUCTION_SHOP_ACCESS';

}
