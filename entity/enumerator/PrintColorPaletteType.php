<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintColorPalette
 *
 * @author Jan Maslik
 */
class PrintColorPaletteType
{
	const LIST_ = 'LIST';
	const WHEEL_ = 'WHEEL';
}
