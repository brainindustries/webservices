<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TextureMode
 *
 * @author Jan Maslik
 */
class TextureMode
{
	const TILED = 'TILED';
	const FILL = 'FILL';
	const STRETCH = 'STRETCH';
}
