<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductionTransformerModuleName
 *
 * @author Jan Maslik
 */
class ProductionTransformerModuleName
{
	const BASIC_TILING = 'BASIC_TILING';
        const AS_COMPOSITION = 'AS_COMPOSITION';
        const FIXED_POSITION = 'FIXED_POSITION';
        const ADVANCED_TILING = 'ADVANCED_TILING';
        const HORIZONTAL_TILING = 'HORIZONTAL_TILING';
}
