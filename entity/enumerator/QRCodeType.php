<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QRCodeType
 *
 * @author Jan Maslik
 */
class QRCodeType
{
	const TEXT = 'TEXT';
	const SMS = 'SMS';
	const URL = 'URL';
	const PHONE = 'PHONE';
}
