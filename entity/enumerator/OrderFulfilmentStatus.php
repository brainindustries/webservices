<?php

namespace shirtplatform\entity\enumerator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrderFulfilmentStatus
 *
 * @author admin
 */
class OrderFulfilmentStatus
{
        /**
	 * Waiting for reources (pimp results, ...)
	 */
	const WAITING_FOR_RESOURCES = 'WAITING_FOR_RESOURCES';

	/**
	 * All resources available
	 */
	const PREPARED_TO_PRODUCE = 'PREPARED_TO_PRODUCE';

	/**
	 * Order was moved to workbanch
	 */
	const IN_WORKBANCH = 'IN_WORKBANCH';
	
	/**
	 * Order was fulfilled (produced and sent to customer)
	 */
	const FINISHED = 'FINISHED';

	/**
	 * Got reclamation request
	 */
	const RECLAMATION = 'RECLAMATION';

	/**
	 * Order was cancelled
	 */
	const CANCELLED = 'CANCELLED';
    
}
