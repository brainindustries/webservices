<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ProductionTransformerProperties
{
	const EXCEED_USE = 'EXCEED_USE';
        const EXCEED_USE_AUTO = 'AUTO';
        const EXCEED_USE_SHRINK_COMPOSITION = 'SHRINK';
        const EXCEED_USE_AREA = 'AREA';
        
        const PAGE_SIZE_MODE = 'PAGE_SIZE_MODE';
        const PAGE_SIZE_MODE_AREA = 'AREA';
        const PAGE_SIZE_MODE_MEDIUM = 'MEDIUM';
        const PAGE_SIZE_MODE_ELEMENT_BOUNDS = 'ELEMENT_BOUNDS';
        const PAGE_SIZE_MODE_CUSTOM = 'CUSTOM';
        
        const PAGE_HEIGHT_MM = 'PAGE_HEIGHT_MM';
        const PAGE_WIDTH_MM = 'PAGE_WIDTH_MM';
        
        const ALLOW_ROTATION_TO_FIT = 'ALLOW_ROTATION_TO_FIT';
        const ALIGN_VERTICAL = 'ALIGN_VERTICAL';
        const ALIGN_HORIZONTAL = 'ALIGN_HORIZONTAL';
        
        const ALIGN_VERTICAL_TOP = 'TOP';
        const ALIGN_VERTICAL_MIDDLE = 'MIDDLE';
        const ALIGN_VERTICAL_BOTTOM = 'BOTTOM';
        
        const ALIGN_HORIZONTAL_LEFT = 'LEFT';
        const ALIGN_HORIZONTAL_CENTER = 'CENTER';
        const ALIGN_HORIZONTAL_RIGHT = 'RIGHT';
        
        const PAGE_BORDER_MM = 'PAGE_BORDER_MM';
        const OBJECT_BORDER_MM = 'OBJECT_BORDER_MM';
        
        const SPLIT_TEMPLATE = 'SPLIT_TEMPLATE';
        const SPREAD_CROSS_THE_LINE = 'SPREAD_CROSS_THE_LINE';
        const MINIMIZE_PAGE_HEIGHT = 'MINIMIZE_PAGE_HEIGHT';
        
        //const SPLIT_OVERSIZE = 'SPLIT_OVERSIZE';
        ///const THRESHOLD_EXCEED = 'THRESHOLD_EXCEED'; removed
        
        const EXCEED_STEPS = 'EXCEED_STEPS';
        const EXCEED_PAGE = 'EXCEED_PAGE';
        
        const FOOTER_PAGE_SVG = 'FOOTER_PAGE_SVG';
        const FOOTER_PAGE_SVG_ENABLE = 'FOOTER_PAGE_SVG_ENABLE';
        const FOOTER_PAGE_SVG_WIDTH = 'FOOTER_PAGE_SVG_WIDTH';
        const FOOTER_PAGE_SVG_HEIGHT = 'FOOTER_PAGE_SVG_HEIGHT';
        
        const CSV_TEMPLATE_ENABLE = 'CSV_TEMPLATE_ENABLE';
        const CSV_TEMPLATE = 'CSV_TEMPLATE';
}
