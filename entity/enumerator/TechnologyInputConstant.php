<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TechnologyInputConstant
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class TechnologyInputConstant {
    
    const MOTIVE_COLLECTION = 1;
    const USER_UPLOADS = 2;
    const TEXTS = 3;
    const QR_CODE = 4;
    
}
