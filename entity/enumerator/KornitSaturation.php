<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of KornitSaturation
 *
 * @author Jan Maslik
 */
abstract class KornitSaturation
{
	const NORMAL = 'NORMAL';
	const DARKER = 'DARKER';
	const DARKEST = 'DARKEST';
}

?>
