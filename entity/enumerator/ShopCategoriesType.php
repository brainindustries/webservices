<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShopCategoriesType
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class ShopCategoriesType {
    
    const IMMEDIATELY = 'IMMEDIATELY';
    const AUTOMATIC = 'AUTOMATIC';
    const NONE = 'NONE';
    
}
