<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductionDisplayProperties
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class ProductionDisplayProperties {
    
    const ENABLE_OUTLINE = 'ENABLE_OUTLINE';
    const FILL_OUTLINE = 'FILL_OUTLINE';
    const HORIZONTAL_MIRROR_RESULT = 'HORIZONTAL_MIRROR_RESULT';
    
    const DESCRIPTION_TEMPLATE_SVG = 'DESCRIPTION_TEMPLATE_SVG';
    const DESCRIPTION_TEMPLATE_WIDTH = 'DESCRIPTION_TEMPLATE_WIDTH';
    const DESCRIPTION_TEMPLATE_HEIGHT = 'DESCRIPTION_TEMPLATE_HEIGHT';
    const DESCRIPTION_TEMPLATE_LOCATION  = 'DESCRIPTION_TEMPLATE_LOCATION';
    
    const DESCRIPTION_TEMPLATE_X = 'DESCRIPTION_TEMPLATE_X';
    const DESCRIPTION_TEMPLATE_Y = 'DESCRIPTION_TEMPLATE_Y';
    
}
