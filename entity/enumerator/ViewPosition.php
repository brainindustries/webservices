<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ViewPosition
 *
 * @author Jan Maslik
 */
abstract class ViewPosition
{
	const NONE = 'NONE';
	const FRONT = 'FRONT';
	const BACK = 'BACK';
	const LEFT = 'LEFT';
	const RIGHT = 'RIGHT';
	const TOP = 'TOP';
	const BOTTOM = 'BOTTOM';
        const NECKTAG = 'NECKTAG';
}
