<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UploadPriceTypeConstant
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class ShopProperties {
    
    const BOX_IMG = 'BOX_IMG';
    const BOX_ICON = 'BOX_ICON';
    
    const PRIMARY_URL = 'PRIMARY_URL';
    const OTHER_URLS = 'OTHER_URLS';
    
    const CREATE_CATEGORIES = 'CREATE_CATEGORIES';
    const CATEGORIES_SYNCHRONISED = 'CATEGORIES_SYNCHRONISED';
    
    const IS_COLLECTION = 'IS_COLLECTION';
    const EORI_NUMBER = 'EORI_NUMBER';
    
    const EORIGB_NUMBER = 'EORIGB_NUMBER';
    const EORICONSIGNOR_NUMBER = 'EORICONSIGNOR_NUMBER';
    const VAT = 'VAT';
    
}
