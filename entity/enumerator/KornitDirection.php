<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of KornitDirection
 *
 * @author Jan Maslik
 */
abstract class KornitDirection
{
	const UNIDIRECTIONAL = 'UNIDIRECTIONAL';
	const BIDIRECTIONAL = 'BIDIRECTIONAL';
}

?>
