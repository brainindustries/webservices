<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Font
 *
 * @author Jan Maslik
 */
class FontConstants 
{
    const WEIGHT_NORMAL = 'NORMAL';
    const WEIGHT_BOLD = 'BOLD';
    const WEIGHT_HEAVY = 'HEAVY';
    
    const STYLE_NORMAL = 'NORMAL';
    const STYLE_ITALIC = 'ITALIC';
    const STYLE_OBLIQUE = 'OBLIQUE';
}
