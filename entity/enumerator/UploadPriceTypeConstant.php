<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UploadPriceTypeConstant
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class UploadPriceTypeConstant {

    const FREE = 1;
    const CONSTANT = 2;
    const CONSTANT_PRODUCT_AMOUNT = 3;
    const SURFACE = 4;
    const SURFACE_PRODUCT_AMOUNT = 5;
    const SURFACE_STEPS = 6;
    const SURFACE_STEPS_PRODUCT_AMOUNT = 7;
    const COLORS_AMOUNT = 8;
    const COLOR_AMOUNT_PRODUCT_AMOUNT = 9;
    
}
