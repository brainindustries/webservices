<?php

namespace shirtplatform\entity\enumerator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SplitOversizeConstant
 *
 * @author admin
 */
class SplitOversizeConstant
{
    const IGNORE = 'IGNORE';
    const SPLIT = 'SPLIT';
    const NEW_PAGE = 'NEW_PAGE';
    const NEW_FILE = 'NEW_FILE';
}
