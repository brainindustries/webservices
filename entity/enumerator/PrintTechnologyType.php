<?php

namespace shirtplatform\entity\enumerator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class PrintTechnologyType {
    
    const OTHER = 'OTHER'; 
    const FLEX_FLOCK = 'FLEX_FLOCK';
    const DIGITAL = 'DIGITAL';
    const DIGITAL_DIRECT = 'DIGITAL_DIRECT';
    const SUBLIMATION = 'SUBLIMATION';
    const LASER = 'LASER';
    const ENGRAVING = 'ENGRAVING';
    const SCREENPRINT = 'SCREENPRINT';
    const DIRECT_TO_FOIL = 'DIRECT_TO_FOIL';
    const KORNIT = 'KORNIT';
    
}
