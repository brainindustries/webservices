<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TextPriceTypeConstant
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class TextPriceTypeConstant {
    
    const _GLOBAL = 1;
    const BY_FONT = 2;
    
}
