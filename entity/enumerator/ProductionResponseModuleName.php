<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductionResponseModuleName
 *
 * @author Jan Maslik
 */
class ProductionResponseModuleName
{
    const ZIP_PDF = 'ZIP_PDF';
    const ZIP_IMG = 'ZIP_IMG';
}
