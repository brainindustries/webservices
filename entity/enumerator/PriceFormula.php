<?php

namespace shirtplatform\entity\enumerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PriceFormula
 *
 * @author Jan Maslik
 */
class PriceFormula
{

    const OBJECTS_PRICE = 'OBJECTS_PRICE';
    const SETUP_COST_GENERAL = 'SETUP_COST_GENERAL';
    const SETUP_COST_SIDE_TOTAL = 'SETUP_COST_SIDE_TOTAL';
    //const SETUP_COST_TOTAL = 'SETUP_COST_TOTAL';
    //const SETUP_COST_OBJECT_TOTAL = 'SETUP_COST_OBJECT_TOTAL';
    //const SETUP_COST_COLOR_TOTAL = 'SETUP_COST_COLOR_TOTAL';

    const LABOR_COST_GENERAL = 'LABOR_COST_GENERAL';
    const LABOR_COST_SIDE_TOTAL = 'LABOR_COST_SIDE_TOTAL';
    //const LABOR_COST_TOTAL = 'LABOR_COST_TOTAL';
    //const LABOR_COST_OBJECT_TOTAL = 'LABOR_COST_OBJECT_TOTAL';
    //const LABOR_COST_COLOR_TOTAL = 'LABOR_COST_COLOR_TOTAL';
    const COLOR_AMOUNT = 'COLOR_AMOUNT';
    const PRODUCT_AMOUNT = 'PRODUCT_AMOUNT';
    const COLOR_AMOUNT_PER_VIEW = 'COLOR_AMOUNT_PER_VIEW';
    const SETUP_COST_COLOR_PER_VIEW_TOTAL = 'SETUP_COST_COLOR_PER_VIEW_TOTAL';
    const FILLED_VIEWS = 'FILLED_VIEWS';
    const LAYER_SPLITS_AMOUNT = 'LAYER_SPLITS_AMOUNT';

}
