<?php

namespace shirtplatform\entity\enumerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PriceFormulaMotive
 *
 * @author Jan Maslik
 */
class PriceFormulaMotive
{

    const COLOR_PRICE = 'MATERIAL_PRICE';
    const MOTIVE_PRICE = 'MOTIVE_COMPLEXITY';

    //const LABOR_GENERAL = 'LABOR_COST_GENERAL';
    const LABOR_OBJECT = 'LABOR_COST_OBJECT';
    const LABOR_COLOR = 'LABOR_COST_COLOR';

    //const SETUP_GENERAL = 'SETUP_COST_GENERAL';
    const SETUP_OBJECT = 'SETUP_COST_OBJECT';
    const SETUP_COLOR = 'SETUP_COST_COLOR';
    const FONT_PRICE = 'FONT_PRICE';
    const CHAR_ADD = 'CHARACTER_ADDON';
    const COLOR_AMOUNT = 'COLOR_AMOUNT';
    const PIMP_TASK_PRICE = 'PIMP_TASK_PRICE';

}
