<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SnapScaleType
 *
 * @author Jan Maslik
 */
class SnapScaleType
{
	const EXACT_SCALE = 'EXACT_SCALE';
	const REDUCE_ONLY = 'REDUCE_ONLY';
	const ENLARGE_ONLY = 'ENLARGE_ONLY';
}
