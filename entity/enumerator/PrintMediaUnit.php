<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintMediaUnit
 *
 * @author Jan Maslik
 */
class PrintMediaUnit
{
	const CM = 'cm';
	const MM = 'mm';
	const INCH = 'inches';
}
