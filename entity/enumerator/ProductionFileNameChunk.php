<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductionFileNameChunk
 *
 * @author Jan Maslik
 */
class ProductionFileNameChunk
{
    /* General */
    const TIME = '%TIME%';
    const SUMMARY= '%SUMMARY%';
    const AMOUNT_INDEX= '%AMOUNT_INDEX%';
    const EXCEED= '%EXCEED%';
    const IS_WHITE= '%IS_WHITE%';
    const ACCOUNT_ID = '%ACCOUNT_ID%';
    const ACCOUNT_NAME = '%ACCOUNT_NAME%';
    const SHOP_ID = '%SHOP_ID%';
    const SHOP_NAME = '%SHOP_NAME%';
    const SRC_ACCOUNT_ID = '%SRC_ACCOUNT_ID%';
    const SRC_ACCOUNT_NAME = '%SRC_ACCOUNT_NAME%';
    
    /* View */
    const VIEW_ID= '%VIEW_ID%';
    const VIEW_NAME= '%VIEW_NAME%';
    const COMPOSITION_ID= '%COMPOSITION_ID%';
    const MOTIVE_ID= '%MOTIVE_ID%';
    const LAYER_ID= '%LAYER_ID%';
    const COLOR_NAME= '%COLOR_NAME%';
    const COLOR_ID= '%COLOR_ID%';
    const AREA_ID = '%AREA_ID%';
    const AREA_INDEX = '%AREA_INDEX%';
    
    /* Product */
    const PRODUCTS= '%PRODUCTS%';
    const PRODUCT_COPIES = '%PRODUCT_COPIES%';
    const PRODUCT_NAME = '%PRODUCT_NAME%';
    const PRODUCT_ID= '%PRODUCT_ID%';
    const PRODUCT_COLOR = '%PRODUCT_COLOR%';
    const PRODUCT_SIZE = '%PRODUCT_SIZE%';
    
    /* Summary */
    const ORDERS= '%ORDERS%';
    const PRODUCT_INDEX = '%PRODUCT_INDEX%';
    const ORDER_ID= '%ORDER_ID%';
	
    /* Technology  */
    const TECHNOLOGY_ID= '%TECHNOLOGY_ID%';
    const TECHNOLOGY_NAME= '%TECHNOLOGY_NAME%';
    const MEDIA_NAME = '%MEDIA_NAME%';
    const MEDIA_ID = '%MEDIA_ID%';
    const COLOR_USED_ID = '%COLOR_ID%';
    const COLOR_USED_NAME = '%COLOR_NAME%';
    const COLOR_AMOUNT = '%COLOR_AMOUNT%';
    const SHOP_TECHNOLOGY_ID = "%SHOP_TECHNOLOGY_ID%";
    const SHOP_TECHNOLOGY_NAME = "%SHOP_TECHNOLOGY_NAME%";
    const BROTHER_GT3_PRETREAD = '%BROTHER_GT3_PRETREAD%';
    
    const SPLIT_INDEX = '%|SPLIT_INDEX%';
    
    const DEF_FACTORY_SKU = '%DEF_FACTORY_SKU%';
    
}
