<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TextureIconMode
 *
 * @author Jan Maslik
 */
class TextureIconMode
{
	const CROP = 'CROP';
	const STRETCH = 'STRETCH';
}
