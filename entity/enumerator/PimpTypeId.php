<?php

namespace shirtplatform\entity\enumerator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PimpTypeId
 *
 * @author admin
 */
class PimpTypeId
{
    
    const BASIC_ADJUSTMENTS = 1;
    const PICTURE_FRAMES = 5;
    const IMAGE_MASKS = 2;
    const INSTANT_EFFECTS = 4;
    const EFFECT_TEMPLATES = 6;
    
    const MANUAL_SERVICES = 0;
    const WORD_CLOUDS = -1;
    const EMBROIDERY = -3;
    const COOL_BITMAP_FONTS = -10;
            
}
