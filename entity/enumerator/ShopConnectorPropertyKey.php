<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\enumerator;

/**
 * Description of ShopConnectorPropertyKey
 *
 * @author admin
 */
class ShopConnectorPropertyKey
{
    const SHOPIFY_IMAGETYPE = 'SHOPIFY_IMAGETYPE';
    const SHOPIFY_IMAGEBGCOLOR = 'SHOPIFY_IMAGEBGCOLOR';
    const SHOPIFY_ATTR_COLOR_NAME = 'SHOPIFY_ATTR_COLOR_NAME';
    const SHOPIFY_ATTR_SIZE_NAME = 'SHOPIFY_ATTR_SIZE_NAME';
    const SHOPIFY_STOCK_TRACKING = 'SHOPIFY_STOCK_TRACKING';
    const MAGENTO_COLLECTION_HUMAN_VIEWS_IMPORT = 'MAGENTO_COLLECTION_HUMAN_VIEWS_IMPORT';
    const MAGENTO_NEW_SKU_ENABLED = 'MAGENTO_NEW_SKU_ENABLED';
    const MAGENTO_ASYNC_IMPORT_ENABLED = 'MAGENTO_ASYNC_IMPORT_ENABLED';
}
