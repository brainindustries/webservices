<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MotiveType
 *
 * @author Jan Maslik
 */
class MotiveType
{
	const VECTOR= 'VECTOR';
	const HIGH_DENSITY_VECTOR= 'HIGH_DENSITY_VECTOR';
	const HIGH_RESOLUTION_BITMAP= 'HIGH_RESOLUTION_BITMAP';
	const USER_BITMAP= 'USER_BITMAP';

}
