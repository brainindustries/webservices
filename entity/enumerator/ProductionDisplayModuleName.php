<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductionDisplayModuleName
 *
 * @author Jan Maslik
 */
class ProductionDisplayModuleName
{
	const LAYER = 'LAYER';
	const MOTIVE = 'MOTIVE';
	const COMPOSITION  = 'COMPOSITION';
        const VIEW = 'VIEW';
}
