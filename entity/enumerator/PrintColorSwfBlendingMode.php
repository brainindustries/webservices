<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintColorSwfBlendingMode
 *
 * @author Jan Maslik
 */
abstract class PrintColorSwfBlendingMode
{
	const NORMAL = 'NORMAL';
	const LUMINOSITY = 'LUMINOSITY';
	const OVERLAY = 'OVERLAY';
}
