<?php

namespace shirtplatform\entity\enumerator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NecklabelFileNameChunk
 *
 * @author admin
 */
class NecklabelFileNameChunk
{

    const SUMMARY = '%SUMMARY%';
    const ORDER_ID = '%ORDER_ID%';
    const SHOP_ID = '%SHOP_ID%';
    const SHOP_NAME = '%SHOP_NAME%';
    const SRC_ACCOUNT_ID = '%SRC_ACCOUNT_ID%';
    const SRC_ACCOUNT_NAME = '%SRC_ACCOUNT_NAME%';
    const PRODUCT_ID = '%PRODUCT_ID%';
    const TECHNOLOGY_ID = '%TECHNOLOGY_ID%';
    const TECHNOLOGY_NAME = '%TECHNOLOGY_NAME%';
    const SHOP_TECHNOLOGY_ID = '%SHOP_TECHNOLOGY_ID%';
    const SHOP_TECHNOLOGY_NAME = '%SHOP_TECHNOLOGY_NAME%';
    const TIME = '%TIME%';
    const SPLIT_INDEX = '%SPLIT_INDEX%';
    const NECKLABEL_ID = '%NECKLABEL_ID%';
    
}

