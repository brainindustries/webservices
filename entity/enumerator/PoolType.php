<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PoolType
 *
 * @author Jan Maslik
 */
class PoolType {
    const CORE = 'CORE';
    const PUBLISH = 'PUBLISH';
}
