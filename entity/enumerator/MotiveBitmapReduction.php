<?php

namespace shirtplatform\entity\enumerator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class MotiveBitmapReduction {
    
    const DITHER_NONE = 0;
    const DITHER_FLOYDSTEINBERG = 1;
    const DITHER_FALSEFLOYDSTEINBERG = 2;
    const DITHER_STUCKI = 3;
    const DITHER_ATKINSON = 4;
    const DITHER_JARVIS = 5;
    const DITHER_BURKES = 6;
    const DITHER_SIERRA = 7;
    const DITHER_TWOSIERRA = 8;
    const DITHER_SIERRALITE = 9;

    const METHOD_NONE = 0;
    const METHOD_EUCLIDEAN = 1;
    const METHOD_MANHATTAN = 2;
    
}
