<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductionFilePager
 *
 * @author Jan Maslik
 */
class ProductionFilePager
{
	const NONE = 'NONE';
	const ORDER = 'ORDER';
	const PRODUCT= 'PRODUCT';
	const VIEW = 'VIEW';
	const COMPOSITION = 'COMPOSITION';
	const ELEMENT = 'ELEMENT';
	const LAYER = 'LAYER';
	const COLOR = 'COLOR';
}
