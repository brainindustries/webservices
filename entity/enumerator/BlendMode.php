<?php

namespace shirtplatform\entity\enumerator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BlendMode
 *
 * @author admin
 */
class BlendMode
{
    const NORMAL = 'NORMAL';
    const MULTIPLY = 'MULTIPLY';
    const SCREEN = 'SCREEN';
    
}
