<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of KornitColorPrintMode
 *
 * @author Jan Maslik
 */
abstract class KornitColorPrintMode
{
	const NONE = 'NONE';
	const NATURAL = 'NATURAL';
	const INTERLACE = 'INTERLACE';
}

?>
