<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShopType
 *
 * @author Jan Maslik
 */
class ShopType
{
	const SHOP = 'SHOP';
	const PRODUCT_POOL = 'PRODUCT_POOL';
	const MOTIVE_POOL = 'MOTIVE_POOL';
        const PRODUCT_PUBLISH = 'PRODUCT_PUBLISH';
        const MOTIVE_PUBLISH = 'MOTIVE_PUBLISH';
        const CORE = 'CORE';
        const PUBLISH = 'PUBLISH';
        const SUBPARTNER_SAMPLE = 'SUBPARTNER_SAMPLE';
        const SUBPARTNER_LIBRARY = 'SUBPARTNER_LIBRARY';
}
