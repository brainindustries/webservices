<?php

namespace shirtplatform\entity\enumerator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CreatorConfigResource
 *
 * @author admin
 */
class CreatorConfigResource
{
    const AddNewMotiveButton_motiveIcoUp = 'addNewMotiveButton_motiveIcoUp';
    const AddNewMotiveButton_textIcoUp = 'addNewMotiveButton_textIcoUp';
    const AddNewMotiveButton_uploadIcoUp = 'addNewMotiveButton_uploadIcoUp';
    const AddNewMotiveButton_gfxTextIcoUp = 'addNewMotiveButton_gfxTextIcoUp';
}
