<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintProfileIntent
 *
 * @author Jan Maslik
 */
class PrintProfileIntent
{
    const ABSOLUTE = 'ABSOLUTE';
    const PERCEPTUAL = 'PERCEPTUAL';
    const RELATIVE = 'RELATIVE';
    const SATURATION = 'SATURATION';

}
