<?php
namespace shirtplatform\entity\enumerator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FactoryOrderStatus
 *
 * @author Jan Maslik
 */
class FactoryOrderStatus
{
    const NEW_ = 'PENDING';
    const WAITING = 'WAITING';
    const PREPARED = 'PREPARED';
    const IN_WORKBANCH = 'IN_WORKBANCH';
    const PRODUCED = 'PRODUCED';
    const CANCELLED = 'PAID';
    const ERROR = 'ERROR';
}
