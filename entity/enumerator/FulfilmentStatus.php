<?php
namespace shirtplatform\entity\enumerator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class FulfilmentStatus {
    
    const WAITING_FOR_RESOURCES = 'WAITING_FOR_RESOURCES';
    const PREPARED_TO_PRODUCE = 'PREPARED_TO_PRODUCE';
    const NOT_EDITABLE = 'NOT_EDITABLE';
    const FINISHED = 'FINISHED';
    const RECLAMATION = 'RECLAMATION';
    const CANCELLED = 'CANCELLED';
    const ERROR_HQ_MOTIVES = 'ERROR_HQ_MOTIVES';
    
}
