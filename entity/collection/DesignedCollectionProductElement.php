<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\collection;

/**
 * 
 */
class DesignedCollectionProductElement extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
    
    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collections/{parentId}/templateProducts/{parentId}/designElements';
    const VAR_NAME = 'collectionDesignElement';
    
    public $elementEditable;
    public $elementName;
    public $elementId;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
}
