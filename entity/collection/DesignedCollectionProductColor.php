<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\collection;

/*
 * 
 */
class DesignedCollectionProductColor extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collections/{parentId}/templateProducts/{parentId}/colors';
    const VAR_NAME = 'designedTemplateProductColor';

    public $assignedColor;
    public $active;
    public $version;
    
    public static $classMap = array(
        'assignedColor' => '\shirtplatform\entity\product\AssignedProductColor'
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
