<?php

namespace shirtplatform\entity\collection;

use \User;
use \shirtplatform\rest\REST;
use shirtplatform\parser\WsParse;
use shirtplatform\parser\WsModel;
use shirtplatform\filter\WsParameters;

/**
 * Description of CollectionCategoryTree
 *
 * @author Jan Maslik
 */
class CollectionCategoryTree extends \shirtplatform\entity\abstraction\JsonEntity
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collectionCategories/trees';
    const VAR_NAME = 'collectionCategoryTree';

    public static $classMap = array(
        'category' => '\shirtplatform\entity\collection\CollectionCategory'
    );
    public $category = null;
    public $parentId;
    public $childs = array();

    /**
     * Find parent tree.
     * 
     * @param int $categoryId
     * @param WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return CollectionCategoryTree|null
     */
    public static function findParentTree($categoryId, $wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        $entities = self::findParents($categoryId, $wsParameters, $shopId, $isAsync );

        if (empty($entities))
        {
            return null;
        }

        $rootEntities = array();

        foreach ($entities as $entity)
        {
            if ($entity->parentId == null)
            {
                $rootEntities[$entity->id] = $entity;
            } else
            {
                $entities[$entity->parentId]->childs[$entity->id] = $entity;
            }
        }

        return ($rootEntities === NULL) ? NULL : reset($rootEntities);
    }

    /**
     * Find child tree.
     * 
     * @param int $categoryId
     * @param WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return array
     */
    public static function findChildTree($categoryId, $wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        $entities = self::findChilds($categoryId, $wsParameters, $shopId, $isAsync);

        $rootEntity = array();

        foreach ($entities as $entity)
        {
            if ($entity->parentId == null)
            {
                $rootEntity[$entity->id] = $entity;
            } else
            {
                $entities[$entity->parentId]->childs[$entity->id] = $entity;
            }
        }

        return $rootEntity;
    }

    /**
     * Get path string.
     * 
     * @param int|null $shopId
     * @return string
     */
    public static function getUrl(&$shopId = null)
    {
        $className = get_called_class();
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);

        return $url;
    }

    /**
     * Find parents.
     * 
     * @param int $categoryId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return CollectionCategoryTree[]
     */
    public static function findParents($categoryId, $wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\collection\CollectionCategory'); //use motive category filter

        $url = self::getUrl($shopId) . '/' . $categoryId . '/parents';
        
        return self::findAllFromUrl($url, $wsParameters , $shopId , null , $isAsync );
    }

    /**
     * Find childs.
     * 
     * @param int $categoryId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return CollectionCategoryTree[]
     */
    public static function findChilds($categoryId, $wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\collection\CollectionCategory'); //use motive category filter

        $url = self::getUrl($shopId) . '/' . $categoryId . '/childs';
        
        return self::findAllFromUrl($url, $wsParameters , $shopId , null , $isAsync );
    }

}
