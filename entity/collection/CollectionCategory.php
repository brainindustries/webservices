<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\collection;

/**
 * Description of CollectionCategory
 *
 * @author Jan Maslik
 */
use \User;
use \shirtplatform\rest\REST;
use shirtplatform\parser\WsModel;
use shirtplatform\parser\WsParse;
use shirtplatform\parser\ArrayUtil;
use shirtplatform\filter\Filter;
use shirtplatform\filter\WsParameters;
use \DebugException;

class CollectionCategory extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collectionCategories';
    const VAR_NAME = 'collectionCategory';

    public static $classMap = array(
        'localizations' => '\shirtplatform\entity\localization\CollectionCategoryLocalized',
        'assignedCollectionProducts' => '\shirtplatform\entity\collection\CollectionProductAssignedToCategory'
    );
    public $name;
    public $orderIndex;
    public $active;
    public $deleted;
    public $version;
    protected $localizations = null;
    protected $assignedCollectionProducts = null;
    protected $countAssignedProducts = null;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get products assigned to category.
     * 
     * @param WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\collection\CollectionProductAssignedToCategory[]
     */
    public function getAssignedProducts($wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('assignedCollectionProducts', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Assign parent category.
     * 
     * @param int $parentCategoryId
     */
    public function __assignParentCategory($parentCategoryId = '')
    {
        self::assignParentCategory($this->id, $parentCategoryId);
    }

    /**
     * Get category childs.
     * 
     * @return CollectionCategory[]
     */
    public function getChildCategories()
    {
        return self::findChildCategories($this->id, null);
    }

    /**
     * Assign parent category.
     * 
     * @param int $categoryId
     * @param int $parentCategoryId
     * @param int|null $shopId
     * @param boolean $isAsync
     */
    public static function assignParentCategory($categoryId, $parentCategoryId = '', $shopId=null ,$isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', self::getUrl($shopId) . '/' . $parentCategoryId . '/ownedCategory/' . $categoryId , [] , [], $isAsync);
        $rest->_call($promise);
    }

    /**
     * Find threaded categories.
     * 
     * @param int $categoryId
     * @param int|null $shopId
     * @return CollectionCategory[]
     */
    public static function findThreadCategories($categoryId, $shopId = null)
    {
        $url = self::getUrl($shopId) . '/' . $categoryId . '/thread';

        return self::findAllFromUrl($url, null);
    }

    /**
     * Find category childs.
     * 
     * @param int $parentCategoryId
     * @param WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return CollectionCategory[]
     */
    public static function &findChildCategories($parentCategoryId = '', $wsParameters = null, $shopId = null , $isAsync = null)
    {
         $url = self::getUrl() . '/' . $parentCategoryId . '/ownedCategory';
        
        return self::findAllFromUrl($url, $wsParameters, $shopId, [] , $isAsync);
    }

    /**
     * Get category assigned products count.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceReload
     * @return int
     */
    public function &getAssignedProductsCount(\shirtplatform\filter\WsParameters $wsParameters = null , $isAsync = false , $forceReload = false)
    {
        if( $this->countAssignedProducts == null || $forceReload )
        {
            $this->countAssignedProducts = CollectionProductAssignedToCategory::count( $this->getParentList() , $wsParameters, $this->getShopId() , $isAsync);
        }
        
        return $this->countAssignedProducts;
    }

    /**
     * Get category localizations.
     * 
     * @return \shirtplatform\entity\localization\CollectionCategoryLocalized[]
     */
    public function getLocalizations()
    {
        return $this->getAtomLinkValue('localizations');
    }

    /**
     * Duplicate an category.
     * 
     * @param int $categoryId
     * @param int|null $shopId
     * @return CollectionCategory
     */
    public static function &duplicate($categoryId, $shopId = null)
    {
        $url = self::getUrl($shopId);
        $url .= '/duplicate/' . $categoryId;
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class() , [], [], false);
        $promise->shopId = $shopId;
        return $rest->_call($promise);
    }

    /**
     * Import own category.
     * 
     * @param int $categoryId
     * @param int|null $dstShopId
     * @return CollectionCategory
     */
    public static function importOwn($categoryId, $dstShopId = null)
    {
        $url = self::getUrl($dstShopId);
        $url .= '/importOwn/' . $categoryId;
         
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [], false);
        $promise->shopId = $dstShopId;
        return $rest->_call($promise);
    }

    /**
     * Find category roots.
     * 
     * @param WsParameters|null $wsParameters
     * @param int|null $shopId
     * @return CollectionCategory[]
     */
    public static function &findRoots($wsParameters = null, $shopId = NULL)
    {
        $url = self::getUrl($shopId);
        $url .= '/roots';
        return self::findAllFromUrl($url, $wsParameters);
    }

}
