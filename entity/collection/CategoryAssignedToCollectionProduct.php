<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\collection;

/**
 * Description of CategoryAssignedToMotive
 *
 * @author Jan Maslik
 */

use shirtplatform\filter\WsParameters;

class CategoryAssignedToCollectionProduct extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collections/{parentId}/templateProducts/{parentId}/assignedCategories';
	const VAR_NAME = 'categoryAssignedToCollectionProduct';
	
	public static $classMap = array(
		'category' => '\shirtplatform\entity\collection\CollectionCategory',
	);
	
	public $active;
	public $orderIndex;
	public $category;
	public $version;
	
	
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}