<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\collection;
/**
 * Description of DesignedCollectionProductColorSize
 *
 * @author Jan Maslik
 */
class DesignedCollectionProductViewDisabled extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collections/{parentId}/templateProducts/{parentId}/disabledViews';
	const VAR_NAME = 'designedTemplateProductViewDisabled';

	public $assignedView;
	
	public static $classMap = array(
		'assignedView' => '\shirtplatform\entity\product\AssignedProductView'
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
      
}
