<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\collection;
/**
 * Description of DesignedCollectionProductPrice
 *
 * @author Jan Maslik
 */
class DesignedCollectionProductPrice extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collections/{parentId}/templateProducts/{parentId}/prices';
	const VAR_NAME = 'designedTemplateProductPrice';

	public $price=0;
	public $version;
	
	public $country;
	
	public static $classMap = array(
		'country' => '\shirtplatform\entity\account\Country',
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
      
}
