<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\collection;

/**
 * Description of DesignedCollectionProduct
 *
 * @author Jan Maslik
 */
class DesignedCollectionProduct extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collections/{parentId}/templateProducts';
    const PATH_TEMPLATE_ALL = 'accounts/{accountId}/templateProducts';
    const PATH_CATEGORY_ASSIGN_ALL = 'accounts/{accountId}/shops/{shopId}/collections/{parentId}/templateProducts/assignedCategories';
    const PATH_TEMPLATE_ALL_SHOP = 'accounts/{accountId}/shops/{shopId}/collections/templateProducts';
    const VAR_NAME = 'templateProduct';

    public static $classMap = array(
        'color' => '\shirtplatform\entity\product\AssignedProductColor',
        'size' => '\shirtplatform\entity\product\AssignedProductSize',
        'design' => '\shirtplatform\entity\order\Design',
        'prices' => '\shirtplatform\entity\collection\DesignedCollectionProductPrice',
        'localizations' => '\shirtplatform\entity\localization\DesignedCollectionProductLocalized'
    );
    public $design;
    public $created;
    public $version;
    public $engineVersion;
    public $useAllColorSize;
    public $includingNewColors;
    public $color;
    public $size;
    public $orderIndex;
    protected $prices = null;
    protected $localizations = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get product preview url string.
     * 
     * @param int $width
     * @param int $height
     * @param int $bestfit
     * @return string
     */
    public function getPreviewUrl($width = -1, $height = -1, $bestfit = 1)
    {
        return $this->getAtomLink('image') . '?width=' . $width . '&height=' . $height . '&bestfit=' . $bestfit;
    }

    /**
     * Find all in shop.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return DesignedCollectionProduct[]
     */
    public static function &findAllByShop(\shirtplatform\filter\WsParameters $wsParameters = null, $shopId = null, $isAsync = false )
    {
        $entityName = get_called_class();

        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE_ALL_SHOP);
        $url = str_replace('{shopId}', $shopId, $url);

        $rest = \shirtplatform\rest\REST::getInstance();

        return self::findAllFromUrl($url, $wsParameters, $shopId, [], $isAsync);
    }
    
    /**
     * Count all in shop.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return int
     */
    public static function &countAllByShop(\shirtplatform\filter\WsParameters $wsParameters = null, $shopId = null, $isAsync = false )
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->page = 0;
        $wsParameters->size = 1;
        $wsParameters->setRootEntityName('shirtplatform\entity\collection\DesignedCollectionProduct');
        $rest = \shirtplatform\rest\REST::getInstance();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE_ALL_SHOP);
        $url = str_replace('{shopId}', $shopId, $url);

        $promise = new \shirtplatform\rest\promise\CountPromise('GET', $url, $wsParameters->buildParams() , [] , false);
        $promise->filterPromise = $wsParameters->getPromise(false);
        return $rest->_call($promise);
    }

    /**
     * Find unsaved entity.
     * 
     * @param int $templateId
     * @param boolean $isAsync
     * @return DesignedCollectionProduct
     */
    public static function findUnsaved($templateId , $isAsync = false )
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE_ALL);
        $url .= '/' . $templateId;

        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), [], [], $isAsync);

        return $rest->_call($promise);
    }

    /**
     * Get product prices.
     * 
     * @return \shirtplatform\entity\collection\DesignedCollectionProductPrice[]
     */
    public function &getPrices($isAsync = false)
    {
        return $this->getAtomLinkValue('prices', NULL, $isAsync);
    }

    /**
     * Get product localizations.
     * 
     * @return \shirtplatform\entity\localization\DesignedCollectionProductLocalized[]
     */
    public function getLocalizations()
    {
        return $this->getAtomLinkValue('localizations');
    }

    /**
     * Duplicate product.
     * 
     * @return DesignedCollectionProduct
     */
    public function __duplicate()
    {
        return self::duplicate($this->id, $this->getParents(), $this->getShopId());
    }

    /**
     * Assign categories.
     * 
     * @param int $newCategory
     * @param int $collectionId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return void
     */
    public static function assignCategoryAll($newCategory, $collectionId, $wsParameters, $shopId = null,$isAsync = false)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }


        $wsParameters->setRootEntityName('\shirtplatform\entity\collection\DesignedCollectionProduct');

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_CATEGORY_ASSIGN_ALL);
        $url = str_replace('{shopId}', $shopId, $url);
        $url = str_replace('{parentId}', $collectionId, $url);

        $rest = \shirtplatform\rest\REST::getInstance();
        $options = [
            'json' => [$newCategory::VAR_NAME => $newCategory->getWebserviceAttributes()]
        ];
        $loadAll=false;
        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        
        $promise = new \shirtplatform\rest\promise\JsonPromise('POST', $url, $wsParameters->buildParams(), $options , $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        $rest->_call($promise);
        
        $result = intval($promise->message);

        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::deleteCategoryAll($wsParameters, $shopId);
        }
        return $result;
    }

    /**
     * Delete category assignments.
     * 
     * @param int $collectionId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @return void
     */
    public static function deleteCategoryAll($collectionId, $wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\motive\DesignedCollectionProduct');

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_CATEGORY_ASSIGN_ALL);
        $url = str_replace('{shopId}', $shopId, $url);
        $url = str_replace('{parentId}', $collectionId, $url);
        $rest = \shirtplatform\rest\REST::getInstance();
        $loadAll=false;
        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $url, $wsParameters->buildParams(), [] , false);
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);
        
        $result = intval($promise->message);

        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::deleteCategoryAll($wsParameters, $shopId);
        }
        return $result;
    }

    /**
     * Duplicate product.
     * 
     * @param int $templProductId
     * @param int $collectionId
     * @param int|null $shopId
     * @return DesignedCollectionProduct
     */
    public static function duplicate($templProductId, $collectionId, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if (!is_array($collectionId))
        {
            $collectionId = array($collectionId);
        }
        $url = self::getUrl($collectionId, $shopId);
        $url .= '/' . $templProductId . '/duplicate';
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [], false);
        $promise->shopId = $shopId;
        $promise->parents = $templProductId;
        
        return $rest->_call($promise);
    }
    
    /**
     * Move multiple collection products.
     * 
     * @param int $newCollectionId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @return void
     */
    public static function moveAll($newCollectionId, $wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $loadAll = false;

        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        $wsParameters->setRootEntityName(get_called_class());

        $url = self::getUrl($shopId);
        $url .= '/moveAll/' . $newCollectionId;
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url, $wsParameters->buildParams(), array(), false);
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);

        $result = intval($promise->message);

        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::moveAll($newCollectionId, $wsParameters, $shopId);
        }
        return $result;
    }
    
    /**
     * Move designed product.
     * 
     * @param int $designedProductId
     * @param int $newCollectionId
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return void
     */
    public static function &move($designedProductId, $newCollectionId, $shopId = null, $isAsync = false)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = self::getUrl(array($newCollectionId), $shopId);
        $url .= '/' . $designedProductId . '/move';
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], [], $isAsync);
        $promise->shopId = $shopId;
        return $rest->_call($promise);
    }
    
    /**
     * Disable unused views on product.
     * 
     * @param int $templProductId
     * @param int $collectionId
     * @param int|null $shopId
     * @return DesignedCollectionProduct
     */
    public static function disableUnusuedViews($templProductId, $collectionId, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if (!is_array($collectionId))
        {
            $collectionId = array($collectionId);
        }
        $url = self::getUrl($collectionId, $shopId);
        $url .= '/' . $templProductId . '/disableUnusedViews';
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), [], [], false);
        $promise->shopId = $shopId;
        $promise->parents = $templProductId;
        
        return $rest->_call($promise);
    }

}
