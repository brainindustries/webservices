<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\collection;

/**
 * Description of CollectionProductAssignedToCategory
 *
 * @author Jan Maslik
 */
use \shirtplatform\rest\REST;
use shirtplatform\filter\WsParameters;

class CollectionProductAssignedToCategory extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collectionCategories/{parentId}/assignedCollectionProducts';
    const VAR_NAME = 'collectionProductAssignedToCategory';

    public static $classMap = array(
        'category' => '\shirtplatform\entity\motive\CollectionProductCategory',
        'designedTemplateProduct' => '\shirtplatform\entity\collection\DesignedCollectionProduct',
    );
    public $active;
    public $orderIndex;
    public $designedTemplateProduct;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Delete all assignments.
     * 
     * @param int $categoryId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return void
     */
    public static function deleteAll($categoryId, $wsParameters = null, $shopId = null,$isAsync = false)
    {
        $url = self::getUrl(array($categoryId), $shopId);

        $loadAll = false;

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName(get_called_class());

        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $url, $wsParameters->buildParams() , [] , $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        $rest->_call($promise);
        
        $result = intval($promise->message);

        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::deleteCategoryAll($wsParameters, $shopId);
        }
        return $result;
    }

}
