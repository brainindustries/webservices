<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\connection;

class ShopConnectionConfig extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/connectors';
    const PATH_SHOP_TEMPLATE = 'accounts/{accountId}/connectors/shops/{shopId}';
    const VAR_NAME = 'shopConnection';

    public static $classMap = array(
        'shop' => '\shirtplatform\entity\account\Shop',
        'country' => '\shirtplatform\entity\account\Country',
        'language' => '\shirtplatform\entity\account\Language',
    );
    
    public $shopName;
    public $connector;
    public $shop;
    public $country;
    public $language;
    public $email;
    public $url;
    public $newAccount;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
    /**
     * Find ShopConfigShopify by platform shop id
     * @param int $shopId
     * @param \shirtplatform\filter\WsParameters $wsParameters
     * @param boolean $isAsync
     * @return ShopConfigShopify
     */
    public static function &findByShop($shopId, \shirtplatform\filter\WsParameters $wsParameters = null, $isAsync = FALSE)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_SHOP_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), $wsParameters, [], $isAsync);
        $promise->shopId = $shopId;
        return $rest->_call($promise);
    }

}
