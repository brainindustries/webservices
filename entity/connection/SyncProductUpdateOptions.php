<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\connection;

/**
 * Description of SyncProductUpdateOptions
 *
 * @author admin
 */
class SyncProductUpdateOptions extends \shirtplatform\entity\abstraction\JsonEntity
{

    const VAR_NAME = 'syncProductOptionsPrime';
    
    public $name;
    public $description;
    public $pricing;
    public $variantAssignAndImages;
    public $variantImages;
    public $colorSizeNames;
    
    public function __construct($data = null)
    {
        if ($data != null)
        {
            \shirtplatform\parser\WsModel::copy($data, $this);
        }
    }
    
}
