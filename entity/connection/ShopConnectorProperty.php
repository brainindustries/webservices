<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\connection;

/**
 * Description of ShopConnectorProperty
 *
 * @author admin
 */
class ShopConnectorProperty extends \shirtplatform\entity\abstraction\ParentOwnedDao
{ 
    
    const PATH_TEMPLATE = 'accounts/{accountId}/connectors/shops/{shopId}/connector/{parentId}/properties';
    const VAR_NAME = 'shopConnectorProperty';
    
    public $key;
    public $value;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
}
