<?php
namespace shirtplatform\entity\creator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserMotive
 *
 * @author Jan Maslik
 */
class UserMotive extends \shirtplatform\entity\abstraction\JsonEntity
{
    const VAR_NAME = 'creator.Motive';

    public static $classMap = array(
//        'bitmap' => '\shirtplatform\entity\creator\MotiveDataBitmap',
//        'pimpTask' => '\shirtplatform\entity\creator\PimpTask',
    );
    public $name;
    public $type;
    public $priceMultiplier;
    public $authorProvision;
    public $motiveProfit;
    public $overridePrice;
    public $exactPrice;
    public $pimpTaskId;
    public $pimpTask;
    public $bitmap;
    public $layers;
    public $motivePrintTechnologies;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
    public static function &createFromPimpTask($referencedMotiveId , $pimpImageTaskId , $langId , $countryId , $shopId = null )
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = 'accounts/'. \shirtplatform\utils\user\User::getAccountId() .'/shops/'.$shopId.'/creator/userMotives/'.$referencedMotiveId.'/createNewFromPimpTask/'.$pimpImageTaskId;
        $url .=';langId='.$langId.';countryId='.$countryId;
        
        $rest = \shirtplatform\rest\REST::getInstance();
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class() , [], []);
        return $rest->_call($promise);
    }
}
