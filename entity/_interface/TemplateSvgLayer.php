<?php
namespace shirtplatform\entity\_interface;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TemplateSvgLayer
 *
 * @author Jan Maslik
 */
class TemplateSvgLayer
{
	public $clip = null;
	public $background = null;
	public $bounds = null;
}
