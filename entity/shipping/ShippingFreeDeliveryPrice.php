<?php

namespace shirtplatform\entity\shipping;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShippingFreeDeliveryPrice
 *
 * @author admin
 */
class ShippingFreeDeliveryPrice extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shippingModules/{parentId}/carriers/{parentId}/freeDeliveryPrices';
    const VAR_NAME = 'carrierFreeDeliveryPrice';
    
    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country'
    );
    
    public $price;
    public $country;
    public $version;
    
}
