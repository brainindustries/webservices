<?php

namespace shirtplatform\entity\shipping;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShippingZone
 *
 * @author admin
 */
class ShippingZoneDestination extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shippingModules/{parentId}/carriers/{parentId}/zones/{parentId}/destinations';
    const VAR_NAME = 'carrierDestination';
    
    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country'
    );
    
    public $country;
    public $version;
    
}
