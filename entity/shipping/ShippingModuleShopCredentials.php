<?php

namespace shirtplatform\entity\shipping;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ShippingModuleShopCredentials extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/shippingModuleShopCredentials';
    const VAR_NAME = 'shippingModuleShopCredentials';
    
    public static $classMap = array(
        'module' => '\shirtplatform\entity\shipping\ShippingModule'
    );
    
    public $module;
    public $apiUser;
    public $apiKey;
    
}
