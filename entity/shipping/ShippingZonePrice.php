<?php

namespace shirtplatform\entity\shipping;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShippingZone
 *
 * @author admin
 */
class ShippingZonePrice extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shippingModules/{parentId}/carriers/{parentId}/zones/{parentId}/prices';
    const VAR_NAME = 'carrierPrice';
    
    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country'
    );
    
    public $weightLimit;
    public $price;
    public $country;
    public $version;
    
}
