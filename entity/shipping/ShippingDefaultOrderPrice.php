<?php

namespace shirtplatform\entity\shipping;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 */
class ShippingDefaultOrderPrice extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shippingModules/{parentId}/carriers/{parentId}/defaultOrderPrices';
    const VAR_NAME = 'carrierDefaultOrderPrice';
    
    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country'
    );
    
    public $price;
    public $country;
    public $version;
    
}
