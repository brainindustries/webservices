<?php

namespace shirtplatform\entity\shipping;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShippingModule
 *
 * @author admin
 */
class ShippingModule extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shippingModules';
    const VAR_NAME = 'shippingModule';
    
    public static $classMap = array(
        'moduleType' => '\shirtplatform\entity\open\ShippingModuleType'
    );
    
    public $name;
    public $apiUser;
    public $apiKey;
    public $moduleType;
    public $version;
    public $deleted;
    
}
