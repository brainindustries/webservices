<?php

namespace shirtplatform\entity\shipping;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShippingCarrier
 *
 * @author admin
 */
class ShippingCarrier extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shippingModules/{parentId}/carriers';
    const VAR_NAME = 'carrier';
    
    public static $classMap = array(
        'moduleProvider' => '\shirtplatform\entity\open\ShippingModuleProvider'
    );
    
    public $name;
    public $description;
    public $phoneRequired;
    public $phoneDefault;
    public $moduleProvider;
    public $active;
    public $freeDelivery;
    public $customProvider;
    public $premium;
    public $ddp;
    public $emdorsement;
    public $defaultOrderPrice;
    public $showTicketLogo;
    public $customApiUser = "";
    public $customApiKey = "";
    public $version;
    public $deleted;
    
}
