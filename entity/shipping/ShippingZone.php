<?php

namespace shirtplatform\entity\shipping;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShippingZone
 *
 * @author admin
 */
class ShippingZone extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shippingModules/{parentId}/carriers/{parentId}/zones';
    const VAR_NAME = 'carrierZone';
    
    public static $classMap = array(

    );
    
    public $name;
    public $version;
    
}
