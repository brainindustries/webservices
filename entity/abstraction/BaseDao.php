<?php

namespace shirtplatform\entity\abstraction;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseDao
 *
 * @author Jan Maslik
 */
use shirtplatform\filter\WsParameters,
    shirtplatform\parser\WsParse,
    \shirtplatform\rest\REST,
    \User;

class BaseDao extends JsonEntity
{

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get entity path.
     * 
     * @return string
     */
    public function __url()
    {
        return self::getUrl();
    }

    /**
     * Update current entity.
     * 
     * @param boolean $isAsync
     */
    public function __update($isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url();

        $key = $this->getPrimaryKey();

        if (empty($key))
        {
            $this->clearAtomLinks();
            $method = 'POST';
        } 
        else
        {
            $method = 'PUT';
            $url .= '/' . $key;
        }
        
        $promise = new \shirtplatform\rest\promise\ItemPromise($method, $url, get_called_class() , array(), array($this::VAR_NAME => $this->getWebserviceAttributes()), $isAsync);
        $promise->message = $this;
        
        $rest->_call($promise);
    }

    /**
     * Delete current entity.
     */
    public function __delete()
    {
        self::delete($this->getPrimaryKey());
    }

    /**
     * Get entity path.
     * 
     * @return string
     */
    public static function getUrl()
    {
        $className = get_called_class();

        return str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);
    }

    /**
     * Find an single entity.
     * 
     * @param int $id
     * @param array $params
     * @param boolean $isAsync
     * @return BaseDao
     */
    public static function &find($id, $params = array() , $isAsync = false)
    {
        if($id == null)
        {
            throw new \shirtplatform\exception\DebugException('Missing id, cannot be null.');
        }
        
        $entityName = get_called_class();

        $url = self::getUrl();
        $url .= '/' . $id;

        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, $entityName, $params, array(), $isAsync);


        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Find multiple entities.
     * 
     * @param \shirtplatform\filter\WsParameters $wsParameters
     * @param boolean $isAsync
     * @return BaseDao[]
     */
    public static function &findAll(\shirtplatform\filter\WsParameters $wsParameters = null, $isAsync = false, callable $onFullfilled = null)
    {
        $entityName = get_called_class();

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName($entityName);

        $promise = new \shirtplatform\rest\promise\PagePromise('GET', self::getUrl(), $entityName, $wsParameters->buildParams(), array(), $isAsync);
        $promise->onFullfilled($onFullfilled);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
 

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Delete an entity.
     * 
     * @param int $id
     * @param boolean $isAsync
     * @return type
     */
    public static function &delete($id,$isAsync = false)
    {
        $uri = self::getUrl() . '/'.$id;
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $uri , array(), array(), $isAsync);

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Count entities.
     * 
     * @param \shirtplatform\filter\WsParameters $wsParameters
     * @param boolean $isAsync
     * @return int
     */
    public static function &count($wsParameters = null, $isAsync = false)
    {
         if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->setRootEntityName(get_called_class());
        
        $promise = new \shirtplatform\rest\promise\CountPromise('GET', self::getUrl(), $wsParameters->buildParams() , array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        
        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

}
