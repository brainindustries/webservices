<?php

namespace shirtplatform\entity\abstraction;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ResourceRecord
 *
 * @author Jan Maslik
 */
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\rest\REST;
use shirtplatform\parser\WsParse;
use shirtplatform\parser\WsModel;
use shirtplatform\filter\WsParameters;
use shirtplatform\filter\Filter;
use shirtplatform\exception\DebugException;

abstract class JsonEntity extends Model
{

    public $id = null;
    private $foreignKeyOnly;
    private $atomLinks = array();

    /**
     * Available only in developer mode
     */
    private $unmapedAttributes = array();

    public static function newInstance($id)
    {
        $class = get_called_class();
        
        return new $class(['id' => $id ] );
    }
    
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        $this->setParents($parents);

        if ($data !== null)
        {
            $this->set($data);
        }

        $this->foreignKeyOnly = $foreignKeyOnly;
    }

    protected function processAtomLinks($atom)
    {
        if (isset($atom['@rel']))
        {
            $this->atomLinks[$atom['@rel']] = $atom['@href'];
            return;
        }

        foreach ($atom as $value)
        {
            if (isset($value['@rel']))
            {
                $this->atomLinks[$value['@rel']] = $value['@href'];
            }
        }
    }

    public function set($data)
    {
        if (!is_array($data) && !is_object($data))
        {
            return;
        }

        foreach ($data AS $key => $value)
        {
            if (!property_exists($this, $key) && $key != 'atom.link')
            {
                if (\shirtplatform\constants\WsConstants::$DEVELOPER_MODE)
                {
                    $this->unmapedAttributes[$key] = $value;
                }
                continue;
            }

            if (is_array($value))
            {
                if ($key == 'atom.link')
                {
                    self::processAtomLinks($value);
                } elseif (isset($this::$classMap[$key]))
                {
                    $className = $this::$classMap[$key];
                    $parents = $this->getParents();
                    $parents[] = $this->getPrimaryKey();
                    $this->{$key} = new $className($value, $parents);
                } elseif (isset($this::$classArrayMap) && isset($this::$classArrayMap[$key]))
                {
                    $this->{$key} = array();

                    reset($value);
                    $first_key = key($value);
                    if ($first_key != '0')
                    {
                        //contains just one item
                        $className = $this::$classArrayMap[$key];
                        $parents = $this->getParents();
                        $parents[] = $this->getPrimaryKey();
                        $instance = new $className($value, $parents);
                        $this->{$key}[$instance->id] = $instance;
                    } else
                    {
                        foreach ($value as $valueData)
                        {
                            $className = $this::$classArrayMap[$key];
                            $parents = $this->getParents();
                            $parents[] = $this->getPrimaryKey();
                            $instance = new $className($valueData, $parents);
                            $this->{$key}[$instance->id] = $instance;
                        }
                    }
                } else
                {

                    $this->{$key} = $value;
                }
            } else
            {
                $this->{$key} = $value;
            }
        }
    }

    public function attributeNames()
    {
        return array_keys($this->getModelAttributes());
    }

    /**
     * Models data part. Ususaly same as webservice attributes. Except abstration part.
     * 
     * @return array
     */
    public function getModelAttributes()
    {
        return $this->getWebserviceAttributes(false);
    }

    /**
     * This is webservice attributes. Attributes passed through webservices.
     * 
     * @return array
     */
    public function getWebserviceAttributes($removeNulls = true)
    {
        $attributes = get_object_vars($this);

        unset($attributes['atomLinks']);
        $attributes = array_merge($attributes, $attributes['unmapedAttributes']);
        unset($attributes['unmapedAttributes']);
        unset($attributes['foreignKeyOnly']);
        
        if ( !$removeNulls)
        {
           return $attributes; 
        }
        
        foreach ($attributes as $attrName => $attrValue)
        {
            if ($attrValue === null)
            {
                unset($attributes[$attrName]);
            }
            elseif ( $attrValue instanceof JsonEntity ) 
            {
                $attributes[$attrName] = $attrValue->getWebserviceAttributes($removeNulls);
            }
        }        
        
        return $attributes;
    }

    /**
     * Get atomlink value item.
     * 
     * @param string $atomLinkName
     * @param boolean $forceLoad
     * @param boolean $isAsync
     * @return mixed
     */
    public function &getAtomLinkValueItem($atomLinkName, $forceLoad = false, $isAsync = false)
    {
        if ($this->{$atomLinkName} === null || $forceLoad)
        {
            $this->{$atomLinkName} = $this->loadAtomLinkValueItem($atomLinkName, $isAsync);
        }

        return $this->{$atomLinkName};
    }

    protected function loadAtomLinkValueItem($atomLinkName,$isAsync = false)
    {
        if (!isset($this::$classMap[$atomLinkName]))
        {
            throw new DebugException('No ' . $atomLinkName . ' class definition found in classMap!');
        }
        if (ArrayUtil::getSafe($this->atomLinks, $atomLinkName) == null)
        {
            return array();
            //throw new DebugException('Unintialized atomlink '.$atomLinkName.' in '.  get_class($this).'!' );
        }

        $className = $this::$classMap[$atomLinkName];

        $rest = \shirtplatform\rest\REST::getInstance();

        
        if ( defined($className . '::VAR_NAME')  )
        {
            $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $this->atomLinks[$atomLinkName], $className, [], [], $isAsync);
            $promise->parents = $this->getParentList();
           
            if (method_exists($this, 'getShopId'))
            {
                $promise->shopId = $this->getShopId();
            }
            else
            {
                $promise->shopId = \shirtplatform\utils\user\User::getShopId();
            }
        } 
        else
        {
            $promise = new \shirtplatform\rest\promise\JsonPromise('GET', $this->atomLinks[$atomLinkName],  [], [], $isAsync);
        }
        
        return $rest->_call($promise);
    }

    /**
     * Get atomlink value.
     * 
     * @param string $atomLinkName
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return mixed
     */
    public function &getAtomLinkValue($atomLinkName, $wsParameters = null, $isAsync = false , $forceLoad = false )
    {
        if ($this->{$atomLinkName} === null || $forceLoad)
        {
            $this->loadAtomLinkValue($atomLinkName, $wsParameters , $isAsync);
        }

        return $this->{$atomLinkName};
    }

    protected function loadAtomLinkValue($atomLinkName,  WsParameters $wsParameters = null, $isAsync = false)
    {
        if (!isset($this::$classMap[$atomLinkName]))
        {
            throw new DebugException('No ' . $atomLinkName . ' class definition found in classMap! ::'.get_class($this));
        }
        if (ArrayUtil::getSafe($this->atomLinks, $atomLinkName) == null)
        {
            return array();
            //throw new DebugException('Unintialized atomlink '.$atomLinkName.' in '.  get_class($this).'!' );
        }

        $className = $this::$classMap[$atomLinkName];
        if($wsParameters == null )
        {
            $wsParameters = new WsParameters();
        }
        $wsParameters->setRootEntityName($className);
        
        $promise = new \shirtplatform\rest\promise\PagePromise('GET', $this->atomLinks[$atomLinkName] , $className , $wsParameters->buildParams() , array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        $promise->parents = $this->getParentList();
        
        if (method_exists($this, 'getShopId'))
        {
            $promise->shopId = $this->getShopId();
        } 
        else
        {
            $promise->shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $this->{$atomLinkName} = &$rest->_call($promise);
    }

    //TODO move to localized DAO
    public function getDefaultLocalizationName($nameProperty = 'name')
    {
        if (!property_exists($this, 'localizations'))
        {
            $className = get_class($this);
            throw new DebugException("{$className} have not localizations!");
        }

        if ($this->localizations == null)
        {
            $this->getLocalizations();
        }

        if (reset($this->localizations) == NULL)
        {
            return '';
        }

        return reset($this->localizations)->$nameProperty;
    }

    public function isForeignKeyOnly()
    {
        return $this->foreignKeyOnly;
    }

    public function setForeignKeyOnly($foreignKeyOnly)
    {
        $this->foreignKeyOnly = $foreignKeyOnly;
    }

    public function isLazy($name)
    {
        return array_key_exists($name, $this::$classMap);
    }

    /**
     * Get atom link string path. 
     * 
     * @param string $atomLinkName
     * @return string
     */
    public function getAtomLink($atomLinkName)
    {
        if (!isset($this->atomLinks[$atomLinkName]))
        {
            return null;
        }
        return $this->atomLinks[$atomLinkName];
    }

    protected function setAtomlink($atomLinkName, $value)
    {
        if (!isset($this->atomLinks[$atomLinkName]))
        {
            return;
        }
        $this->atomLinks[$atomLinkName] = $value;
    }

    /**
     * Find entities from url.
     * 
     * @param string $url
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @param array|int $parents
     * @param boolean $isAsync
     * @return JsonEntity[]
     */
    protected static function &findAllFromUrl($url, \shirtplatform\filter\WsParameters $wsParameters=null,$shopId=null,$parents = null,$isAsync = false )
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        if($shopId == null )
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        if( $parents!= null && !is_array($parents))
        {
            $parents = array($parents);
        }
        
        $class= get_called_class();
        
        if($wsParameters->getFilterName() == null )
        {
            $wsParameters->setRootEntityName( $class );
        }

        $promise = new \shirtplatform\rest\promise\PagePromise('GET', $url, $class, $wsParameters->buildParams(), array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        $promise->shopId = $shopId;
        $promise->parents = $parents;
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $rest->_call($promise);
        return $promise->message;
    }
    
    /**
     * Find entities from url.
     * 
     * @param string $url
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @param array|int $parents
     * @param boolean $isAsync
     * @return JsonEntity[]
     */
    protected static function &findAllFromUrlPost($url, \shirtplatform\filter\WsParameters $wsParameters=null,$shopId=null,$parents = null,$isAsync = false )
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        if($shopId == null )
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        if( $parents!= null && !is_array($parents))
        {
            $parents = array($parents);
        }
        
        $class= get_called_class();
        
        if($wsParameters->getFilterName() == null )
        {
            $wsParameters->setRootEntityName($class, true);
        }

        if( $wsParameters->appendDefaultFilter )
        {
            $url .= ';setDefaults=true';
        }
        
        $promise = new \shirtplatform\rest\promise\PagePromise('POST', $url, $class, $wsParameters->buildParams(), $wsParameters->buildPostData(), $isAsync);
        $promise->filterPromise = null;
        $promise->shopId = $shopId;
        $promise->parents = $parents;
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $rest->_call($promise);
        return $promise->message;
    }

    /**
     * NOTE: This ignore M:N subentities!
     * 
     * @return array
     */
    public function toArray()
    {
        $result = array();
        $attributes = $this->getModelAttributes();
        foreach ($attributes as $attribute => $value)
        {
            if (is_object($value))
            {
                $result[$attribute] = $value->toArray();
            } else
            {
                $result[$attribute] = $value;
            }
        }

        return $result;
    }

    
    /**
     * Get entity primary key.
     * 
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->id;
    }

    /**
     * Get parent entities Ids.
     * 
     * @return array
     */
    public function getParents()
    {
        return array();
    }

    /**
     * Set parent entities Ids.
     * 
     * @param array $parents
     */
    public function setParents($parents)
    {
        //do nothing
    }

    /**
     * Get entities parent Ids (incl. current entity Id).
     * 
     * @return array
     */
    public function getParentList()
    {
        $parents = $this->getParents();
        $parents[] = $this->id;
        return $parents;
    }

    public function clearAtomLinks()
    {
        $this->atomLinks = array();
    }

}
