<?php
namespace shirtplatform\entity\abstraction;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StringDataStructure
 *
 * @author Jan Maslik
 */
class StringDataStructure extends \shirtplatform\entity\abstraction\JsonEntity
{
	const VAR_NAME = 'string';
	public $value;
	public $id;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		$this->value = $data;
	}
        
        public function toString()
        {
            return $this->value;
        }
}
