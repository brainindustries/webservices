<?php

namespace shirtplatform\entity\abstraction;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseWithParentDao
 *
 * @author Jan Maslik
 */
use \shirtplatform\parser\WsModel,
    \shirtplatform\filter\WsParameters,
    \shirtplatform\rest\REST,
    \User,
    \shirtplatform\parser\WsParse;

class BaseWithParentDao extends JsonEntity
{

    private $parents = array();

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get parent entities Ids.
     * 
     * @return array
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * Set entity parent Ids.
     * 
     * @param array|int $parents may be array for multiple parents, or integer for one parent
     */
    public function setParents($parents)
    {
        if (!is_array($parents))
        {
            $parents = array($parents);
        }
        $this->parents = $parents;
    }

    /**
     * Get entity path.
     * 
     * @return string
     */
    public function __url()
    {
        return self::getUrl($this->parents);
    }

    /**
     * Update current entity.
     * 
     * @param boolean $isAsync
     */
    public function __update($isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url();

        $key = $this->getPrimaryKey();

        if (empty($key))
        {
            $this->clearAtomLinks();
            $method = 'POST';
        } 
        else
        {
            $method = 'PUT';
            $url .= '/' . $key;
        }
        
        $promise = new \shirtplatform\rest\promise\ItemPromise($method, $url, get_called_class() , array(), array($this::VAR_NAME => $this->getWebserviceAttributes()), $isAsync);
        $promise->parents = $this->parents;
        $promise->message = $this;
        
        $rest->_call($promise);
    }

    /**
     * Compare entity data and update if changed.
     * 
     * @param BaseWithParentDao|array $originalWsData
     * @param array $ignoredAttributes
     * @param callable|null $afterUpdate
     */
    public function __updateOnChange($originalWsData, $ignoredAttributes = array(), $afterUpdate = NULL)
    {
        if (is_array($originalWsData))
        {
            $newEntity = new $this();
            $newEntity->set($originalWsData);
            $originalWsData = $newEntity;
        }
        if (!\shirtplatform\parser\WsModel::compare($originalWsData, $this, $ignoredAttributes))
        {
            $this->__update();
            if ($afterUpdate != NULL)
            {
                call_user_func($afterUpdate);
            }
        }
    }

    /**
     * Delete current entity.
     */
    public function __delete()
    {
        self::delete($this->getPrimaryKey(), $this->parents);
    }

    /**
     * Get entity data attributes.
     * 
     * @return array
     */
    public function getModelAttributes()
    {
        $attributes = parent::getModelAttributes();
        unset($attributes['parents']);

        return $attributes;
    }

    /**
     * Get entity path.
     * 
     * @param array $parents
     * @return string
     */
    public static function getUrl($parents)
    {
        $className = get_called_class();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);

        foreach ($parents as $parent)
        {
            $url = preg_replace('/{parentId}/', $parent, $url, 1);
        }

        return $url;
    }

    /**
     * Find an single entity.
     * 
     * @param int $id
     * @param array|int $parentsId
     * @param boolean $isAsync
     * @return BaseWithParentDao
     */
    public static function &find($id, $parentsId, $isAsync = false)
    {
        if($id == null)
        {
            throw new \shirtplatform\exception\DebugException('Missing id, cannot be null.');
        }
        
        if (!is_array($parentsId))
        {
            $parentsId = array($parentsId);
        }

        $entityName = get_called_class();

        $url = self::getUrl($parentsId);
        $url .= '/' . $id;

        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, $entityName, array(), array(), $isAsync);
        $promise->parents = $parentsId;


        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Find multiple entities.
     * 
     * @param array|int $parentsId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @return BaseWithParentDao[]
     */
    public static function &findAll($parentsId, \shirtplatform\filter\WsParameters $wsParameters = null, $isAsync = false)
    {
        if (!is_array($parentsId))
        {
            $parentsId = array($parentsId);
        }

        $entityName = get_called_class();

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName($entityName);

        $promise = new \shirtplatform\rest\promise\PagePromise('GET', self::getUrl($parentsId), $entityName, $wsParameters->buildParams(), array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        $promise->parents = $parentsId;


        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Delete entity.
     * 
     * @param int $id
     * @param array|int $parentsId
     * @param boolean $isAsync
     * @return type
     */
    public static function &delete($id, $parentsId , $isAsync = false)
    {
        if (!is_array($parentsId))
        {
            $parentsId = array($parentsId);
        }

        $uri = self::getUrl($parentsId) . '/'.$id;
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $uri , array(), array(), $isAsync);
        $promise->parents = $parentsId; //not required

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    
    /**
     * Count entities.
     * 
     * @param array|int $parentsId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @return int
     */
    public static function &count($parentsId, $wsParameters = null , $isAsync = false)
    {
        if (!is_array($parentsId))
        {
            $parentsId = array($parentsId);
        }
        
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        
        $wsParameters->setRootEntityName(get_called_class());
        
        $promise = new \shirtplatform\rest\promise\CountPromise('GET', self::getUrl($parentsId), $wsParameters->buildParams() , array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        $promise->parents = $parentsId; //not required

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

}
