<?php

namespace shirtplatform\entity\abstraction;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ParentOwnedDao
 *
 * @author Jan Maslik
 */
use \User,
    \shirtplatform\rest\REST,
    \shirtplatform\filter\WsParameters,
    \shirtplatform\parser\WsParse,
    \shirtplatform\parser\WsModel;

class ParentOwnedDao extends JsonEntity
{

    private $dao_shopId = null;
    private $parents = array();

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get entity shop Id.
     * 
     * @return int
     */
    public function getShopId()
    {
        return $this->dao_shopId;
    }

    /**
     * Set entity shop Id.
     * 
     * @param int $shopId
     */
    public function setShopId($shopId)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $this->dao_shopId = $shopId;
    }

    /**
     * Get parent entities Ids.
     * 
     * @return array
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * Set parent entities Ids.
     * 
     * @param array|int $parents
     */
    public function setParents($parents)
    {
        if (!is_array($parents))
        {
            $this->parents = array($parents);
        }
        else 
        {
            $this->parents = $parents;
        }
    }

    /**
     * Get entity path string.
     * 
     * @return string
     */
    public function __url()
    {
        return self::getUrl($this->parents, $this->dao_shopId);
    }

    /**
     * Update an entity.
     * 
     * @param boolean $isAsync
     */
    public function __update($isAsync = false)
    {
        if ($this->getShopId() == null)
        {
            $this->setShopId(\shirtplatform\utils\user\User::getShopId());
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url();

        $key = $this->getPrimaryKey();

        if (empty($key))
        {
            $this->clearAtomLinks();
            $method = 'POST';
        } else
        {
            $method = 'PUT';
            $url .= '/' . $key;
        }

        $promise = new \shirtplatform\rest\promise\ItemPromise($method, $url, get_called_class(), array(), array($this::VAR_NAME => $this->getWebserviceAttributes()), $isAsync);
        $promise->shopId = $this->dao_shopId;
        $promise->parents = $this->parents;
        $promise->message = $this;

        $rest->_call($promise);
    }

    /**
     * Compare entity data and update if changed.
     * 
     * @param ParentOwnedDao|array $originalWsData
     * @param array $ignoredAttributes
     * @param callable|null $afterUpdate
     */
    public function __updateOnChange($originalWsData, $ignoredAttributes = array(), $afterUpdate = NULL)
    {
        if (is_array($originalWsData))
        {
            $newEntity = new $this();
            $newEntity->set($originalWsData);
            $originalWsData = $newEntity;
        }

        if (!\shirtplatform\parser\WsModel::compare($originalWsData, $this, $ignoredAttributes))
        {
            $this->__update();
            if ($afterUpdate != NULL)
            {
                call_user_func($afterUpdate);
            }
            //Debugger::barDump('update');
        }
    }

    /**
     * Delete current entity.
     */
    public function __delete()
    {
        self::delete($this->getPrimaryKey(), $this->parents, $this->dao_shopId);
    }

    /**
     * Get entity data attributes.
     * 
     * @return array
     */
    public function getModelAttributes()
    {
        $attributes = parent::getModelAttributes();
        unset($attributes['dao_shopId']);
        unset($attributes['parents']);

        return $attributes;
    }

    /**
     * Get entity path string.
     * 
     * @param array $parents
     * @param int $shopId
     * @return string
     */
    public static function getUrl($parents, &$shopId = null)
    {
        $className = get_called_class();
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);

        foreach ($parents as $parent)
        {
            $url = preg_replace('/{parentId}/', $parent, $url, 1);
        }

        return $url;
    }

    /**
     * Find single entity.
     * 
     * @param int $id
     * @param array|int $parentsId
     * @param int $shopId
     * @param boolean $isAsync
     * @return ParentOwnedDao
     */
    public static function &find($id, $parentsId, $shopId = null, $isAsync = false )
    {
        if($id == null)
        {
            throw new \shirtplatform\exception\DebugException('Missing id, cannot be null.');
        }
        
        if (!is_array($parentsId))
        {
            $parentsId = array($parentsId);
        }

        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        $entityName = get_called_class();

        $url = self::getUrl($parentsId,$shopId);
        $url .= '/' . $id;
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, $entityName, array(), array(), $isAsync);
        $promise->parents = $parentsId;
        $promise->shopId = $shopId;

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Find multiple entities.
     * 
     * @param array|int $parentsId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @param boolean $isAsync
     * @return ParentOwnedDao[]
     */
    public static function &findAll($parentsId, \shirtplatform\filter\WsParameters $wsParameters = null, $shopId = null, $isAsync = false )
    {
        if (!is_array($parentsId))
        {
            $parentsId = array($parentsId);
        }

        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        $entityName = get_called_class();

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName($entityName);

        $promise = new \shirtplatform\rest\promise\PagePromise('GET', self::getUrl($parentsId, $shopId ), $entityName, $wsParameters->buildParams(), array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        $promise->parents = $parentsId;
        $promise->shopId = $shopId;

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Delete entity.
     * 
     * @param int $id
     * @param array|int $parentsId
     * @param int $shopId
     * @param boolean $isAsync
     * @return type
     */
    public static function &delete($id, $parentsId, $shopId = null, $isAsync = false )
    {
        if (!is_array($parentsId))
        {
            $parentsId = array($parentsId);
        }

        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        $uri = self::getUrl($parentsId,$shopId) . '/'.$id;
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $uri , array(), array(), $isAsync);
        $promise->parents = $parentsId; //not required
        $promise->shopId = $shopId; //not required

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Count entities.
     * 
     * @param array|int $parentsId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @param boolean $isAsync
     * @return int
     */
    public static function &count($parentsId, $wsParameters = null, $shopId = null, $isAsync = false )
    {
        if (!is_array($parentsId))
        {
            $parentsId = array($parentsId);
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        
        $wsParameters->setRootEntityName(get_called_class());
        
        $promise = new \shirtplatform\rest\promise\CountPromise('GET', self::getUrl($parentsId , $shopId), $wsParameters->buildParams() , array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        $promise->parents = $parentsId; //not required
        $promise->shopId = $shopId;
        
        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }
    
    /**
     * 
     * @return array
     */
    public function getParentsFromAtomLink()
    {
        $self = $this->getAtomLink('self');
        preg_match_all('/\d+/', $self, $matches);
        $p = $matches[0];
        if (strpos($self, 'accounts') !== FALSE)
        {
            array_shift($p);
        }
        array_pop($p);
        return $p;
    }

}
