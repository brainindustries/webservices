<?php

namespace shirtplatform\entity\abstraction;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShopOwnedDao
 *
 * @author Jan Maslik
 */
use shirtplatform\filter\WsParameters,
    shirtplatform\parser\WsParse,
    \shirtplatform\rest\REST,
    \User;

class ShopOwnedDao extends JsonEntity
{

    private $dao_shopId = null;

    /**
     * 
     * @param type $data
     * @param type $parents UNUSED
     * @param type $foreignKeyOnly
     */
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get entity shop Id.
     * 
     * @return int
     */
    public function getShopId()
    {
        return $this->dao_shopId;
    }

    /**
     * Set entity shop Id.
     * 
     * @param int $shopId
     */
    public function setShopId($shopId)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $this->dao_shopId = $shopId;
    }

    /**
     * Get entity path string.
     * 
     * @return string
     */
    public function __url()
    {
        return self::getUrl($this->dao_shopId);
    }

    /**
     * Update an entity.
     * 
     * @param boolean $isAsync
     */
    public function __update($isAsync = false)
    {
        if ($this->getShopId() == null)
        {
            $this->setShopId(\shirtplatform\utils\user\User::getShopId());
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url();

        $key = $this->getPrimaryKey();

        if (empty($key))
        {
            $this->clearAtomLinks();
            $method = 'POST';
        } 
        else
        {
            $method = 'PUT';
            $url .= '/' . $key;
        }
        
        $promise = new \shirtplatform\rest\promise\ItemPromise($method, $url, get_called_class() , array(), array($this::VAR_NAME => $this->getWebserviceAttributes()), $isAsync);
        $promise->shopId = $this->dao_shopId;
        $promise->message = $this;
        
        $rest->_call($promise);
    }

    /**
     * Delete an entity.
     */
    public function __delete()
    {
        self::delete($this->getPrimaryKey(), $this->getShopId());
    }

    /**
     * Get entity data attributes.
     * 
     * @return array
     */
    public function getModelAttributes()
    {
        $attributes = parent::getModelAttributes();
        unset($attributes['dao_shopId']);

        return $attributes;
    }

    /**
     * Get entity path string.
     * 
     * @param int $shopId
     * @return string
     */
    public static function getUrl(&$shopId = null)
    {
        $className = get_called_class();
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);

        return $url;
    }

    /**
     * Find single entity.
     * 
     * @param int $id
     * @param int $shopId
     * @param boolean $isAsync
     * @return ShopOwnedDao
     */
    public static function &find($id, $shopId = null, $isAsync = false)
    {
        if($id == null)
        {
            throw new \shirtplatform\exception\DebugException('Missing id, cannot be null.');
        }
        
        $entityName = get_called_class();

        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = self::getUrl($shopId);
        $url .= '/' . $id;
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, $entityName, array(), array(), $isAsync);
        $promise->shopId = $shopId;

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Find multiple entities.
     * 
     * @param \shirtplatform\filter\WsParameters|int $wsParameters
     * @param int $shopId
     * @param boolean $isAsync
     * @return ShopOwnedDao[]
     */
    public static function &findAll(WsParameters $wsParameters = null, $shopId = null, $isAsync = false)
    {
        $entityName = get_called_class();

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName($entityName);

        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        $promise = new \shirtplatform\rest\promise\PagePromise('GET', self::getUrl($shopId), $entityName, $wsParameters->buildParams(), array(), $isAsync);
        $promise->shopId = $shopId;
        $promise->filterPromise = $wsParameters->getPromise($isAsync);

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Delete entity.
     * 
     * @param int $id
     * @param int $shopId
     * @param boolean $isAsync
     * @return type
     */
    public static function &delete($id, $shopId = null, $isAsync = false)
    {
        $uri = self::getUrl($shopId) . '/'.$id;
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $uri , array(), array(), $isAsync);
        $promise->shopId = $shopId; //not required

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Count entities.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @param boolean $isAsync
     * @return int
     */
    public static function &count($wsParameters = null, $shopId = null , $isAsync = false )
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->setRootEntityName(get_called_class());
        
        $promise = new \shirtplatform\rest\promise\CountPromise('GET', self::getUrl($shopId), $wsParameters->buildParams() , array(), $isAsync);
        $promise->shopId = $shopId;
        $promise->filterPromise = $wsParameters->getPromise($isAsync);  
        
        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

}
