<?php

namespace shirtplatform\entity\abstraction;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of OneToOneParentDao
 *
 * @author Jan Maslik
 */
class BaseOneToOneDao extends JsonEntity
{

    private $parents = array();

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get parent entities Ids.
     * 
     * @return array
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * Set parent entities Ids.
     * 
     * @param array|int $parents
     */
    public function setParents($parents)
    {
        if (!is_array($parents))
        {
            $this->parents = array($parents);
            return;
        }
        $this->parents = $parents;
    }

    /**
     * Get entity path string.
     * 
     * @return string
     */
    public function __url()
    {
        return self::getUrl($this->parents);
    }

    /**
     * Update an entity.
     * 
     * @param boolean $isAsync
     */
    public function __update($isAsync=false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url();

        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class() , array(), array($this::VAR_NAME => $this->getWebserviceAttributes()), $isAsync);
        $promise->parents = $this->parents;
        $promise->message = $this;
        
        $rest->_call($promise);
    }

    /**
     * Compare entity data and update if changed.
     * 
     * @param OneToOneParentDao|array $originalWsData
     * @param array $ignoredAttributes
     * @param callable|null $afterUpdate
     */
    public function __updateOnChange($originalWsData, $ignoredAttributes = array(), $afterUpdate = NULL)
    {
        if (is_array($originalWsData))
        {
            $newEntity = new $this();
            $newEntity->set($originalWsData);
            $originalWsData = $newEntity;
        }

        if (!\shirtplatform\parser\WsModel::compare($originalWsData, $this, $ignoredAttributes))
        {
            $this->__update();
            if ($afterUpdate != NULL)
            {
                call_user_func($afterUpdate);
            }
            //Debugger::barDump('update');
        }
    }

    /**
     * Delete current entity.
     */
    public function __delete()
    {
        self::delete( $this->parents);
    }

    /**
     * Get entity data attributes.
     * 
     * @return array
     */
    public function getModelAttributes()
    {
        $attributes = parent::getModelAttributes();
        unset($attributes['parents']);

        return $attributes;
    }

    /**
     * Get entity path string.
     * 
     * @param array $parents
     * @param int $shopId
     * @return string
     */
    public static function getUrl($parents)
    {
        $className = get_called_class();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);

        foreach ($parents as $parent)
        {
            $url = preg_replace('/{parentId}/', $parent, $url, 1);
        }

        return $url;
    }

    /**
     * Find single entity.
     * 
     * @param array|int $parentsId
     * @param int $shopId
     * @param boolean $isAsync
     * @return OneToOneParentDao
     */
    public static function &find($parentsId, $isAsync = false)
    {
        if (!is_array($parentsId))
        {
            $parentsId = array($parentsId);
        }

        $entityName = get_called_class();

        $url = self::getUrl($parentsId);

        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, $entityName, array(), array(), $isAsync);
        $promise->parents = $parentsId;

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Delete an entity.
     * 
     * @param int $id
     * @param array|int $parentsId
     * @param int $shopId
     * @param boolean $isAsync
     * @return type
     */
    public static function &delete( $parentsId,  $isAsync = false)
    {
        if (!is_array($parentsId))
        {
            $parentsId = array($parentsId);
        }

        $uri = self::getUrl($parentsId);
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $uri , array(), array(), $isAsync);
        $promise->parents = $parentsId; //not required

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

}
