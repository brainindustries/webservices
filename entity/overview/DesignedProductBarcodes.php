<?php

namespace shirtplatform\entity\overview;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignedProductBarcodes
 *
 * @author Jan Maslik
 */
class DesignedProductBarcodes extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/overview/orderedProducts/barcodes';
    const VAR_NAME = 'designedProductBarcodes';

    public static $classArrayMap = array(
        'viewBarcodes' => 'shirtplatform\entity\overview\ViewBarcode',
    );
    public static $classMap = array(
    );
    public $viewBarcodes;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);

        $reindexed = array();
        foreach ($this->viewBarcodes as $barcode)
        {
            $reindexed[$barcode->viewId][$barcode->technologyId] = $barcode;
        }

        $this->viewBarcodes = $reindexed;
    }

    /**
     * Get all product barcodes.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @return DesignedProductBarcodes[]
     */
    public static function &findAll(\shirtplatform\filter\WsParameters $wsParameters = null, $isAsync = false, callable $onFullfilled = null)
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\order\DesignedOrderedProduct');

        return self::findAllFromUrlPost(self::getUrl(), $wsParameters, null, null , $isAsync);
    }

}
