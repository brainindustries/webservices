<?php

namespace shirtplatform\entity\overview;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignElementMotive
 *
 * @author Jan Maslik
 */
class DesignElementMotive extends DesignElement
{

    public static $classMap = array(
        'motive' => 'shirtplatform\entity\overview\Motive'
    );

    const TYPE = 'MOTIVE';

    public $motive;
    public $realWidth;
    public $realHeight;
    public $pimpTaskId;
    public $pimpTaskDownloadStatus;
    public $imageQuality;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
