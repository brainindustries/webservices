<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignElementText
 *
 * @author Jan Maslik
 */
class DesignElementText extends DesignElement
{
	public static $classMap = array(
		'font' => '\shirtplatform\entity\overview\Font',
	);
	const TYPE = 'TEXT';
	public $text;
	public $special;
	public $font;
    public $realWidth;
    public $realHeight;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}


