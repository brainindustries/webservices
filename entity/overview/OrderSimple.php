<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SimpleOrder
 *
 * @author Jan Maslik
 */
class OrderSimple extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/overview/simpleOrders';
	const VAR_NAME = 'orderSimple';
	
	public static $classMap = array(
		'shop' => 'shirtplatform\entity\overview\Shop',
		'country' =>'shirtplatform\entity\overview\Country'
	);
	
	public $shop;
	public $created;
	public $financialStatus;
	public $uniqueId;
	public $country;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
