<?php

namespace shirtplatform\entity\overview;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignedOrderedProductFull
 *
 * @author Jan Maslik
 */
class DesignedOrderedProduct extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/overview/orderedProducts';
    const VAR_NAME = 'designedOrderedProduct';

    public static $classMap = array(
        'color' => 'shirtplatform\entity\overview\ProductAssignedColor',
        'size' => 'shirtplatform\entity\overview\ProductAssignedSize',
        'design' => 'shirtplatform\entity\overview\Design',
        'skus' => 'shirtplatform\entity\overview\ProductSku'
    );
    
    public static $classArrayMap = array(
        'elementRealSizes' => 'shirtplatform\entity\overview\DesignedElementRealSize'
    );
    
    public $color;
    public $size;
    public $design;
    public $created;
    public $amount;
    public $price;
    public $orderId;
    public $elementRealSizes;
    public $uuid;
    protected $skus;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get preview image url.
     * 
     * @param int $width
     * @param int $height
     * @param int $bestfit
     * @return string
     */
    public function getPreviewUrl($width = -1, $height = -1, $bestfit = 1)
    {
        return $this->getAtomLink('image') . '?width=' . $width . '&height=' . $height . '&bestfit=' . $bestfit;
    }

    /**
     * Get SKUs.
     * 
     * @param boolean $isAsync
     * @return shirtplatform\entity\overview\ProductSku[]
     */
    public function getSkus($isAsync = false)
    {
        return $this->getAtomLinkValue('skus', null, $isAsync);
    }

    /**
     * Get all designed products.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @return DesignedOrderedProduct[]
     */
    public static function &findAll(\shirtplatform\filter\WsParameters $wsParameters = null , $isAsync = false, callable $onFullfilled = null)
    {
        
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\order\DesignedOrderedProduct');

        return self::findAllFromUrl(self::getUrl(), $wsParameters, null, null , $isAsync);
    }

}
