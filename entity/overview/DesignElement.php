<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignElement
 *
 * @author Jan Maslik
 */
class DesignElement extends \shirtplatform\entity\abstraction\JsonEntity
{
	public static $classMap = array(
	);
	
	public static $classArrayMap = array(
		'layers' => 'shirtplatform\entity\overview\DesignElementLayer',
	);
	public $layerIndex;
	public $layers;
    public $deleted;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}