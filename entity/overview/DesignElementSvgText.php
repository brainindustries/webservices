<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignElementQrcode
 *
 * @author Jan Maslik
 */
class DesignElementSvgText extends DesignElement
{
	public static $classMap = array(
	);
        
	const TYPE = 'SVGTEXT';
	public $text;
	public $svgFont;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}
