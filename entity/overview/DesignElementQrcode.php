<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignElementQrcode
 *
 * @author Jan Maslik
 */
class DesignElementQrcode extends DesignElement
{
	public static $classMap = array(
	);
	const TYPE = 'QRCODE';
	public $text = \shirtplatform\entity\enumerator\QRCodeType::TEXT;
	public $errorCorrectionLevel;
	public $dataType;
    public $realWidth;
    public $realHeight;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
	
}
