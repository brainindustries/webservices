<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductAssignedColor
 *
 * @author Jan Maslik
 */
class ProductAssignedColor extends \shirtplatform\entity\abstraction\JsonEntity
{
	public static $classMap = array(
		'color' => 'shirtplatform\entity\overview\ProductColor',
	);
	
	public $color;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
