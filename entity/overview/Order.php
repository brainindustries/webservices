<?php

namespace shirtplatform\entity\overview;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Order
 *
 * @author Jan Maslik
 */
class Order extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/overview/orders';
    const VAR_NAME = 'order';

    public static $classMap = array(
        'country' => 'shirtplatform\entity\overview\Country',
        'shop' => 'shirtplatform\entity\overview\Shop',
    );
    public static $classArrayMap = array(
        'designedProducts' => '\shirtplatform\entity\overview\DesignedOrderedProduct',
    );
    public $shop;
    public $designedProducts = array();
    public $created;
    public $financialStatus;
    public $uniqueId;
    public $country;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get all orders.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @return Order[]
     */
    public static function &findAll(\shirtplatform\filter\WsParameters $wsParameters = null, $isAsync = false, callable $onFullfilled = null)
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('shirtplatform\entity\order\Order');
        
        return self::findAllFromUrlPost(self::getUrl(), $wsParameters, null, null , $isAsync);
    }

}
