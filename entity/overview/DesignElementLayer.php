<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignElementLayer
 *
 * @author Jan Maslik
 */
class DesignElementLayer extends \shirtplatform\entity\abstraction\JsonEntity
{
	public static $classMap = array(
		'assignedPrintColor' => 'shirtplatform\entity\overview\AssignedPrintColor',
		'printTechnology' => 'shirtplatform\entity\overview\PrintTechnology'
	);
	
	public $assignedPrintColor;
	public $printTechnology;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}

