<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductAssignedSize
 *
 * @author Jan Maslik
 */
class ProductAssignedSize extends \shirtplatform\entity\abstraction\JsonEntity
{
	public static $classMap = array(
		'size' => 'shirtplatform\entity\overview\ProductSize',
	);
	
	public $size;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}

}
