<?php

namespace shirtplatform\entity\overview;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignElementPimpText
 *
 * @author Jan Maslik
 */
class DesignElementPimpText extends DesignElement
{

    public static $classMap = array(
    );

    const TYPE = 'PIMP';

    public $text;
    public $fontId;
    public $taskId;
    public $realWidth;
    public $realHeight;
    public $pimpTaskId;
    public $pimpTaskDownloadStatus;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
