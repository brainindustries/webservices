<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Country
 *
 * @author Jan Maslik
 */
class Country extends \shirtplatform\entity\abstraction\JsonEntity
{
	public $name;
	public $code;
	
	public static $classMap = array(
	);
	
	public static $classArrayMap = array(
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
