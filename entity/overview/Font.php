<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Font
 *
 * @author Jan Maslik
 */
class Font extends \shirtplatform\entity\abstraction\JsonEntity
{
	const PATH_TEMPLATE = 'accounts/{accountId}/fonts';
	const VAR_NAME = 'font';

	public static $classMap = array(
	);
	
	public $name;
	public $deleted;
	public $enabled;
	public $version;
	public $className;
	public $defSize;
	public $minSize;
	public $maxSize;
	public $fontDataId;
    public $orderIndex;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
