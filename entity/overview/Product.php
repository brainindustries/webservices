<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product
 *
 * @author Jan Maslik
 */
class Product extends \shirtplatform\entity\abstraction\JsonEntity
{
	public $name;
    public $model;
    public $artNr;
	public $sourceId;
	
	public static $classMap = array(
	);
	
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}