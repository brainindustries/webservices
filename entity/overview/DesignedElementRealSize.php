<?php

namespace shirtplatform\entity\overview;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignedElementRealSize
 *
 * @author Jan Maslik
 */
class DesignedElementRealSize extends \shirtplatform\entity\abstraction\JsonEntity
{
    const VAR_NAME = 'designedElementRealSize';
    
    public static $classMap = array(
    );
    public static $classArrayMap = array(
    );
    
    public $realWidth;
    public $realHeight;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
