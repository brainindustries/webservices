<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductArea
 *
 * @author Jan Maslik
 */
class ProductArea  extends \shirtplatform\entity\abstraction\JsonEntity
{
	public static $classMap = array(
		//'elements' => 'shirtplatform\entity\overview\DesignElement',
		'assignedView' => 'shirtplatform\entity\overview\ProductAssignedView',
	);
	
	public $assignedView;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}