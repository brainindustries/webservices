<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductAssignedView
 *
 * @author Jan Maslik
 */
class ProductAssignedView  extends \shirtplatform\entity\abstraction\JsonEntity
{
	public static $classMap = array(
		'view' => '\shirtplatform\entity\product\ProductView',
	);
	
	public $view;
        public $prioritizeOnOverview;
	public $orderIndex;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
