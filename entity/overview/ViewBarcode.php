<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ViewBarcodes
 *
 * @author Jan Maslik
 */
class ViewBarcode extends \shirtplatform\entity\abstraction\JsonEntity
{
	const VAR_NAME = 'viewBarcode';
	 
        
	public static $classMap = array(
	);
	
	public $viewId;
	public $technologyId;
	public $barcode;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	
		if( !empty($data) )
		{
			$this->viewId = $data['id'];
			$this->id = $this->viewId.'-'.$this->technologyId;
		}
	}
}
