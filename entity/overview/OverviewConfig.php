<?php

namespace shirtplatform\entity\overview;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OverviewConfig
 *
 * @author Jan Maslik
 */
class OverviewConfig extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/overview';
    const VAR_NAME = 'overviewConfig';

    public static $classMap = array();
    public $techColors;
    public $mainViews;
    public $qualityLow = '{}';
    public $qualityAcceptable = '{}';
    public $qualityGood = '{}';
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Find an single entity.
     * 
     * @param int $id
     * @param array $params
     * @param boolean $isAsync
     * @return OverviewConfig
     */
    public static function &find($id = NULL, $params = array() , $isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = self::getUrl();
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), [] , [], $isAsync);

        return $rest->_call($promise);
    }

}
