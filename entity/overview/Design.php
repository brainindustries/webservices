<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Design
 *
 * @author Jan Maslik
 */
class Design extends \shirtplatform\entity\abstraction\JsonEntity
{
	public $product;
	public $compositions;
	
	public static $classMap = array(
		'product' => 'shirtplatform\entity\overview\Product',
	);
	
	public static $classArrayMap = array(
		'compositions' => 'shirtplatform\entity\overview\DesignComposition',
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
