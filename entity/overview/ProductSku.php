<?php
namespace shirtplatform\entity\overview;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductSku
 *
 * @author Jan Maslik
 */
class ProductSku extends \shirtplatform\entity\abstraction\BaseWithParentDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/overview/orderedProducts/{parentId}/factories/{parentId}/sku';
        
	const VAR_NAME = 'productSku';
	 
	public static $classMap = array(
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
	public $plu;
	public $available;
        public $string;
        
        public static function findAll($parentsId, \shirtplatform\filter\WsParameters $wsParameters = null, $isAsync = false)
        {
            throw new \Exception('Doesnt implemented.');
        }
	
        /**
         * 
         * @param array $parentsId
         * @param boolean $isAsync
         * @return ProductSku
         */
        public static function find($parentsId, $isAsync = false)
        {
            if (!is_array($parentsId))
            {
                $parentsId = array($parentsId);
            }

            $entityName = get_called_class();
            $url = self::getUrl($parentsId);

            $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, $entityName, array(), array(), $isAsync);
            $promise->parents = $parentsId;

            $rest = \shirtplatform\rest\REST::getInstance();
            return $rest->_call($promise);
        }
}
