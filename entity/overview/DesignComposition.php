<?php

namespace shirtplatform\entity\overview;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignComposition
 *
 * @author Jan Maslik
 */
class DesignComposition extends \shirtplatform\entity\abstraction\JsonEntity
{

    public static $classMap = array(
        'originalArea' => 'shirtplatform\entity\overview\ProductArea',
    );
    public static $classArrayMap = array(
        'designElementMotive' => 'shirtplatform\entity\overview\DesignElementMotive',
        'designElementText' => 'shirtplatform\entity\overview\DesignElementText',
        'designElementPimpText' => 'shirtplatform\entity\overview\DesignElementPimpText',
        'designElementQrcode' => 'shirtplatform\entity\overview\DesignElementQrcode',
        'designElementSvgText' => 'shirtplatform\entity\overview\DesignElementSvgText'
    );
    
    public $designElementMotive = array();
    public $designElementText = array();
    public $designElementPimpText = array();
    public $designElementQrcode = array();
    public $designElementSvgText = array();
    public $originalArea;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get design elements.
     * 
     * @return array
     */
    public function getElements()
    {
        $elements = array_merge(
                $this->designElementMotive,
                $this->designElementText,
                $this->designElementPimpText,
                $this->designElementQrcode,
                $this->designElementSvgText
        );
        return \shirtplatform\parser\WsModel::sortBy($elements, 'layerIndex');
    }

}
