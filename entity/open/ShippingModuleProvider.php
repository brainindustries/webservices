<?php

namespace shirtplatform\entity\open;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShippingModuleProvider
 *
 * @author admin
 */
class ShippingModuleProvider extends \shirtplatform\entity\abstraction\BaseDao
{
    
    const PATH_TEMPLATE = 'public/shippingModuleProviders';
    const VAR_NAME = 'shippingModuleProvider';
    
    public static $classMap = array(
        'moduleType' => '\shirtplatform\entity\open\ShippingModuleType'
    );
    
    public $name;
    public $provider;
    public $serviceType;
    public $moduleType;
    
}
