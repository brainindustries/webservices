<?php

namespace shirtplatform\entity\open;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserSignUp
 *
 * @author admin
 */
class UserSignUp extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_USER_SIGNUP = 'public/users/signup';
    const VAR_NAME = 'userSignUp';
    
    public $login;
    public $email;
    public $password;
    public $affiliateId;
    
    public function __signup()
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', self::PATH_USER_SIGNUP, get_called_class(), [], [self::VAR_NAME => $this->getWebserviceAttributes()]);
        return $rest->_call($promise);
    }
    
    public static function find($id, $params = array(), $isAsync = false)
    {
        throw new Exception("No implemented method!");
    }
    
    public static function findAll(\shirtplatform\filter\WsParameters $wsParameters = null, $isAsync = false, callable $onFullfilled = null)
    {
        throw new Exception("No implemented method!");
    }
    
}
