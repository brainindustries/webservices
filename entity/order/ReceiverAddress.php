<?php

namespace shirtplatform\entity\order;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReceiverAddress
 *
 * @author admin
 */
class ReceiverAddress extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orders/{parentId}/receiverAddress';
    const VAR_NAME = 'address';
    
    public static $classMap = array(
    );

    public $version;
    public $street;
    public $streetNo;
    public $address2;
    public $city;
    public $company;
    public $country;
    public $firstName;
    public $lastName;
    public $latitude;
    public $longitude;
    public $name;
    public $phone;
    public $email;
    public $province;
    public $zip;
    public $countryCode;
    public $provinceCode;
    public $careOf;
    public $stateCode;
    public $stateName;
    
    public function __update($isAsync = false)
    {
        throw new \Exception('No implemented method.');
    }
    
    public function __delete()
    {
        throw new \Exception('No implemented method.');
    }
    
    public static function findAll($parentsId, \shirtplatform\filter\WsParameters $wsParameters = null, $shopId = null, $isAsync = false)
    {
        throw new \Exception('No implemented method.');
    }
    
    public static function find($orderId, $shopId = null, $isAsync = false)
    {
        $parentsId = [$orderId];
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        $entityName = get_called_class();
        $url = self::getUrl($parentsId, $shopId);
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, $entityName, array(), array(), $isAsync);
        $promise->parents = $parentsId;
        $promise->shopId = $shopId;

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

}
