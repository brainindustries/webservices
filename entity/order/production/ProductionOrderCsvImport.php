<?php

namespace shirtplatform\entity\order\production;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ProductionOrderCsvImport extends \shirtplatform\entity\abstraction\ShopOwnedDao
{
    
    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orderCsvImport';

    const VAR_NAME = 'productionOrderCsvImport';

    public static $classMap = array(
    );
    
    public $fileName;
    public $createdDate;
    public $processedDate;
    public $processed;
    public $error;
    public $errorMessage;
    
    /**
     * 
     * @param type $shopId
     * @param type $sourcePath
     * @param type $fileName
     * @param type $charset
     * @param type $separator
     * @param type $quote
     * @param type $ignoreFirstLine
     * @return type
     */
    public static function uploadCsv($shopId, $sourcePath, $fileName, $charset, $separator, $quote, $ignoreFirstLine)
    {
        $url = str_replace(['{accountId}', '{shopId}'], [\shirtplatform\utils\user\User::getAccountId(), $shopId], self::PATH_TEMPLATE);
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $fileName, [
            'charset' => $charset,
            'separator' => $separator,
            'quote' => $quote,
            'ignoreFirstLine' => $ignoreFirstLine
        ], false);
        return $rest->_call($promise);
    }
    
    /**
     * 
     * @param type $shopId
     * @param type $importId
     * @return type
     */
    public static function getCsvContent($shopId, $importId)
    {
        $url = self::PATH_TEMPLATE . '/{importId}/content';
        $url = str_replace(['{accountId}', '{shopId}', '{importId}'], [\shirtplatform\utils\user\User::getAccountId(), $shopId, $importId], $url);
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\PlatformPromise('GET', $url, ['query' => []]);
        return $rest->_call($promise);
    }
    
    /**
     * 
     * @param type $shopId
     * @param type $importId
     * @return type
     */
    public static function getExportCsvContent($shopId, $importId)
    {
        $url = self::PATH_TEMPLATE . '/{importId}/export';
        $url = str_replace(['{accountId}', '{shopId}', '{importId}'], [\shirtplatform\utils\user\User::getAccountId(), $shopId, $importId], $url);
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\PlatformPromise('GET', $url, ['query' => []]);
        return $rest->_call($promise);
    }
    
}
