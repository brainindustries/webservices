<?php

namespace shirtplatform\entity\order\production;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ProductionOrderCsvImportLine extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
    
    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orderCsvImport/{parentId}/lines';
    
    const VAR_NAME = 'orderCsvImportLine';

    public static $classMap = array(
    );
    
    public $lineIndex;
    public $error;
    public $errorMessage;
    public $orderId;
    
}
