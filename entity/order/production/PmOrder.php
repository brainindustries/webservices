<?php

namespace shirtplatform\entity\order\production;

/**
 * Description of Order
 *
 * @author admin
 */
class PmOrder extends \shirtplatform\entity\abstraction\JsonEntity
{

    const VAR_NAME = 'pmOrder';

    public static $classMap = array(
        'senderAddress' => '\shirtplatform\entity\order\production\PmAddress',
        'receiverAddress' => '\shirtplatform\entity\order\production\PmAddress',
        'shipping' => '\shirtplatform\entity\order\production\PmShipping'
    );
    
    public $senderAddress;
    public $receiverAddress;
    public $shipping;
    public $productIgnoreReservation = array();
    public $flags = array();
    public $invoiceGroup = '';
    
}
