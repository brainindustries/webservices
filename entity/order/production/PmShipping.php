<?php

namespace shirtplatform\entity\order\production;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PmShipping extends \shirtplatform\entity\abstraction\JsonEntity
{
    const VAR_NAME = 'shippingSetup';
    public static $classMap = array(
    );

    public $providerId;
    public $price;
    public $codPrice;
    public $orderValue;
    public $preferedDeliveryTime;
    public $branchId;
        
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
}
