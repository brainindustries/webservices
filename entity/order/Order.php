<?php

//namespace shirtplatform\entity;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\order;

/**
 * Description of ProductionOrder
 *
 * @author Jan Maslik
 */
use \shirtplatform\entity\account\Country;
use \shirtplatform\entity\order\DesignedOrderedProduct;
use \User;
use \shirtplatform\rest\REST;
use \shirtplatform\filter\WsParameters;
use \shirtplatform\filter\Filter;
use \shirtplatform\parser\WsParse;
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\parser\WsModel;

class Order extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orders';
    const PATH_TEMPLATE_PRODUCTION_FLAG = 'accounts/{accountId}/shops/{shopId}/orders/{parentId}/production/flag/{flagName}';
    const PATH_TEMPLATE_PRODUCTION_COMMENT = 'accounts/{accountId}/shops/{shopId}/orders/{parentId}/production/comment';
    
    const VAR_NAME = 'productionOrder';

    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country',
        'orderedProducts' => '\shirtplatform\entity\order\DesignedOrderedProduct',
        'orderedManualServices' => '\shirtplatform\entity\order\OrderedManualService'
    );
    public $uniqueId;
    public $paid;
    public $deleted = false;
    public $created;
    public $financialStatus;
    public $country;
    public $version;
    public $fulfilmentStatus;
    
    protected $orderedProducts = null;
    protected $orderedManualServices = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
    /**
     * 
     * @return \shirtplatform\entity\order\OrderedManualService[]
     */
    public function getOrderedManualServices()
    {
        return $this->getAtomLinkValue('orderedManualServices');
    }

    /**
     * Get ordered products.
     * 
     * @return \shirtplatform\entity\order\DesignedOrderedProduct[]
     */
    public function getOrderedProducts()
    {
        return $this->getAtomLinkValue('orderedProducts');
    }

    /**
     * Sets financial status
     * 
     * @param type $financialStatus
     * @return void
     */
    public function &__commit($financialStatus , $isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $uri = $this->getAtomLink('commit');
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $uri, get_called_class(), array('financialStatus' => $financialStatus) , [], $isAsync);

        return $rest->_call($promise);
    }

    /**
     * Associate order with current session.
     * 
     * @param boolean $isAsync
     * @return void
     */
    public function &__associateWithSession($isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $uri = $this->getAtomLink('associate');
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $uri, get_called_class(),[] , [], $isAsync);

        return $rest->_call($promise);
    }

    /**
     * Transfer order items
     * 
     * @param int $sourceOrderId
     * @param int $destinationOrderId
     * @param boolean $isAsync
     * @return void
     */
    public function &__transfer($sourceOrderId, $destinationOrderId , $isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $uri = $this->getAtomLink('transfer');
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $uri, get_called_class(), array('sourceOrderId' => $sourceOrderId, 'destinationOrderId' => $destinationOrderId) , [], $isAsync);

        return $rest->_call($promise);
    }

    /**
     * Find order by session. 
     * Order is created by Creator or self::createBySession method
     * 
     * @param int|null $shopId
     * @param boolean $isAsync
     * @throws Exception if no order assigned to this session.
     * @return Order
     */
    public static function &findBySession($shopId = null, $isAsync = false )
    {
        $entityName = get_called_class();

        $wsParameters = new \shirtplatform\filter\WsParameters();
        $wsParameters->setRootEntityName($entityName);

        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = self::getUrl($shopId);
        $url .= '/session';
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, $entityName, $wsParameters->buildParams(), array(), $isAsync);
        $promise->shopId = $shopId;

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

      /**
     * Transfer order items
     * 
     * @param int $sourceOrderId
     * @param int $destinationOrderId
     * @param boolean $isAsync
     * @return void
     */
    public static function &transfer($sourceOrderId, $destinationOrderId ,$shopId = null, $isAsync = false, array $orderedProductIds = NULL)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = self::getUrl($shopId).'/transferItems';
        
        $query = [
            'sourceOrderId' => $sourceOrderId, 
            'destinationOrderId' => $destinationOrderId
        ];
        
        if($orderedProductIds != NULL)
        {
            $query['orderedProductList'] = implode(',', $orderedProductIds);
        }
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), $query, [], $isAsync);
        return $rest->_call($promise);
    }
    
      /**
     * Associate order with current session.
     * 
     * @param boolean $isAsync
     * @throws Exception if already is assigned some order in this session
     * @return void
     */
    public static function &associateWithSession($orderId, $shopId = null,$isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $uri = self::getUrl($shopId).'/'.$orderId.'/associate';
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $uri, get_called_class(),[] , [], $isAsync);

        return $rest->_call($promise);
    }
  
    /**
     * Sets financial status
     * 
     * @param type $financialStatus
     * @return void
     */
    public static function &commit($orderId, $financialStatus ,$shopId = null, $isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $uri = self::getUrl($shopId).'/'.$orderId.'/commit';
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $uri, get_called_class(), array('financialStatus' => $financialStatus) , [], $isAsync);

        return $rest->_call($promise);
    }
    
    /**
     * Creates new Order and assign it with session. 
     * 
     * 
     * @param type $countryId
     * @param type $shopId
     * @param boolean|null $isAsync
     * @param string|null $uniqueId
     * @throws Exception if already some order is assigned to this session
     * @return \Order
     */
    public static function &createBySession($countryId, $shopId = null , $isAsync = false, $uniqueId = null )
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/session';
        $order = new Order();
        $order->country = new Country(array('id' => $countryId));
        if($uniqueId != null)
        {
            $order->uniqueId = $uniqueId;
        }
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class() , [] , array(Order::VAR_NAME => $order->getWebserviceAttributes()) , $isAsync);
        return $rest->_call($promise);
    }
    
    public static function undelete($id, $shopId)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $id . '/undelete';
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class());
        return $rest->_call($promise);
    }
    
    public function __undelete()
    {
       self::undelete($this->id, $this->getShopId());
    }
    
    public function __addToProductionTool(production\PmOrder $pmOrder, $warehouseId)
    {
        return self::addToProductionTool($this->getPrimaryKey(), $this->getShopId(), $pmOrder, $warehouseId);
    }
    
    public static function addToProductionTool($orderId, $shopId, production\PmOrder $pmOrder = NULL, $warehouseId = NULL)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/production';
        
        $queryParams = [];
        if($warehouseId !== NULL)
        {
            $queryParams = ['warehouseId' => $warehouseId];
        }

        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), $queryParams, array(production\PmOrder::VAR_NAME => ($pmOrder != NULL) ? $pmOrder->getWebserviceAttributes() : null), FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }
    
    public function __updateProductionSenderAddress(production\PmAddress $address)
    {
        return self::updateProductionSenderAddress($this->getPrimaryKey(), $address, $this->getShopId());
    }
    
    public static function updateProductionSenderAddress($orderId, \shirtplatform\entity\order\production\PmAddress $address, $shopId = NULL)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/production/senderAddress';

        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], [production\PmAddress::VAR_NAME => $address->getWebserviceAttributes()], FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }
    
    public function __updateProductionReceiverAddress(production\PmAddress $address)
    {
        return self::updateProductionReceiverAddress($this->getPrimaryKey(), $address, $this->getShopId());
    }
    
    public static function updateProductionReceiverAddress($orderId, \shirtplatform\entity\order\production\PmAddress $address, $shopId = NULL)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/production/receiverAddress';

        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], [production\PmAddress::VAR_NAME => $address->getWebserviceAttributes()], FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }
    
    public function __updateProductionShipping(production\PmShipping $address)
    {
        return self::updateProductionShipping($this->getPrimaryKey(), $address, $this->getShopId());
    }
    
    public static function updateProductionShipping($orderId, production\PmShipping $shipping, $shopId = NULL)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/production/shipping';

        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], [production\PmShipping::VAR_NAME => $shipping->getWebserviceAttributes()], FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }
    
    public static function addIgnoreReservation($orderId, $shopId, array $productIds)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/production/ignore/reservation';

        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], ['value' => $productIds], FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }
    
    public static function removeIgnoreReservation($orderId, $shopId, array $productIds)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/production/ignore/reservation';

        $promise = new \shirtplatform\rest\promise\ItemPromise('DELETE', $url, get_called_class(), [], ['value' => $productIds], FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }
    
    public function __removeFromProductionTool()
    {
        return self::removeFromProductionTool($this->getPrimaryKey(), $this->getShopId());
    }
    
    public static function removeFromProductionTool($orderId, $shopId)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/production';

        $promise = new \shirtplatform\rest\promise\ItemPromise('DELETE', $url, get_called_class(), [], [], FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }
    
    public function __setToCancel()
    {
        return self::setToCancel($this->getPrimaryKey(), $this->getShopId());
    }
    
    public static function setToCancel($orderId, $shopId)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/cancel';

        $promise = new \shirtplatform\rest\promise\ItemPromise('DELETE', $url, get_called_class(), [], [], FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }
    
    public function __addFlag($flagName)
    {
        return self::addFlag($this->getPrimaryKey(), $this->getShopId(), $flagName);
    }
    
    public static function addFlag($orderId, $shopId, $flagName)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/production/flag/' . $flagName;
        $url = str_replace(['{parentId}'], [$orderId], $url);

        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [], FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }
    
    public function __removeFlag($flagName)
    {
        return self::removeFlag($this->getPrimaryKey(), $this->getShopId(), $flagName);
    }
    
    public static function removeFlag($orderId, $shopId, $flagName)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/production/flag/' . $flagName;
        $url = str_replace(['{parentId}'], [$orderId], $url);

        $promise = new \shirtplatform\rest\promise\ItemPromise('DELETE', $url, get_called_class(), [], [], FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }
    
    public function __addComment($comment)
    {
        return self::addComment($this->getPrimaryKey(), $this->getShopId(), $comment);
    }
    
    public static function addComment($orderId, $shopId, $comment)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/production/comment';
        $url = str_replace(['{parentId}'], [$orderId], $url);

        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], ['value' => $comment], FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }
    
    /**
     * 
     * @param int $orderedProductId
     * @param int $amount
     */
    public function __wrongPrinted($orderedProductId, $amount)
    {
        return self::markAsCorrupted($this->getPrimaryKey(), $this->getShopId(), $orderedProductId, $amount);
    }
    
    /**
     * 
     * @param int $orderId
     * @param int $shopId
     * @param int $orderedProductId
     * @param int $amount
     */
    public static function wrongPrinted($orderId, $shopId, $orderedProductId, $amount)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = Order::getUrl($shopId) . '/' . $orderId . '/orderedProducts/' . $orderedProductId . '/corrupted/' . $amount;

        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], [], FALSE);
        $promise->shopId = $shopId;
        $rest->_call($promise);
    }

}
