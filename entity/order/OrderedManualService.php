<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\order;

/**
 * Description of OrderedManualService
 *
 * @author admin
 */
class OrderedManualService extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orders/{parentId}/orderedManualServices';
    
    const VAR_NAME = 'orderedManualService';
    
    public $name;
    public $code;
    public $price;
    public $amount;
    public $created;
    public $pimpOutputActionId;
    public $deleted;
    public $version;
    
}
