<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\order;

/**
 * Description of DesignComposition
 *
 * @author Jan Maslik
 */
class DesignComposition extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/design/{parentId}/compositions';
	const VAR_NAME = 'designComposition';
	 
        
	public static $classMap = array(
		'originalArea' => '\shirtplatform\entity\product\ProductArea'
	);
	
	public $originalArea;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
