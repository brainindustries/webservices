<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\order;

/**
 * Description of Design
 *
 * @author Jan Maslik
 */
class Design extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	//const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orders/{parentId}/orderedProducts';
	const VAR_NAME = 'design';
	 
        
	public static $classMap = array(
		'product' => '\shirtplatform\entity\product\Product'
	);
	
	public $product;
	public $version;
        public $creatorType;
        
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}

}