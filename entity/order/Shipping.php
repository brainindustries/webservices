<?php

namespace shirtplatform\entity\order;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Shipping
 *
 * @author admin
 */
class Shipping extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
    
    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orders/{parentId}/shipping';
    const VAR_NAME = 'shippingSetup';
    
    public static $classMap = array(
    );

    public $providerId;
    public $price;
    public $codPrice;
    public $orderValue;
    public $preferedDeliveryTime;
    public $branchId;
    
    public static function find($id, $parentsId, $shopId = null, $isAsync = false)
    {
        throw new \Exception('No implemented method.');
    }
    
    public function __update($isAsync = false)
    {
        throw new \Exception('No implemented method.');
    }
    
    public function __delete()
    {
        throw new \Exception('No implemented method.');
    }
    
}
