<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\order;

use shirtplatform\entity\product\DesignedOrderedProductCustomAttribute;

/**
 * Description of DesignedOrderedProduct
 *
 * @author Jan Maslik
 */
class DesignedOrderedProduct extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orders/{parentId}/orderedProducts';
    const PATH_CREATE_CUSTOMIZED = 'accounts/{accountId}/shops/{shopId}/orders/{parentId}/orderedProducts/usingCustomizedTemplateProduct';
    const PATH_TEMPLATE_FROM_COLLECTION = 'accounts/{accountId}/shops/{shopId}/orders/{orderId}/orderedProducts/usingTemplateProduct/{designedTemplateProductId}';
    const PATH_TEMPLATE_FROM_BASE = 'accounts/{accountId}/shops/{shopId}/orders/{orderId}/orderedProducts/usingBaseProduct/{productId}';
    const PATH_FROM_CREATORSE = 'accounts/{accountId}/shops/{shopId}/orders/{orderId}/orderedProducts/usingCreatorSE';
    const PATH_REPLACE_TECHNOLOGY = 'accounts/{accountId}/shops/{shopId}/orders/{orderId}/orderedProducts/{orderedProductId}/changePrintTechnology';
    
    const VAR_NAME = 'orderedProduct';

    public static $classMap = array(
        'assignedProductColor' => '\shirtplatform\entity\product\AssignedProductColor',
        'assignedProductSize' => '\shirtplatform\entity\product\AssignedProductSize',
        'design' => '\shirtplatform\entity\order\Design',
        'teamDesignCollection' => '\shirtplatform\entity\order\TeamDesignCollection'
    );
    
    public $amount;
    public $price;
    public $design;
    public $created;
    public $deleted;
    public $version;
    public $assignedProductColor;
    public $assignedProductSize;
    public $teamDesignCollection;
    public $uuid;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get preview image url.
     * 
     * @param int $width
     * @param int $height
     * @param int $bestfit
     * @return string
     */
    public function getPreviewUrl($width = -1, $height = -1, $bestfit = 1)
    {
        return $this->getAtomLink('image') . '?width=' . $width . '&height=' . $height . '&bestfit=' . $bestfit;
    }
    
    /**
     * Evaluate designed product price.
     * 
     * @param int $orderId
     * @param int $designedOrderedProductId
     * @param int $countryId
     * @param int|null $shopId
     */
    public static function evaluatePrice($orderId, $designedOrderedProductId, $countryId, $shopId = NULL)
    {
        $url = self::getUrl(array($orderId), $shopId);
        $url .= '/' . $designedOrderedProductId . '/country/'.$countryId.'/evaluate';
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], [], false);
        return $rest->_call($promise);
    }

    public static function createFromCollection($shopId, $orderId,  $collectionProductId , $assignedColorId = 0 , $assignedSizeId = 0 , $amount = 1)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE_FROM_COLLECTION);
        $url = str_replace('{shopId}', $shopId, $url);
        $url = str_replace('{orderId}', $orderId, $url);
        $url = str_replace('{designedTemplateProductId}', $collectionProductId, $url);
        
        $query=[
            'amount' => $amount,
            'assignedColorId' => $assignedColorId,
            'assignedSizeId' => $assignedSizeId
        ];
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), $query, [], false);
        return $rest->_call($promise); 
    }
    
    /**
     * 
     * @param int|null $shopId
     * @param int $orderId
     * @param int $designedTemplateProductId
     * @param int|0 $assignedColorId
     * @param int|0 $assignedSizeId
     * @param int $amount
     * @param array $customAttributes
     * @return DesignedOrderedProduct
     */
    public static function createCustomizedOrderedProduct($shopId, $orderId, $designedTemplateProductId, $assignedColorId, $assignedSizeId, $amount, array $customAttributes)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_CREATE_CUSTOMIZED);
        $url = str_replace('{shopId}', $shopId, $url);
        $url = str_replace('{parentId}', $orderId, $url);
        $url .= '/' . $designedTemplateProductId;
        
        $query = [
            'assignedColorId' => $assignedColorId,
            'assignedSizeId' => $assignedSizeId,
            'amount' => $amount
        ];
        
        $attrs = [new \shirtplatform\entity\product\DesignedOrderedProductCustomAttribute('_shrCustomizationData', '{"com.shirtplatform.creator.struct.CustomizationData":{"elements":{"com.shirtplatform.creator.struct.ElementUpdateData":' . json_encode($customAttributes) . '}}}')];
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), $query, ['list' => ['orderedProductCustomAttribute' => $attrs]], false);
        return $rest->_call($promise); 
    }
    
    /**
     * 
     * @param int|null $shopId
     * @param int $orderId
     * @param int $productId
     * @param int $assignedColorId
     * @param int $assignedSizeId
     * @param int $amount
     * @return DesignedOrderedProduct
     */
    public static function createFromBase($shopId, $orderId, $productId , $assignedColorId, $assignedSizeId, $amount)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE_FROM_BASE);
        $url = str_replace('{shopId}', $shopId, $url);
        $url = str_replace('{orderId}', $orderId, $url);
        $url = str_replace('{productId}', $productId, $url);
        
        $query = [
            'amount' => $amount,
            'assignedColorId' => $assignedColorId,
            'assignedSizeId' => $assignedSizeId
        ];
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), $query, [], false);
        return $rest->_call($promise); 
    }
    
    /**
     * 
     * @param int|null $shopId
     * @param int $orderId
     * @param array $seDesign
     * @return DesignedOrderedProduct
     */
    public static function createUsingCreatorSE($shopId, $orderId, array $seDesign)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_FROM_CREATORSE);
        $url = str_replace('{shopId}', $shopId, $url);
        $url = str_replace('{orderId}', $orderId, $url);
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], $seDesign, false);
        return $rest->_call($promise); 
    }
    
    /**
     * 
     * @param int $shopId
     * @param int $orderId
     * @param int $designedOrederedProductId
     * @param int $printTechnologyId
     * @return type
     */
    public static function replacePrintTechnology($shopId, $orderId, $designedOrederedProductId, $printTechnologyId)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_REPLACE_TECHNOLOGY);
        $url = str_replace('{shopId}', $shopId, $url);
        $url = str_replace('{orderId}', $orderId, $url);
        $url = str_replace('{orderedProductId}', $designedOrederedProductId, $url);
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), ['printTechnologyId' => $printTechnologyId], [], false);
        return $rest->_call($promise); 
    }
    
}
