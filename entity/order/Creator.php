<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\order;

/**
 * Description of Creator
 *
 * @author Jan Maslik
 */
use \User;
use \shirtplatform\rest\REST;
use \shirtplatform\filter\WsParameters;
use \shirtplatform\filter\Filter;
use \shirtplatform\parser\WsParse;
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\parser\WsModel;

class Creator extends \shirtplatform\entity\abstraction\JsonEntity
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/creator/snippet';
    const PATH_TEMPLATE_CREATOR = 'accounts/{accountId}/shops/{shopId}/creator/preview';
    const VAR_NAME = 'creator';

    private $shopId;
    public static $classMap = array(
        'snippet' => '\XhtmlSnippet',
        'orders' => '\shirtplatform\entity\order\Order'
    );
    public $width;
    public $height;
    public $sessionId;
    public $langId;
    public $countryId;
    public $snippet;
    protected $orders = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get orders.
     * 
     * @return \shirtplatform\entity\order\Order[]
     */
    public function getOrder()
    {
        return $this->getAtomLinkValue('orders');
    }

    /**
     * Get shop Id.
     * 
     * @return int
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set shop Id.
     * 
     * @param int $shopId
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;
    }

    /**
     * Get Creator data with custom settings.
     * <p>
     * <b>Parameters may be:</b>
     * langId ,countryId,productId ,assignedColorId ,assignedSizeId ,amount ,
     * orderedProductId ,editOrderedProductId ,width  , heigh ,addToBasketCallback ,player(Flash,Svg) 
     * productChangeCallback,creatorStartCallback 
     * </p>
     * 
     * */
    public static function &find($params = array(), $shopId = null, $isAsync = false)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), Creator::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), $params, [], $isAsync);
        $promise->shopId = $shopId;
        return $rest->_call($promise);
    }


    /**
     * Get creator url.
     * 
     * @param int|null $shopId
     * @param bolean $isAsync
     * @return string
     */
    public static function &getCreatorUlr($shopId = null,$isAsync = false )
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), Creator::PATH_TEMPLATE_CREATOR);
        $url = str_replace('{shopId}', $shopId, $url);

        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), [], [], $isAsync);

        return $rest->_call($promise);
    }

}

class XhtmlSnippet extends \shirtplatform\entity\abstraction\JsonEntity
{

    public $content;

    public function __construct($data = null, $foreignKeyOnly = false)
    {
        parent::__construct($data, $foreignKeyOnly);
    }

}
