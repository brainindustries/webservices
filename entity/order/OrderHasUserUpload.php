<?php

namespace shirtplatform\entity\order;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrderHasUserUpload
 *
 * @author admin
 */
class OrderHasUserUpload extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orders/hasUserUpload';
    const VAR_NAME = 'orderUserUpload';

    public static $classMap = array(
        
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
    public static function findAll(\shirtplatform\filter\WsParameters $wsParameters = null, $shopId = null, $isAsync = false)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);
        if($wsParameters == null )
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->setRootEntityName('\shirtplatform\entity\order\Order');
        
        return self::findAllFromUrl($url, $wsParameters, $shopId, [] , $isAsync);
    }
    
    public static function count($wsParameters = null, $shopId = null, $isAsync = false)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);
        if($wsParameters == null )
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->setRootEntityName('\shirtplatform\entity\order\Order');
        $wsParameters->page = 0;
        $wsParameters->size = 1;
        
        $promise = new \shirtplatform\rest\promise\CountPromise('GET', $url, $wsParameters->buildParams(), array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        $promise->shopId = $shopId;
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $rest->_call($promise);
        return $promise->message;
    }

}
