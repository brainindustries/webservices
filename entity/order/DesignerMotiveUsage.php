<?php

namespace shirtplatform\entity\order;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesignerMotiveUsage
 *
 * @author admin
 */
class DesignerMotiveUsage extends \shirtplatform\entity\abstraction\JsonEntity
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orders/authorProvision/motiveUsage/{designerId}';
    const VAR_NAME = 'designerMotiveUsage';

    public static $classMap = array(

    );
    
    public $name;
    public $amount;
    public $version;
    public $active;
    public $authorProvision;
    public $motiveProfit;
    public $overridePrice;
    public $exactPrice;
    public $orderIndex;
    public $priceMultiplier;
    public $deleted;
    public $uuid;
    
    public static function &findAll($designerId , \shirtplatform\filter\WsParameters $wsParameters = null , $shopId = null ,$isAsync=false )
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);
        $url = str_replace('{designerId}', $designerId, $url);
        if($wsParameters == null )
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->setRootEntityName('\shirtplatform\entity\order\Order');
        
        return self::findAllFromUrl($url, $wsParameters, $shopId, [] , $isAsync);
    }
    
    public static function &count($designerId , \shirtplatform\filter\WsParameters $wsParameters = null , $shopId = null ,$isAsync=false )
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);
        $url = str_replace('{designerId}', $designerId, $url);
        if($wsParameters == null )
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->setRootEntityName('\shirtplatform\entity\order\Order');
        $wsParameters->page = 0;
        $wsParameters->size = 1;
        
        $promise = new \shirtplatform\rest\promise\CountPromise('GET', $url, $wsParameters->buildParams(), array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        $promise->shopId = $shopId;
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $rest->_call($promise);
        return $promise->message;
    }
    
}
