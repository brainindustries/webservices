<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\order;

/**
 * 
 */
class DesignedOrderedProductAttribute extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orders/{parentId}/orderedProducts/{parentId}/customAttributes';
    const VAR_NAME = 'orderedProductCustomAttribute';

    public static $classMap = array(

    );
    
    public $name;
    public $value;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
}
