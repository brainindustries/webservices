<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnologyPriceColor
 *
 * @author Jan Maslik
 */
class PrintTechnologyPriceColor extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/price/assignedColors/{parentId}/colors';
    const VAR_NAME = 'printTechnologyPriceColor';

    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country',
    );
    public $version;
    public $country;
    public $flatSteps;
    public $productAmount;
    public $whiteOnly;
    public $price;
    public $stitchPrice;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
