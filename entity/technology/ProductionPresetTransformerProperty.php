<?php

namespace shirtplatform\entity\technology;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductionPresetTransformerProperty
 *
 * @author Jan Maslik
 */
class ProductionPresetTransformerProperty extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/production/presets/{parentId}/transformerProperties';
    const VAR_NAME = 'productionPresetTransformerProperty';

    public static $classMap = array(
    );
    public $name;
    public $value;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
