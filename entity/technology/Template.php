<?php

namespace shirtplatform\entity\technology;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Template
 *
 * @author Jan Maslik
 */
class Template extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/templates';
    const VAR_NAME = 'template';

    public static $classMap = array(
        'factories' => '\shirtplatform\entity\technology\TemplateToFactory'
    );
    public static $classArrayMap = array(
    );
    public $name;
    public $version;
    public $orderIndex;
    protected $factories;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get factories.
     * 
     * @return \shirtplatform\entity\technology\TemplateToFactory[]
     */
    public function getFactories()
    {
        return $this->getAtomLinkValue('factories');
    }

}
