<?php

namespace shirtplatform\entity\technology;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintTechnologyPriceConst
 * TODO -- add to testcases
 * @author Jan Maslik
 */
class PrintTechnologyPriceConst extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/price/consts';
    const VAR_NAME = 'printTechnologyPriceConst';

    public static $classMap = array(
    );
    public $version;
    public $name;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
