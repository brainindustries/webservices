<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnologyInputPreset
 *
 * @author Jan Maslik
 */
class PrintTechnologyInputPreset extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/inputPresets';
    const VAR_NAME = 'printTechnologyInputPreset';

    public $version;
    public $name;
    public $orderIndex;
    public $motiveCollection;
    public $text;
    public $qrcode;
    public $upload;
    public static $classMap = array(
        'motiveCollection' => '\shirtplatform\entity\technology\PrintTechnologyInputMotiveCollection',
        'text' => '\shirtplatform\entity\technology\PrintTechnologyInputText',
        'qrcode' => '\shirtplatform\entity\technology\PrintTechnologyInputQrcode',
        'upload' => '\shirtplatform\entity\technology\PrintTechnologyInputUpload',
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
