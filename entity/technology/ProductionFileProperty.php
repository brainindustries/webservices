<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of ProductionPageAddonProperty
 *
 * @author Jan Maslik
 */
class ProductionFileProperty extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/productions/{parentId}/fileAddonProperties';
    const VAR_NAME = 'productionFileProperty';

    public static $classMap = array(
    );
    public $name;
    public $value;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
