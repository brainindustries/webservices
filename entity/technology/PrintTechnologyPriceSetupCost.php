<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnologyPriceSetupCost
 *
 * @author Jan Maslik
 */
class PrintTechnologyPriceSetupCost extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/price/setupCosts';
    const VAR_NAME = 'printTechnologyPriceSetupCost';

    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country',
    );
    public $version;
    public $general;
    public $eachView;
    public $eachObject;
    public $eachColor;
    public $country;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
