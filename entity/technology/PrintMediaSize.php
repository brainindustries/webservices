<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintMediaSize
 *
 * @author Jan Maslik
 */
use shirtplatform\entity\enumerator\PrintMediaUnit;

class PrintMediaSize extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/mediaSizePalettes/{parentId}/mediaSizes';
    const VAR_NAME = 'printMediaSize';

    public static $classMap = array(
        'localizations' => '\shirtplatform\entity\localization\PrintMediaSizeLocalized'
    );
    public $name;
    public $width;
    public $length;
    public $version;
    public $displayUnit = PrintMediaUnit::MM;
    public $orderIndex;
    //layze
    protected $localizations = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    public function getLocalizations(\shirtplatform\filter\WsParameters $wsParams = null, $isAsync = false)
    {
        return $this->getAtomLinkValue('localizations', $wsParams, $isAsync);
    }

}
