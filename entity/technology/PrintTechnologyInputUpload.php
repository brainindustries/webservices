<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnologyInputUpload
 *
 * @author Jan Maslik
 */
class PrintTechnologyInputUpload extends \shirtplatform\entity\abstraction\JsonEntity
{
    //const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/motiveCollections';

    const VAR_NAME = 'printTechnologyInputUpload';

    public static $classMap = array(
    );
    public $version;
    public $active;
    public $transparencyEnabled;
    public $vectorEnabled;
    public $baseImageEnabled;
    public $dpiThresholdLevelLow;
    public $dpiThresholdLevelHigh;
    public $matchPrintQuality;
    public $matchPrintQualityLevel;
    public $showLowQualityWarning;
    public $allowOverrideLowQualityRestriction;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
