<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of ProductionPreset
 *
 * @author Jan Maslik
 */
class ProductionPreset extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/production/presets';
    const VAR_NAME = 'productionPreset';

    public $name;
    public $version;
    public $orderIndex;
    public $fileModule;
    public $displayModule;
    public $transformerModule;
    public $responseModule;
   /* protected $fileProperties;
    protected $displayProperties;
    protected $transformerProperties;
    protected $responseProperties;*/
    public static $classMap = array(
        'fileModule' => '\shirtplatform\entity\technology\ProductionFileModule',
        'displayModule' => '\shirtplatform\entity\technology\ProductionDisplayModule',
        'transformerModule' => '\shirtplatform\entity\technology\ProductionTransformerModule',
        'responseModule' => '\shirtplatform\entity\technology\ProductionResponseModule',
            /* 	'fileProperties' => '\shirtplatform\entity\technology\ProductionFileProperty',
              'displayProperties' => '\shirtplatform\entity\technology\ProductionDisplayProperty',
              'transformerProperties' => '\shirtplatform\entity\technology\ProductionTransformerProperty',
              'responseProperties' => '\shirtplatform\entity\technology\ProductionResponseProperty', */
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get file properties.
     * 
     * @return \shirtplatform\entity\technology\ProductionFileProperty[]
     */
    public function getFileProperties()
    {
        return $this->getAtomLinkValue('fileProperties');
    }

    /**
     * Get display properties.
     * 
     * @return \shirtplatform\entity\technology\ProductionDisplayProperty[]
     */
    public function getDisplayProperties()
    {
        return $this->getAtomLinkValue('displayProperties');
    }

    /**
     * Get transformer properties.
     * 
     * @return \shirtplatform\entity\technology\ProductionTransformerProperty[]
     */
    public function getTransformerProperties()
    {
        return $this->getAtomLinkValue('transformerProperties');
    }

    /**
     * Get response properties.
     * 
     * @return \shirtplatform\entity\technology\ProductionResponseProperty[]
     */
    public function getResponseProperties()
    {
        return $this->getAtomLinkValue('responseProperties');
    }

}
