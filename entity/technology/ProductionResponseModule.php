<?php

namespace shirtplatform\entity\technology;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductionResponseModule
 *
 * @author Jan Maslik
 */
class ProductionResponseModule extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'production/modules/response';
    const VAR_NAME = 'productionResponseModule';

    public static $classMap = array(
        'properties' => '\shirtplatform\entity\technology\ProductionResponseProperty'
    );
    public $name;
    public $version;
    protected $properties = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get properties.
     * 
     * @return \shirtplatform\entity\technology\ProductionResponseProperty[]
     */
    public function getProperties()
    {
        return $this->getAtomLinkValue('properties');
    }

}
