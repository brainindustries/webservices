<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnologyPriceText
 *
 * @author Jan Maslik
 */
class PrintTechnologyPriceText extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/price/texts';
    const VAR_NAME = 'printTechnologyPriceText';

    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country',
        'font' => '\shirtplatform\entity\account\Font'
    );
    public $version;
    public $country;
    public $font;
    public $whiteOnly;
    public $usagePrice = 0;
    public $characterTreshold;
    public $characterPrice = 0;
    public $active;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
