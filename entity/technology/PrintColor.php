<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintColor
 *
 * @author Jan Maslik
 */
use \shirtplatform\entity\enumerator\PrintColorSwfBlendingMode;
use \shirtplatform\rest\REST;
use \shirtplatform\filter\WsParameters;
use \shirtplatform\filter\Filter;
use \shirtplatform\parser\WsParse;
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\parser\WsModel;

class PrintColor extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printColorPalettes/{parentId}/printColors';
    const PATH_ALL_COLORS = 'accounts/{accountId}/printColors';
    const VAR_NAME = 'printColor';

    public static $classMap = array(
        'localizations' => '\shirtplatform\entity\localization\PrintColorLocalized'
    );
    public $name;
    public $version;
    public $deleted;
    public $rgb = '';
    public $rgbPrint = '';
    public $cmykPrint = '';
    public $orderIndex;
    public $bitmapId;
    public $previewId;
    public $iconId;
    public $textureMode = \shirtplatform\entity\enumerator\TextureMode::FILL;
    public $iconMode = \shirtplatform\entity\enumerator\TextureIconMode::CROP;
    public $overlayBlendingMode = PrintColorSwfBlendingMode::NORMAL;
    public $overlayAlpha;
    public $overlayId;
    //layze
    protected $localizations = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get localizations.
     * 
     * @return \shirtplatform\entity\localization\PrintColorLocalized[]
     */
    public function getLocalizations(WsParameters $wsParams = null, $isAsync = false)
    {
        return $this->getAtomLinkValue('localizations', $wsParams, $isAsync);
    }

    /**
     * Find all in account.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameter
     * @return PrintColor[]
     */
    public static function findAllByAccount(\shirtplatform\filter\WsParameters $wsParameter = null)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_ALL_COLORS);
        return self::findAllFromUrl($url, $wsParameter); //TODO set parents by url
    }

    /**
     * Get preview image url.
     * 
     * @return string
     */
    public function getPreviewUrl()
    {
        return $this->getAtomLink('preview');
    }

}
