<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of FactoryAssigned
 *
 * @author Jan Maslik
 */
class FactoryAssigned extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/factories/assigned';
    const VAR_NAME = 'factoryAssigned';

    public static $classMap = array(
        'factory' => '\shirtplatform\entity\technology\Factory',
    );
    public $version;
    public $orderIndex;
    public $active;
    public $default;
    public $factory;
    public $deleted;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get an factory.
     * 
     * @return \shirtplatform\entity\technology\Factory
     */
    public function getFactory()
    {
        return $this->factory;
    }

}
