<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnologyNecklabel
 *
 * @author admin
 */
class PrintTechnologyNecklabel extends \shirtplatform\entity\abstraction\BaseOneToOneDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/productions/{parentId}/necklabel';
    const VAR_NAME = 'printTechnologyNecklabel';
    
    public $fileSortTemplate;
    public $pageWidthMM;
    public $pageHeightMM;
    public $pageMarginMM;
    public $objectSpacingMM;
    public $pagesNotAllowed;
    public $version;
    
    public static $classMap = array(
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
}
