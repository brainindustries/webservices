<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnologyPriceLaborCost
 *
 * @author Jan Maslik
 */
class PrintTechnologyPriceLaborCost extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/price/laborCosts';
    const VAR_NAME = 'printTechnologyPriceLaborCost';

    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country',
    );
    public $version;
    public $general;
    public $eachView;
    public $eachObject;
    public $eachColor;
    public $country;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
