<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnology
 *
 * @author Jan Maslik
 */
use \User;
use \shirtplatform\rest\REST;
use \shirtplatform\filter\WsParameters;
use \shirtplatform\filter\Filter;
use \shirtplatform\parser\WsParse;
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\parser\WsModel;

class PrintTechnology extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology';
    const PATH_IMPORT = 'accounts/{accountId}/printTechnology/import/{importablePrintTechnologyId}';
    const PATH_DUPLICATE = 'accounts/{accountId}/printTechnology/{printTechnologyId}/duplicate';
    const PATH_ORDERED = 'accounts/{accountId}/production/technologies';
    const VAR_NAME = 'printTechnology';

    public static $classMap = array(
        'colorGroups' => '\shirtplatform\entity\technology\PrintTechnologyColorGroup',
        'assignedColors' => '\shirtplatform\entity\technology\PrintTechnologyAssignedColor',
        'assignedMediaSizes' => '\shirtplatform\entity\technology\PrintTechnologyAssignedMediaSize',
        'priceBitmaps' => '\shirtplatform\entity\technology\PrintTechnologyPriceBitmap',
        'priceTexts' => '\shirtplatform\entity\technology\PrintTechnologyPriceText',
        'priceLaborCosts' => '\shirtplatform\entity\technology\PrintTechnologyPriceLaborCost',
        'priceSetupCosts' => '\shirtplatform\entity\technology\PrintTechnologyPriceSetupCost',
        'inputs' => '\shirtplatform\entity\technology\PrintTechnologyInput',
        'localizations' => 'shirtplatform\entity\localization\PrintTechnologyLocalized',
        'productions' => '\shirtplatform\entity\technology\PrintTechnologyProduction'
    );
    public $version;
    public $name;
    public $active = true;
    public $priceFormula;
    public $motivePriceFormula;
    public $orderIndex;
    public $deleted;
    public $vectorEnabled = true;
    public $bitmapEnabled = true;
    public $occupiedViewsCount = false;
    public $summarySymbolsEnable = false;
    public $barcodeNaming;
    public $type = \shirtplatform\entity\enumerator\PrintTechnologyType::OTHER;
    protected $colorGroups = null;
    protected $assignedColors = null;
    protected $assignedMediaSizes = null;
    protected $priceBitmaps = null;
    protected $priceTexts = null;
    protected $priceLaborCosts = null;
    protected $priceSetupCosts = null;
    protected $inputs = null;
    protected $localizations = null;
    protected $productions = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get colors groups.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\PrintTechnologyColorGroup[]
     */
    public function getColorGroups($isAsync = false)
    {
        return $this->getAtomLinkValue('colorGroups', null, $isAsync);
    }

    /**
     * Get assigned colors.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\PrintTechnologyAssignedColor[]
     */
    public function getAssignedColors($isAsync = false)
    {
        return $this->getAtomLinkValue('assignedColors', null, $isAsync);
    }

    /**
     * Get media sizes.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\PrintTechnologyAssignedMediaSize[]
     */
    public function getAssignedMediaSizes($isAsync = false)
    {
        return $this->getAtomLinkValue('assignedMediaSizes', null, $isAsync);
    }

    /**
     * Get price bitmaps.
     * 
     * @return \shirtplatform\entity\technology\PrintTechnologyPriceBitmap[]
     */
    public function getPriceBitmaps()
    {
        return $this->getAtomLinkValue('priceBitmaps');
    }

    /**
     * Get price texts.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\PrintTechnologyPriceText[]
     */
    public function getPriceTexts($isAsync = false)
    {
        return $this->getAtomLinkValue('priceTexts', null, $isAsync);
    }

    /**
     * Get price labor costs.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\PrintTechnologyPriceLaborCost[]
     */
    public function getPriceLaborCosts($isAsync = false)
    {
        return $this->getAtomLinkValue('priceLaborCosts', null, $isAsync);
    }

    /**
     * Get price setup costs.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\PrintTechnologyPriceSetupCost[]
     */
    public function getPriceSetupCosts($isAsync = false)
    {
        return $this->getAtomLinkValue('priceSetupCosts', null, $isAsync);
    }

    /**
     * Get inputs.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\PrintTechnologyInput[]
     */
    public function getInputs($isAsync = false)
    {
        return $this->getAtomLinkValueItem('inputs', false, $isAsync);
    }

    /**
     * Get localizations.
     * 
     * @param boolean $isAsync
     * @return shirtplatform\entity\localization\PrintTechnologyLocalized[]
     */
    public function getLocalizations($isAsync = false)
    {
        return $this->getAtomLinkValue('localizations', null, $isAsync);
    }

    /**
     * Get productions.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\PrintTechnologyProduction[]
     */
    public function getProductions($isAsync = false)
    {
        return $this->getAtomLinkValueItem('productions', false, $isAsync);
    }

    /**
     * Import an print technology.
     * 
     * @param int $importablePrinttechnologyId
     * @return PrintTechnology
     */
    public static function import($importablePrinttechnologyId)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_IMPORT);
        $url = str_replace('{importablePrintTechnologyId}', $importablePrinttechnologyId, $url);

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [], false);

        return $rest->_call($promise);
    }

    /**
     * Find technology by ordered data.
     * 
     * @param array|null $orderedProductIds
     * @param array|null $orderIds
     * @param int $summaryId
     * @return PrintTechnology[]
     */
    public static function findByOrderedData($orderedProductIds = null, $orderIds = null, $summaryId = null)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_ORDERED);
        $params = new \shirtplatform\filter\WsParameters();

        if ($orderedProductIds != null)
        {
            $params->addQuery(array('products' => $orderedProductIds));
        }
        if ($orderIds != null)
        {
            $params->addQuery(array('orders' => $orderIds));
        }

        if ($summaryId != null)
        {
            $params->addQuery(array('summary' => $summaryId));
        }

        return self::findAllFromUrl($url, $params);
    }

    /**
     * Duplicate an technology.
     * 
     * @param int $printTechnologyId
     * @return PrintTechnology
     */
    public static function duplicate($printTechnologyId)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_DUPLICATE);
        $url = str_replace('{printTechnologyId}', $printTechnologyId, $url);

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [], false);

        return $rest->_call($promise);
    }

}
