<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of ProductionPresetResponseProperty
 *
 * @author Jan Maslik
 */
class ProductionPresetResponseProperty extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/production/presets/{parentId}/responseProperties';
    const VAR_NAME = 'productionPresetResponseProperty';

    public static $classMap = array(
    );
    public $name;
    public $value;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
