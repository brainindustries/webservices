<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of KornitSetup
 *
 * @author Jan Maslik
 */
class KornitPreset extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/kornit/presets';
    const VAR_NAME = 'kornitPreset';

    public static $classMap = array(
        'mediaName' => '\shirtplatform\entity\technology\KornitMediaName'
    );
    public $name;
    public $mediaName;
    public $sprayAmount;
    public $printSpeed;
    public $colorPrintMode;
    public $saturation;
    public $whitePrintMode;
    public $opacity;
    public $hilightWhite;
    public $hilightOpacity;
    public $direction;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
