<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnologyInputText
 *
 * @author Jan Maslik
 */
class PrintTechnologyInputText extends \shirtplatform\entity\abstraction\JsonEntity
{
    //const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/motiveCollections';

    const VAR_NAME = 'printTechnologyInputText';

    public static $classMap = array(
    );
    public $version;
    public $active;
    public $fontSizeLimit;
    public $minWidth;
    public $minHeight;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
