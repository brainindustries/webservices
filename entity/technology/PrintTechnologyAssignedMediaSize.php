<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnologyAssignedMediaSize
 *
 * @author Jan Maslik
 */
class PrintTechnologyAssignedMediaSize extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/assignedMediaSizes';
    const VAR_NAME = 'printTechnologyAssignedMediaSize';

    public $default;
    public $mediaSize;
    public $version;
    public $orderIndex;
    public static $classMap = array(
        'mediaSize' => '\shirtplatform\entity\technology\PrintMediaSize',
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
