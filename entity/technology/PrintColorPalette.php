<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintColorPalette
 *
 * @author Jan Maslik
 */
use \shirtplatform\entity\enumerator\PrintColorPaletteType;
use \User;
use \shirtplatform\rest\REST;
use \shirtplatform\filter\WsParameters;
use \shirtplatform\filter\Filter;
use \shirtplatform\parser\WsParse;
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\parser\WsModel;
use \shirtplatform\entity\technology\PrintColor;

class PrintColorPalette extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printColorPalettes';
    const PATH_MOVE = 'accounts/{accountId}/printColorPalettes/{paletteId}/movePrintColors/{newPaletteId}';
    const PATH_IMPORT = 'accounts/{accountId}/printColorPalettes/{paletteId}/import/{colorIdToImport}';
    const VAR_NAME = 'printColorPalette';

    public static $classMap = array(
    );
    public $name;
    public $static;
    public $default;
    public $version;
    public $type = PrintColorPaletteType::LIST_;
    public $orderIndex;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Move colors from one pool to another.
     * 
     * @param int $newPoolId
     * @param Filter|WsParameters|null $filter
     */
    public function moveColors($newPaletteId, $filter = null)
    {
        $entityAsArray = $this->getWebserviceAttributes();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_MOVE);

        foreach ($this->getParents() as $parent)
        {
            $url = preg_replace('/{parentId}/', $parent, $url, 1);
        }

        $url = str_replace('{paletteId}', $this->id, $url);
        $url = str_replace('{newPaletteId}', $newPaletteId, $url);

        if ($filter instanceof Filter || $filter == NULL)
        {
            $w = new \shirtplatform\filter\WsParameters();
            $w->setFilter($filter);
        }
        elseif($filter instanceof WsParameters)
        {
            $w = $filter;
        }
        $w->setRootEntityName('\shirtplatform\entity\technology\PrintColor');

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, '\shirtplatform\entity\technology\PrintColor', $w->buildParams(), array(self::VAR_NAME => $entityAsArray), false);
        $promise->filterPromise = $w->getPromise();

        return $rest->_call($promise);
    }

    /**
     * imports print color into pool. Possible to import print color if you are owner or shared print colors.
     * 
     * @param int $colorIdToImport
     * @return \PrintColor
     */
    public function __import($colorIdToImport)
    {
        return self::import($this->id, $colorIdToImport);
    }

    /**
     * Import print color.
     * 
     * @param int $paletteId
     * @param int $colorIdToImport
     * @return \PrintColor
     */
    public static function import($paletteId, $colorIdToImport)
    {
        $rest = \shirtplatform\rest\REST::getInstance();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_IMPORT);
        $url = str_replace('{paletteId}', $paletteId, $url);
        $url = str_replace('{colorIdToImport}', $colorIdToImport, $url);

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, '\shirtplatform\entity\technology\PrintColor', [], [], false);

        return $rest->_call($promise);
    }

}
