<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintMediaSizePalette
 *
 * @author Jan Maslik
 */
use \User;
use \shirtplatform\rest\REST;
use \shirtplatform\filter\WsParameters;
use \shirtplatform\filter\Filter;
use \shirtplatform\parser\WsParse;
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\parser\WsModel;
use shirtplatform\entity\technology\PrintMediaSize;

class PrintMediaSizePalette extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/mediaSizePalettes';
    const PATH_MOVE = 'accounts/{accountId}/mediaSizePalettes/{paletteId}/moveMediaSizes/{newPaletteId}';
    const PATH_IMPORT = 'accounts/{accountId}/mediaSizePalettes/{paletteId}/import/{mediaSizeIdToImport}';
    const VAR_NAME = 'printMediaSizePalette';

    public static $classMap = array(
    );
    public $name;
    public $static;
    public $default;
    public $version;
    public $orderIndex;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Move media sizes from one palette to another.
     * 
     * @param int $newPaletteId
     * @param Filter|null $filter
     */
    public function __moveMediaSizes($newPaletteId, Filter $filter = null)
    {
        return self::moveMediaSizes($this->id, $newPaletteId, $filter);
    }

    /**
     * Import print media size.
     * 
     * @param int $mediaSizeIdToImport
     * @return PrintMediaSize
     */
    public function __import($mediaSizeIdToImport)
    {
        return self::import($this->id, $mediaSizeIdToImport);
    }

    /**
     * Move media sizes from one palette to another.
     * 
     * @param int $srcPaletteId
     * @param int $dstPaletteId
     * @param Filter|WsParameters|null $filter
     */
    public static function moveMediaSizes($srcPaletteId, $dstPaletteId, $filter = null)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_MOVE);
        $url = str_replace('{paletteId}', $srcPaletteId, $url);
        $url = str_replace('{newPaletteId}', $dstPaletteId, $url);


        if ($filter instanceof Filter || $filter == NULL)
        {
            $w = new \shirtplatform\filter\WsParameters();
            $w->setFilter($filter);
        }
        elseif($filter instanceof WsParameters)
        {
            $w = $filter;
        }
        $w->setRootEntityName('\shirtplatform\entity\technology\PrintMediaSize');

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, '\shirtplatform\entity\technology\PrintMediaSize', $w->buildParams(), [], false);
        $promise->filterPromise = $w->getPromise();

        return $rest->_call($promise);
    }

    /**
     * Import print media size.
     * 
     * @param int $paletteId
     * @param int $mediaSizeIdToImport
     * @return PrintMediaSize
     */
    public static function import($paletteId, $mediaSizeIdToImport)
    {
        $rest = \shirtplatform\rest\REST::getInstance();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_IMPORT);
        $url = str_replace('{paletteId}', $paletteId, $url);
        $url = str_replace('{mediaSizeIdToImport}', $mediaSizeIdToImport, $url);

        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, '\shirtplatform\entity\technology\PrintMediaSize', [], [], false);

        return $rest->_call($promise);
    }

}
