<?php

namespace shirtplatform\entity\technology;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductionResponseAddonProperty
 *
 * @author Jan Maslik
 */
class ProductionResponseProperty extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/productions/{parentId}/responseAddonProperties';
    const VAR_NAME = 'productionResponseProperty';

    public static $classMap = array(
    );
    public $name;
    public $value;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
