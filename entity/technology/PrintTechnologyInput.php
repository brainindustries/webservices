<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Input data for print technology.
 * primary key is key of print technology (PrintTechnology->id == PrintTechnologyInput->id )
 * 
 * POST/DELETE NOT allowed
 * delete input attributes is NOT allowed
 * update input attributes is allowed
 * 
 * @author Jan Maslik
 */
class PrintTechnologyInput extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/inputs';
    const VAR_NAME = 'printTechnologyInput';

    public $version;
    public $orderIndex;
    public $motiveCollection;
    public $text;
    public $qrcode;
    public $upload;
    public static $classMap = array(
        'motiveCollection' => '\shirtplatform\entity\technology\PrintTechnologyInputMotiveCollection',
        'text' => '\shirtplatform\entity\technology\PrintTechnologyInputText',
        'qrcode' => '\shirtplatform\entity\technology\PrintTechnologyInputQrcode',
        'upload' => '\shirtplatform\entity\technology\PrintTechnologyInputUpload',
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}

