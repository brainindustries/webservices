<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnologyColorGroup
 *
 * @author Jan Maslik
 */
class PrintTechnologyColorGroup extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/colorGroups';
    const VAR_NAME = 'printTechnologyColorGroup';

    public $name = '';
    public $version;
    public $active;
    public $default;
    public $orderIndex;
    public static $classMap = array(
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
