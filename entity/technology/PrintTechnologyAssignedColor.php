<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of PrintTechnologyAssignedColor
 *
 * @author Jan Maslik
 */
class PrintTechnologyAssignedColor extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/assignedColors';
    const VAR_NAME = 'printTechnologyAssignedColor';

    public $active;
    public $textEnabled;
    public $qrcodeEnabled;
    public $motiveEnabled;
    public $printColor;
    public $colorGroup;
    public $orderIndex;
    public $version;
    public static $classMap = array(
        'colorGroup' => '\shirtplatform\entity\technology\PrintTechnologyColorGroup',
        'printColor' => '\shirtplatform\entity\technology\PrintColor',
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
