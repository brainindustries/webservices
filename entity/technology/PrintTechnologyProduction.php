<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Production data for print technology.
 * primary key is key of print technology (PrintTechnology->id == PrintTechnologyProduction->id )
 * 
 * POST/DELETE NOT allowed
 * delete production attributes is NOT allowed
 * update production attributes is allowed
 *
 * @author Jan Maslik
 */
class PrintTechnologyProduction extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/productions';
    const VAR_NAME = 'printTechnologyProduction';

    public $version;
    public $orderIndex;
    public $fileModule;
    public $displayModule;
    public $transformerModule;
    public $responseModule;
    protected $fileAddonProperties;
    protected $displayAddonProperties;
    protected $transformerAddonProperties;
    protected $responseAddonProperties;
    public static $classMap = array(
        'fileModule' => '\shirtplatform\entity\technology\ProductionFileModule',
        'displayModule' => '\shirtplatform\entity\technology\ProductionDisplayModule',
        'transformerModule' => '\shirtplatform\entity\technology\ProductionTransformerModule',
        'responseModule' => '\shirtplatform\entity\technology\ProductionResponseModule',
        'fileAddonProperties' => '\shirtplatform\entity\technology\ProductionFileProperty',
        'displayAddonProperties' => '\shirtplatform\entity\technology\ProductionDisplayProperty',
        'transformerAddonProperties' => '\shirtplatform\entity\technology\ProductionTransformerProperty',
        'responseAddonProperties' => '\shirtplatform\entity\technology\ProductionResponseProperty',
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get file properties.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\ProductionFileProperty[]
     */
    public function getFileProperties($isAsync = false)
    {
        return $this->getAtomLinkValue('fileAddonProperties', null, $isAsync);
    }

    /**
     * Get display properties.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\ProductionDisplayProperty[]
     */
    public function getDisplayProperties($isAsync = false)
    {
        return $this->getAtomLinkValue('displayAddonProperties', null, $isAsync);
    }

    /**
     * Get transformer properties.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\ProductionTransformerProperty[]
     */
    public function getTransformerProperties($isAsync = false)
    {
        return $this->getAtomLinkValue('transformerAddonProperties', null, $isAsync);
    }

    /**
     * Get response properties.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\technology\ProductionResponseProperty[]
     */
    public function getResponseProperties($isAsync = false)
    {
        return $this->getAtomLinkValue('responseAddonProperties', null, $isAsync);
    }

}
