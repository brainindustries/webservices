<?php

namespace shirtplatform\entity\technology;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TemplateSvgLayer
 *
 * @author Jan Maslik
 */
class TemplateSvgLayer extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'root/templateSvg/{parentId}/layers';
    const VAR_NAME = 'templateSvgLayer';

    public static $classMap = array(
        'rect' => '\shirtplatform\entity\product\AreaGeomRect',
        'elipse' => '\shirtplatform\entity\product\AreaGeomElipse',
        'spline' => '\shirtplatform\entity\product\AreaGeomSpline',
    );
    public static $classArrayMap = array(
    );
    public $rect;
    public $elipse;
    public $spline;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
