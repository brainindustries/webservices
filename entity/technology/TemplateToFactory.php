<?php

namespace shirtplatform\entity\technology;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TemplateToFactory
 *
 * @author Jan Maslik
 */
class TemplateToFactory extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/templates/{parentId}/factories';
    const VAR_NAME = 'templateToFactory';

    public static $classMap = array(
        'factoryAssigned' => '\shirtplatform\entity\technology\FactoryAssigned',
        'svg' => '\shirtplatform\entity\technology\TemplateSvg',
        'layers' => '\shirtplatform\entity\technology\TemplateSvgLayer'
    );
    public static $classArrayMap = array(
    );
    public $svg;
    public $factoryAssigned;
    protected $layers;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get image preview url.
     * 
     * @return string
     */
    public function getPreview()
    {
        return $this->getAtomLink('preview');
    }

    /**
     * Get layers.
     * 
     * @return \shirtplatform\entity\technology\TemplateSvgLayer[]
     */
    public function getLayers()
    {
        return $this->getAtomLinkValue('layers');
    }

    public static function &getLayerGeom($templateId, $id, $layerName = null , $isAsync = false)
    {
        $url = self::getUrl([$templateId]);
        $url .= '/'.$id.'/geom';
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $query = [];
        
        if($layerName != null )
        {
               $query['layerName'] =  $layerName; 
        }
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, "\shirtplatform\\entity\abstraction\StringDataStructure", $query, [], $isAsync);
        return $rest->_call($promise);
    }
    
}
