<?php

namespace shirtplatform\entity\technology;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TemplateSvg
 *
 * @author Jan Maslik
 */
class TemplateSvg extends \shirtplatform\entity\abstraction\JsonEntity
{

    const VAR_NAME = 'templateSvg';

    public static $classMap = array(
    );
    public static $classArrayMap = array(
    );
    public $fileName;
    public $fileSize;
    public $baseWidthTwips;
    public $baseHeightTwips;
    protected $factories;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
