<?php

namespace shirtplatform\entity\technology;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductionTransformerModule
 *
 * @author Jan Maslik
 */
class ProductionTransformerModule extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'production/modules/transformer';
    const VAR_NAME = 'productionTransformerModule';

    public static $classMap = array(
        'properties' => '\shirtplatform\entity\technology\ProductionTransformerProperty'
    );
    public $name;
    public $version;
    protected $properties = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get properties.
     * 
     * @return \shirtplatform\entity\technology\ProductionTransformerProperty[]
     */
    public function getProperties()
    {
        return $this->getAtomLinkValue('properties');
    }

}
