<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of Factory
 *
 * @author Jan Maslik
 */
class Factory extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/factories';
    const VAR_NAME = 'factory';

    public static $classMap = array(
    );
    public $name;
    public $version;
    public $publicFactory;
    public $ownerAccountId;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
