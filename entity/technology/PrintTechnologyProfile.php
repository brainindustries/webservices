<?php
namespace shirtplatform\entity\technology;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintTechnologyProfile
 *
 * @author Jan Maslik
 */
class PrintTechnologyProfile  extends \shirtplatform\entity\abstraction\BaseOneToOneDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/profile';
    const VAR_NAME = 'printTechnologyProfile';
    
    public $enabled;
    public $advanced;
    public $intent = \shirtplatform\entity\enumerator\PrintProfileIntent::RELATIVE;
    public $blackPointCompensation;
    public $version;
    /**
     * Must be set ,otherwise it will be removed in update
     */
    public $whiteProfileId;

    /**
     * Must be set ,otherwise it will be removed in update
     */
    public $colourProfileId;
    
    public static $classMap = array(
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
}
