<?php

namespace shirtplatform\entity\technology;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintTechnologyPriceConstValue
 *
 * @author Jan Maslik
 */
class PrintTechnologyPriceConstValue extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/price/consts/{parentId}/values';
    const VAR_NAME = 'printTechnologyPriceConstValue';

    public static $classMap = array(
    );
    public $version;
    public $productAmount;
    public $value;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
