<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of ProductionPageModule
 *
 * @author Jan Maslik
 */
class ProductionFileModule extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'production/modules/file';
    const VAR_NAME = 'productionFileModule';

    public static $classMap = array(
        'properties' => '\shirtplatform\entity\technology\ProductionFileProperty'
    );
    public $name;
    public $version;
    protected $properties = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    public function getProperties()
    {
        return $this->getAtomLinkValue('properties');
    }

}
