<?php
namespace shirtplatform\entity\technology;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintTechnologyPreviewBlendMode
 *
 * @author Jan Maslik
 */
class PrintTechnologyPreviewBlendMode extends \shirtplatform\entity\abstraction\BaseOneToOneDao
{
    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/blendMode';
    const VAR_NAME = 'printTechnologyPreviewBlendMode';
    
    /**
     *
     * @var shirtplatform\entity\enumerator\BlendMode blendMode
     */
    public $blendMode; 
    public $opacity;
    public $version;
    
    
    public static $classMap = array(
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
}
