<?php
namespace shirtplatform\entity\technology;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintTechnologyPreviewDropShadowDark
 *
 * @author Jan Maslik
 */
class PrintTechnologyPreviewDropShadow extends \shirtplatform\entity\abstraction\BaseOneToOneDao
{
    const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/dropShadow';
    const VAR_NAME = 'printTechnologyPreviewDropShadow';

    public $distance;
    public $angle;
    public $color;
    public $opacity;
    public $size;
    public $version;
    
    public static $classMap = array(
    );

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
    
   	
    
}
