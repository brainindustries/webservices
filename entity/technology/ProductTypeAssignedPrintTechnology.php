<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\technology;

/**
 * Description of ProductTypeAssignedPrintTechnology
 *
 * @author Jan Maslik
 */
class ProductTypeAssignedPrintTechnology extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/assignedPrintTechnologies';
    const VAR_NAME = 'productTypeAssignedPrintTechnology';

    public static $classMap = array(
        'printTechnology' => '\shirtplatform\entity\technology\PrintTechnology'
    );
    public $default;
    public $version;
    public $active;
    public $printTechnology;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
