<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\localization;
/**
 * Description of ProductSizeLocalized
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class ProductSizeLocalized extends \shirtplatform\entity\abstraction\BaseWithParentDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/sizePools/{parentId}/sizes/{parentId}/localizations';
	const VAR_NAME = 'productSizeLocalized';

	public $name='';
	public $version;
	public $language;
	public $altName='';
	public $width='';
	public $height='';
	public $length='';
	public $units='';
	
	public static $classMap = array(
		'language' => '\shirtplatform\entity\account\Language',
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
       
}

?>