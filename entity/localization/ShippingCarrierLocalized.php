<?php

namespace shirtplatform\entity\localization;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShippingCarrierLocalized
 *
 * @author admin
 */
class ShippingCarrierLocalized extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shippingModules/{parentId}/carriers/{parentId}/localizations';
    const VAR_NAME = 'carrierLocalized';
    
    public static $classMap = array(
        'language' => '\shirtplatform\entity\account\Language'
    );
    
    public $name;
    public $description;
    public $language;
    public $version;
    
}
