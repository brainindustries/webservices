<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\localization;
/**
 * Description of ProductTypeAttributeListLocalized
 *
 * @author Jan Maslik
 */
class ProductTypeAttributeListLocalized extends \shirtplatform\entity\abstraction\BaseWithParentDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/attributeLists/{parentId}/localizations';
	const VAR_NAME = 'attributeListLocalized';

	public $name='';
	public $itemName='';
	public $version;
	public $language;
	
	public static $classMap = array(
		'language' => '\shirtplatform\entity\account\Language',
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
       
}

?>
