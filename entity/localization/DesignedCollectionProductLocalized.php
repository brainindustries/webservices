<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\localization;
/**
 * Description of DesignedCollectionProductLocalized
 *
 * @author Jan Maslik
 */
class DesignedCollectionProductLocalized extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collections/{parentId}/templateProducts/{parentId}/localizations';
	const VAR_NAME = 'designedTemplateProductLocalized';

	public $name='';
	public $description='';
	public $version;
	
	public $language;
	
	public static $classMap = array(
		'language' => '\shirtplatform\entity\account\Language',
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
      
}
