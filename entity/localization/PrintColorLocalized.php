<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\localization;
/**
 * Description of PrintColorLocalized
 *
 * @author Jan Maslik
 */
class PrintColorLocalized extends \shirtplatform\entity\abstraction\BaseWithParentDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/printColorPalettes/{parentId}/printColors/{parentId}/localizations';
	const VAR_NAME = 'printColorLocalized';

	public $name = '';
	public $version;
	
	public $language;
	
	public static $classMap = array(
		'language' => '\shirtplatform\entity\account\Language',
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
      
}

