<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\localization;
/**
 * Description of ProductTypeAttributeListItemLocalized
 *
 * @author Jan Maslik
 */
class ProductTypeAttributeListItemLocalized extends \shirtplatform\entity\abstraction\BaseWithParentDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/attributeLists/{parentId}/listItems/{parentId}/localizations';
	const VAR_NAME = 'attributeListItemLocalized';

	public $name='';
    public $url='';
	public $version;
	public $language;
	public $pictureId;

	public static $classMap = array(
		'language' => '\shirtplatform\entity\account\Language',
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
       
}

?>
