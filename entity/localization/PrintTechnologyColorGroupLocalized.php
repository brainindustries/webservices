<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\localization;
/**
 * Description of PrinttechnologyColorGroupLocalized
 *
 * @author Jan Maslik
 */
class PrintTechnologyColorGroupLocalized extends \shirtplatform\entity\abstraction\BaseWithParentDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/printTechnology/{parentId}/colorGroups/{parentId}/localizations';
	const VAR_NAME = 'printTechnologyColorGroupLocalized';

	public $name = '';
	public $description = '';
	public $version;
	
	public $language;
	
	public static $classMap = array(
		'language' => '\shirtplatform\entity\account\Language',
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
      
}
