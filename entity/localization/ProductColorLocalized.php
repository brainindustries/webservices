<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\localization;
/**
 * Description of ProductColorLocalized
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class ProductColorLocalized extends \shirtplatform\entity\abstraction\BaseWithParentDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/colorPools/{parentId}/colors/{parentId}/localizations';
	const VAR_NAME = 'productColorLocalized';

	public $name = '';
	public $version;
	
	public $language;
	
	public static $classMap = array(
		'language' => '\shirtplatform\entity\account\Language',
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
      
}

?>