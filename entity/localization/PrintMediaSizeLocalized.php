<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace shirtplatform\entity\localization;
/**
 * Description of PrintMediaSizeLocalized
 *
 * @author Jan Maslik
 */
class PrintMediaSizeLocalized extends \shirtplatform\entity\abstraction\BaseWithParentDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/mediaSizePalettes/{parentId}/mediaSizes/{parentId}/localizations';
	const VAR_NAME = 'printMediaSizeLocalized';

	public $name = '';
	public $version;
	
	public $language;
	
	public static $classMap = array(
		'language' => '\shirtplatform\entity\account\Language',
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
      
}