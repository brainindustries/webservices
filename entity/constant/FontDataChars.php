<?php

namespace shirtplatform\entity\constant;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FontDataSupportedChars
 *
 * @author Jan Maslik
 */
class FontDataChars extends \shirtplatform\entity\abstraction\JsonEntity
{

    const PATH_TEMPLATE = 'constants/fontData/{parentId}/characters';
    const VAR_NAME = 'fontDataChars';

    public static $classMap = array(
    );
    public $fontDataId;
    public $chars;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get supported font characters.
     * 
     * @param int $fontDataId
     * @param boolean $isAsync
     * @return string
     */
    public static function &find($fontDataId , $isAsync = false )
    {
        $url = str_replace('{parentId}', $fontDataId, self::PATH_TEMPLATE);

        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), [], [], $isAsync);

        $promise->message->fontDataId = $fontDataId;

        return $rest->_call($promise);
        
    }

}
