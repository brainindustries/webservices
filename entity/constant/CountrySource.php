<?php
namespace shirtplatform\entity\constant;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CountrySource
 *
 * @author Jan Maslik
 */
class CountrySource extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'constants/countrySources';
	const VAR_NAME = 'countrySource';

	public static $classMap = array(
	);
	
	public $code;
	public $name;

	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
