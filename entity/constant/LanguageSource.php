<?php
namespace shirtplatform\entity\constant;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LanguageSource
 *
 * @author Jan Maslik
 */
class LanguageSource extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'constants/languageSources';
	const VAR_NAME = 'languageSource';

	public static $classMap = array(
	);
	
	public $code;
	public $name;

	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
