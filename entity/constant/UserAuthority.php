<?php
namespace shirtplatform\entity\constant;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserAuthority
 *
 * @author Jan Maslik
 */
class UserAuthority extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'authorities';
	const VAR_NAME = 'authority';

	
	public static $classMap = array(
	);
	
	public $name;
	public $version;

	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}

}
