<?php
namespace shirtplatform\entity\constant;
/**
 * Description of BusinessEntity
 *
 * @author Jan Maslik
 */
class BusinessEntity extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'constants/countrySources/{parentId}/businessEntities';
	const VAR_NAME = 'businessEntity';


	
	public static $classMap = array(
		'countrySource' => 'shirtplatform\entity\constant\CountrySource'
	);
	
	public $name;
	public $countrySource;

	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}