<?php
namespace shirtplatform\entity\constant;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FontPublic
 *
 * @author Jan Maslik
 */
class FontPublic extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'constants/fontPublicLibraries/{parentId}/fontPublics';
	const VAR_NAME = 'fontPublic';

	public static $classMap = array(
		'chars' => 'FontDataChars',
	);
	
	public $name;
	public $className;
	public $defSize;
	public $minSize;
	public $maxSize;
	public $fontDataId;
	public $orderIndex;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
        /**
         * Get supported font characters.
         * 
         * @return string
         */
	public function getSupportedChars()
	{
		return $this->getAtomLinkValue('chars');
	}
}