<?php
namespace shirtplatform\entity\constant;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CreatorThemes
 *
 * @author Jan Maslik
 */
class CreatorTheme  extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'constants/creatorThemes';
	const VAR_NAME = 'creatorTheme';

	public static $classMap = array(
	);
	
	public $name;
        public $mobile;

	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
