<?php
namespace shirtplatform\entity\constant;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StateSource
 *
 * @author admin
 */
class StateSource extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'constants/stateSources';
	const VAR_NAME = 'stateSource';

	public static $classMap = array(
            'country' => '\shirtplatform\entity\constant\CountrySource',
	);
	
	public $code;
	public $name;
        public $country;

	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
