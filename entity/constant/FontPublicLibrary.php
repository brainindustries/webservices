<?php
namespace shirtplatform\entity\constant;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FontPublicLibrary
 *
 * @author Jan Maslik
 */
class FontPublicLibrary extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'constants/fontPublicLibraries';
	const VAR_NAME = 'fontPublicLibrary';

	public static $classMap = array(
	);
	
	public $name;
	public $orderIndex;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
