<?php

namespace shirtplatform\entity\pimp;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use shirtplatform\entity\abstraction\BaseDao;

/**
 * Description of PimpBasePrice
 *
 * @author Slavomír Mikolaj
 */
class PimpBasePrice extends BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/pimp/basePrices';
    const VAR_NAME = 'pimpBasePrice';

    public $actionTypeId;
    public $price;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
