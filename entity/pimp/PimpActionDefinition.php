<?php

namespace shirtplatform\entity\pimp;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use shirtplatform\entity\abstraction\BaseDao;

/**
 * Description of PimpActionDefinition
 *
 * @author Slavomír Mikolaj
 */
class PimpActionDefinition extends BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/pimp/availableEffects';
    const VAR_NAME = 'pimpActionDefinition';

    public $actionType;
    public $icon;
    public $title;
    public $name;
    public $pngPreview;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
