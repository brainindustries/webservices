<?php

namespace shirtplatform\entity\pimp;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PimpActionGroupPrice
 *
 * @author Jan Maslik
 */
class PimpActionGroupPrice extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/pimp/pimpActionGroup/{parentId}/price';
    const VAR_NAME = 'pimpActionGroupPrice';

    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country',
    );
    public $version;
    public $price;
    public $country;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
