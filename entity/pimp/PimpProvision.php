<?php
namespace shirtplatform\entity\pimp;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PimpProvision
 *
 * @author Jan Maslik
 */
class PimpProvision extends \shirtplatform\entity\abstraction\JsonEntity
{
    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/orders/pimpProvision';
    const VAR_NAME = 'pimpProvision';

    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country',
    );
    public $uniqueId;
    public $deleted = false;
    public $created;
    public $financialStatus;
    public $country;
    public $version;
    public $fulfilmentStatus;
    public $designerId;
    public $provision;
    
    public static function &findAll( \shirtplatform\filter\WsParameters $wsParameters = null , $shopId = null ,$isAsync=false )
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);
        if($wsParameters == null )
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->setRootEntityName('\shirtplatform\entity\order\Order');
        
        return self::findAllFromUrl($url, $wsParameters, $shopId, [] , $isAsync);
    }
    
    public static function &count( \shirtplatform\filter\WsParameters $wsParameters = null , $shopId = null ,$isAsync=false )
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);

        if($wsParameters == null )
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->setRootEntityName('\shirtplatform\entity\order\Order');
        $wsParameters->page = 0;
        $wsParameters->size = 1;
        
        $promise = new \shirtplatform\rest\promise\CountPromise('GET', $url, $wsParameters->buildParams(), array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
        $promise->shopId = $shopId;
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $rest->_call($promise);
        return $promise->message;
    }
    
}