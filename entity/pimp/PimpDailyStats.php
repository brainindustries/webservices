<?php

namespace shirtplatform\entity\pimp;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PimpDailyStatsPrime
 *
 * @author Jan Maslik
 */
class PimpDailyStats extends \shirtplatform\entity\abstraction\JsonEntity
{

    const PATH_TEMPLATE = 'accounts/{accountId}/pimp/stats';
    const VAR_NAME = 'pimpDailyStats';

    public $date;
    public $fonts;
    public $fontsall;
    public $fontscount;
    public $backgrounds;
    public $backgroundsall;
    public $backgroundscount;
    public $frames;
    public $framesall;
    public $framescount;
    public $templates;
    public $templatesall;
    public $templatescount;
    public $wordcloud;
    public $wordcloudall;
    public $wordcloudcount;
    public $embroidery;
    public $embroideryall;
    public $embroiderycount;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get an primary key.
     * 
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->date;
    }

    /**
     * Find all stats.
     * 
     * @param string $from
     * @param string $to
     * @param boolean $isAsync
     * @return PimpDailyStats[]
     */
    public static function &findAll($from, $to, $isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE);
        $query = [
            'dFrom' => $from,
            'dTo' => $to
        ];
        $promise = new \shirtplatform\rest\promise\PagePromise('GET', $url, get_called_class(), $query, [], $isAsync);

        return $rest->_call($promise);
    }

}
