<?php

namespace shirtplatform\entity\pimp;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use shirtplatform\entity\abstraction\BaseDao;

/**
 * Description of PimpActionGroup
 *
 * @author Slavomír Mikolaj
 */
class PimpActionGroup extends BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/pimp/pimpActionGroup';
    const VAR_NAME = 'pimpActionGroup';

    public $version;
    public $pimpTypeId;
    public $enabled;
    public $basePrice;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
