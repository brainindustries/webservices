<?php

namespace shirtplatform\entity\pimp;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PimpAdmin
 *
 * @author Jan Maslik
 */
class PimpAdmin
{

    const PATH_TEMPLATE = 'accounts/{accountId}/pimp';

    /**
     * Create an Pimp user.
     * 
     * @param boolean $isAsync
     */
    public static function createPimpUser($isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE);

        $promise = new \shirtplatform\rest\promise\JsonPromise('POST', $url, [], [], $isAsync);
        $rest->_call($promise);
    }

    /**
     * Get invoice price.
     * 
     * @param string $from
     * @param string $to
     * @param boolean $isAsync
     * @return int
     */
    public static function &getInvoicePrice($from, $to, $isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE);
        $url .='/invoicePrice';

        $query = [
            'dFrom' => $from,
            'dTo' => $to
        ];

        $promise = new \shirtplatform\rest\promise\JsonPromise('GET', $url, $query, [], $isAsync);
        return $rest->_call($promise);
    }

}
