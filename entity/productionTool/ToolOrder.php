<?php
namespace shirtplatform\entity\productionTool;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ToolOrder
 *
 * @author Jan Maslik
 */
class ToolOrder extends \shirtplatform\entity\abstraction\JsonEntity
{
    const VAR_NAME = 'toolOrder';
    public static $classMap = array(
        'reservedWarehouse' => '\shirtplatform\entity\productionTool\ToolWarehouse',
        'senderAddress' => '\shirtplatform\entity\productionTool\ToolAddress',
        'senderAddress' => '\shirtplatform\entity\productionTool\ToolAddress',
        'shipping' => '\shirtplatform\entity\productionTool\ToolShippingSetup',
    );
    
    /**
     * On insert filled automatically
     */
    public $status;
    /**
     * On insert platform product filled automatically
     */
    public $publicUniqueId;
    /**
     * On insert filled automatically
     */
    public $producable;
    
    public $version;
    public $reservedWarehouse;
    
    public $senderAddress;
    public $receiverAddress;
    public $shipping;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
}
