<?php
namespace shirtplatform\entity\productionTool;


class ToolOrderFlag extends \shirtplatform\entity\abstraction\JsonEntity
{
    const PATH_TEMPLATE = 'productionTool/accounts/{accountId}/flags';
    
    const VAR_NAME = 'toolOrderFlag';
    public static $classMap = array(
    );
    
    public $name;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
    
         /**
     * Find multiple entities.
     * 
     * @param \shirtplatform\filter\WsParameters|int $wsParameters
     * @param int $shopId
     * @param boolean $isAsync
     * @return ShopOwnedDao[]
     */
    public static function &findAll(\shirtplatform\filter\WsParameters $wsParameters = null, $factoryAccountId = 0, $isAsync = false)
    {
        $entityName = get_called_class();

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $query = $wsParameters->buildParams();
        $query['factoryAccountId'] = $factoryAccountId;
        
        $promise = new \shirtplatform\rest\promise\PagePromise('GET', self::getUrl(), $entityName, $query , array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }
    
    public static function getUrl()
    {
        $className = get_called_class();
        return  str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);
    }
    
}

