<?php

namespace shirtplatform\entity\productionTool;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ToolShippingProvider
 *
 * @author admin
 */
class ToolShippingProvider extends \shirtplatform\entity\abstraction\JsonEntity
{
    
    public static $classMap = array(
        'moduleType' => '\shirtplatform\entity\productionTool\ToolShippingModule',
    );

    public $name;
    public $moduleType;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
}
