<?php
namespace shirtplatform\entity\productionTool;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ToolShippingSetup
 *
 * @author Jan Maslik
 */
class ToolShippingSetup  extends \shirtplatform\entity\abstraction\JsonEntity
{
    const VAR_NAME = 'toolShippingSetup';
    public static $classMap = array(
        'carrier' => '\shirtplatform\entity\productionTool\ToolShippingCarrier',
    );
    /**
     * Id of the provider. Log into production tool and check Shipping providers in configuration part
     * @var type 
     */
    public $shippingProviderId ;
    /**
     * Service type of provider. Like standard , express
     * @var type 
     */
    public $providerServiceType;
    /**
     * Price of the order
     * @var type 
     */
    public $price;
    /**
     * Cash on delivery price
     * @var type 
     */
    public $codPrice;
    public $currency;
    public $preferedDeliveryTime;
    public $carrier;


    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
}
