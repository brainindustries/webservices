<?php

namespace shirtplatform\entity\productionTool;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FactoryOrder
 *
 * @author Jan Maslik
 */
class ToolPlatformOrder extends \shirtplatform\entity\abstraction\JsonEntity
{
    const PATH_TEMPLATE = 'productionTool/accounts/{accountId}/shops/{shopId}/orders';
    const VAR_NAME = 'toolPlatformOrder';

    public static $classMap = array(
        'factoryOrder' => '\shirtplatform\entity\productionTool\ToolOrder',
    );

    public $factoryOrder;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
    
     private $dao_shopId = null;

    /**
     * Get entity shop Id.
     * 
     * @return int
     */
    public function getShopId()
    {
        return $this->dao_shopId;
    }

    /**
     * Set entity shop Id.
     * 
     * @param int $shopId
     */
    public function setShopId($shopId)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $this->dao_shopId = $shopId;
    }

    /**
     * Get entity path string.
     * 
     * @return string
     */
    public function __url()
    {
        return self::getUrl($this->dao_shopId);
    }

    /**
     * Add new entity. PUT is not allowed yet
     * 
     * @param boolean $isAsync
     */
    public function __update($toolAccountId = null ,$factoryId = null, $isAsync = false)
    {
        if ($this->getShopId() == null)
        {
            $this->setShopId(\shirtplatform\utils\user\User::getShopId());
        }
        
        if($toolAccountId == null )
        {
            $toolAccountId = \shirtplatform\utils\user\User::getAccountId();
        }
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url() . '/factoryAccounts/' . $toolAccountId ;

        $this->clearAtomLinks();
        $method = 'POST';

        $query = [];
        
        if($factoryId != null )
        {
            $query['factoryId'] = $factoryId;
        }
        
        $promise = new \shirtplatform\rest\promise\ItemPromise($method, $url, get_called_class() , $query , array($this::VAR_NAME => $this->getWebserviceAttributes()), $isAsync);
        $promise->shopId = $this->dao_shopId;
        $promise->message = $this;
        
        $rest->_call($promise);
    }

     /**
     * Add new entity. PUT is not allowed yet
     * 
     * @param boolean $isAsync
     */
    public function __addFlag(ToolOrderFlag $flag)
    {
        if ($this->getShopId() == null)
        {
            $this->setShopId(\shirtplatform\utils\user\User::getShopId());
        }
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url() ."/". $this->id  . "/flags" ;

        $method = 'POST';

        $query = [];
        
        $promise = new \shirtplatform\rest\promise\ItemPromise($method, $url, get_called_class() , $query , array(ToolOrderFlag::VAR_NAME => $flag->getWebserviceAttributes()), false);
        $promise->shopId = $this->dao_shopId;
        $promise->message = $this;
        
        $rest->_call($promise);
    }
    
    
      /**
     * Add new entity. PUT is not allowed yet
     * 
     * @param boolean $isAsync
     */
    public function __addComment($orderedProductId,ToolProductComment $comment)
    {
        if ($this->getShopId() == null)
        {
            $this->setShopId(\shirtplatform\utils\user\User::getShopId());
        }
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url() ."/". $this->id  ."/products/". $orderedProductId."/comments" ;

        $method = 'POST';

        $query = [];
        
        $promise = new \shirtplatform\rest\promise\ItemPromise($method, $url, get_called_class() , $query , array(ToolProductComment::VAR_NAME => $comment->getWebserviceAttributes()), false);
        $promise->shopId = $this->dao_shopId;
        $promise->message = $this;
        
        $rest->_call($promise);
    }
     /**
     * Delete an entity.
     */
    public function __delete()
    {
        self::delete($this->getPrimaryKey(), $this->getShopId());
    }
    
    /**
     * Get entity data attributes.
     * 
     * @return array
     */
    public function getModelAttributes()
    {
        $attributes = parent::getModelAttributes();
        unset($attributes['dao_shopId']);

        return $attributes;
    }

    /**
     * Get entity path string.
     * 
     * @param int $shopId
     * @return string
     */
    public static function getUrl(&$shopId = null)
    {
        $className = get_called_class();
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);

        return $url;
    }

    /**
     * Find single entity.
     * 
     * @param int $id
     * @param int $shopId
     * @param boolean $isAsync
     * @return ShopOwnedDao
     */
    public static function &find($id, $shopId = null, $isAsync = false)
    {
        $entityName = get_called_class();

        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = self::getUrl($shopId);
        $url .= '/' . $id;
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, $entityName, array(), array(), $isAsync);
        $promise->shopId = $shopId;

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Find multiple entities.
     * 
     * @param \shirtplatform\filter\WsParameters|int $wsParameters
     * @param int $shopId
     * @param boolean $isAsync
     * @return ShopOwnedDao[]
     */
    public static function &findAll(\shirtplatform\filter\WsParameters $wsParameters = null, $shopId = null, $isAsync = false)
    {
        $entityName = get_called_class();

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\order\Order', true);

        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        $url = self::getUrl($shopId);
        if( $wsParameters->appendDefaultFilter )
        {
            $url .= ';setDefaults=true';
        }
        
        $promise = new \shirtplatform\rest\promise\PagePromise('POST', $url, $entityName, $wsParameters->buildParams(), $wsParameters->buildPostData(), $isAsync);
        $promise->shopId = $shopId;
        $promise->filterPromise = null;

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    /**
     * Count entities.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int $shopId
     * @param boolean $isAsync
     * @return int
     */
    public static function &count($wsParameters = null, $shopId = null , $isAsync = false )
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->setRootEntityName(get_called_class());
        
        $promise = new \shirtplatform\rest\promise\CountPromise('GET', self::getUrl($shopId), $wsParameters->buildParams() , array(), $isAsync);
        $promise->shopId = $shopId;
        $promise->filterPromise = $wsParameters->getPromise($isAsync);  
        
        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

    
        /**
     * Delete entity.
     * 
     * @param int $id
     * @param int $shopId
     * @param boolean $isAsync
     * @return type
     */
    public static function &delete($id, $shopId = null, $isAsync = false)
    {
        $uri = self::getUrl($shopId) . '/'.$id;
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $uri , array(), array(), $isAsync);
        $promise->shopId = $shopId; //not required

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }
    
}
