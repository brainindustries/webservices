<?php
namespace shirtplatform\entity\productionTool;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ToolAddress
 *
 * @author Jan Maslik
 */
class ToolAddress  extends \shirtplatform\entity\abstraction\JsonEntity
{
    const VAR_NAME = 'toolAddress';
    public static $classMap = array(
    );
    
    public $country;
    /**
     * Optional. Filled automatically if used right country name. 
     * @see \shirtplatform\entity\constant\CountrySource
     */
    public $countryCode;
    public $name;
    public $firstName;
    public $lastName;
    public $phone;
    public $email;
    
    public $city;
    public $zip;
    public $street;
    public $streetNo;
    public $careOf;
    public $company;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
}
