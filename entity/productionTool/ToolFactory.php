<?php
namespace shirtplatform\entity\productionTool;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ToolFactory
 *
 * @author Jan Maslik
 */
class ToolFactory extends \shirtplatform\entity\abstraction\JsonEntity
{
    const PATH_TEMPLATE = 'productionTool/accounts/{accountId}/factories';
    const VAR_NAME = 'factory';
    
    public static $classMap = array(
    );
    
    public $name;
    public $publicFactory;
    public $version;
    public $ownerAccountId;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
     /**
     * Find multiple factories assigned to $factoryAccountId.
     * 
     * @param \shirtplatform\filter\WsParameters|int $wsParameters
     * @param int $shopId
     * @param boolean $isAsync
     * @return ShopOwnedDao[]
     */
    public static function &findAll(\shirtplatform\filter\WsParameters $wsParameters = null, $factoryAccountId = 0, $isAsync = false)
    {
        $entityName = get_called_class();

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $query = $wsParameters->buildParams();
        $query['factoryAccountId'] = $factoryAccountId;
        
        $promise = new \shirtplatform\rest\promise\PagePromise('GET', self::getUrl(), $entityName, $query , array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }
    
    public static function getUrl()
    {
        $className = get_called_class();
        return  str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);
    }
}
