<?php
namespace shirtplatform\entity\productionTool;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ToolProductComment
 *
 * @author Jan Maslik
 */
class ToolProductComment extends \shirtplatform\entity\abstraction\JsonEntity
{
    const PATH_TEMPLATE = 'productionTool/accounts/{accountId}/shops/{shopId}/orders/{orderId}/products/{productId}/comments';
    
    const VAR_NAME = 'toolProductComment';
    public static $classMap = array(
    );
    
    public $name;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
    
}
