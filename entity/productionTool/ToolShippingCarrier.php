<?php

namespace shirtplatform\entity\productionTool;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ToolShippingCarrier
 *
 * @author admin
 */
class ToolShippingCarrier extends \shirtplatform\entity\abstraction\JsonEntity
{
    
    public static $classMap = array(
        'moduleProvider' => '\shirtplatform\entity\productionTool\ToolShippingProvider',
    );
    
    public $name;
    public $moduleProvider;
    
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
