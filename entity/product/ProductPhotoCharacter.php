<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductPhotoCharacter
 *
 * @author Jan Maslik
 */
class ProductPhotoCharacter extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/photo/characters';
    const VAR_NAME = 'productPhotoCharacter';

    public static $classMap = array(
        'character' => '\shirtplatform\entity\product\PhotoCharacter',
        'assignedSize' => '\shirtplatform\entity\product\AssignedProductSize'
    );
    public $fileName;
    public $character;
    public $assignedSize;
    public $imageId;
    public $version;
    public $orderIndex;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
