<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductTypeAttributeFlag
 *
 * @author Jan Maslik
 */
class AttributeFlag extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypeAttributeFlags';
    const VAR_NAME = 'attributeFlag';

    public static $classMap = array(
    );
    public $name;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Clear cache.
     * 
     * @return void
     */
    public static function evictCache()
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\JsonPromise('POST', self::PATH_TEMPLATE . '/evictCache');

        return $rest->_call($promise);
    }

}
