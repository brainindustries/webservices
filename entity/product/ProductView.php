<?php

namespace shirtplatform\entity\product;

/**
 * Description of ProductView
 *
 * @author Jan Maslik
 */
use \User;
use shirtplatform\filter\WsParameters;

class ProductView extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/views';
    const PATH_ALL_VIEWS = 'accounts/{accountId}/views';
    const VAR_NAME = 'productView';

    public static $classMap = array(
        'localizations' => '\shirtplatform\entity\localization\ProductViewLocalized',
    );
    public $name;
    public $version;
    public $active = 1;
    public $static;
    public $default;
    public $orderIndex;
    public $position = \shirtplatform\entity\enumerator\ViewPosition::NONE;
    //layze
    protected $localizations = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get localizations.
     * 
     * @param boolean $isAsync
     * @param WsParameters|null $wsParameters
     * @return \shirtplatform\entity\localization\ProductViewLocalized[]
     */
    public function getLocalizations($isAsync = false, $wsParameters = null)
    {
        return $this->getAtomLinkValue('localizations', $wsParameters, $isAsync);
    }

    /**
     * Find all in account.
     * 
     * @param WsParameters|null $wsParameter
     * @return ProductView[]
     */
    public static function findAllByAccount($wsParameter = null)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_ALL_VIEWS);
        return self::findAllFromUrl($url, $wsParameter); //TODO set parents by url
    }

}
