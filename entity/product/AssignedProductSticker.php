<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of AssignedProductSticker
 *
 * @author Jan Maslik
 */
class AssignedProductSticker extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedViews/{parentId}/assignedStickers';
	const VAR_NAME = 'assignedProductSticker';

	
	public static $classMap = array(
		'sticker' => '\shirtplatform\entity\account\Sticker',
	);
	
	public $sticker;
	public $onAllColors;
	public $aboveMotives;
	public $creatorOnly;
	public $xTwips;
	public $yTwips;
	public $widthTwips;
	public $heightTwips;
	public $designWidthTwips;
	public $designHeightTwips;
	public $displayUntil;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}
