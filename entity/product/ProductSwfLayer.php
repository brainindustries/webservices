<?php

namespace shirtplatform\entity\product;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductSwfLayer
 *
 * @author Jan Maslik
 */
class ProductSwfLayer extends \shirtplatform\entity\abstraction\JsonEntity
{

    const PATH_TEMPLATE = 'root/productSwf/';
    const VAR_NAME = 'string';

    public static $classMap = array(
    );
    public $value;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get single swf layer.
     * 
     * @param int $swfId
     * @param boolean $isAsync
     * @return ProductSwfLayer
     */
    public static function &find($swfId, $isAsync = false)
    {
        $url = 'root/productSwf/' . $swfId . '/layers';
        return self::findAllFromUrl($url, null, null, [], $isAsync);
    }

}
