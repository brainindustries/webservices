<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductPhotoDetail
 *
 * @author Jan Maslik
 */
class ProductPhotoDetail extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/photo/details';
    const VAR_NAME = 'productPhotoDetail';

    public static $classMap = array(
    );
    public $fileName;
    public $productPhoto;
    public $detailStripe;
    public $imageId;
    public $version;
    public $orderIndex;
    public $assignedColorId;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
