<?php

namespace shirtplatform\entity\product;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductToTemplate
 *
 * @author Jan Maslik
 */
class ProductToTemplate extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedViews/{parentId}/areas/{parentId}/templates';
    const VAR_NAME = 'productToTemplate';

    public static $classMap = array(
        'template' => '\shirtplatform\entity\technology\Template',
        'assignedPrintTechnology' => '\shirtplatform\entity\product\AssignedPrintTechnology',
    );
    public $template;
    public $assignedPrintTechnology;
    public $showBoundaries = true;
    public $version;
    public $ignoreClip=false;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
