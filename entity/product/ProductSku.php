<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductSku
 *
 * @author Jan Maslik
 */
class ProductSku extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/sku';
    const VAR_NAME = 'productSku';

    public static $classMap = array(
        'assignedSize' => '\shirtplatform\entity\product\AssignedProductSize',
        'assignedColor' => '\shirtplatform\entity\product\AssignedProductColor'
    );
    
    public $plu; //temporary
    public $stockId;
    public $availability;
    public $version;
    public $available;
    public $assignedSize;
    public $assignedColor;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
