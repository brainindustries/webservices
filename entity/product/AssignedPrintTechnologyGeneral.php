<?php

namespace shirtplatform\entity\product;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssignedPrintTechnologyGeneral
 *
 * @author Jan Maslik
 */
class AssignedPrintTechnologyGeneral extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedPrintTechnologies/{parentId}/printTechnologyGenerals';
	const VAR_NAME = 'assignedPrintTechnologyGeneral';

	
	public static $classMap = array(
		'assignedView' => '\shirtplatform\entity\product\AssignedProductView',
		'assignedColor' => '\shirtplatform\entity\product\AssignedProductColor',
	);
	
	public $assignedView;
	public $assignedColor;
	
	public $active;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}

}
