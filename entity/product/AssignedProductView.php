<?php

namespace shirtplatform\entity\product;

use shirtplatform\filter\WsParameters;

/**
 * Description of AssignedProductView
 *
 * @author Jan Maslik
 */
use \shirtplatform\entity\enumerator\AssignedViewType;

class AssignedProductView extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedViews';
    const VAR_NAME = 'assignedProductView';

    public static $classMap = array(
        'productView' => '\shirtplatform\entity\product\ProductView',
        'assignedColors' => '\shirtplatform\entity\product\ProductViewColorDetail',
        'areas' => '\shirtplatform\entity\product\ProductArea'
    );
    public $orderIndex;
    public $defaultView;
    public $defaultInCreator;
    public $visibleInCreator;
    public $prioritizeOnOverview;
    public $showInCreator;
    public $version;
    public $swfId;
    public $productView = null;
    public $type = \shirtplatform\entity\enumerator\AssignedViewType::TECHNICAL;
    //lazy
    protected $assignedColors = null;
    protected $areas = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get assigned colors.
     * 
     * @return \shirtplatform\entity\product\ProductViewColorDetail[]
     */
    public function getAssignedColors()
    {
        return $this->getAtomLinkValue('assignedColors');
    }

    /**
     * Add assigned view color.
     * 
     * @param \shirtplatform\entity\product\ProductViewColorDetail $assignedColorView
     */
    public function addAssignedViewColor($assignedColorView)
    {
        if ($this->assignedColors == null)
        {
            $this->assignedColors = array();
        }

        $this->assignedColors[$assignedColorView->id] = $assignedColorView;
    }

    /**
     * Get assigned view areas.
     * 
     * @param WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\ProductArea[]
     */
    public function getAreas($wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('areas', $wsParameters, $isAsync, $forceLoad);
    }

}
