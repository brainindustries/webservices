<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of AreaGeomElipse
 *
 * @author Jan Maslik
 */
class AreaGeomElipse extends \shirtplatform\entity\abstraction\JsonEntity
{
	const VAR_NAME = 'areaGeomElipse';
	
	public $xTwips;
	public $yTwips;
	public $aTwips;
	public $bTwips;
	
	public static $classMap = array(
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}


?>
