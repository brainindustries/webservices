<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductColision
 *
 * @author Jan Maslik
 */
class ProductColision extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedViews/{parentId}/colisions';
    const VAR_NAME = 'productColision';

    public static $classMap = array(
    );
    public $version;
    public $boundsXTwips;
    public $boundsYTwips;
    public $boundsWidthTwips;
    public $boundsHeightTwips;
    public $baseWidthTwips;
    public $baseHeightTwips;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
