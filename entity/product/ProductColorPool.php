<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductColorPool
 *
 * @author Jan Maslik
 */
use \shirtplatform\rest\REST;
use \User;
use shirtplatform\filter\Filter;

class ProductColorPool extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/colorPools';
    const PATH_MOVE_COLORS = 'accounts/{accountId}/productTypes/{parentId}/colorPools/{poolId}/moveColors/{newColorPoolId}';
    const VAR_NAME = 'productColorPool';

    public static $classMap = array(
        'colors' => '\shirtplatform\entity\product\ProductColor'
    );
    public $name;
    public $version;
    public $defaultPool;
    public $orderIndex;
    public $staticPool;
    protected $colors;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get colors in pool.
     * 
     * @param type $wsParameters
     * @return \shirtplatform\entity\product\ProductColor[]
     */
    public function getColors($wsParameters = null)
    {
        return $this->getAtomLinkValue('colors', $wsParameters);
    }

    /**
     * Move colors from one pool to another.
     * 
     * @param int $newPoolId
     * @param Filter|null $filter
     */
    public function moveColors($newPoolId, $filter = null)
    {
        $entityAsArray = $this->getWebserviceAttributes();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_MOVE_COLORS);

        foreach ($this->getParents() as $parent)
        {
            $url = preg_replace('/{parentId}/', $parent, $url, 1);
        }

        $url = str_replace('{poolId}', $this->id, $url);
        $url = str_replace('{newColorPoolId}', $newPoolId, $url);

        $w = new \shirtplatform\filter\WsParameters();
        $w->setFilter($filter);
        $w->setRootEntityName('\shirtplatform\entity\product\ProductColor');

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), $w->buildParams(), array(self::VAR_NAME => $entityAsArray), false);
        $promise->filterPromise = $w->getPromise();

        return $rest->_call($promise);
    }

}
