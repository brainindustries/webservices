<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductViewColorDetail
 *
 * @author Jan Maslik
 */
use \User;
use \shirtplatform\rest\REST;
use \DebugException;
use shirtplatform\parser\WsModel;
use shirtplatform\parser\WsParse;
use shirtplatform\parser\ArrayUtil;
use shirtplatform\filter\Filter;
use shirtplatform\filter\WsParameters;

class ProductViewColorDetail extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedViewColors';
    const VAR_NAME = 'productViewColorDetail';

    public static $IMG_COUNTER = 1;
    public static $classMap = array(
        'assignedColor' => '\shirtplatform\entity\product\AssignedProductColor',
        'assignedView' => '\shirtplatform\entity\product\AssignedProductView',
        'defaultColorValue' => '\shirtplatform\entity\product\ProductColorValue'
    );
    public $colorValueIndex;
    public $layer1Color;
    public $layer2Color;
    public $shadows1Alpha;
    public $shadows2Alpha;
    public $texAlpha;
    public $tex2Alpha;
    public $imageId;
    public $version;
    public $defaultColorValue;
    public $assignedColor = null;
    public $assignedView = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);

        if ($this->getAtomLink('image') != null)
        {
            $previewUrl = str_replace(\shirtplatform\constants\WsConstants::IMG_REPLACEMENT, self::$IMG_COUNTER . \shirtplatform\constants\WsConstants::IMG_REPLACEMENT, $this->getAtomLink('image'));
            $this->setAtomLink('image', $previewUrl);
            self::$IMG_COUNTER = (self::$IMG_COUNTER >= 5 ? 1 : self::$IMG_COUNTER + 1 );
        }
    }

    /**
     * Get preview image url.
     * 
     * @return string
     */
    public function getPreview()
    {
        return $this->getAtomLink('image');
    }

    /**
     * Find by view color.
     * 
     * @param int $productId
     * @param int $assignedViewId
     * @param int $assignedColorId
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return ProductViewColorDetail[]
     */
    public static function &findByViewColor($productId, $assignedViewId, $assignedColorId, $shopId = null, $isAsync = false)
    {
        $accountId = \shirtplatform\utils\user\User::getAccountId();
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = "accounts/{$accountId}/shops/{$shopId}/products/{$productId}/assignedViews/{$assignedViewId}/assignedColors/{$assignedColorId}";

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), [], [], $isAsync);
        $promise->shopId = $shopId;
        return $rest->_call($promise);
    }

    /**
     * Find by view.
     * 
     * @param int $productId
     * @param int $assignedViewId
     * @param WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return ProductViewColorDetail[]
     */
    public static function &findByView($productId, $assignedViewId, $wsParameters = null, $shopId = null, $isAsync = false)
    {

        $accountId = \shirtplatform\utils\user\User::getAccountId();

        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = "accounts/{$accountId}/shops/{$shopId}/products/{$productId}/assignedViews/{$assignedViewId}/assignedColors";

        return self::findAllFromUrl($url, $wsParameters, $shopId, array($productId, $assignedViewId), $isAsync);
    }

}
