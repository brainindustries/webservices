<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductTypeAttributeListItem
 *
 * @author Jan Maslik
 */
class ProductTypeAttributeListItem extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/attributeLists/{parentId}/listItems';
    const PATH_ALL_BY_PTYPE = 'accounts/{accountId}/productTypes/{typeId}/listItems';
    const VAR_NAME = 'attributeListItem';

    public static $classMap = array(
        'localizations' => '\shirtplatform\entity\localization\ProductTypeAttributeListItemLocalized'
    );
    public $name;
    public $version;
    public $orderIndex;
    protected $localizations;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get localizations.
     * 
     * @return \shirtplatform\entity\localization\ProductTypeAttributeListItemLocalized[]
     */
    public function getLocalizations(\shirtplatform\filter\WsParameters $wsParams = null, $isAsync = false)
    {
        return $this->getAtomLinkValue('localizations', $wsParams, $isAsync);
    }

    /**
     * Find items by product type.
     * 
     * @param int $productTypeId
     * @param \shirtplatform\filter\WsParameters|null $wsParameter
     * @return ProductTypeAttributeListItem[]
     */
    public static function &findByProductType($productTypeId, \shirtplatform\filter\WsParameters $wsParameter = null)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_ALL_BY_PTYPE);
        $url = str_replace('{typeId}', $productTypeId, $url);

        return self::findAllFromUrl($url, $wsParameter); //TODO set parents by url
    }

}
