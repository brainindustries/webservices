<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of Transformation
 *
 * @author Jan Maslik
 */
class Transformation extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedViews/{parentId}/transform';
    const VAR_NAME = 'transformation';

    public static $classMap = array(
        'area' => '\shirtplatform\entity\abstraction\Entity'
    );
    public $m00;
    public $m01;
    public $m02;
    public $m10;
    public $m11;
    public $m12;
    public $m20;
    public $m21;
    public $m22;
    public $baseWidthTwips;
    public $baseHeightTwips;
    public $surfaceLines;
    public $surfaceColumns;
    public $surfaceVectors;
    public $version;
    public $area;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
