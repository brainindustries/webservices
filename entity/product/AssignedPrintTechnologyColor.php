<?php

namespace shirtplatform\entity\product;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssignedPrintTechnologyColor
 *
 * @author Jan Maslik
 */
class AssignedPrintTechnologyColor extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedPrintTechnologies/{parentId}/printTechnologyColors';
	const VAR_NAME = 'assignedPrintTechnologyColor';

	
	public static $classMap = array(
		'colorValue' => '\shirtplatform\entity\product\ProductColorValue',
	);
	
	public $colorValue;
	public $whiteColor;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}

}
