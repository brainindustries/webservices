<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of AssignedProductViewSnap
 *
 * @author Jan Maslik
 */
class AssignedProductViewSnap extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedViews/{parentId}/snapViews';
	const VAR_NAME = 'assignedProductViewSnap';

	
	public static $classMap = array(
		'snapView' => '\shirtplatform\entity\product\AssignedProductView'
	);
	
	public $version;
	public $boundsXTwips;
	public $boundsYTwips;
	public $baseWidthTwips;
	public $baseHeightTwips;
	public $snapView;
	
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
