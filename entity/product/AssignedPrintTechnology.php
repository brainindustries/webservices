<?php

namespace shirtplatform\entity\product;

use shirtplatform\filter\WsParameters;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssignedPrintTechnology
 *
 * @author Jan Maslik
 */
class AssignedPrintTechnology extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedPrintTechnologies';
    const VAR_NAME = 'assignedPrintTechnology';

    public static $classMap = array(
        'productTypePrintTechnology' => '\shirtplatform\entity\technology\ProductTypeAssignedPrintTechnology',
        'kornits' => '\shirtplatform\entity\product\AssignedPrintTechnologyKornit',
        'generals' => '\shirtplatform\entity\product\AssignedPrintTechnologyGeneral',
        'medias' => '\shirtplatform\entity\product\AssignedPrintTechnologyMedia',
        'colors' => '\shirtplatform\entity\product\AssignedPrintTechnologyColor'
    );
    public $default;
    public $orderIndex;
    public $version;
    public $deleted;
    public $productTypePrintTechnology;
    protected $kornits;
    protected $generals;
    protected $medias;
    protected $colors;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get generals setup entities.
     * 
     * @param WsParameters|null $wsParameter
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\AssignedPrintTechnologyGeneral[]
     */
    public function getGenerals($wsParameter = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('generals', $wsParameter, $isAsync, $forceLoad);
    }

    /**
     * Get medias setup entities.
     * 
     * @param WsParameters|null $wsParameter
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\AssignedPrintTechnologyMedia[]
     */
    public function getMedias($wsParameter = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('medias', $wsParameter, $isAsync, $forceLoad);
    }

    /**
     * Get colors setup entities.
     * 
     * @param WsParameters|null $wsParameter
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\AssignedPrintTechnologyColor[]
     */
    public function getColors($wsParameter = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('colors', $wsParameter, $isAsync, $forceLoad);
    }

}
