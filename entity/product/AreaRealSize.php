<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductAreaRealSizePrime
 *
 * @author Jan Maslik
 */
class AreaRealSize extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedViews/{parentId}/areas/{parentId}/realSizes';
    const VAR_NAME = 'areaRealSize';

    public static $classMap = array(
        'area' => 'ProductArea',
        'assignedSize' => '\shirtplatform\entity\product\AssignedProductSize'
    );
    public $id;
    public $width;
    public $height;
    public $origWidth;
    public $origHeight;
    public $version;
    public $deleted;
    public $assignedSize;
    //lazy
    protected $area = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get area.
     * 
     * @return ProductArea
     */
    public function getArea()
    {
        return $this->getAtomLinkValue('area');
    }

}
