<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of AssignedProductSize
 *
 * @author Jan Maslik
 */
class AssignedProductSize extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedSizes';
    const VAR_NAME = 'assignedProductSize';

    public static $classMap = array(
        'productSize' => '\shirtplatform\entity\product\ProductSize',
    );
    public $orderIndex;
    //width in mm
    public $width;
    //height in mm
    public $height;
    //length in mm
    public $length;
    //value with interval <-1;1>
    public $accuracy;
    public $default;
    public $version;
    //lazy
    public $productSize = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get product size.
     * 
     * @return \shirtplatform\entity\product\ProductSize
     */
    public function getProductSize()
    {
        return $this->getAtomLinkValue('productSize');
    }

}
