<?php

namespace shirtplatform\entity\product;

use shirtplatform\filter\WsParameters;
/**
 * Description of ProductSize
 *
 * @author Jan Maslik
 */
use \User;

class ProductSize extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/sizePools/{parentId}/sizes';
    const PATH_ALL_SIZES = 'accounts/{accountId}/sizes';
    const VAR_NAME = 'productSize';

    public static $classMap = array(
        'localizations' => '\shirtplatform\entity\localization\ProductSizeLocalized',
    );
    public $name;
    public $version;
    public $dimensions = 2;
    public $orderIndex;
    public $groupingId = '';
    public $groupingDefault = false;
    protected $localizations = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get localizations.
     * @param WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\localization\ProductSizeLocalized[]
     */
    public function getLocalizations($wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('localizations', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get all sizes in account.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameter
     * @param boolean $isAsync
     * @return ProductSize[]
     */
    public static function &findAllByAccount(\shirtplatform\filter\WsParameters $wsParameter = null, $isAsync = false)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_ALL_SIZES);
        return self::findAllFromUrl($url, $wsParameter, null, null, $isAsync); //TODO set parents by url
    }

}
