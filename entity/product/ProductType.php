<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductType
 *
 * @author Jan Maslik
 */
use \User;
use \shirtplatform\rest\REST;
use \shirtplatform\filter\WsParameters;
use \shirtplatform\filter\Filter;
use \shirtplatform\parser\WsParse;
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\parser\WsModel;

class ProductType extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypes';
    const PATH_DUPLICATE = 'accounts/{accountId}/productTypes/{dstId}/importAttributesFrom/{srcId}';
    const VAR_NAME = 'productType';

    public static $classMap = array(
        'localizations' => '\shirtplatform\entity\localization\ProductTypeLocalized',
        'colorPools' => '\shirtplatform\entity\product\ProductColorPool',
        'sizePools' => '\shirtplatform\entity\product\ProductSizePool',
        'views' => '\shirtplatform\entity\product\ProductView',
        'attributeLists' => '\shirtplatform\entity\product\ProductTypeAttributeList',
        'assignedPrintTechnologys' => '\shirtplatform\entity\technology\ProductTypeAssignedPrintTechnology'
    );
    public $name;
    public $deleted = false;
    public $description = "";
    public $version;
    protected $localizations = null;
    protected $colorPools = null;
    protected $sizePools = null;
    protected $views = null;
    protected $attributeLists = null;
    protected $assignedPrintTechnologys = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get localizations.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\localization\ProductTypeLocalized[]
     */
    public function getLocalizations($isAsync = false)
    {
        return $this->getAtomLinkValue('localizations', null, $isAsync);
    }

    /**
     * Get color pools.
     * 
     * @return \shirtplatform\entity\product\ProductColorPool[]
     */
    public function getColorPools()
    {
        return $this->getAtomLinkValue('colorPools');
    }

    /**
     * Get size pools.
     * 
     * @return \shirtplatform\entity\product\ProductSizePool[]
     */
    public function getSizePools()
    {
        return $this->getAtomLinkValue('sizePools');
    }

    /**
     * Get product views.
     * 
     * @return \shirtplatform\entity\product\ProductView[]
     */
    public function getViews()
    {
        return $this->getAtomLinkValue('views');
    }

    /**
     * Get attribute lists.
     * 
     * @return \shirtplatform\entity\product\ProductTypeAttributeList[]
     */
    public function getAttributeLists()
    {
        return $this->getAtomLinkValue('attributeLists');
    }

    /**
     * Get assigned print technologies.
     * 
     * @param booelan $isAsync
     * @return \shirtplatform\entity\technology\ProductTypeAssignedPrintTechnology[]
     */
    public function getAssignedPrintTechnologys($isAsync = false)
    {
        return $this->getAtomLinkValue('assignedPrintTechnologys', null, $isAsync);
    }

    /**
     * Imports all attributes of specified product type
     * 
     * @param int $srcProductTypeId
     */
    public function importAttributesFrom($srcProductTypeId)
    {
        $rest = \shirtplatform\rest\REST::getInstance();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_DUPLICATE);
        $url = str_replace('{dstId}', $this->id, $url);
        $url = str_replace('{srcId}', $srcProductTypeId, $url);

        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url, [], [], false);
        $rest->_call($promise);
    }

}
