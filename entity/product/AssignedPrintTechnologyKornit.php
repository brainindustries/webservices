<?php

namespace shirtplatform\entity\product;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssignedKornitSetup
 *
 * @author Jan Maslik
 */
use \shirtplatform\entity\enumerator\KornitColorPrintMode,
    \shirtplatform\entity\enumerator\KornitDirection,
    \shirtplatform\entity\enumerator\KornitPrintSpeed,
    \shirtplatform\entity\enumerator\KornitSaturation,
    \shirtplatform\entity\enumerator\KornitWhitePrintMode;

class AssignedPrintTechnologyKornit extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedPrintTechnologies/{parentId}/printTechnologyKornits';
	const VAR_NAME = 'assignedPrintTechnologyKornit';

	
	public static $classMap = array(
		'assignedSize' => '\shirtplatform\entity\product\AssignedProductSize',
		'assignedView' => '\shirtplatform\entity\product\AssignedProductView',
		'assignedColor' => '\shirtplatform\entity\product\AssignedProductColor',
		'mediaName' => '\shirtplatform\entity\technology\KornitMediaName'
	);
	
	public $assignedSize;
	public $assignedView;
	public $assignedColor;
	public $mediaName;
	
	public $sprayAmount = -1 ;
	public $printSpeed = KornitPrintSpeed::PRODUCTION;
	public $colorPrintMode = KornitColorPrintMode::INTERLACE;
	public $saturation = KornitSaturation::NORMAL;
	public $whitePrintMode = KornitWhitePrintMode::SINGLE;
	public $opacity = 85;
	public $hilightWhite = 1;
	public $hilightOpacity = 85;
	public $direction = KornitDirection::BIDIRECTIONAL;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}

}

