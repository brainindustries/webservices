<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of CategoryAssignedToProduct
 *
 * @author Jan Maslik
 */
use shirtplatform\filter\WsParameters;

class CategoryAssignedToProduct extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedCategories';
    const PATH_ASSIGN_ALL = 'accounts/{accountId}/shops/{shopId}/products/assignedCategories';
    const VAR_NAME = 'categoryAssignedToProduct';

    public static $classMap = array(
        'category' => '\shirtplatform\entity\product\ProductCategory',
    );
    public $active;
    public $orderIndex;
    public $category;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Move all.
     * 
     * @param \shirtplatform\entity\product\shirtplatform\filter\WsParameters|null $wsParameters
     */
    public function __moveAll(shirtplatform\filter\WsParameters $wsParameters)
    {
        self::moveAll($this, $wsParameters, $this->getShopId());
    }

}
