<?php

namespace shirtplatform\entity\product;

class ProductPhotoMeasure extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/photo/measures';
    const VAR_NAME = 'productPhotoMeasure';

    public static $classMap = array(
    );
    public $fileName;
    public $imageId;
    public $version;
    public $orderIndex;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
