<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of asignedProductFactory
 *
 * @author Jan Maslik
 */
class AssignedProductFactory extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedFactories';
	const VAR_NAME = 'assignedProductFactory';

	
	public static $classMap = array(
		'factoryAssigned' => '\shirtplatform\entity\technology\FactoryAssigned',
	);
	
	public $version;
	public $factoryAssigned = null;
        public $deleted;
	
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}