<?php
namespace shirtplatform\entity\product;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductCategoryMargin
 *
 * @author Jan Maslik
 */
class ProductCategoryMargin extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/productCategories/{parentId}/margins';
	const VAR_NAME = 'productCategoryMargin';

	public static $classMap = array(
		'country' => '\shirtplatform\entity\account\Country',
	);
        
	public $margin=0;
	public $version;
	
	public $country;
	
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
      
}
