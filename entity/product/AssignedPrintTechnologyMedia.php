<?php

namespace shirtplatform\entity\product;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssignedPrintTechnologyMedia
 *
 * @author Jan Maslik
 */
class AssignedPrintTechnologyMedia extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedPrintTechnologies/{parentId}/printTechnologyMedias';
	const VAR_NAME = 'assignedPrintTechnologyMedia';

	
	public static $classMap = array(
		'assignedSize' => '\shirtplatform\entity\product\AssignedProductSize',
		'assignedView' => '\shirtplatform\entity\product\AssignedProductView',
		'assignedPrintMediaSize' => '\shirtplatform\entity\technology\PrintTechnologyAssignedMediaSize'
	);
	
	public $assignedSize;
	public $assignedView;
	public $assignedPrintMediaSize;
	public $downgradable;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}

}