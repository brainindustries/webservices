<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductSizePool
 *
 * @author Jan Maslik
 */
use \User;
use \shirtplatform\rest\REST;
use shirtplatform\filter\Filter;
use shirtplatform\filter\WsParameters;

class ProductSizePool extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/sizePools';
    const PATH_MOVE_SIZES = 'accounts/{accountId}/productTypes/{parentId}/sizePools/{poolId}/moveSizes/{newSizePoolId}';
    const VAR_NAME = 'productSizePool';

    public static $classMap = array(
        'sizes' => '\shirtplatform\entity\product\ProductSize'
    );
    public $name;
    public $version;
    public $defaultPool;
    public $orderIndex;
    public $staticPool;
    protected $sizes;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get sizes in pool.
     * 
     * @param WsParameters|null $wsParameters
     * @return \shirtplatform\entity\product\ProductSize[]
     */
    public function getSizes($wsParameters = null)
    {
        return $this->getAtomLinkValue('sizes', $wsParameters);
    }

    /**
     * Move sizes from one pool to another.
     * 
     * @param int $newPoolId
     * @param Filter|null $filter
     */
    public function moveSizes($newPoolId, $filter = null)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_MOVE_SIZES);

        foreach ($this->getParents() as $parent)
        {
            $url = preg_replace('/{parentId}/', $parent, $url, 1);
        }

        $url = str_replace('{poolId}', $this->id, $url);
        $url = str_replace('{newSizePoolId}', $newPoolId, $url);

        $w = new \shirtplatform\filter\WsParameters();
        $w->setFilter($filter);
        $w->setRootEntityName('\shirtplatform\entity\product\ProductSize');

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), $w->buildParams(), array(self::VAR_NAME => $this->getWebserviceAttributes()), false);
        $promise->filterPromise = $w->getPromise();

        return $rest->_call($promise);
    }

}
