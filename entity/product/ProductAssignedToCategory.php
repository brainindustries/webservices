<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductAssignedToCategory
 *
 * @author Jan Maslik
 */
use \shirtplatform\rest\REST;
use \shirtplatform\filter\WsParameters;

class ProductAssignedToCategory extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/productCategories/{parentId}/assignedProducts';
    const VAR_NAME = 'productAssignedToCategory';

    public static $classMap = array(
        'product' => '\shirtplatform\entity\product\Product',
    );
    public $active;
    public $orderIndex;
    public $product;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Delete multiple assignments.
     * 
     * @param int $categoryId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @return void
     */
    public static function deleteAll($categoryId, $wsParameters = null, $shopId = null)
    {
        $url = self::getUrl(array($categoryId), $shopId);

        $rest = \shirtplatform\rest\REST::getInstance();

        $loadAll = false;

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName(get_called_class());

        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $url, $wsParameters->buildParams(), [], false);
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);

        $result = intval($promise->message);

        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::deleteCategoryAll($wsParameters, $shopId);
        }
        return $result;
    }

}
