<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductTypeAttributeFlag
 *
 * @author Jan Maslik
 */
class ProductTypeAttributeFlag extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/attributeLists/{parentId}/flags';
    const VAR_NAME = 'attributeFlag';

    public static $classMap = array(
    );
    public $name;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
