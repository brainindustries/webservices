<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductArea
 *
 * @author Jan Maslik
 */
class ProductArea extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedViews/{parentId}/areas';
    const VAR_NAME = 'productArea';

    public static $classMap = array(
        'boundaryRect' => '\shirtplatform\entity\product\AreaGeomRect',
        'boundaryElipse' => '\shirtplatform\entity\product\AreaGeomElipse',
        'maskRect' => '\shirtplatform\entity\product\AreaGeomRect',
        'maskElipse' => '\shirtplatform\entity\product\AreaGeomElipse',
        'maskSpline' => '\shirtplatform\entity\product\AreaGeomSpline',
        'boundarySpline' => '\shirtplatform\entity\product\AreaGeomSpline',
        'realSizes' => '\shirtplatform\entity\product\AreaRealSize',
        'snaps' => '\shirtplatform\entity\product\AreaSnap',
        'templates' => '\shirtplatform\entity\product\ProductToTemplate'
    );
    public $version;
    public $boundsXTwips;
    public $boundsYTwips;
    public $boundsWidthTwips;
    public $boundsHeightTwips;
    public $baseWidthTwips;
    public $baseHeightTwips;
    public $name;
    public $default;
    public $xHotSpotTwips;
    public $yHotSpotTwips;
    public $sizeHotSpotTwips;
    public $lockAspectRatio;
    public $horizontalCenterLineEnabled;
    public $verticalCenterLineEnabled;
    public $fillScaling;
    public $deleted = false;
    public $enabled = true;
    public $boundaryRect;
    public $boundaryElipse;
    public $boundarySpline;
    public $maskRect;
    public $maskElipse;
    public $maskSpline;
    //lazy
    protected $realSizes = null;
    protected $snaps = null;
    protected $templates = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get real sizes.
     * 
     * @return \shirtplatform\entity\product\AreaRealSize[]
     */
    public function getRealSizes()
    {
        return $this->getAtomLinkValue('realSizes');
    }

    /**
     * Check if area contain mask.
     * 
     * @return boolean
     */
    public function containMask()
    {
        if ($this->maskElipse != null || $this->maskRect != null || $this->maskSpline)
        {
            return true;
        }

        return false;
    }

    /**
     * Get model attributes.
     * 
     * @param boolean $removeNulls
     * @return array
     */
    public function getWebserviceAttributes($removeNulls = true)
    {
        $attributes = parent::getWebserviceAttributes();

        if (empty($attributes['boundaryRect']))
        {
            unset($attributes['boundaryRect']);
        }

        if (empty($attributes['boundaryElipse']))
        {
            unset($attributes['boundaryElipse']);
        }
        if (empty($attributes['boundarySpline']))
        {
            unset($attributes['boundarySpline']);
        }

        if (empty($attributes['maskRect']))
        {
            unset($attributes['maskRect']);
        }

        if (empty($attributes['maskElipse']))
        {
            unset($attributes['maskElipse']);
        }

        if (empty($attributes['maskSpline']))
        {
            unset($attributes['maskSpline']);
        }

        return $attributes;
    }

    /**
     * Get area snap points.
     * 
     * @return \shirtplatform\entity\product\AreaSnap[]
     */
    public function getSnaps()
    {
        return $this->getAtomLinkValue('snaps');
    }

    /**
     * Get are print templates.
     * 
     * @param type $isAsync
     * @param type $forceLoad
     * @return \shirtplatform\entity\product\ProductToTemplate[]
     */
    public function getTemplates($isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('templates', null, $isAsync, $forceLoad);
    }

}
