<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of PhotoCharacter
 *
 * @author Jan Maslik
 */
class PhotoCharacter extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/photo/characters';
    const VAR_NAME = 'photoCharacter';

    public static $classMap = array(
    );
    public $name;
    public $weight;
    public $height;
    public $version;
    public $deleted;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
