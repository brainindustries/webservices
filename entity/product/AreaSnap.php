<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of AreaSnap
 *
 * @author Jan Maslik
 */
class AreaSnap extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedViews/{parentId}/areas/{parentId}/snaps';
	const VAR_NAME = 'areaSnap';

	
	public static $classMap = array(
	);
	
	public $id;
	public $xTwips;
	public $yTwips;
	public $sizeTwips;
	public $scale = \shirtplatform\entity\enumerator\SnapScaleType::EXACT_SCALE;
	public $version;
	
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}
