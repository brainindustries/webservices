<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductCategory
 *
 * @author Jan Maslik
 */
use \shirtplatform\parser\WsModel;
use \shirtplatform\parser\WsParse;
use \shirtplatform\filter\Filter;
use \shirtplatform\filter\WsParameters;
use \DebugException;
use \shirtplatform\rest\REST;

class ProductCategory extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/productCategories';
    const VAR_NAME = 'productCategory';

    public static $classMap = array(
        'localizations' => '\shirtplatform\entity\localization\ProductCategoryLocalized',
        'assignedProducts' => '\shirtplatform\entity\product\ProductAssignedToCategory'
    );
    
    public $name;
    public $orderIndex;
    public $active;
    public $deleted;
    public $version;
    protected $localizations = null;
    protected $assignedProducts = null;
    protected $countAssignedProducts = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get assigned products.
     * 
     * @param WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\ProductAssignedToCategory[]
     */
    public function getAssignedProducts($wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('assignedProducts', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get assigned products count.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceReload
     * @return int
     */
    public function getAssignedProductsCount(\shirtplatform\filter\WsParameters $wsParameters = null, $isAsync = false, $forceReload = false)
    {
        if ($this->countAssignedProducts == null || $forceReload)
        {
            $this->countAssignedProducts = ProductAssignedToCategory::count($this->getParentList(), $wsParameters, $this->getShopId(), $isAsync);
        }

        return $this->countAssignedProducts;
    }

    /**
     * Assign parent category.
     * 
     * @param int $parentCategoryId
     */
    public function __assignParentCategory($parentCategoryId = '')
    {
        self::assignParentCategory($this->id, $parentCategoryId);
    }

    /**
     * Get child categories.
     * 
     * @return ProductCategory[]
     */
    public function getChildCategories()
    {
        return self::findChildCategories($this->id, null, $this->shopId);
    }

    /**
     * Assign parent category.
     * 
     * @param int $categoryId
     * @param int $parentCategoryId
     * @param int|null $shopId
     * @param boolean $isAsync
     */
    public static function assignParentCategory($categoryId, $parentCategoryId = '', $shopId = null, $isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = self::getUrl($shopId) . '/' . $parentCategoryId . '/ownedCategory/' . $categoryId;
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url, [], [], $isAsync);
        $rest->_call($promise);
    }

    /**
     * Get category localizations.
     * 
     * @return \shirtplatform\entity\localization\ProductCategoryLocalized[]
     */
    public function getLocalizations(WsParameters $wsParams = NULL, $isAsync = false)
    {
        return $this->getAtomLinkValue('localizations', $wsParams, $isAsync);
    }

    /**
     * Duplicate an category.
     * 
     * @param int $categoryId
     * @param int|null $shopId
     * @return ProductCategory
     */
    public static function &duplicate($categoryId, $shopId = null)
    {
        $url = self::getUrl($shopId);
        $url .= '/duplicate/' . $categoryId;

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [], false);
        $promise->shopId = $shopId;

        return $rest->_call($promise);
    }

    /**
     * Import an own category.
     * 
     * @param int $categoryId
     * @param int $dstShopId
     * @return ProductCategory
     */
    public static function &importOwn($categoryId, $dstShopId = null)
    {
        $url = self::getUrl($dstShopId);
        $url .= '/importOwn/' . $categoryId;

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [], false);
        $promise->shopId = $dstShopId;

        return $rest->_call($promise);
    }

    /**
     * Find threaded categories.
     * 
     * @param int $categoryId
     * @param int|null $shopId
     * @return ProductCategory[]
     */
    public static function &findThreadCategories($categoryId, $shopId = null)
    {
        $url = self::getUrl($shopId) . '/' . $categoryId . '/thread';

        return self::findAllFromUrl($url, null, $shopId);
    }

    /**
     * Find child categories.
     * 
     * @param int $parentCategoryId
     * @param WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return ProductCategory[]
     */
    public static function &findChildCategories($parentCategoryId = '', $wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        $result = self::findAllFromUrl(self::getUrl($shopId) . '/' . $parentCategoryId . '/ownedCategory', $wsParameters, $shopId, [], $isAsync);

        if (isset($result[$parentCategoryId]))
        {
            unset($result[$parentCategoryId]);
        }

        return $result;
    }

    /**
     * Find category roots.
     * 
     * @param WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return ProductCategory[]
     */
    public static function &findRoots($wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        $url = self::getUrl($shopId);
        $url .= '/roots';
        return self::findAllFromUrl($url, $wsParameters, $shopId, [], $isAsync);
    }

}
