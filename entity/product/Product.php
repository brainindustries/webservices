<?php

namespace shirtplatform\entity\product;

/**
 * Description of Product
 *
 * @author Jan Maslik
 */
use \User;
use \shirtplatform\rest\REST;
use \DebugException;
use \shirtplatform\parser\WsModel;
use \shirtplatform\parser\WsParse;
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\filter\Filter;
use \shirtplatform\filter\WsParameters;
use \shirtplatform\entity\sharing\synchronize\SynchronizeProduct;

class Product extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products';
    const PATH_ALL = 'accounts/{accountId}/products';
    const PATH_CATEGORY_ASSIGN_ALL = 'accounts/{accountId}/shops/{shopId}/products/assignedCategories';
    const VAR_NAME = 'product';

    public static $IMG_COUNTER = 1;
    public static $classMap = array(
        'type' => '\shirtplatform\entity\product\ProductType',
        'assignedCategories' => '\shirtplatform\entity\product\CategoryAssignedToProduct',
        'assignedColors' => '\shirtplatform\entity\product\AssignedProductColor',
        'assignedSizes' => '\shirtplatform\entity\product\AssignedProductSize',
        'assignedViews' => '\shirtplatform\entity\product\AssignedProductView',
        'localizations' => '\shirtplatform\entity\localization\ProductLocalized',
        'assignedViewColors' => '\shirtplatform\entity\product\ProductViewColorDetail',
        'assignedPrintTechnologies' => '\shirtplatform\entity\product\AssignedPrintTechnology',
        'assignedAttributeListItems' => '\shirtplatform\entity\product\AssignedAttributeListItem',
        'prices' => '\shirtplatform\entity\product\ProductPrice',
        'assignedFactories' => '\shirtplatform\entity\product\AssignedProductFactory',
        'photoDetails' => '\shirtplatform\entity\product\ProductPhotoDetail',
        'photoCharacters' => '\shirtplatform\entity\product\ProductPhotoCharacter',
        'photoMeasures' => '\shirtplatform\entity\product\ProductPhotoMeasure',
        'sku' => '\shirtplatform\entity\product\ProductSku',
        'childVersion' => '\shirtplatform\entity\abstraction\BasicDataStructure',
        'pool' => '\shirtplatform\entity\abstraction\Entity'
    );
    public $sourceId;
    public $name = null;
    public $artNr = null;
    public $model = null;
    public $hsCode = null;
    public $active = null;
    public $deleted = null;
    public $type = null;
    public $version;
    public $orderIndex;
    public $pool;
    //layze
    protected $assignedCategories = null;
    protected $assignedColors = null;
    protected $assignedSizes = null;
    protected $assignedViews = null;
    protected $localizations = null;
    protected $assignedViewColors = null;
    protected $assignedPrintTechnologies = null;
    protected $assignedAttributeListItems = null;
    protected $prices = null;
    protected $assignedFactories = null;
    protected $photoDetails = null;
    protected $photoCharacters = null;
    protected $photoMeasures = null;
    protected $sku = null;
    protected $childVersion;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);

        if ($this->getAtomLink('preview') != null)
        {
            $previewUrl = str_replace(\shirtplatform\constants\WsConstants::IMG_REPLACEMENT, self::$IMG_COUNTER . \shirtplatform\constants\WsConstants::IMG_REPLACEMENT, $this->getAtomLink('preview'));
            $this->setAtomLink('preview', $previewUrl);
            self::$IMG_COUNTER = (self::$IMG_COUNTER >= 5 ? 1 : self::$IMG_COUNTER + 1 );
        }
    }

    /**
     * Get localizations.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\localization\ProductLocalized[]
     */
    public function &getLocalizations(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('localizations', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get assigned categories.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\CategoryAssignedToProduct[]
     */
    public function &getAssignedCategories(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('assignedCategories', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get assigned colors.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\AssignedProductColor[]
     */
    public function &getAssignedColors(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('assignedColors', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get assigned sizes.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\AssignedProductSize[]
     */
    public function &getAssignedSizes(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('assignedSizes', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get assigned views.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\AssignedProductView[]
     */
    public function &getAssignedViews(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('assignedViews', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get assigned print technologies.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\AssignedPrintTechnology[]
     */
    public function getAssignedPrintTechnologies(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('assignedPrintTechnologies', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get assigned attribute list items.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\AssignedAttributeListItem[]
     */
    public function getAssignedAttributeListItems(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('assignedAttributeListItems', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get prices.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\ProductPrice[]
     */
    public function getPrices(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('prices', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get assigned factories.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\AssignedProductFactory[]
     */
    public function getAssignedFactories(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('assignedFactories', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get photo details.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\ProductPhotoDetail[]
     */
    public function getPhotoDetails(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('photoDetails', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get photo characters.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\ProductPhotoCharacter[]
     */
    public function getPhotoCharacters(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('photoCharacters', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get photo measures.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\ProductPhotoMeasure[]
     */
    public function getPhotoMeasures(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('photoMeasures', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get SKUs.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\ProductSku[]
     */
    public function getSku(WsParameters $wsParameters = null, $isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('sku', $wsParameters, $isAsync, $forceLoad);
    }

    /**
     * Get preview image url.
     * 
     * @return string
     */
    public function getPreviewUrl()
    {
        return $this->getAtomLink('preview');
    }

    /**
     * Get child version.
     * 
     * @return int
     */
    public function getChildVersion()
    {
        return $this->loadAtomLinkValueItem('childVersion');
    }
    
    /**
     * Load all assignedViewColors & sets assignedViews and assignedColors.
     * 
     * @param WsParameters|null $wsParameter
     * @param boolean $isAsync
     * @param boolean $forceReload
     * @return \shirtplatform\entity\product\ProductViewColorDetail[]
     */
    public function getAssignedViewColors($wsParameter = null, $isAsync = false, $forceReload = false)
    {

        $updateViewAndColors = false;

        if ($this->assignedViewColors == null || $forceReload)
        {
            $updateViewAndColors = true;
        }

        $viewColors = $this->getAtomLinkValue('assignedViewColors', $wsParameter, $isAsync, $forceReload);

        if ($updateViewAndColors)
        {
            if ($this->assignedColors == null || $forceReload)
            {
                $this->assignedColors = \shirtplatform\parser\WsParse::joinAttributes($viewColors, 'assignedColor', false, true);
            }

            if ($this->assignedViews == null || $forceReload)
            {
                $this->assignedViews = \shirtplatform\parser\WsParse::joinAttributes($viewColors, 'assignedView', false, true);
            }

            if (is_array($viewColors))
            {
                foreach ($viewColors as $viewColorEntity)
                {
                    $this->assignedViews[$viewColorEntity->assignedView->id]->addAssignedViewColor($viewColorEntity);
                }

                foreach ($this->assignedColors as &$assignedColor)
                {
                    $assignedColor->setParents($this->id);
                }

                foreach ($this->assignedViews as &$assignedView)
                {
                    $assignedView->setParents($this->id);
                }
            }
        }

        return $viewColors;
    }

    /**
     * Duplicate an product.
     * 
     * @return Product
     */
    public function __duplicate()
    {
        return self::duplicate($this->id, $this->getShopId());
    }

    /**
     * Set pool.
     * 
     * @param int $poolId
     * @return void
     */
    public function __setPool($poolId)
    {
        return self::setPool($this->id, $poolId, $this->getShopId());
    }

    /**
     * Set pool.
     * 
     * @param int $productId
     * @param int $poolId
     * @param int|null $shopId
     * @param boolean $isAsync
     */
    public static function setPool($productId, $poolId, $shopId = null, $isAsync = false)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = self::getUrl($shopId);
        $url .= '/' . $productId . '/pools/' . $poolId;
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url, [], [], $isAsync);
        $rest->_call($promise);
    }

    /**
     * Duplicate an product.
     * 
     * @param int $productId
     * @param int|null $shopId
     * @return Product
     */
    public static function duplicate($productId, $shopId = null)
    {
        $url = self::getUrl($shopId);
        $url .= '/' . $productId . '/duplicate';
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [], false);
        $promise->shopId = $shopId;

        return $rest->_call($promise);
    }

    /**
     * Synchronize product.
     * 
     * @param int $productId
     * @param \shirtplatform\entity\sharing\synchronize\SynchronizeProduct $syncStuct
     * @param boolean $shopId
     * @param boolean $isAsync
     * @return void
     */
    public static function &synchronize($productId, SynchronizeProduct $syncStuct = null, $shopId = null, $isAsync = false)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        if ($syncStuct == null)
        {
            $syncStuct = new SynchronizeProduct();
        }
        $url = self::getUrl($shopId);
        $url .= '/' . $productId . '/synchronize';
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], array(SynchronizeProduct::VAR_NAME => $syncStuct), $isAsync);
        $promise->shopId = $shopId;
        return $rest->_call($promise);
    }

     /**
     * Synchronize source product by desination copy.
     * 
     * @param int $productId destination product 
     * @param \shirtplatform\entity\sharing\synchronize\SynchronizeProduct $syncStuct
     * @param boolean $shopId destination shop
     * @param boolean $isAsync
     * @return void
     */
    public static function &revertedSynchronize($productId, SynchronizeProduct $syncStuct = null, $shopId = null, $isAsync = false)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        if ($syncStuct == null)
        {
            $syncStuct = new SynchronizeProduct();
        }
        $url = self::getUrl($shopId);
        $url .= '/' . $productId . '/revertedsynchronize';
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], array(SynchronizeProduct::VAR_NAME => $syncStuct), $isAsync);
        $promise->shopId = $shopId;
        return $rest->_call($promise);
    }
    
    /**
     * Find all product of account.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameter
     * @return Product[]
     */
    public static function &findAllByAccount(\shirtplatform\filter\WsParameters $wsParameter = null)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_ALL);
        return self::findAllFromUrl($url, $wsParameter); //set parents by url
    }

    /**
     * Count all products of account.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @return int
     */
    public static function &countByAccount($wsParameters = null, $isAsync = false)
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $wsParameters->page = 0;
        $wsParameters->size = 1;
        $wsParameters->setRootEntityName('\shirtplatform\entity\product\Product');
        $rest = \shirtplatform\rest\REST::getInstance();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_ALL);

        $promise = new \shirtplatform\rest\promise\CountPromise('GET', $url, $wsParameters->buildParams(), [], $isAsync);
        $promise->filterPromise = $wsParameters->getPromise(false);
        return $rest->_call($promise);
    }

    /**
     * Move product.
     * 
     * @param int $productId
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return void
     */
    public static function &move($productId, $shopId = null, $isAsync = false)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = self::getUrl($shopId);
        $url .= '/move/' . $productId;
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], [], $isAsync);
        $promise->shopId = $shopId;
        return $rest->_call($promise);
    }

    /**
     * Set active.
     * 
     * @param boolean $isActive
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @return void
     */
    public static function setActive($isActive, $wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        $loadAll = false;

        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        $wsParameters->setRootEntityName(get_called_class());

        $url = self::getUrl($shopId);
        $url .= '/active/' . ($isActive ? 'true' : 'false');

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url, $wsParameters->buildParams(), array(), false);
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);

        $result = intval($promise->message);

        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::setActive($isActive, $wsParameters, $shopId);
        }
        return $result;
    }

    /**
     * Move multiple products.
     * 
     * @param int $newShopId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @return void
     */
    public static function moveAll($newShopId, $wsParameters, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $loadAll = false;

        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        $wsParameters->setRootEntityName(get_called_class());

        $url = self::getUrl($shopId);
        $url .= '/moveAll/' . $newShopId;
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url, $wsParameters->buildParams(), array(), false);
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);

        $result = intval($promise->message);

        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::moveAll($newShopId, $wsParameters, $shopId);
        }
        return $result;
    }

    /**
     * Assign multiple categories.
     * 
     * @param int $newCategory
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @return void
     */
    public static function assignCategoryAll($newCategory, $wsParameters = null, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName(get_called_class());

        $rest = \shirtplatform\rest\REST::getInstance();
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_CATEGORY_ASSIGN_ALL);
        $url = str_replace('{shopId}', $shopId, $url);

        $options = [
            'json' => [$newCategory::VAR_NAME => $newCategory->getWebserviceAttributes()]
        ];

        $loadAll = false;
        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        $promise = new \shirtplatform\rest\promise\JsonPromise('POST', $url, $wsParameters->buildParams(), $options, false);
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);

        $result = intval($promise->message);
        
        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::deleteCategoryAll($wsParameters, $shopId);
        }
        return $result;
    }

    /**
     * Delete multiple category assignments.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @return void
     */
    public static function deleteCategoryAll($wsParameters = null, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $loadAll = false;

        if ($wsParameters->size == null)
        {
            $wsParameters->size = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $loadAll = true;
        }

        $wsParameters->setRootEntityName(get_called_class());

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_CATEGORY_ASSIGN_ALL);
        $url = str_replace('{shopId}', $shopId, $url);
        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $url, $wsParameters->buildParams(), [], false);
        $promise->filterPromise = $wsParameters->getPromise(false);
        $rest->_call($promise);

        $result = intval($promise->message);

        if ($loadAll && $result === \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE)
        {
            $wsParameters->page++;
            $wsParameters->size = null;
            $result += self::deleteCategoryAll($wsParameters, $shopId);
        }
        return $result;
    }

}
