<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of AreaGeomRect
 *
 * @author Jan Maslik
 */
class AreaGeomRect extends \shirtplatform\entity\abstraction\JsonEntity
{
	const VAR_NAME = 'areaGeomRect';
	
	public $xTwips;
	public $yTwips;
	public $widthTwips;
	public $heightTwips;
	
	public static $classMap = array(
	);
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}

?>
