<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductColor
 *
 * @author Jan Maslik
 */
use \User;

class ProductColor extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/colorPools/{parentId}/colors';
    //const PATH_ALLOWED_REFERENCES = 'accounts/{accountId}/productTypes/{parentId}/colorPools/{parentId}/colors/allowedReferencedColors';
    const PATH_ALL_COLORS = 'accounts/{accountId}/colors';
    const VAR_NAME = 'productColor';

    public static $classMap = array(
        'values' => '\shirtplatform\entity\product\ProductColorValue',
        'localizations' => '\shirtplatform\entity\localization\ProductColorLocalized',
    );
    public $name;
    public $version;
    public $orderIndex;
    //layze
    protected $values = null;
    protected $localizations = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get color values.
     * 
     * @param boolean $isAsync
     * @param boolean $forceLoad
     * @return \shirtplatform\entity\product\ProductColorValue[]
     */
    public function getValues($isAsync = false, $forceLoad = false)
    {
        return $this->getAtomLinkValue('values', null, $isAsync, $forceLoad);
    }

    /**
     * Get localizations.
     * 
     * @return \shirtplatform\entity\localization\ProductColorLocalized[]
     */
    public function getLocalizations(\shirtplatform\filter\WsParameters $wsParams = null, $isAsync = false)
    {
        return $this->getAtomLinkValue('localizations', $wsParams, $isAsync);
    }

    /**
     * Get image preview url.
     * 
     * @return string
     */
    public function getPreviewUrl()
    {
        return $this->getAtomLink('preview');
    }

    /**
     * Find all colors in account.
     * 
     * @param \shirtplatform\filter\WsParameters|null $wsParameter
     * @param boolean $isAsync
     * @return ProductColor[]
     */
    public static function &findAllByAccount(\shirtplatform\filter\WsParameters $wsParameter = null, $isAsync = false)
    {
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_ALL_COLORS);
        return self::findAllFromUrl($url, $wsParameter, null, null, $isAsync);
    }

    /**
     * Get color pool Id.
     * 
     * @return int
     */
    public function getPoolId()
    {
        $s = $this->getAtomLink('self');
        $a = explode('/', $s);
        return $a[10];
    }

    /**
     * Find colors which can be assigned as reference.
     * 
     * @param array $parents
     * @return ProductColor[]
     */
    public static function &findReferenceableColors($parents , \shirtplatform\filter\WsParameters $ws = null, $isAsync = false)
    {
        $url = self::getUrl($parents) . '/allowedReferencedColors';

        return self::findAllFromUrl($url, $ws , null, $parents, $isAsync);
    }

}
