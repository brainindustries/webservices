<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductColorValue
 *
 * @author Jan Maslik
 */
class ProductColorValue extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/colorPools/{parentId}/colors/{parentId}/values';
    const VAR_NAME = 'productColorValue';

    public static $classMap = array(
        'reference' => '\shirtplatform\entity\product\ProductColorValue'
    );
    public $colorValue;
    public $orderIndex;
    public $version;
    public $reference = null;
    public $textureId;
    public $tiled;
    public $colorId;
    public $deleted;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get preview image url.
     * 
     * @return string
     */
    public function getPreviewUrl()
    {
        return $this->getAtomLink('preview');
    }

}
