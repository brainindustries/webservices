<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of AssignedAttributeListItem
 *
 * @author Jan Maslik
 */
class AssignedAttributeListItem extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedAttributeListItems';
    const VAR_NAME = 'assignedAttributeListItem';

    public static $classMap = array(
        'listItem' => '\shirtplatform\entity\product\ProductAttributeListItem',
    );
    public $listItem;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}

?>
