<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of AssignedProductColor
 *
 * @author Jan Maslik
 */
class AssignedProductColor extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedColors';
    const VAR_NAME = 'assignedProductColor';

    public static $classMap = array(
        'productColor' => '\shirtplatform\entity\product\ProductColor',
    );
    public $orderIndex;
    public $default;
    public $active;
    public $version;
    public $productColor = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get product color.
     * 
     * @return \shirtplatform\entity\product\ProductColor
     */
    public function getProductColor()
    {
        return $this->productColor;
    }

}
