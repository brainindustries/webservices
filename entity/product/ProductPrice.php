<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductPrice
 *
 * @author Jan Maslik
 */
class ProductPrice extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/prices';
    const VAR_NAME = 'productPrice';

    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country',
        'assignedColor' => '\shirtplatform\entity\product\AssignedProductColor',
        'assignedSize' => '\shirtplatform\entity\product\AssignedProductSize',
    );
    public $country;
    public $price;
    public $version;
    public $assignedColor;
    public $assignedSize;
    public $amount;
    public $margin = 0;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
