<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductTypeAttributeListPrime
 *
 * @author Jan Maslik
 */
class ProductTypeAttributeList extends \shirtplatform\entity\abstraction\BaseWithParentDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/productTypes/{parentId}/attributeLists';
    const VAR_NAME = 'attributeList';

    public static $classMap = array(
        'listItems' => '\shirtplatform\entity\product\ProductTypeAttributeListItem',
        'localizations' => '\shirtplatform\entity\localization\ProductTypeAttributeListLocalized',
        'flags' => '\shirtplatform\entity\product\ProductTypeAttributeFlag',
    );
    public $name;
    public $version;
    public $containPicture;
    protected $localizations;
    protected $listItems;
    protected $flags;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get list items.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\product\ProductTypeAttributeListItem[]
     */
    public function getItems($isAsync = false)
    {
        return $this->getAtomLinkValue('listItems', null, $isAsync);
    }

    /**
     * Get localizations.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\entity\localization\ProductTypeAttributeListLocalized[]
     */
    public function getLocalizations($isAsync = false)
    {
        return $this->getAtomLinkValue('localizations', null, $isAsync);
    }

    /**
     * Get list flags.
     * 
     * @return \shirtplatform\entity\product\ProductTypeAttributeFlag[]
     */
    public function getFlags()
    {
        return $this->getAtomLinkValue('flags');
    }

}
