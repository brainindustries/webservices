<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductCategoryTree
 *
 * @author Jan Maslik
 */
use \User;
use \shirtplatform\rest\REST;
use shirtplatform\filter\WsParameters;
use shirtplatform\parser\WsModel;
use shirtplatform\parser\WsParse;
use shirtplatform\filter\Filter;
use \DebugException;

class ProductCategoryTree extends \shirtplatform\entity\abstraction\JsonEntity
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/productCategories/trees';
    const VAR_NAME = 'productCategoryTree';

    public static $classMap = array(
        'category' => '\shirtplatform\entity\product\ProductCategory'
    );
    public $category = null;
    public $parentId;
    public $childs = array();

    /**
     * Get url path string.
     * 
     * @param int|null $shopId
     * @return string
     */
    public static function getUrl(&$shopId = null)
    {
        $className = get_called_class();
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $className::PATH_TEMPLATE);
        $url = str_replace('{shopId}', $shopId, $url);

        return $url;
    }

    /**
     * Find parent category tree.
     * 
     * @param int $categoryId
     * @param WsParameters|null $wsParameters
     * @param int|null $shopId
     * @return ProductCategoryTree
     */
    public static function findParentTree($categoryId, $wsParameters = null, $shopId = NULL)
    {
        $entities = self::findParents($categoryId, $wsParameters, $shopId);

        if (empty($entities))
        {
            return null;
        }

        $rootEntities = array();

        foreach ($entities as $entity)
        {
            if ($entity->parentId == null)
            {
                $rootEntities[$entity->id] = $entity;
            }
            else
            {
                $entities[$entity->parentId]->childs[$entity->id] = $entity;
            }
        }
        return reset($rootEntities);
    }

    /**
     * Find child category tree.
     * 
     * @param int $categoryId
     * @param WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return ProductCategoryTree
     */
    public static function &findChildTree($categoryId, $wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        $entities = self::findChilds($categoryId, $wsParameters, $shopId, $isAsync);

        $rootEntity = array();

        foreach ($entities as $entity)
        {
            if ($entity->parentId == null)
            {
                $rootEntity[$entity->id] = $entity;
            }
            else
            {
                $entities[$entity->parentId]->childs[$entity->id] = $entity;
            }
        }

        return $rootEntity;
    }

    /**
     * Find parent category trees.
     * 
     * @param int $categoryId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return ProductCategoryTree[]
     */
    public static function &findParents($categoryId, $wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\product\ProductCategory'); //use motive category filter
        $url = self::getUrl($shopId) . '/' . $categoryId . '/parents';

        return self::findAllFromUrl($url, $wsParameters, $shopId, [], $isAsync);
    }

    /**
     * Find child category trees.
     * 
     * @param int $categoryId
     * @param \shirtplatform\filter\WsParameters|null $wsParameters
     * @param int|null $shopId
     * @param boolean $isAsync
     * @return ProductCategoryTree[]
     */
    public static function &findChilds($categoryId, $wsParameters = null, $shopId = NULL, $isAsync = false)
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName('\shirtplatform\entity\product\ProductCategory'); //use motive category filter

        $url = self::getUrl($shopId) . '/' . $categoryId . '/childs';

        return self::findAllFromUrl($url, $wsParameters, $shopId, [], $isAsync);
    }

}
