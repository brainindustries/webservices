<?php

namespace shirtplatform\entity\product;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssignedPrintTechnologyBrother
 *
 * @author admin
 */
class AssignedPrintTechnologyBrother extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedPrintTechnologies/{parentId}/printTechnologyBrothers';
    const VAR_NAME = 'assignedPrintTechnologyBrother';

    public static $classMap = array(
	'assignedView' => '\shirtplatform\entity\product\AssignedProductView',
	'assignedColor' => '\shirtplatform\entity\product\AssignedProductColor',
        'assignedSize' => '\shirtplatform\entity\product\AssignedProductSize'
    );
	
    public $assignedView;
    public $assignedColor;
    public $assignedSize;
    public $type;
    
    public $pretreadSpeed;
    public $pretreadDirectionDouble;
    public $inkVolume;
    public $colorMultiplePass;
    public $printBidirectional;

    public $version;

    public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
    {
        parent::__construct($data,$parents,$foreignKeyOnly);
    }
    
}
