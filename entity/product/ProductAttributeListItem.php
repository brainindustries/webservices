<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\product;

/**
 * Description of ProductAttributeListItem
 *
 * @author Jan Maslik
 */
class ProductAttributeListItem extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	//const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/{parentId}/assignedAttributeListItems';
	const VAR_NAME = 'productAttributeListItem';
	
	public static $classMap = array(
		'attributeList' => '\shirtplatform\entity\product\ProductTypeAttributeList',
	);
	
	public $name;
	public $attributeList;
	public $version;
	
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}

?>
