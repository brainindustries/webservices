<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImportedAssignedView
 *
 * @author Jan Maslik
 */
class ImportedAssignedView extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/imported/products/{parentId}/assignedViews';
    const VAR_NAME = 'importedAssignedView';

    public static $classMap = array(
        'source' => '\shirtplatform\entity\product\AssignedProductView',
        'destination' => '\shirtplatform\entity\product\AssignedProductView'
    );
    public $source;
    public $destination;
    public $created;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
