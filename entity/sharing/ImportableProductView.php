<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImportableProductView
 *
 * @author Jan Maslik
 */
class ImportableProductView extends \shirtplatform\entity\abstraction\JsonEntity
{
    //const PATH_TEMPLATE = 'accounts/{accountId}/sharing/products';

    const VAR_NAME = 'importableView';

    public static $classMap = array(
    );
    public $name;

    public function __construct($data = null, $foreignKeyOnly = false)
    {
        parent::__construct($data, $foreignKeyOnly);
    }

}
