<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ImportablePrintMediaSizePalette extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/sharing/importablePrintMediaSizes';
    const VAR_NAME = 'importablePrintMediaSizePalette';

    public static $classMap = array(
        'printMediaSizePalette' => '\shirtplatform\entity\technology\PrintMediaSizePalette'
    );
    public static $classArrayMap = array(
        'printMediaSizes' => '\shirtplatform\entity\technology\PrintMediaSize'
    );
    public $printMediaSizePalette;
    public $printMediaSizes;
    public $version;

    public function __construct($data = null, $foreignKeyOnly = false)
    {
        parent::__construct($data, $foreignKeyOnly);
    }

}
