<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImportableProductAssignedColor
 *
 * @author Jan Maslik
 */
class ImportableProductAssignedColor extends \shirtplatform\entity\abstraction\JsonEntity
{
    //const PATH_TEMPLATE = 'accounts/{accountId}/sharing/products';

    const VAR_NAME = 'importableAssignedColor';

    public static $classMap = array(
        'color' => 'shirtplatform\entity\sharing\ImportableProductColor'
    );
    public $color;

    public function __construct($data = null, $foreignKeyOnly = false)
    {
        parent::__construct($data, $foreignKeyOnly);
    }

}
