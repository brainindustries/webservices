<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImportedProductPhotoMeasure
 *
 * @author Jan Maslik
 */
class ImportedProductPhotoMeasure extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/imported/products/{parentId}/photo/measures';
    const VAR_NAME = 'importedProductPhotoMeasure';

    public static $classMap = array(
        'source' => '\shirtplatform\entity\product\ProductPhotoMeasure',
        'destination' => '\shirtplatform\entity\product\ProductPhotoMeasure'
    );
    public $source;
    public $destination;
    public $created;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
