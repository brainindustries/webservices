<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImportablePrintColorPalette
 *
 * @author Jan Maslik
 */
class ImportablePrintColorPalette extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/sharing/importablePrintColors';
    const VAR_NAME = 'importablePrintColorPalette';

    public static $classMap = array(
        'printColorPalette' => '\shirtplatform\entity\technology\PrintColorPalette'
    );
    public static $classArrayMap = array(
        'printColors' => '\shirtplatform\entity\technology\PrintColor'
    );
    public $printColors;
    public $printColorPalette;
    public $version;

    public function __construct($data = null, $foreignKeyOnly = false)
    {
        parent::__construct($data, $foreignKeyOnly);
    }

}
