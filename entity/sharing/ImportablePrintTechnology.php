<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImportablePrintTechnology
 *
 * @author Jan Maslik
 */
class ImportablePrintTechnology extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/sharing/importablePrintTechnologies';
    const VAR_NAME = 'importablePrintTechnology';

    public static $classMap = array(
        'printTechnology' => '\shirtplatform\entity\technology\PrintTechnology'
    );
    public $printTechnology;
    public $version;

    public function __construct($data = null, $foreignKeyOnly = false)
    {
        parent::__construct($data, $foreignKeyOnly);
    }

}
