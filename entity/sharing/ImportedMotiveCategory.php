<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use \shirtplatform\rest\REST,
    \WsParse;

/**
 * Description of ImportedMotiveCategory
 *
 * @author Jan Maslik
 */
class ImportedMotiveCategory extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/imported/motiveCategories';
    const PATH_TEMPLATE_THREAD = 'accounts/{accountId}/shops/{shopId}/imported/motiveCategories/thread';
    const VAR_NAME = 'importedMotiveCategory';

    public static $classMap = array(
        'source' => '\shirtplatform\entity\motive\MotiveCategory',
        'destination' => '\shirtplatform\entity\motive\MotiveCategory'
    );
    public $source;
    public $destination;
    public $created;
    public $version;
    public $sourceType;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Find threaded categories.
     * 
     * @param int $srcCategoryId
     * @param int $dstShopId
     * @param boolean $isAsync
     * @return ImportedMotiveCategory[]
     */
    public static function &findThreaded($srcCategoryId, $dstShopId = null, $isAsync = false)
    {
        $url = self::getUrl($dstShopId) . '/thread/' . $srcCategoryId;

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), [], [], $isAsync);
        $promise->shopId = $dstShopId;
        return $rest->_call($promise);
    }

}
