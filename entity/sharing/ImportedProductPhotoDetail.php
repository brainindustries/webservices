<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImportedProductPhotoDetail
 *
 * @author Jan Maslik
 */
class ImportedProductPhotoDetail extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/imported/products/{parentId}/photo/details';
    const VAR_NAME = 'importedProductPhotoDetail';

    public static $classMap = array(
        'source' => '\shirtplatform\entity\product\ProductPhotoDetail',
        'destination' => '\shirtplatform\entity\product\ProductPhotoDetail'
    );
    public $source;
    public $destination;
    public $created;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
