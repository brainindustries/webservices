<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImportableProduct
 *
 * @author Jan Maslik
 */
class ImportableProduct extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/sharing/products';
    const VAR_NAME = 'imporableProduct';

    public static $classMap = array(
    );
    public static $classArrayMap = array(
        'assignedColors' => 'shirtplatform\entity\sharing\ImportableProductAssignedColor',
        'assignedViews' => 'shirtplatform\entity\sharing\ImportableProductAssignedView',
        'sizes' => 'shirtplatform\entity\sharing\ImportableProductSize',
    );
    public $productId;
    public $name;
    public $price;
    public $premiumPrice;
    public $productTypeName;
    public $shopName;
    public $version;
    public $premium;
    public $onChangeMode;
    public $assignedColors;
    public $assignedViews;
    public $sizes;
    public $additionPhotoContent;
    public $areaCount;

    public function __construct($data = null, $foreignKeyOnly = false)
    {
        parent::__construct($data, $foreignKeyOnly);
    }

}
