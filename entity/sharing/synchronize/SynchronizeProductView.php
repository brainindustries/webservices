<?php

namespace shirtplatform\entity\sharing\synchronize;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SynchronizeProductView
 *
 * @author Jan Maslik
 */
class SynchronizeProductView extends \shirtplatform\entity\abstraction\Model
{

    public $viewAssignment = true;
    public $colorTuning = true;
    public $subviews = true;
    public $photos = true;

}
