<?php

namespace shirtplatform\entity\sharing\synchronize;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use shirtplatform\parser\WsModel;

/**
 * Description of SynchronizeProduct
 *
 * @author Jan Maslik
 */
class SynchronizeProduct extends \shirtplatform\entity\abstraction\Model
{

    const VAR_NAME = 'synchronizeProduct';

    public $properties;
    public $views;
    public $areas;
    public $technologies;
    public $skus;

    public function __construct($data = null)
    {
        $this->properties = new SynchronizeProductProperties();
        $this->views = new SynchronizeProductView();
        $this->areas = new SynchronizeProductArea();
        $this->technologies = new SynchronizePrintTechnology();
        $this->skus = new SynchronizeStockControll();

        if ($data != null)
        {
            \shirtplatform\parser\WsModel::copy($data, $this);
        }
    }

}
