<?php
namespace shirtplatform\entity\sharing\synchronize;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SynchronizeMotive
 *
 * @author Jan Maslik
 */
class SynchronizeMotive extends \shirtplatform\entity\abstraction\Model
{
    const VAR_NAME = 'synchronizeMotive';

    public $properties;
    public $imageSetup = true;
    public $technologySetup = true;

    public function __construct($data = null)
    {
        $this->properties = new SynchronizeMotiveProperty();
        
        if ($data != null)
        {
            \shirtplatform\parser\WsModel::copy($data, $this);
        }
    }
}
