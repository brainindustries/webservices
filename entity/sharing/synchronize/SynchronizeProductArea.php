<?php

namespace shirtplatform\entity\sharing\synchronize;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SynchronizeProductArea
 *
 * @author Jan Maslik
 */
class SynchronizeProductArea extends \shirtplatform\entity\abstraction\Model
{

    public $areas = true;
    public $collisions = true;
    public $viewPoints = true;
    public $overlays = true;
    public $humanView = true;

}
