<?php

namespace shirtplatform\entity\sharing\synchronize;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SynchronizeProductProperties
 *
 * @author Jan Maslik
 */
class SynchronizeProductProperties extends \shirtplatform\entity\abstraction\Model
{

    public $name = true;
    public $description = true;
    public $attributes = true;
    public $customAttrributes = true;
    public $sizes = true;
    public $colors = true;
    public $price = true;

}
