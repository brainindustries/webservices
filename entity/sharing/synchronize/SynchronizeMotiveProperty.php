<?php
namespace shirtplatform\entity\sharing\synchronize;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SynchronizeMotiveProperty
 *
 * @author Jan Maslik
 */
class SynchronizeMotiveProperty extends \shirtplatform\entity\abstraction\Model
{
    public $name = true;
    public $complexicityFactor = true;
    public $designer = true;
    public $enabledCountries = true;
    public $priceSetup = true;
    public $tags = true;
}
