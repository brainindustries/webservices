<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImportedAssignedSize
 *
 * @author Jan Maslik
 */
use shirtplatform\entity\enumerator\ImportedSourceType;

class ImportedAssignedSticker extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/imported/products/{parentId}/assignedViews/{parentId}/assignedStickers';
    const VAR_NAME = 'importedProductAssignedSticker';

    public static $classMap = array(
        'source' => '\shirtplatform\entity\product\AssignedProductSticker',
        'destination' => '\shirtplatform\entity\product\AssignedProductSticker'
    );
    public $source;
    public $destination;
    public $created;
    public $version;
    public $sourceType = ImportedSourceType::OWN;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
