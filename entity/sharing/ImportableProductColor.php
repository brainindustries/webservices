<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImportableProductColor
 *
 * @author Jan Maslik
 */
class ImportableProductColor extends \shirtplatform\entity\abstraction\JsonEntity
{

    //const PATH_TEMPLATE = 'accounts/{accountId}/sharing/products';
    //const VAR_NAME = 'imporableProduct';
    public static $classMap = array(
    );
    public $name;

    public function __construct($data = null, $foreignKeyOnly = false)
    {
        parent::__construct($data, $foreignKeyOnly);
    }

    public function getPreviewUrl()
    {
        return $this->getAtomLink('preview');
    }

}
