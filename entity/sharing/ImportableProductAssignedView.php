<?php

namespace shirtplatform\entity\sharing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImportableProductAssignedView
 *
 * @author Jan Maslik
 */
class ImportableProductAssignedView extends \shirtplatform\entity\abstraction\JsonEntity
{
    //const PATH_TEMPLATE = 'accounts/{accountId}/sharing/products';

    const VAR_NAME = 'importableAssignedView';

    public static $classMap = array(
        'view' => 'shirtplatform\entity\sharing\ImportableProductView'
    );
    public $view;

    public function __construct($data = null, $foreignKeyOnly = false)
    {
        parent::__construct($data, $foreignKeyOnly);
    }

}
