<?php

namespace shirtplatform\entity\sharing\publish;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use \User;
use \shirtplatform\rest\REST;
use \DebugException;
use \shirtplatform\parser\WsModel;
use \shirtplatform\parser\WsParse;
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\filter\Filter;
use \shirtplatform\filter\WsParameters;
use \shirtplatform\entity\sharing\synchronize\SynchronizeProduct;
use shirtplatform\entity\sharing\ImportedProduct;

/**
 * Description of Product 
 *
 * @author Jan Maslik
 */
class Product extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'publish/products';
    const PATH_IMPORT = 'publish/accounts/{accountId}/shops/{shopId}/products/import/{srcProductId}';
    const VAR_NAME = 'product';

    public static $classMap = array(
        'type' => '\shirtplatform\entity\product\ProductType',
    );
    public $sourceId;
    public $name = null;
    public $artNr = null;
    public $model = null;
    public $active = null;
    public $deleted = null;
    public $type = null;
    public $version;
    public $orderIndex;
    public $pool;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Import product from publish.
     * 
     * @param int $srcProductId
     * @param int $dstShopId
     * @return \shirtplatform\entity\sharing\ImportedProduct
     */
    public static function &importProduct($srcProductId, $dstShopId = null)
    {
        if ($dstShopId == null)
        {
            $dstShopId = \shirtplatform\utils\user\User::getShopId();
        }
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_IMPORT);
        $url = str_replace('{shopId}', $dstShopId, $url);
        $url = str_replace('{srcProductId}', $srcProductId, $url);

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, '\shirtplatform\entity\sharing\ImportedProduct', [], [], false);

        return $rest->_call($promise);
    }

}
