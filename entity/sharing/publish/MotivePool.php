<?php

namespace shirtplatform\entity\sharing\publish;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MotivePool
 *
 * @author Jan Maslik
 */
class MotivePool extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'publish/shops/{shopId}/pools/motives';
    const VAR_NAME = 'motivePool';

    public static $classMap = array(
    );
    public $name;
    public $version;
    public $orderIndex;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
