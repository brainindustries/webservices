<?php

namespace shirtplatform\entity\sharing\publish;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use \User;
use \shirtplatform\rest\REST;
use \DebugException;
use shirtplatform\entity\sharing\ImportedMotive;
use \shirtplatform\parser\WsModel;
use \shirtplatform\parser\WsParse;
use \shirtplatform\parser\ArrayUtil;
use \shirtplatform\filter\Filter;
use \shirtplatform\filter\WsParameters;

/**
 * Description of Motive 
 *
 * @author Jan Maslik
 */
class Motive extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'publish/motives';
    const PATH_IMPORT = 'publish/accounts/{accountId}/shops/{shopId}/motives/import/{srcMotiveId}';
    const VAR_NAME = 'motive';

    public static $classMap = array(
    );
    public $name;
    public $type;
    public $priceMultiplier;
    public $deleted;
    public $active;
    public $version;
    public $designerId;
    public $authorProvision;
    public $motiveProfit;
    public $overridePrice;
    public $exactPrice;
    public $pool;
    public $uuid;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Import motive from publish.
     * 
     * @param int $srcMotiveId
     * @param int $dstShopId
     * @return \shirtplatform\entity\sharing\ImportedMotive
     */
    public static function &importMotive($srcMotiveId, $dstShopId = null)
    {
        if ($dstShopId == null)
        {
            $dstShopId = \shirtplatform\utils\user\User::getShopId();
        }
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_IMPORT);
        $url = str_replace('{shopId}', $dstShopId, $url);
        $url = str_replace('{srcMotiveId}', $srcMotiveId, $url);

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, '\shirtplatform\entity\sharing\ImportedMotive', [], [], false);

        return $rest->_call($promise);
    }

}
