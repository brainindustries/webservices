<?php

namespace shirtplatform\entity\sharing\publish;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Shop
 *
 * @author Jan Maslik
 */
class Shop extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'publish/shops';
    const VAR_NAME = 'shop';

    public static $classMap = array(
    );
    public $name;
    public $version;
    public $type = \shirtplatform\entity\enumerator\ShopType::SHOP;
    public $default;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
