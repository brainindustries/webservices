<?php

namespace shirtplatform\entity\sharing\publish;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductPoolProperty
 *
 * @author Jan Maslik
 */
class ProductPoolProperty extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'publish/shops/{shopId}/pools/products/{parentId}/properties';
    const VAR_NAME = 'productPoolProperty';

    public static $classMap = array(
    );
    public $name;
    public $value;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
