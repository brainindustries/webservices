<?php

namespace shirtplatform\entity\stock;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StockToFactory
 *
 * @author admin
 */
class StockToFactory extends \shirtplatform\entity\abstraction\BaseDao
{
    
    const PATH_TEMPLATE = 'stock/{stockItemId}/factories/{factoryId}';
    
    public static $classMap = [
        'factory' => '\shirtplatform\entity\technology\Factory'
    ];

    public $id;
    public $plu;
    public $factory;
    public $version;
    public $segment;
    
    public function __update($stockItemId)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url();
        $url = str_replace('{stockItemId}', $stockItemId, $url);
        $url = str_replace('{factoryId}', $this->factory->id, $url);
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url, [], ['body' => json_encode($this->getWebserviceAttributes())], false);
        $promise->message = $this;
        $res = $rest->_call($promise);
        $this->id = $res['id'];
    }
    
    public static function delete($stockItemId, $factoryId)
    {
        $url = self::getUrl();
        $url = str_replace('{stockItemId}', $stockItemId, $url);
        $url = str_replace('{factoryId}', $factoryId, $url);
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $url, [], [], false);

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }
    
}
