<?php

namespace shirtplatform\entity\stock;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StockItem
 *
 * @author admin
 */
class StockItem extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'stock';
    
    public static $classMap = [
        
    ];
    
    public static $classArrayMap = [
        'factoryMutations' => '\shirtplatform\entity\stock\StockToFactory'
    ];
    
    public $name;
    public $ownerAccountId;
    public $active;
    public $factoryMutations = [];
    public $version;
    
    /**
     * 
     * @param \shirtplatform\filter\WsParameters $wsParameters
     * @param type $isAsync
     * @return type
     */
    public static function findAll(\shirtplatform\filter\WsParameters $wsParameters = null, $isAsync = false, callable $onFullfilled = null)
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        
        $promise = new \shirtplatform\rest\promise\ArrayPagePromise('POST', self::getUrl(), get_called_class(), [
            'page' => $wsParameters->page,
            'size' => $wsParameters->size
        ], $wsParameters->buildJsonFilter(), $isAsync);

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }
    
    public static function findAllExtended(\shirtplatform\filter\WsParameters $wsParameters = null, $isAsync = false)
    {
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }
        
        $promise = new \shirtplatform\rest\promise\ArrayPagePromise('POST', self::getUrl() . '/extended', get_called_class(), [
            'page' => $wsParameters->page,
            'size' => $wsParameters->size
        ], $wsParameters->buildJsonFilter(), $isAsync);

        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }
    
    /**
     * 
     */
    public function __update($isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url();
        $key = $this->getPrimaryKey();

        if(!empty($key))
        {
            $url .= '/' . $key;
        }
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url, [], ['body' => json_encode($this->getWebserviceAttributes())], false);
        $promise->message = $this;
        $res = $rest->_call($promise);
        $this->id = $res['id'];
    }
    
    public function __updatePrivate()
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url() . '/accounts/' . \shirtplatform\utils\user\User::getAccountId();
        $key = $this->getPrimaryKey();

        if(!empty($key))
        {
            $url .= '/' . $key;
        }
        else 
        {
            $url .= '/insertPrivate';
        }
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url, [], ['body' => json_encode($this->getWebserviceAttributes())], false);
        $promise->message = $this;
        $res = $rest->_call($promise);
        $this->id = $res['id'];
    }
    
    public function set($data)
    {
        if (!is_array($data) && !is_object($data))
        {
            return;
        }

        foreach ($data AS $key => $value)
        {
            if (!property_exists($this, $key) && $key != 'atom.link')
            {
                if (\shirtplatform\constants\WsConstants::$DEVELOPER_MODE)
                {
                    $this->unmapedAttributes[$key] = $value;
                }
                continue;
            }

            if (is_array($value))
            {
                
                if ($key == 'atom.link')
                {
                    self::processAtomLinks($value);
                } elseif (isset($this::$classMap[$key]))
                {
                    $className = $this::$classMap[$key];
                    $parents = $this->getParents();
                    $parents[] = $this->getPrimaryKey();
                    $this->{$key} = new $className($value, $parents);
                } elseif (isset($this::$classArrayMap) && isset($this::$classArrayMap[$key]))
                {
                    $this->{$key} = array();
                    if(empty($value))
                    {
                        continue;
                    }

                    /*reset($value);
                    $first_key = key($value);
                    if ($first_key != '0')
                    {
                        //contains just one item
                        $className = $this::$classArrayMap[$key];
                        $parents = $this->getParents();
                        $parents[] = $this->getPrimaryKey();
                        $instance = new $className($value, $parents);
                        $this->{$key}[$instance->id] = $instance;
                    } else
                    {*/
                        $i = 0;
                        foreach ($value as $valueData)
                        {
                            $className = $this::$classArrayMap[$key];
                            $parents = $this->getParents();
                            $parents[] = $this->getPrimaryKey();
                            $instance = new $className($valueData, $parents);
                            $this->{$key}[$i] = $instance;
                            $i++;
                        }
                    //}
                } else
                {

                    $this->{$key} = $value;
                }
            } else
            {
                $this->{$key} = $value;
            }
        }
    }
    
}
