<?php

namespace shirtplatform\entity\account;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserGrantedShopAccess
 *
 * @author admin
 */
class UserGrantedShopAccess extends \shirtplatform\entity\abstraction\JsonEntity
{

    const PATH_TEMPLATE = 'accounts/{accountId}/users/{userId}/grantedShopAccesses';
    const VAR_NAME = 'userGrantedShopAccess';

    public $shop;
    public $writeAllowed;
    public $version;
    
    public static $classMap = array(
        'shop' => '\shirtplatform\entity\account\Shop'
    );
 
    public static function &findAll($userId, \shirtplatform\filter\WsParameters $wsParameters = NULL, $isAsync = FALSE)
    {
        $entityName = get_called_class();
        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName($entityName);

        $promise = new \shirtplatform\rest\promise\PagePromise('GET', self::getUrl($userId), $entityName, $wsParameters->buildParams(), array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);
 
        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }
    
    public static function getUrl($userId)
    {
        $url = self::PATH_TEMPLATE;
        $url = str_replace('{userId}', $userId, $url);
        return str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $url);
    }
    
    public function grantShopAccess($userId)
    {
        $url = str_replace('{userId}', $userId, self::PATH_TEMPLATE);
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $url);
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_called_class(), [], [self::VAR_NAME => $this->getWebserviceAttributes()]);
        return $rest->_call($promise);
    }
    
    public static function delete($userId, $id)
    {
        $uri = self::getUrl($userId) . '/'. $id;
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $uri , array(), array(), FALSE);
        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }
    
}
