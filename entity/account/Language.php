<?php
/**
 * Description of Languages
 *
 * @author Jan Maslik
 */

namespace shirtplatform\entity\account;

class Language extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/languages';
	const VAR_NAME = 'language';

	public static $classMap = array(); 
	
	public $code;
	public $name;
	public $version;
	public $default;
	public $deleted;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}

