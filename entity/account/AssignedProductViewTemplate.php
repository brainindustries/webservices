<?php
namespace shirtplatform\entity\account;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssignedProductViewTemplateFilter
 *
 * @author Jan Maslik
 */
class AssignedProductViewTemplate extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collections/{parentId}/templateProducts/{parentId}/assignedViews';
	const VAR_NAME = 'assignedProductViewTemplate';

	
	public static $classMap = array(
		'productView' => '\shirtplatform\entity\product\ProductView',
		'assignedColors' => '\shirtplatform\entity\product\ProductViewColorDetail',
	);
	
	
	public $orderIndex;
	public $defaultView;
	public $defaultInCreator;
	public $showInCreator;
	
	public $version;
	public $swfId;
	
	public $productView = null;
    public $type = \shirtplatform\entity\enumerator\AssignedViewType::TECHNICAL;

	//lazy
	protected $assignedColors = null;
	protected $areas = null;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
	public function getAssignedColors()
	{
		return $this->getAtomLinkValue('assignedColors');
	}
	
	public function addAssignedViewColor($assignedColorView)
	{
		if($this->assignedColors == null)
		{
			$this->assignedColors = array();
		}
		
		$this->assignedColors[$assignedColorView->id] = $assignedColorView ;
	}
        
	public function getAreas($wsParameters = null , $isAsync = false)
	{
		return $this->getAtomLinkValue('areas', $wsParameters, $isAsync);
	}
}
