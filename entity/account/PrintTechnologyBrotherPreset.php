<?php

namespace shirtplatform\entity\account;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintTechnologyBrotherPreset
 *
 * @author admin
 */
class PrintTechnologyBrotherPreset extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/brother/presets';
    const VAR_NAME = 'brotherPreset';

    public static $classMap = array();
	
    public $name;
    public $pretreadSpeed;
    public $pretreadDirectionDouble;
    public $inkVolume;
    public $colorMultiplePass;
    public $printBidirectional;
    public $version;
    
}
