<?php
namespace shirtplatform\entity\account;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ColorTexture
 *
 * @author Jan Maslik
 */
class ColorTexture extends \shirtplatform\entity\abstraction\BaseDao
{
    const VAR_NAME = 'colorTexture';
    
    public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
    {
	parent::__construct($data,$parents,$foreignKeyOnly);
    }
}
