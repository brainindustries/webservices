<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\account;

/**
 * Description of Sticker
 *
 * @author Jan Maslik
 */
class Sticker extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/stickers';
	const VAR_NAME = 'sticker';
	
	public static $classMap = array(
	);
	public $name;
	public $imageId;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
