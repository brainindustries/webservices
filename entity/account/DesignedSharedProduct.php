<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\account;

/**
 * Description of DesignedSharedProduct
 *
 * @author Jan Maslik
 */
class DesignedSharedProduct extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/sharedProducts';
    const VAR_NAME = 'sharedProduct';

    public static $classMap = array(
        'assignedProductColor' => '\shirtplatform\entity\product\AssignedProductColor',
        'design' => '\shirtplatform\entity\order\Design'
    );
    public $design;
    public $created;
    public $deleted;
    public $version;
    public $assignedProductColor;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get product preview url string.
     * 
     * @param int $width
     * @param int $height
     * @param int $bestfit
     * @return string
     */
    public function getPreviewUrl($width = -1, $height = -1, $bestfit = 1)
    {
        return $this->getAtomLink('image') . '?width=' . $width . '&height=' . $height . '&bestfit=' . $bestfit;
    }

}
