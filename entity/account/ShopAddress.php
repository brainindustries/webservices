<?php

namespace shirtplatform\entity\account;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShopAddress
 *
 * @author admin
 */
class ShopAddress extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/shopAddresses';
    const PATH_BY_COUNTRY_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/shopAddresses/countries/{countryId}';
    const PATH_BY_COUNTRY_FACTORY_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/shopAddresses/countries/{countryId}/{factoryId}';
    const VAR_NAME = 'shopAddress';

    public static $classMap = array(
        'country' => '\shirtplatform\entity\account\Country',
        'address' => '\shirtplatform\entity\account\Address',
        'factory' => '\shirtplatform\entity\technology\Factory'
    );
    
    public $address;
    public $country;
    public $factory;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * 
     * @param type $countryId
     * @param type $shopId
     * @param type $isAsync
     * @return type
     */
    public static function findByCountry($countryId, $shopId = NULL, $isAsync = FALSE)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        $url = str_replace('{countryId}', $countryId, self::PATH_BY_COUNTRY_TEMPLATE);
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $url);
        $url = str_replace('{shopId}', $shopId, $url);
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), [], [], $isAsync);
        return $rest->_call($promise);
    }
    
    /**
     * 
     * @param type $countryId
     * @param type $factoryId
     * @param type $shopId
     * @param type $isAsync
     * @return type
     */
    public static function findByCountryAndFactory($countryId, $factoryId, $shopId = NULL, $isAsync = FALSE)
    {
        if($shopId == NULL)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        $url = str_replace('{countryId}', $countryId, self::PATH_BY_COUNTRY_FACTORY_TEMPLATE);
        $url = str_replace('{factoryId}', $factoryId, $url);
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $url);
        $url = str_replace('{shopId}', $shopId, $url);
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), [], [], $isAsync);
        return $rest->_call($promise);
    }
    
    /**
     * 
     * @param \shirtplatform\filter\WsParameters $wsParameters
     * @param type $shopId
     * @param type $isAsync
     * @throws \Exception
     */
    public static function findAll(\shirtplatform\filter\WsParameters $wsParameters = null, $shopId = null, $isAsync = false)
    {
        throw new \Exception('Non implemented method!');
    }
    
}
