<?php

namespace shirtplatform\entity\account;

/**
 * Description of Account
 *
 * @author Jan Maslik
 */
class Account extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts';
    const VAR_NAME = 'account';

    public static $classMap = array(
        'accountDataRetention' => '\shirtplatform\entity\account\AccountDataRetention'
    );
    
    public $coreAllowed;
    public $shopAllowed;
    public $publishAllowed;
    public $name;
    public $fullName;
    public $version;
    public $pimpKey;
    public $pimpCreated;
    public $subPartnersAllowed;
    public $affiliateId;
    public $accountDataRetention;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    public function __evictCache()
    {
        self::evictCache($this->id);
    }

    /**
     * Clear cache for account.
     * 
     * @param int $accountId
     * @return void
     */
    public static function evictCache($accountId)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\JsonPromise('POST', self::PATH_TEMPLATE . '/' . $accountId . '/evictCache' );
        return $rest->_call($promise);
    }

    /**
     * Upload account logo.
     * 
     * @param string $sourcePath
     * @param string $name
     * @return void
     */
    public function uploadLogo($sourcePath, $name)
    {
        return \shirtplatform\resource\UploadResource::uploadAccountLogo($sourcePath, $name);
    }

    /**
     * Get account logo path string.
     * 
     * @return string
     */
    public function getLogoUrl()
    {
        return $this->getAtomLink('preview');
    }

    /**
     * Register a new user.
     * 
     * @param \shirtplatform\entity\account\User $user
     */
    public function __register($user)
    {
        self::register($this, $user);
    }

    /**
     * Register a new user.
     * 
     * @param Account $account
     * @param \shirtplatform\entity\account\User $user
     * @param boolean $isAsync
     * @return void
     */
    public static function register($account, $user, $isAsync = false)
    {
        $object = $account->getWebserviceAttributes();
        $object['user'] = $user;

        $rest = \shirtplatform\rest\REST::getInstance();
        
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $account->__url(), get_called_class() , [], array('accountRegister' => $object) , $isAsync);
        
        return $rest->_call($promise);
    }

}
