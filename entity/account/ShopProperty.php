<?php

namespace shirtplatform\entity\account;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShopProperty
 *
 * @author Jan Maslik
 */
class ShopProperty extends \shirtplatform\entity\abstraction\ShopOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/properties';
	const VAR_NAME = 'shopProperty';
	public static $classMap = array(
	);
	
	public $name;
	public $value;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}
