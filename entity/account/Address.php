<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\account;

/**
 * Description of Address
 *
 * @author admin
 */
class Address extends \shirtplatform\entity\abstraction\BaseDao
{

    public $address2;
    public $street;
    public $streetNo;
    public $city;
    public $company;
    public $country;
    public $countryCode;
    public $firstName;
    public $lastName;
    public $latitude;
    public $longitude;
    public $name;
    public $phone;
    public $province;
    public $provinceCode;
    public $zip;
    public $email;
    public $stateCode;
    public $stateName;
    public $version;

}
