<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\account;

/**
 * Description of Font
 *
 * @author Jan Maslik
 */
class Font extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/fonts';
    const VAR_NAME = 'font';

    public static $classMap = array(
    );
    public $name;
    public $deleted;
    public $enabled;
    public $version;
    public $className;
    public $defSize;
    public $minSize;
    public $maxSize;
    public $fontDataId;
    public $orderIndex;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get font preview url string.
     * 
     * @param int $size
     * @param string|null $text
     * @return string
     */
    public function getPreviewUrl($size = 100, $text = NULL)
    {
        $url = $this->getAtomLink('preview') . '?size=' . $size;
        if ($text !== NULL)
        {
            $url .='&text=' . urlencode($text);
        }
        return $url;
    }

}
