<?php

namespace shirtplatform\entity\account;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class UserAuthority extends \shirtplatform\entity\abstraction\ParentOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/users/{parentId}/authorities';
    const VAR_NAME = 'authority';

    public static $classMap = array(
    );
    public $name;
    public $version;

}
