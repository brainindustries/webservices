<?php

namespace shirtplatform\entity\account;

/**
 * Description of StockSupplierProperty
 *
 * @author admin
 */
class StockSupplierProperty extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
    
    const PATH_TEMPLATE = 'accounts/{accountId}/stockSuppliers/{parentId}/properties';
    const VAR_NAME = 'stockSupplierProperty';
    
    public static $classMap = array(
        'supplier' => '\shirtplatform\entity\account\StockSupplier',
    );

    public $id;
    public $supplier;
    public $name;
    public $value;

    
    public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
    {
        parent::__construct($data,$parents,$foreignKeyOnly);
    }
    
}
