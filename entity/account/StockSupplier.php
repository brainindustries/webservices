<?php

namespace shirtplatform\entity\account;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StockSupplier
 *
 * @author admin
 */
class StockSupplier extends \shirtplatform\entity\abstraction\BaseDao
{
    
    const PATH_TEMPLATE = 'accounts/{accountId}/stockSuppliers';
    const VAR_NAME = 'stockSupplier';
    
    public static $classMap = array(
        'ownerAccount' => '\shirtplatform\entity\account\Account',
    );

    public $id;
    public $name;
    public $description;
    public $logoPath;
    public $deleted;
    public $enabled;
    public $ownerAccount;
    public $enabledForAccount;
    public $type;
    public $version;
    
    public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
    {
        parent::__construct($data,$parents,$foreignKeyOnly);
    }
    
    public static function clearAll($stockSupplierId)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = self::PATH_TEMPLATE . '/{supplierId}/clearAll';
        $url = str_replace(['{accountId}', '{supplierId}'], [\shirtplatform\utils\user\User::getAccountId(), $stockSupplierId], $url);
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url);
        return $rest->_call($promise);
    }
    
    public static function exportCsv($stockSupplierId)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = self::PATH_TEMPLATE . '/{supplierId}/exportCsv';
        $url = str_replace(['{accountId}', '{supplierId}'], [\shirtplatform\utils\user\User::getAccountId(), $stockSupplierId], $url);
        
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\PlatformPromise('GET', $url, ['query' => [
            'page' => 0,
            'size' => 999999999
        ]]);
        return $rest->_call($promise);
    }
    
    public static function enableForAccount($stockSupplierId)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = self::PATH_TEMPLATE . '/{supplierId}/enableForAccount';
        $url = str_replace(['{accountId}', '{supplierId}'], [\shirtplatform\utils\user\User::getAccountId(), $stockSupplierId], $url);
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url);
        return $rest->_call($promise);
    }
    
    public static function disableForAccount($stockSupplierId)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = self::PATH_TEMPLATE . '/{supplierId}/disableForAccount';
        $url = str_replace(['{accountId}', '{supplierId}'], [\shirtplatform\utils\user\User::getAccountId(), $stockSupplierId], $url);
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $url);
        return $rest->_call($promise);
    }

}
