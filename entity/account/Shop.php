<?php

namespace shirtplatform\entity\account;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Shop
 *
 * @author Jan Maslik
 */
use shirtplatform\entity\sharing\ImportedProduct,
    shirtplatform\entity\sharing\ImportedMotive,
    shirtplatform\entity\enumerator\ShopType,
    shirtplatform\filter\WsParameters,
    shirtplatform\parser\WsParse,
    shirtplatform\parser\WsModel,
    shirtplatform\filter\Filter,
    shirtplatform\parser\ArrayUtil,
    \shirtplatform\rest\REST;
use \shirtplatform\entity\order\Creator;

class Shop extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops';
    const PATH_IMPORT_PRODUCT = 'accounts/{accountId}/shops/{shopId}/products/import/{importableProductId}';
    const PATH_IMPORT_OWN_PRODUCT = 'accounts/{accountId}/shops/{shopId}/products/importOwn/{productId}';
    const PATH_IMPORT_MOTIVE = 'accounts/{accountId}/shops/{shopId}/motives/importOwn/{importableMotiveId}';
    const PATH_IMPORT_OWN_MOTIVE = 'accounts/{accountId}/shops/{shopId}/motives/importOwn/{motiveId}';
    const VAR_NAME = 'shop';

    public static $classMap = array(
    );
    public $name;
    public $version;
    //public $pool;

    public $type = \shirtplatform\entity\enumerator\ShopType::SHOP;
    public $default;
    public $showPricesExcludingVat;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get Creator data with custom settings.
     * <p>
     * <b>Parameters may be:</b>
     * langId ,countryId,productId ,assignedColorId ,assignedSizeId ,amount ,
     * orderedProductId ,editOrderedProductId ,width  , heigh ,addToBasketCallback ,
     * productChangeCallback,creatorStartCallback 
     * </p>
     * 
     * */
    public function getCreator($params = array())
    {
        return Creator::find($params, $this->id);
    }

    /**
     * Duplicate an shop.
     * 
     * @return Shop
     */
    public function __duplicate()
    {
        return self::duplicate($this->id);
    }

    /**
     * Duplicate an shop.
     * 
     * @param int $shopId
     * @param boolean $isAsync
     * @return Shop
     */
    public static function duplicate($shopId,$isAsync = false)
    {
        $url = self::getUrl();
        $url .= '/' . $shopId . '/duplicate';
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Find default shop.
     * 
     * @param string $shopType
     * @return Shop
     */
    public static function findDefault($shopType)
    {
        $wsParameters = new \shirtplatform\filter\WsParameters(0, 1);
        $wsParameters->addSimpleExpression('type', '=', $shopType);
        $wsParameters->addOrderBy('default', 'desc');
        $find = Shop::findAll($wsParameters);
        return reset($find);
    }

    /**
     * Import product from publish.
     * 
     * @param int $importableProductId
     * @return \shirtplatform\entity\sharing\ImportedProduct
     */
    public function __importProduct($importableProductId)
    {
        return self::importProduct($importableProductId, $this->id);
    }

    /**
     * Import own product.
     * 
     * @param int $productId
     * @return \shirtplatform\entity\sharing\ImportedProduct
     */
    public function __importOwnProduct($productId)
    {
        return self::importOwnProduct($productId, $this->id);
    }

    /**
     * Import own motive.
     * 
     * @param int $motiveId
     * @return \shirtplatform\entity\sharing\ImportedMotive
     */
    public function __importOwnMotive($motiveId)
    {
        return self::importOwnMotive($motiveId, $this->id);
    }

    /**
     * Import motive from publish.
     * 
     * @param int $importableMotiveId
     * @return \shirtplatform\entity\sharing\ImportedMotive
     */
    public function __importMotive($importableMotiveId)
    {
        return self::importMotive($importableMotiveId, $this->id);
    }

    /**
     * Find all shops.
     * 
     * @param \shirtplatform\filter\WsParameters|null $params
     * @param string|null $outputClassName
     * @return Shop[]
     */
    public static function findAllShops(\shirtplatform\filter\WsParameters $params = null, $outputClassName = null)
    {
        if ($params == null)
        {
            $params = new \shirtplatform\filter\WsParameters();
        }
        //$params->addSimpleExpression('type', '=', shirtplatform\entity\enumerator\ShopType::SHOP );
        return self::findAll($params, $outputClassName);
    }

    /**
     * Import product from publish.
     * 
     * @param int $importableProductId
     * @param int|null $dstShopId
     * @return \shirtplatform\entity\sharing\ImportedProduct
     */
    public static function importProduct($importableProductId, $dstShopId = null)
    {
        if ($dstShopId == null)
        {
            $dstShopId = \shirtplatform\utils\user\User::getShopId();
        }
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_IMPORT_PRODUCT);
        $url = str_replace('{shopId}', $dstShopId, $url);
        $url = str_replace('{importableProductId}', $importableProductId, $url);

        $className = 'shirtplatform\entity\sharing\ImportedProduct';

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, $className, [], [], false);
        $promise->shopId = $dstShopId;
        
        return $rest->_call($promise);
    }

    /**
     * Import own product.
     * 
     * @param int $productId
     * @param int|null $dstShopId
     * @return \shirtplatform\entity\sharing\ImportedProduct
     */
    public static function importOwnProduct($productId, $dstShopId = null)
    {
        if ($dstShopId == null)
        {
            $dstShopId = \shirtplatform\utils\user\User::getShopId();
        }
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_IMPORT_OWN_PRODUCT);
        $url = str_replace('{shopId}', $dstShopId, $url);
        $url = str_replace('{productId}', $productId, $url);

        $className = 'shirtplatform\entity\sharing\ImportedProduct';
         
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, $className, [], [], false);
        $promise->shopId = $dstShopId;

        return $rest->_call($promise);
    }

    /**
     * Import motive from publish.
     * 
     * @param int $importableMotiveId
     * @param int|null $dstShopId
     * @return \shirtplatform\entity\sharing\ImportedMotive
     */
    public static function importMotive($importableMotiveId, $dstShopId = null)
    {
        if ($dstShopId == null)
        {
            $dstShopId = \shirtplatform\utils\user\User::getShopId();
        }
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_IMPORT_MOTIVE);
        $url = str_replace('{shopId}', $dstShopId, $url);
        $url = str_replace('{importableMotiveId}', $importableMotiveId, $url);
        
        $className = 'shirtplatform\entity\sharing\ImportedMotive';
         
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, $className, [], [], false);
        $promise->shopId = $dstShopId;

        return $rest->_call($promise);
    }

    /**
     * Import own motive.
     * 
     * @param int $motiveId
     * @param int|null $dstShopId
     * @return \shirtplatform\entity\sharing\ImportedMotive
     */
    public static function importOwnMotive($motiveId, $dstShopId = null)
    {
        if ($dstShopId == null)
        {
            $dstShopId = \shirtplatform\utils\user\User::getShopId();
        }
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_IMPORT_OWN_MOTIVE);
        $url = str_replace('{shopId}', $dstShopId, $url);
        $url = str_replace('{motiveId}', $motiveId, $url);

        $className = 'shirtplatform\entity\sharing\ImportedMotive';
         
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, $className, [], [], false);
        $promise->shopId = $dstShopId;

        return $rest->_call($promise);
    }

}
