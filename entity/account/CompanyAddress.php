<?php
namespace shirtplatform\entity\account;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CompanyAddress
 *
 * @author Jan Maslik
 */
class CompanyAddress extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/company/addresses';
	const VAR_NAME = 'companyAddress';

	public static $classMap = array(
		'countrySource' => 'shirtplatform\entity\constant\CountrySource'
	);
	
	public $name;
	public $countrySource;
	public $street;
	public $postNr;
	public $zipCode;
	public $city;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
