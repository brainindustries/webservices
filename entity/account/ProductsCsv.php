<?php

namespace shirtplatform\entity\account;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductsCsv
 *
 * @author admin
 */
class ProductsCsv extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/products/factories/{factoryId}/csv';
    
    public static function download($shopId, $factoryId)
    {
        $url = self::getUrl();
        $url = str_replace('{shopId}', $shopId, $url);
        $url = str_replace('{factoryId}', $factoryId, $url);
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\PlatformPromise('GET', $url, ['query' => [
            'page' => 0,
            'size' => 999999999
        ]]);
        return $rest->_call($promise);
    }
    
}
