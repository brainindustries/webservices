<?php

namespace shirtplatform\entity\account;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShopConfigCreator
 *
 * @author Jan Maslik
 */
class ShopConfigCreator extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/configCreator';
    const VAR_NAME = 'shopConfigCreator';

    public static $classMap = array(
        'theme' => 'shirtplatform\entity\constant\CreatorTheme',
        'mobileTheme' => 'shirtplatform\entity\constant\CreatorTheme',
        'addNewMotiveButtonMotiveIcoUp' => 'shirtplatform\entity\account\ShopConfigCreatorResource',
        'addNewMotiveButtonGfxTextIcoUp' => 'shirtplatform\entity\account\ShopConfigCreatorResource',
        'addNewMotiveButtonTextIcoUp' => 'shirtplatform\entity\account\ShopConfigCreatorResource',
        'addNewMotiveButtonUploadIcoUp' => 'shirtplatform\entity\account\ShopConfigCreatorResource',
    );
    public $version;
    public $enableMotive;
    public $enableText;
    public $enablePimpText;
    public $enableUserUpload;
    public $decorationColor;
    public $enableChangeProduct;
    public $enableShareProduct;
    public $enablePimpService;
    public $enableStartControll;
    public $enableColorChange;
    public $enableSizeChange;
    public $enableTeamText;
    public $keepAccrosSizes;
    public $fontName = "";
    public $showPriceDetail;
    public $theme;
    public $mobileTheme;
    public $backgroundColor;
    public $backgroundAlpha;
    public $useSvgText;
    public $enableObjectSizeLabel;
    public $enableIndividualisationButton;
    public $addNewMotiveButtonMotiveIcoUp;
    public $addNewMotiveButtonGfxTextIcoUp;
    public $addNewMotiveButtonTextIcoUp;
    public $addNewMotiveButtonUploadIcoUp;
    public $enableUndoRedo;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Unsupported method.
     * 
     * @param type $id
     * @param type $shopId
     * @param type $isAsync
     * @throws Exception
     */
    public static function &findAll(\shirtplatform\filter\WsParameters $wsParameters = NULL, $shopId = NULL, $isAsync = false)
    {
        throw new Exception('Unsupported call ShopConfigCreator.findAll!');
    }

    /**
     * Unsupported method.
     * 
     * @param type $id
     * @param type $shopId
     * @param type $isAsync
     * @throws Exception
     */
    public static function &delete($id, $shopId = null, $isAsync = false)
    {
        throw new Exception('Unsupported call ShopConfigCreator.delete!');
    }

    /**
     * Update an config.
     * 
     * @param boolean $isAsync
     * @return void
     */
    public function __update($isAsync = false)
    {
        if ($this->getShopId() == null)
        {
            $this->setShopId(\shirtplatform\utils\user\User::getShopId());
        }


        $rest = \shirtplatform\rest\REST::getInstance();
        $url = $this->__url();

        $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], array($this::VAR_NAME => $this->getWebserviceAttributes()), $isAsync);
        $promise->shopId = $this->getShopId();
        $this->clearAtomLinks();
        return $rest->_call($promise);
    }

    /**
     * Find an single config.
     * 
     * @param int $shopId
     * @param array|null $params
     * @param boolean $isAsync
     * @return ShopConfigCreator
     */
    public static function &find($shopId = null, $params = null, $isAsync = false)
    {
        $url = self::getUrl($shopId);

        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\ItemPromise('GET', $url, get_called_class(), $params, [], $isAsync);
        $promise->shopId = $shopId;

        return $rest->_call($promise);
    }

    /**
     * Remove custom font.
     * 
     * @param boolean $isAsync
     * @return void
     */
    public function removeFont($isAsync = FALSE)
    {
        if ($this->getShopId() == null)
        {
            $this->setShopId(\shirtplatform\utils\user\User::getShopId());
        }

        $url = $this->__url() . '/ttf';
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $url, array(), array(), $isAsync);
        $promise->shopId = $this->getShopId();
        return $rest->_call($promise);
    }

}
