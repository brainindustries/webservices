<?php
namespace shirtplatform\entity\account;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductPool
 *
 * @author Jan Maslik
 */


class ProductPool extends \shirtplatform\entity\abstraction\ShopOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/pools/products';

	const VAR_NAME = 'productPool';

	
	public static $classMap = array(
	);
	
	public $name;
	public $version;
    public $orderIndex;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}
