<?php
namespace shirtplatform\entity\account;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MotivePool
 *
 * @author Jan Maslik
 */


class MotivePool extends \shirtplatform\entity\abstraction\ShopOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/pools/motives';

	const VAR_NAME = 'motivePool';

	
	public static $classMap = array(
	);
	
	public $name;
	public $version;
    public $orderIndex;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}
