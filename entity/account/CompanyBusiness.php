<?php
namespace shirtplatform\entity\account;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CompanyBusiness
 *
 * @author Jan Maslik
 */
class CompanyBusiness extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/company/businesses';
	const VAR_NAME = 'companyBusiness';

	public static $classMap = array(
		'businessEntity' => 'shirtplatform\entity\constant\BusinessEntity'
	);
	
	public $name;
	public $businessId;
	public $businessVatId;
	public $businessEntity;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
