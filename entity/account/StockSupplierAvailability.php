<?php

namespace shirtplatform\entity\account;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StockSupplierAvailability
 *
 * @author admin
 */
class StockSupplierAvailability extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/stockSuppliers/{supplierId}/availability';
    const VAR_NAME = 'stockSupplierAvailability';
    
    public static $classMap = array(
        
    );
    
    public $id;
    public $supplierId;
    public $stockId;
    public $supplierPlu;
    public $availableInStock;
    public $shortlyInStock;
    public $longerInStock;
    
    /**
     * 
     * @param type $data
     * @param type $parents
     * @param type $foreignKeyOnly
     */
    public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
    {
        parent::__construct($data,$parents,$foreignKeyOnly);
    }
    
    
    /**
     * 
     * @param int $stockSupplierId
     * @param array $stockIds
     * @param boolean $isAsync
     * @return StockSupplierAvailability[]
     */
    public static function &findAll($stockSupplierId, array $stockIds, $isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        $url = self::PATH_TEMPLATE;
        $url = str_replace(['{accountId}', '{supplierId}'], [\shirtplatform\utils\user\User::getAccountId(), $stockSupplierId], $url);
        
        $promise = new \shirtplatform\rest\promise\PagePromise('POST', $url, get_called_class(), [], ['value' => $stockIds], $isAsync);
        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }
    
}
