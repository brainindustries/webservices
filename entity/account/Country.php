<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\account;

/**
 * Description of Country
 *
 * @author Jan Maslik
 */
class Country extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/countries';
	const VAR_NAME = 'country';

	public static $classMap = array(
		'standardVat' => '\shirtplatform\entity\account\Vat',
		'reducedVat' => '\shirtplatform\entity\account\Vat',
		'currency' => '\shirtplatform\entity\account\Currency',
	);
	
	public $name;
	public $deleted;
	public $version;
	public $code;
	public $standardVat;
	public $reducedVat;
	public $currency;
	public $default;
        public $pricePrecision;
        public $priceFractionalDigits;
        
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
