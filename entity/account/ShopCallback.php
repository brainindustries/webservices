<?php

namespace shirtplatform\entity\account;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShopCallback
 *
 * @author admin
 */
class ShopCallback extends \shirtplatform\entity\abstraction\ShopOwnedDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/callbacks';
    const VAR_NAME = 'shopCallback';

    public static $classMap = array(
    );
    public $url;
    public $user;
    public $password;
    public $version;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

}
