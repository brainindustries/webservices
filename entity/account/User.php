<?php

namespace shirtplatform\entity\account;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserAuthority
 *
 * @author Jan Maslik
 */
class User extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/users';
    const PATH_AUTH = 'accounts/{accountId}/users/{userId}/authorities';
    const VAR_NAME = 'user';

    public static $classMap = array(
        'authorities' => 'shirtplatform\entity\account\UserAuthority'
    );
    public $name;
    public $login;
    public $email;
    public $newPassword;
    public $enabled;
    public $userType;
    public $version;
    protected $authorities = null;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    /**
     * Get user authorities.
     * 
     * @param \WsParameters|null $wsParameters
     */
    public function &getAuthorities(\WsParameters $wsParameters = null)
    {
        return $this->getAtomLinkValue('authorities', $wsParameters);
    }

    /**
     * Add authority to user.
     * 
     * @param int $authorityId
     * @param boolean $isAsync
     * @return void
     */
    public function addAuthority($authorityId,$isAsync=false)
    {
        $authorityLink = $this->getAtomLink('authorities');
        $authorityLink .= '/' . $authorityId;
        $rest = \shirtplatform\rest\REST::getInstance();
        
        
         
        $promise = new \shirtplatform\rest\promise\JsonPromise('PUT', $authorityLink, [] , [], $isAsync);
        if (is_array($this->authorities))
        {
            $this->authorities = null;
        }
        return $rest->_call($promise);
    }

    /**
     * Remove user authority.
     * 
     * @param int $authorityId
     * @param boolean $isAsync
     */
    public function removeAuthority($authorityId , $isAsync = false )
    {
        $authorityLink = $this->getAtomLink('authorities');
        $authorityLink .= '/' . $authorityId;
        $rest = \shirtplatform\rest\REST::getInstance();
        
        $promise = new \shirtplatform\rest\promise\JsonPromise('DELETE', $authorityLink, [], [] , $isAsync);
        $rest->_call($promise);
        
        if (isset($this->authorities[$authorityId]))
        {
            unset($this->authorities[$authorityId]);
        }
    }

}
