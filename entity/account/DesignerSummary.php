<?php
namespace shirtplatform\entity\account;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MotiveDesignerProvision
 *
 * @author Jan Maslik
 */
class DesignerSummary extends \shirtplatform\entity\abstraction\JsonEntity
{

    const PATH_TEMPLATE_INVOICE = 'accounts/{accountId}/motiveAuthor/invoices';
    const PATH_TEMPLATE_PRODUCED = 'accounts/{accountId}/motiveAuthor/produced';
    const PATH_TEMPLATE_USED = 'accounts/{accountId}/motiveAuthor/used';
    const VAR_NAME = 'designerSummary';

    public $designerId;
    public $summary;
    
    public static $classMap = array(
    );
    
    
    public static function &invoice($from, $to, $isAsync = false, $cacheKey = null)
    {
        $rest = \shirtplatform\rest\REST::getInstance();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE_INVOICE);
        $query = [
            'dFrom' => $from,
            'dTo' => $to,
        ];
        
        if ($cacheKey != null)
        {
            $query['cacheKey'] = $cacheKey;
        }

        $promise = new \shirtplatform\rest\promise\PagePromise('GET', $url,get_called_class(), $query, [], $isAsync);
        return $rest->_call($promise);
    }
    
     public static function &produced($from, $to, $isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE_PRODUCED);
        $query = [
            'dFrom' => $from,
            'dTo' => $to
        ];

        $promise = new \shirtplatform\rest\promise\PagePromise('GET', $url,get_called_class(), $query, [], $isAsync);
        return $rest->_call($promise);
    }
    
     public static function &used($from, $to, $isAsync = false)
    {
        $rest = \shirtplatform\rest\REST::getInstance();

        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), self::PATH_TEMPLATE_USED);
        $query = [
            'dFrom' => $from,
            'dTo' => $to
        ];

        $promise = new \shirtplatform\rest\promise\PagePromise('GET', $url,get_called_class(), $query, [], $isAsync);
        return $rest->_call($promise);
    }
    
    public function getPrimaryKey()
    {
        return $this->designerId;  
    }
}
