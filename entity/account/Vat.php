<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\account;

/**
 * Description of Vat
 *
 * @author Jan Maslik
 */
class Vat extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/vats';
	const VAR_NAME = 'vat';
	
	public static $classMap = array(
	);

	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
	public $version;
	public $percentValue;
	
	
}
?>
