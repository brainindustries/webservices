<?php

namespace shirtplatform\entity\account;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MotivePoolProperty
 *
 * @author Jan Maslik
 */
class MotivePoolProperty extends \shirtplatform\entity\abstraction\ParentOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/pools/motives/{parentId}/properties';
	const VAR_NAME = 'motivePoolProperty';
	public static $classMap = array(
	);
	
	public $name;
	public $value;
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
	
}
