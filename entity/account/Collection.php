<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\account;

/**
 * Description of Collection
 *
 * @author Jan Maslik
 */
class Collection extends \shirtplatform\entity\abstraction\ShopOwnedDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/shops/{shopId}/collections';
	const VAR_NAME = 'collection';
	 
        
	public static $classMap = array(
	);
	
	public $boxColor;
        public $orderIndex;    
	public $version;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
        
        public static function fontReplace($shopId, $collectionId, $oldId, $newId)
        {
            $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() .'/shops/' . $shopId . '/collections/' . $collectionId . '/font/old/' . $oldId . '/new/' . $newId;
            $rest = \shirtplatform\rest\REST::getInstance();
            $promise = new \shirtplatform\rest\promise\ItemPromise('PUT', $url, get_called_class(), [], [], FALSE);
            $promise->shopId = $shopId;
            return $rest->_call($promise);
        }
	
}
