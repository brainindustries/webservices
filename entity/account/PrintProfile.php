<?php
namespace shirtplatform\entity\account;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintProfile
 *
 * @author Jan Maslik
 */
class PrintProfile extends \shirtplatform\entity\abstraction\BaseDao
{

    const PATH_TEMPLATE = 'accounts/{accountId}/privatePrintProfiles';
    const PATH_TEMPLATE_PUBLIC = 'root/publicPrintProfiles';
    const VAR_NAME = 'printProfile';

    public static $classMap = array(
    );
    public $name;
    public $version;
    public $publicProfile;

    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }

    public static function &findAllPublic(\shirtplatform\filter\WsParameters $wsParameters = null, $isAsync = false)
    {
        $entityName = get_called_class();

        if ($wsParameters == null)
        {
            $wsParameters = new \shirtplatform\filter\WsParameters();
        }

        $wsParameters->setRootEntityName($entityName);

        $promise = new \shirtplatform\rest\promise\PagePromise('GET', self::PATH_TEMPLATE_PUBLIC, $entityName, $wsParameters->buildParams(), array(), $isAsync);
        $promise->filterPromise = $wsParameters->getPromise($isAsync);


        $rest = \shirtplatform\rest\REST::getInstance();
        return $rest->_call($promise);
    }

}
