<?php
namespace shirtplatform\entity\account;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ColorTextures
 *
 * @author Jan Maslik
 */
class ColorTextureGroup extends \shirtplatform\entity\abstraction\BaseDao
{
    const VAR_NAME = 'colorTextureGroup';
   
    public static $classMap = array(
       'icon' => '\shirtplatform\entity\account\ColorTexture',
       'production' => '\shirtplatform\entity\account\ColorTexture',
       'preview' => '\shirtplatform\entity\account\ColorTexture',
    );

    public $production;
    public $preview;
    public $icon;
   
    public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
    {
	parent::__construct($data,$parents,$foreignKeyOnly);
    }
}
