<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\entity\account;

/**
 * Description of Currency
 *
 * @author Jan Maslik
 */
class Currency extends \shirtplatform\entity\abstraction\BaseDao
{
	const PATH_TEMPLATE = 'accounts/{accountId}/currencies';
	const VAR_NAME = 'currency';

	public static $classMap = array();

	public $version;
	public $code;
	public $symbol;
	public $name;
	public $deleted;
	
	public function __construct($data = null , $parents = array() , $foreignKeyOnly = false)
	{
		parent::__construct($data,$parents,$foreignKeyOnly);
	}
}
