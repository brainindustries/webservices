<?php

namespace shirtplatform\entity\account;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountDataRetention
 *
 * @author admin
 */
class AccountDataRetention extends \shirtplatform\entity\abstraction\JsonEntity
{
    
    const VAR_NAME = 'accountDataRetention';
    
    public $keepPersonalDataMonths;
    public $keepPrintDataMonths;
    
    public function __construct($data = null, $parents = array(), $foreignKeyOnly = false)
    {
        parent::__construct($data, $parents, $foreignKeyOnly);
    }
    
}
