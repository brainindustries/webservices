<?php

namespace shirtplatform\exception;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DebugException
 *
 * @author Jan Maslik
 */
class DebugException extends \Exception
{

    function __construct($message, $data = null)
    {
        if (\shirtplatform\constants\WsConstants::$DEVELOPER_MODE)
        {
            echo "<EXCEPTION>";
            \shirtplatform\utils\debug\DebugUtils::dump($message);
            \shirtplatform\utils\debug\DebugUtils::dump($data);
            echo "</EXCEPTION>";
        }
        parent::__construct($message);
    }

}
