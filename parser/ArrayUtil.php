<?php

namespace shirtplatform\parser;

/**
 * Description of ArrayUtil
 *
 * @author Jan Maslik
 */
class ArrayUtil
{

    /**
     * Join item values.
     * 
     * @param array $array
     * @param string|int $itemName
     * @return array
     */
    public static function joinItems(&$array, $itemName)
    {
        $result = array();
        foreach ($array as $value)
        {
            $result[] = $value[$itemName];
        }

        return $result;
    }

    /**
     * Returns array item . Before checks if exists ,if not returns default value
     * 
     * @param array $array
     * @param string|int $key
     * @param mixed|null $default
     * @return mixed
     */
    public static function getSafe($array, $key, $default = null)
    {
        if (isset($array[$key]))
        {
            return $array[$key];
        }

        return $default;
    }

    /**
     * Copy item from srcArray into dstArray defined by index. If item with that key doesnt exists in srcArray, copy is ignored
     * 
     * @param array $srcArray
     * @param array $dstArray
     * @param string|int $key
     */
    public static function copySafeItem(&$srcArray, &$dstArray, $key)
    {
        if (isset($srcArray[$key]))
        {
            $dstArray[$key] = $srcArray[$key];
        }
    }

    /**
     * Sets array item if not sets or if is null
     * 
     * @param array $array
     * @param string|int $key
     * @param mixed $value
     */
    public static function setIfEmpty(&$array, $key, $value)
    {
        if (!is_array($array))
        {
            $array = array();
        }

        if (!isset($array[$key]))
        {
            $array[$key] = $value;
            return;
        }

        if ($array[$key] == null)
        {
            $array[$key] = $value;
            return;
        }
    }

    /**
     * Sort by.
     * 
     * @param array $array
     * @param string|int $key
     * @return array
     */
    public static function sortBy($array, $key)
    {
        uasort($array, function($entity1, $entity2) use ($key)
                {
                    if ($entity1[$key] == $entity2[$key])
                    {
                        return 0;
                    }
                    return ($entity1[$key] < $entity2[$key] ) ? -1 : 1;
                });
        return $array;
    }

}
