<?php

namespace shirtplatform\parser;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WsParse
 *
 * @author Jan Maslik
 */
use \DebugException;

class WsParse
{

    /**
     * Parse single item result.
     * 
     * @param mixed $response
     * @param string $objectName
     * @return mixed
     */
    public static function getItem(&$response, $objectName)
    {
        if (!isset($response[$objectName]))
        {
            return $response;
        }

        return $response[$objectName];
    }

    /**
     * Get total elements.
     * 
     * @param array $page
     * @return int
     */
    public static function getTotalElements($page)
    {
        if (isset($page['pagedData']))
        {
            $page = $page['pagedData'];
        }

        if (isset($page['totalElements']))
        {
            return intval($page['totalElements']);
        }

        return 0;
    }

    /**
     * Parse json pagedData as array.
     * 
     * @param array $page
     * @param string $objectName
     * @return mixed
     */
    public static function getList($page, $objectName)
    {
        if (isset($page['pagedData']))
        {
            $page = $page['pagedData'];
        }

        if (isset($page['totalElements']) && $page['totalElements'] == 0)
        {
            return array();
        }

        if (!isset($page[$objectName]))
        {
            return array();
        }
        if (!isset($page[$objectName][0]))
        {
            return array($page[$objectName]);
        }

        return $page[$objectName];
    }

    /**
     * Parse atom links.
     * 
     * @param array $source
     * @return string
     */
    public static function parseAtomLinks($source)
    {
        $atomLinks = array();

        if (isset($source['atom.link']) && is_array($source['atom.link']))
        {
            foreach ($source['atom.link'] as $value)
            {
                $atomLinks[$value['@rel']] = $value['@href'];
            }
        }

        return $atomLinks;
    }

    /**
     * Join attributes.
     * 
     * @param array $arrayOfObjects
     * @param string $attrName
     * @param boolean $isMethod
     * @param boolean $indexById
     * @return array
     * @throws DebugException
     */
    public static function joinAttributes($arrayOfObjects, $attrName, $isMethod = false, $indexById = false)
    {
        $result = array();
        if (!is_array($arrayOfObjects))
        {
            //throw new DebugException("First parameter of \shirtplatform\parser\WsParse::joinAttribute should be an array" , $arrayOfObjects);
            return $result;
        }

        foreach ($arrayOfObjects as $object)
        {
            if (!is_object($object))
            {
                throw new DebugException("All items first parameter of \shirtplatform\parser\WsParse::joinAttribute should be an objects", $arrayOfObjects);
            }

            if ($isMethod === true)
            {
                $data = $object->{$attrName}();
            }
            else
            {
                $data = $object->$attrName;
            }

            if ($indexById)
            {
                $result[$data->id] = $data;
            }
            else
            {
                $result[] = $data;
            }
        }

        return $result;
    }

    /**
     * Index list of entities by property.
     * 
     * @param array $arrayOfEntities
     * @param string $indexAttribute
     * @return array
     */
    public static function indexEntityListBy($arrayOfEntities, $indexAttribute = 'id')
    {
        $result = array();
        foreach ($arrayOfEntities as $entity)
        {
            $result[$entity->{$indexAttribute}] = $entity;
        }
        return $result;
    }

    /**
     * Index list of entities by entity property.
     * 
     * @param array $arrayOfEntities
     * @param string $objectAttribute
     * @param string $objectAttributeAttribute
     * @return array
     */
    public static function indexEntityListByEntity($arrayOfEntities, $objectAttribute, $objectAttributeAttribute = 'id')
    {
        $result = array();
        foreach ($arrayOfEntities as $entity)
        {
            $result[$entity->{$objectAttribute}->{$objectAttributeAttribute}] = $entity;
        }
        return $result;
    }
    
    /**
     * Array item paging helper.
     * 
     * @param array $items
     * @param \Closure $callback
     * @param int $perPage
     */
    public static function itemPaging(array $items, \Closure $callback, $perPage = 300)
    {
        for ($i = 0; $i < ceil(count($items) / $perPage); $i++)
        {
            call_user_func_array($callback, array(array_slice($items, $i * $perPage, $perPage)));
        }
    }

}
