<?php

namespace shirtplatform\parser;

use shirtplatform\entity\abstraction\JsonEntity;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WsModel
 *
 * @author Jan Maslik
 */
class WsModel
{

    /**
     * Copy all same named attributes from source to destination. Attributes with different naming are ignored.
     * NOTE: ignore array connection (M:N related entities)
     * 
     * @param JsonEntity $sourceModel
     * @param JsonEntity $destinationModel
     * @param array $ignoreList
     * @return JsonEntity
     */
    public static function copy(&$sourceModel, $destinationModel, $ignoreList = array())
    {
        if ($sourceModel == null || $destinationModel == null)
        {
            return null;
        }
        foreach ($sourceModel->getModelAttributes() as $attrName => $attrValue)
        {
            if (in_array($attrName, $ignoreList))
            {
                continue;
            }
            if (!property_exists($destinationModel, $attrName))
            {
                continue;
            }

            if (is_object($destinationModel->$attrName) && is_object($attrValue))
            {
                self::copy($attrValue, $destinationModel->$attrName, ArrayUtil::getSafe($ignoreList, $attrName, array()));
                continue;
            }

            $destinationModel->$attrName = $attrValue;
        }

        return $destinationModel;
    }

    /**
     * Compare two models (BaseForm/JsonEntity).  Attributes with different naming are ignored.
     * NOTE: ignore array connection (M:N related entities)
     * 
     * @param JsonEntity $main
     * @param JsonEntity|array $comparative
     * @param array $ignoreList
     * @return boolean
     */
    public static function compare(&$main, &$comparative, $ignoreList = array())
    {
        if (empty($main) && empty($comparative))
        {
            return true;
        }

        if (empty($main) || empty($comparative))
        {
            return false;
        }

        $comparativeAttr = $comparative;
        $mainAttr = $main;

        if (is_object($comparativeAttr))
        {
            $comparativeAttr = $comparative->getModelAttributes();
        }

        if (is_object($main))
        {
            $mainAttr = $main->getModelAttributes();
        }

        foreach ($mainAttr as $attrName => $attrValue)
        {
            if (in_array($attrName, $ignoreList))
            {
                continue;
            }

            if (!array_key_exists($attrName, $comparativeAttr))
            {
                continue;
            }

            if (is_object($comparativeAttr[$attrName]) && is_object($attrValue) &&
                    !self::compare($comparativeAttr[$attrName], $attrValue, ArrayUtil::getSafe($ignoreList, $attrName, array())))
            {
                return false;
            }

            if ($comparativeAttr[$attrName] != $attrValue)
            {
                //\shirtplatform\utils\debug\DebugUtils::dump( "Diff:".$attrName." . Vals:".$comparativeAttr[$attrName].",".$attrValue);
                return false;
            }
        }
        return true;
    }

    /**
     * Compare model by allowed.
     * 
     * @param JsonEntity|array $main
     * @param JsonEntity|array $comparative
     * @param array $allowedProperties
     * @return boolean
     */
    public static function compareByAllowed($main, $comparative, array $allowedProperties)
    {
        if (is_object($main))
        {
            $main = $main->getModelAttributes();
        }
        if (is_object($comparative))
        {
            $comparative = $comparative->getModelAttributes();
        }

        foreach ($allowedProperties as $property => $subProperties)
        {
            if (!is_array($subProperties))
            {
                $property = $subProperties;
            }
            if ((!isset($main[$property]) && $main[$property] != NULL) || (!isset($comparative[$property]) && $comparative[$property] != NULL))
            {
                return false;
            }
            if (is_object($main[$property]))
            {
                if (!is_array($subProperties))
                {
                    $subProperties = array('id');
                }
                if (!self::compareByAllowed($main[$property], $comparative[$property], $subProperties))
                {
                    return false;
                }
            }
            else
            {
                if ($main[$property] !== $comparative[$property])
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Copy by allowed.
     * 
     * @param JsonEntity $source
     * @param JsonEntity $destination
     * @param array $allowedProperties
     */
    public static function copyByAllowed($source, &$destination, array $allowedProperties)
    {
        foreach ($allowedProperties as $property => $subProperties)
        {
            if (!is_array($subProperties))
            {
                $property = $subProperties;
            }
            $destination->$property = $source->$property;
        }
    }

    /**
     * Compare and copy.
     * 
     * @param JsonEntity $source
     * @param JsonEntity $destination
     * @param array $allowedProperties
     * @return boolean
     */
    public static function compareAndCopy($source, &$destination, array $allowedProperties)
    {
        $same = self::compareByAllowed($source, $destination, $allowedProperties);
        if (!$same)
        {
            self::copyByAllowed($source, $destination, $allowedProperties);
        }
        return $same;
    }

    /**
     * Clear ids of entity and subentities. 
     * NOTE: ignore array connection (M:N related entities)
     * 
     * @param JsonEntity $model
     * @param array $ignoreList
     */
    public static function clearIds($model, $ignoreList = array())
    {
        foreach ($model->getModelAttributes() as $attrName => $attrValue)
        {
            if (in_array($attrName, $ignoreList))
            {
                continue;
            }

            if ($attrName == 'id')
            {
                $model->$attrName = null;
                continue;
            }

            if (is_object($attrValue))
            {
                self::clearIds($attrValue, ArrayUtil::getSafe($ignoreList, $attrName, array()));
            }
        }
    }

    /**
     * Clear generated Ids.
     * 
     * @param JsonEntity $model
     * @param array $ignoreList
     */
    public static function clearGeneratedIds(&$model, $ignoreList = array())
    {
        foreach ($model->getModelAttributes() as $attrName => $attrValue)
        {
            if (in_array($attrName, $ignoreList))
            {
                continue;
            }

            if ($attrName == 'id' && self::isGeneratedId($attrValue))
            {
                $model->$attrName = null;
                continue;
            }

            if (is_object($attrValue))
            {
                self::clearGeneratedIds($attrValue, ArrayUtil::getSafe($ignoreList, $attrName, array()));
            }
        }
    }

    /**
     * Find and return missed indexes in arrays.
     * 
     * @param aray $searchedModels
     * @param array $comaredModels
     * @return array
     */
    public static function missedIndexes($searchedModels, $comaredModels)
    {
        $missedModels = array();
        $comparedArrayKeys = array_keys($comaredModels);

        foreach ($searchedModels as $index => $model)
        {
            if (!in_array($index, $comparedArrayKeys))
            {
                $missedModels[$index] = $model;
            }
        }
        return $missedModels;
    }

    /**
     * Convert entity into Json.
     * 
     * @param mixed $entity
     * @return string
     */
    public static function toJson($entity)
    {
        if (empty($entity))
        {
            return "";
        }
        return json_encode($entity);
    }

    /**
     * Create entity from json.
     * 
     * @param string $json
     * @param string $entityName
     * @return null|JsonEntity
     */
    public static function fromJson($json, $entityName)
    {
        if (empty($json))
        {
            return null;
        }

        return new $entityName(json_decode($json));
    }

    /**
     * Return only model attributes with values
     * 
     * @param array|JsonEntity $data
     * @return mixed
     */
    public static function toArray($data, $skipArray = false, $skipObject = false)
    {
        $result = array();

        if (is_array($data))
        {
            if ($skipArray)
            {
                return 'SKIPPED';
            }

            foreach ($data as $modelId => $model)
            {
                $result[$modelId] = self::toArray($model);
            }

            return $result;
        }

        if (is_object($data))
        {
            if ($skipObject)
            {
                return 'SKIPPED';
            }

            $attributes = $data->getModelAttributes();
            foreach ($attributes as $attrName => $attrValue)
            {
                $result[$attrName] = self::toArray($attrValue);
            }
            return $result;
        }

        return $data;
    }

    /**
     * Generate new Id.
     * 
     * @return string
     */
    public static function generateId()
    {
        return uniqid('NEW');
    }

    /**
     * Check if Id is new generated.
     * 
     * @param string $id
     * @return boolean
     */
    public static function isGeneratedId($id)
    {
        if (strpos($id, 'NEW') === 0)
        {
            return true;
        }
        return false;
    }

    /**
     * Get Id tree.
     * 
     * @param JsonEntity|array $model
     * @return array
     */
    public static function getIdTree($model)
    {
        $result = array();
        foreach ($model as $attrname => $attrValue)
        {
            if ($attrname == 'id')
            {
                $result['id'] = $attrValue;
            }
        }
        foreach ($model as $attrname => $attrValue)
        {
            if (is_object($attrValue))
            {
                $result[$attrname] = self::getIdTree($attrValue);
            }
        }
        return $result;
    }

    /**
     * Remove duplicities.
     * 
     * @param array $models
     * @param string $uniqueKey
     * @return array
     */
    public static function removeDuplicities($models, $uniqueKey)
    {
        $sortedArray = array();

        $keys = explode(".", $uniqueKey);
        foreach ($models as $modelId => $model)
        {
            $currentKey = $model;
            foreach ($keys as $key)
            {
                $currentKey = $currentKey[$key];
            }
            if (!isset($sortedArray[$currentKey]))
            {
                $sortedArray[$currentKey] = $model;
            }
            else
            {
                unset($models[$modelId]);
            }
        }

        return $models;
    }

    /**
     * Update changed.
     * 
     * @param JsonEntity|array $savedEntity
     * @param array $modifiedData
     * @param array $ignoreList
     */
    public static function updateChanged($savedEntity, $modifiedData, $ignoreList = array())
    {
        if (!\shirtplatform\parser\WsModel::compare($savedEntity, $modifiedData, $ignoreList))
        {
            \shirtplatform\parser\WsModel::copy($savedEntity, $modifiedData)->__update();
        }
    }

    /**
     * Sort by.
     * 
     * @param array $modelArray
     * @param string $sortByAttr
     * @return array
     */
    public static function sortBy($modelArray, $sortByAttr = 'orderIndex')
    {
        uasort($modelArray, function($entity1, $entity2) use ($sortByAttr)
                {
                    if ($entity1->$sortByAttr == $entity2->$sortByAttr)
                    {
                        return 0;
                    }
                    return ($entity1->$sortByAttr < $entity2->$sortByAttr ) ? -1 : 1;
                });
        return $modelArray;
    }

    /**
     * Get model parents.
     * 
     * @param JsonEntity $entity
     * @return array
     */
    public static function getParents($entity)
    {
        $parents = array();
        foreach (explode('/', $entity->getAtomLink('self')) as $piece)
        {
            if (is_numeric($piece))
            {
                $parents[] = $piece;
            }
        }
        return $parents;
    }

    /**
     * Compare two Ids.
     * 
     * @param mixed $val1
     * @param mixed $val2
     * @return boolean
     */
    public static function compareIds($val1, $val2)
    {
        if ($val1 == '')
        {
            $val1 = null;
        }

        if ($val2 == '')
        {
            $val2 = null;
        }

        return $val1 == $val2;
    }

    /**
     * Get root level.
     * 
     * @param JsonEntity $model
     * @return array
     */
    public static function getRootLevel($model)
    {
        $attributes = $model->getModelAttributes();

        foreach ($attributes as $name => $value)
        {
            if (is_object($value))
            {
                unset($attributes[$name]);
            }

            if (is_array($value))
            {
                unset($attributes[$name]);
            }
        }

        return $attributes;
    }

}
