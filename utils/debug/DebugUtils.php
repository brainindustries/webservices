<?php

namespace shirtplatform\utils\debug;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DebugUtils
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class DebugUtils
{

    /** @var IDebugger */
    private static $debugger;
    private static $timeHolder = array();

    /**
     * Set debugger class.
     * 
     * @param \shirtplatform\utils\debug\IDebugger $debugger
     */
    public static function setDebugger(IDebugger $debugger)
    {
        self::$debugger = $debugger;
    }

    /**
     * Dumps a variable or the object itself in terms of a string.
     *
     * @param mixed variable to be dumped
     */
    public static function dump($var, $highlight = true, $depth = 15)
    {
        if (self::$debugger === NULL)
        {
            self::$debugger = new SimpleDebugger();
        }
        self::$debugger->dumpVar($var, $highlight, $depth);
    }

    /**
     * Start watch timer.
     * 
     * @param string $key
     */
    public static function watchStart($key)
    {
        self::$timeHolder[$key] = microtime(true);
    }

    /**
     * Print watch timer.
     * 
     * @param string $key
     * @param string $text
     */
    public static function watchDump($key, $text = '')
    {
        \shirtplatform\utils\debug\DebugUtils::dump($text . round((microtime(true) - self::$timeHolder[$key]), 4));
    }

}
