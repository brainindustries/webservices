<?php

namespace shirtplatform\utils\debug;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IDebugger
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
interface IDebugger
{

    /**
     * Dump an variable.
     * 
     * @param string $var
     * @param boolean $highlight
     * @param int $depth
     */
    public function dumpVar($var, $highlight = true, $depth = 15);
}
