<?php

namespace shirtplatform\utils\user;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SimpleUser
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class SimpleUser implements IUser
{

    private static $pass;
    private static $user;
    private static $account;
    private static $session;
    private static $shop;
    private static $userId;

    /**
     * Unset all properties.
     */
    public function unsetAll()
    {
        
    }

    /**
     * Get session Id.
     * 
     * @return int
     */
    public function getSessionId()
    {
        return self::$session;
    }

    /**
     * Set session Id.
     * 
     * @param int $id
     */
    public function setSessionId($id)
    {
        self::$session = $id;
    }

    /**
     * Get account Id.
     * 
     * @return int
     */
    public function getAccountId()
    {
        return self::$account;
    }

    /**
     * Set account Id.
     * 
     * @param int $id
     */
    public function setAccountId($id)
    {
        self::$account = $id;
    }

    /**
     * Get shop Id.
     * 
     * @return int
     */
    public function getShopId()
    {
        return self::$shop;
    }

    /**
     * Set shop Id.
     * 
     * @param int $id
     */
    public function setShopId($id)
    {
        self::$shop = $id;
    }

    /**
     * Get username.
     * 
     * @return string
     */
    public function getUsername()
    {
        return self::$user;
    }

    /**
     * Set username.
     * 
     * @param string $username
     */
    public function setUsername($username)
    {
        self::$user = $username;
    }

    /**
     * Get password.
     * 
     * @return string
     */
    public function getPassword()
    {
        return self::$pass;
    }

    /**
     * Set password
     * 
     * @param string $password
     */
    public function setPassword($password)
    {
        self::$pass = $password;
    }
    
    /**
     * Set User Id
     * 
     * @param string $userId
     */
    public function setUserId($userId)
    {
        self::$userId = $userId;
    }
    
    /**
     * Get User Id
     * 
     * @param string $password
     */
    public function getUserId()
    {
        return self::$userId;
    }

}
