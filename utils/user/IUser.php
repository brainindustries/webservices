<?php

namespace shirtplatform\utils\user;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IUser
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
interface IUser
{

    /**
     * Unset all user properties.
     */
    public function unsetAll();

    /**
     * Get session Id.
     */
    public function getSessionId();

    /**
     * Set session Id.
     * 
     * @param int $id
     */
    public function setSessionId($id);

    /**
     * Get account Id.
     */
    public function getAccountId();

    /**
     * Set account Id.
     * 
     * @param int $id
     */
    public function setAccountId($id);

    /**
     * Get shop Id.
     */
    public function getShopId();

    /**
     * Set shop Id.
     * 
     * @param int $id
     */
    public function setShopId($id);

    /**
     * Get username.
     */
    public function getUsername();

    /**
     * Set username.
     * 
     * @param string $username
     */
    public function setUsername($username);

    /**
     * Get password.
     */
    public function getPassword();

    /**
     * Set password.
     * 
     * @param string $password
     */
    public function setPassword($password);
}
