<?php

namespace shirtplatform\utils\user;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class User
{

    /** @var IUser */
    private static $userClass;

    /**
     * Set user class.
     * 
     * @param \shirtplatform\utils\user\IUser $userClass
     */
    public static function setUserClass(IUser $userClass)
    {
        self::$userClass = $userClass;
    }

    /**
     * Get user class.
     * 
     * @return IUser
     */
    private static function getUserClass()
    {
        return self::$userClass;
    }

    /**
     * Unset all properties.
     * 
     * @return void
     */
    public static function unsetAll()
    {
        return self::getUserClass()->unsetAll();
    }

    /**
     * Get session Id.
     * 
     * @return int
     */
    public static function getSessionId()
    {
        return self::getUserClass()->getSessionId();
    }

    /**
     * Set session Id.
     * 
     * @param int $id
     * @return void
     */
    public static function setSessionId($id)
    {
        return self::getUserClass()->setSessionId($id);
    }

    /**
     * Get account Id.
     * 
     * @return int
     */
    public static function getAccountId()
    {
        return self::getUserClass()->getAccountId();
    }

    /**
     * Set account Id.
     * 
     * @param int $id
     * @return void
     */
    public static function setAccountId($id)
    {
        return self::getUserClass()->setAccountId($id);
    }

    /**
     * Get shop Id.
     * 
     * @return int
     */
    public static function getShopId()
    {
        return self::getUserClass()->getShopId();
    }

    /**
     * Set shop Id.
     * 
     * @param int $id
     * @return void
     */
    public static function setShopId($id)
    {
        return self::getUserClass()->setShopId($id);
    }

    /**
     * Get username.
     * 
     * @return string
     */
    public static function getUsername()
    {
        return self::getUserClass()->getUsername();
    }

    /**
     * Ser username.
     * 
     * @param string $username
     * @return void
     */
    public static function setUsername($username)
    {
        return self::getUserClass()->setUsername($username);
    }

    /**
     * Get username.
     * 
     * @return string
     */
    public static function getPassword()
    {
        return self::getUserClass()->getPassword();
    }

    /**
     * Set password.
     * 
     * @param string $password
     * @return void
     */
    public static function setPassword($password)
    {
        return self::getUserClass()->setPassword($password);
    }
    
    /**
     * Set User Id.
     * 
     * @param string $userId
     * @return void
     */
    public static function setUserId($userId)
    {
        return self::getUserClass()->setUserId($userId);
    }
    
    /**
     * Get User Id.
     * 
     * @return string
     */
    public static function getUserId()
    {
        return self::getUserClass()->getUserId();
    }

}
