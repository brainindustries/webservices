<?php

namespace shirtplatform\utils\log;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Log
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
class Log
{

    /** @var ILogger */
    private static $logger;

    /**
     * Set logger class.
     * 
     * @param \shirtplatform\utils\log\ILogger $logger
     */
    public static function setLogger(ILogger $logger)
    {
        self::$logger = $logger;
    }

    /**
     * Log an error.
     * 
     * @param string $message
     * @param string $class
     */
    public static function error($message, $class)
    {
        self::logMsg($message, 'error', $class);
    }

    /**
     * Log an warning.
     * 
     * @param string $message
     * @param string $class
     */
    public static function warn($message, $class)
    {
        self::logMsg($message, 'warning', $class);
    }

    /**
     * Log an info message.
     * 
     * @param string $message
     * @param string $class
     */
    public static function info($message, $class)
    {
        self::logMsg($message, 'info', $class);
    }

    /**
     * Log trace message.
     * 
     * @param string $message
     * @param string $class
     */
    public static function trace($message, $class)
    {
        self::logMsg($message, 'trace', $class);
    }

    /**
     * Log basic message.
     * 
     * @param string $message
     * @param string $mode
     * @param string $class
     */
    private static function logMsg(&$message, $mode, &$class)
    {
        if (self::$logger === NULL)
        {
            self::$logger = new SimpleLogger();
        }
        self::$logger->logMessage($message, $mode, $class);
    }

}
