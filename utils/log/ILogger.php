<?php

namespace shirtplatform\utils\log;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ILogger
 *
 * @author Slavomír Mikolaj <mikolaj.slavomir@jpsoftware.sk>
 */
interface ILogger
{

    /**
     * Log an message.
     * 
     * @param string $message
     * @param string $mode
     * @param string $class
     */
    public function logMessage($message, $mode, $class);
}
