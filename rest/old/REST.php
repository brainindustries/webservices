<?php

namespace shirtplatform\rest\old;

/**
 * CodeIgniter REST Class
 *
 * Mske REST requests to RESTful services with simple syntax.
 *
 * @package        	CodeIgniter
 * @subpackage    	Libraries
 * @category    	Libraries
 * @author        	Philip Sturgeon
 * @created			04/06/2009
 * @license         http://philsturgeon.co.uk/code/dbad-license
 * @link			http://getsparks.org/packages/restclient/show
 */
use shirtplatform\exception\DebugException;
use shirtplatform\constants\WsConstants;
use shirtplatform\parser\ArrayUtil;
use shirtplatform\rest\CURL;

class REST
{

    protected $supported_formats = array(
        'xml' => 'application/xml',
        'json' => 'application/json',
        'serialize' => 'application/vnd.php.serialized',
        'php' => 'text/plain',
        'csv' => 'text/csv'
    );
    protected $auto_detect_formats = array(
        'application/xml' => 'xml',
        'text/xml' => 'xml',
        'application/json' => 'json',
        'text/json' => 'json',
        'text/csv' => 'csv',
        'application/csv' => 'csv',
        'application/vnd.php.serialized' => 'serialize'
    );
    
    protected $format;
    protected $mime_type;
    protected $_curl;
    protected $http_auth = null;
    protected $http_user = null;
    protected $http_pass = null;
    protected $response_string;
    protected $_headers = array();

    function __construct($credentials = null)
    {
        $this->_curl = new CURL();
        empty($credentials) OR $this->setCredentials($credentials);
    }

    public function setCredentials($credentials)
    {
        isset($credentials['http_auth']) && $this->http_auth = $credentials['http_auth'];
        isset($credentials['http_user']) && $this->http_user = $credentials['http_user'];
        isset($credentials['http_pass']) && $this->http_pass = $credentials['http_pass'];
    }
    
    public function auth($user, $pass)
    {
        $params = array();
		$params['http_user'] = $user;
		$params['http_pass'] = $pass;
		$params['http_auth'] = 'basic';
		$rest = \shirtplatform\rest\REST::getInstance();
        $rest->setCredentials($params);
        return $rest->getItemJson('auth');
    }

    function __destruct()
    {
        $this->_curl->set_defaults();
    }

    /**
     * Load page defined in $params['size'],$params['page'] . For load all data unset params['size'] .
     * 
     * @param type $uri
     * @param type $params
     * @return type
     * @throws Exception
     */
    public function getPageJson($uri, $params = array())
    {
        $getAll = false;

        if (!isset($params['size']) || $params['size'] == null)
        {
            $params['size'] = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            //paging not alowed if we want to load all
            $params['page'] = 0;
            $getAll = true;
        }

        $response = $this->getJson($uri, $params, 'json');

        if (!isset($response['pagedData']['totalElements']) && WsConstants::$eventHandler != null)
        {
            WsConstants::$eventHandler->onWrongReponse("There is no page data!", $response);
        }

        $total = intval($response['pagedData']['totalElements']);

        if ($getAll && $total > intval($params['size']))
        {
            \shirtplatform\utils\log\Log::warn("Default limit is smaller than totalElements. Performance issue. uri:$uri", __CLASS__);
            $params['size'] = ($total < 1) ? 1 : $total;
            $response = $this->getJson($uri, $params, 'json');
        }

        return $response['pagedData'];
    }

    public function getItemJson($uri, $params = array())
    {
        $response = $this->getJson($uri, $params, 'json');

        if (isset($response['pagedData']))
        {
            throw new DebugException("For paged data use getPageJson method!");
        }

        return $response;
    }

    public function getJson($uri, $params = array())
    {
        $this->set_header('Accept', 'application/json');
        $this->set_header('Content-Type', 'application/json');
        $this->_curl->setParamsFormat('json');
        return $this->get($uri, $params, 'json');
    }

    private function get($uri, $params = array(), $format = NULL)
    {
        if (\shirtplatform\utils\user\User::getSessionId() != null)
        {
            $uri .= ';jsessionid=' . \shirtplatform\utils\user\User::getSessionId();
        }

        if ($params)
        {
            $uri .= '?' . (is_array($params) ? http_build_query($params) : $params);
        }
        return $this->_call('get', $uri, NULL, $format);
    }

    public function postJson($uri, $params = array(), $args = array())
    {
        $this->set_header('Accept', 'application/json');
        $this->set_header('Content-Type', 'application/json');
        $this->_curl->setParamsFormat('json');

        return $this->post($uri, $params, 'json', $args);
    }

    public function post($uri, $params = array(), $format = NULL, $args = array())
    {
        $getAll = false;

        if (!empty($args) && (!isset($args['size']) || $args['size'] == null ))
        {
            $args['size'] = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            //paging not alowed if we want to load all
            $args['page'] = 0;
            $getAll = true;
        }


        if (\shirtplatform\utils\user\User::getSessionId() != null)
        {
            $uri .= ';jsessionid=' . \shirtplatform\utils\user\User::getSessionId();
        }

        if (!empty($args))
        {
            $uri .= '?' . http_build_query($args);
        }

        $result = $this->_call('post', $uri, $params, $format);

        if ($getAll && intval($result) == intval($args['size']))
        {
            \shirtplatform\utils\log\Log::warn("Default limit is smaller than totalElements. Performance issue. uri:$uri", __CLASS__);
            $args['page'] ++;
            $result += $this->put($uri, $params, $format, $args);
        }


        return $result;
    }

    public function putJson($uri, $params = array(), $args = array())
    {
        $this->set_header('Accept', 'application/json');
        $this->set_header('Content-Type', 'application/json');
        $this->_curl->setParamsFormat('json');

        return $this->put($uri, $params, 'json', $args);
    }

    public function put($uri, $params = array(), $format = NULL, $args = array())
    {
        $getAll = false;

        if (!empty($args) && (!isset($args['size']) || $args['size'] == null ))
        {
            $args['size'] = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            //paging not alowed if we want to load all
            $args['page'] = 0;
            $getAll = true;
        }

        if (\shirtplatform\utils\user\User::getSessionId() != null)
        {
            $uri .= ';jsessionid=' . \shirtplatform\utils\user\User::getSessionId();
        }

        if (!empty($args))
        {
            $uri .= '?' . http_build_query($args);
        }

        $result = $this->_call('put', $uri, $params, $format);

        if ($getAll && intval($result) == intval($args['size']))
        {
            \shirtplatform\utils\log\Log::warn("Default limit is smaller than totalElements. Performance issue. uri:$uri", __CLASS__);
            $args['page'] ++;
            $result += $this->put($uri, $params, $format, $args);
        }

        return $result;
    }

    public function deleteJson($uri, $params = array(), $args = array())
    {
        $this->set_header('Accept', 'application/json');
        $this->set_header('Content-Type', 'application/json');

        $this->delete($uri, $params, 'json', $args);
    }

    public function delete($uri, $params = array(), $format = NULL, $args = array())
    {
        if (\shirtplatform\utils\user\User::getSessionId() != null)
        {
            $uri .= ';jsessionid=' . \shirtplatform\utils\user\User::getSessionId();
        }

        if (!empty($args))
        {
            $uri .= '?' . http_build_query($args);
        }

        return $this->_call('delete', $uri, $params, $format);
    }

    public function api_key($key, $name = 'X-API-KEY')
    {
        $this->_curl->http_header($name, $key);
    }

    public function language($lang)
    {
        if (is_array($lang))
        {
            $lang = implode(', ', $lang);
        }

        $this->_curl->http_header('Accept-Language', $lang);
    }

    protected function _call($method, $uri, $params = array(), $format = NULL)
    {
        if (WsConstants::$eventHandler != null)
        {
            WsConstants::$eventHandler->preRequest($method, $uri, $params, $format);
        }
        if ($format !== NULL)
        {
            $this->format($format);
        }

        $this->_set_headers();

        if (strpos($uri, 'http://') !== 0 && strpos($uri, 'https://') !== 0)
        {
            $uri = WsConstants::$webservicesURL . $uri;
        }

        // Initialize cURL session
        $this->_curl->create($uri);

        // If authentication is enabled use it
        if ($this->http_auth != null && $this->http_user != null)
        {
            $this->_curl->http_login($this->http_user, $this->http_pass, $this->http_auth);
        }

        // We still want the response even if there is an error code over 400
        $this->_curl->option('failonerror', FALSE);

        //SSL settings
        $this->_curl->option('SSL_VERIFYPEER', false); // For ssl site
        $this->_curl->option('SSL_VERIFYHOST', false);
        $this->_curl->option('SSLVERSION', 3); // end ssl
        // Call the correct method with parameters
        $this->_curl->{$method}($params);

        // Execute and return the response from the REST server
        $response = $this->_curl->execute();

        // Format and return
        $arrayResp = $this->_format_response($response);

        if (isset($arrayResp['exception']) && WsConstants::$eventHandler != null)
        {
            WsConstants::$eventHandler->onException($arrayResp['exception']);
        }

        if (WsConstants::$eventHandler != null)
        {
            WsConstants::$eventHandler->postRequest($arrayResp);
        }


        return $arrayResp;
    }

    // If a type is passed in that is not supported, use it as a mime type
    public function format($format)
    {
        if (array_key_exists($format, $this->supported_formats))
        {
            $this->format = $format;
            $this->mime_type = $this->supported_formats[$format];
        } else
        {
            $this->mime_type = $format;
        }

        return $this;
    }

    public function set_header($name, $value)
    {
        $this->_headers[$name] = $value;
    }

    private function _set_headers()
    {
        if (!array_key_exists("Accept", $this->_headers))
            $this->set_header("Accept", $this->mime_type);

        foreach ($this->_headers as $k => $v)
        {
            $this->_curl->http_header(sprintf("%s: %s", $k, $v));
        }
    }

    public function debug()
    {
        $request = $this->_curl->debug_request();

        echo "=============================================<br/>\n";
        echo "<h2>REST Test</h2>\n";
        echo "=============================================<br/>\n";
        echo "<h3>Request</h3>\n";
        echo $request['url'] . "<br/>\n";
        echo "=============================================<br/>\n";
        echo "<h3>Response</h3>\n";

        if ($this->response_string)
        {
            echo "<code>" . nl2br(htmlentities($this->response_string)) . "</code><br/>\n\n";
        } else
        {
            echo "No response<br/>\n\n";
        }

        echo "=============================================<br/>\n";

        if ($this->_curl->error_string)
        {
            echo "<h3>Errors</h3>";
            echo "<strong>Code:</strong> " . $this->_curl->error_code . "<br/>\n";
            echo "<strong>Message:</strong> " . $this->_curl->error_string . "<br/>\n";
            echo "=============================================<br/>\n";
        }

        echo "<h3>Call details</h3>";
        echo "<pre>";
        print_r($this->_curl->info);
        echo "</pre>";
    }

    // Return HTTP status code
    public function status()
    {
        return $this->info('http_code');
    }

    // Return curl info by specified key, or whole array
    public function info($key = null)
    {
        return $key === null ? $this->_curl->info : @$this->_curl->info[$key];
    }

    // Set custom options
    public function option($code, $value)
    {
        $this->_curl->option($code, $value);
    }

    public function http_header($header, $content = NULL)
    {
        // Did they use a single argument or two?
        $params = $content ? array($header, $content) : array($header);

        // Pass these attributes on to the curl library
        call_user_func_array(array($this->_curl, 'http_header'), $params);
    }

    protected function _format_response($response)
    {
        $this->response_string = & $response;

        // It is a supported format, so just run its formatting method
        if (array_key_exists($this->format, $this->supported_formats))
        {
            return $this->{"_" . $this->format}($response);
        }

        // Find out what format the data was returned in
        $returned_mime = @$this->_curl->info['content_type'];

        // If they sent through more than just mime, stip it off
        if (strpos($returned_mime, ';'))
        {
            list($returned_mime) = explode(';', $returned_mime);
        }

        $returned_mime = trim($returned_mime);

        if (array_key_exists($returned_mime, $this->auto_detect_formats))
        {
            return $this->{'_' . $this->auto_detect_formats[$returned_mime]}($response);
        }

        return $response;
    }

    // Format XML for output
    protected function _xml($string)
    {
        return $string ? (array) simplexml_load_string($string, 'SimpleXMLElement', LIBXML_NOCDATA) : array();
    }

    // Format HTML for output
    // This function is DODGY! Not perfect CSV support but works with my REST_Controller
    protected function _csv($string)
    {
        $data = array();

        // Splits
        $rows = explode("\n", trim($string));
        $headings = explode(',', array_shift($rows));
        foreach ($rows as $row)
        {
            // The substr removes " from start and end
            $data_fields = explode('","', trim(substr($row, 1, -1)));

            if (count($data_fields) == count($headings))
            {
                $data[] = array_combine($headings, $data_fields);
            }
        }

        return $data;
    }

    // Encode as JSON
    protected function _json($string)
    {
        return json_decode($string, TRUE);
        //return CJSON::decode($string);
    }

    // Encode as Serialized array
    protected function _serialize($string)
    {
        return unserialize(trim($string));
    }

    // Encode raw PHP
    protected function _php($string)
    {
        $string = trim($string);
        $populated = array();
        eval("\$populated = \"$string\";");
        return $populated;
    }

    public function postFile($uri, $source, $name = null, $arguments = array())
    {
        $this->set_header('Accept', 'application/json');
        $this->set_header('Content-Type', 'multipart/form-data; boundary=---------------------------2583998328991');
        $this->_curl->setParamsFormat('undefined');

        $this->option('RETURNTRANSFER', true);
        $this->option('POST', true);

        $post = array(
            "uploadedFile" => /* "@". */ file_get_contents($source),
        );

        if (!empty($name))
        {
            $post['name'] = $name;
        }

        $response = $this->post($uri, $post, 'json', $arguments);

        return $response;
    }

    public function postFileByData($uri, &$source, $name = null, $isFileContent = FALSE)
    {
        $this->set_header('Accept', 'application/json');
        $this->set_header('Content-Type', 'multipart/form-data; boundary=---------------------------2583998328991');
        $this->_curl->setParamsFormat('undefined');

        $this->option('RETURNTRANSFER', true);
        $this->option('POST', true);

        $post = array(
            "uploadedFile" => ($isFileContent) ? $source : file_get_contents($source),
        );

        if (!empty($name))
        {
            $post['name'] = $name;
        }

        $response = $this->post($uri, $post, 'json');

        return $response;
    }

    public static function getImagePath($uri, $width = -1, $height = -1, $clearCache = false)
    {
        $url = WsConstants::$webservicesURL . $uri;
        if ($width !== -1 && $height !== -1)
        {
            $url .= '?height=' . $height . '&width=' . $width;
        }
        if ($clearCache)
        {
            $url .= '&' . time();
        }

        return $url;
    }

}
