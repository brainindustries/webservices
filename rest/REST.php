<?php
namespace shirtplatform\rest;
require_once(__DIR__."/vendor/autoload.php");
//require_once 'guzzle.phar';

/**
 * CodeIgniter REST Class
 *
 * Mske REST requests to RESTful services with simple syntax.
 *
 * @package        	CodeIgniter
 * @subpackage    	Libraries
 * @category    	Libraries
 * @author        	Philip Sturgeon
 * @created			04/06/2009
 * @license         http://philsturgeon.co.uk/code/dbad-license
 * @link			http://getsparks.org/packages/restclient/show
 */
use shirtplatform\exception\DebugException;
use shirtplatform\constants\WsConstants;
use shirtplatform\parser\ArrayUtil;

class REST
{
    protected static $instance = null;
    protected $client = null;
    protected $asyncCalls = array();
    
    protected static $pool = null;
    protected $poolFront = [];
    
    public static function getInstance()
    {
        if(version_compare(phpversion(), '5.5') === -1) 
        {
            throw new DebugException("Unsuported PHP version. At least PHP 5.5 required.");
        }
        
        if( \shirtplatform\rest\REST::$instance == null )
        {
            \shirtplatform\rest\REST::$instance = new REST();
        }
        
        return \shirtplatform\rest\REST::$instance;
    }
    
    function __construct()
    {
       $stack = new \GuzzleHttp\HandlerStack(
            new \GuzzleHttp\Handler\CurlMultiHandler()
       );

       $this->client = new \GuzzleHttp\Client([
               'base_uri' => WsConstants::$webservicesURL  ,
               'http_errors' =>false,
               'verify' => false, //disable certificate verification
               'proxy' => WsConstants::$webservicesPROXY,
               'headers' => ['Connection' => 'close'],  
               'handler' => $stack 
       ]);  
    }
    
    public function setHeader($options)
    {
        $this->client->head( WsConstants::$webservicesURL, $options);
    }
    
    public function auth($user, $pass)
    {
        $promise = new promise\JsonPromise('GET', 'auth', [] ,['auth' => [$user, $pass] ] );
        return $promise->processRequest($this->client)->message;
    }

    public function &_call(promise\PlatformPromise &$promise )
    {
       if( $promise->filterPromise != null && $promise->filterPromise->message == null )
       {
           //execute filter first
           $promise->filterPromise->processRequest( $this->client );
       }
       
       if( $promise->isAsync() )
       {
            $promise->processRequest( $this->client );    
            $this->asyncCalls[] = $promise->promise;
            return $promise->message;
       }
       else
       {
           return $promise->processRequest( $this->client )->message;
       }
    }
    
    public function proccessInPool()
    {
        $requests = [];
        foreach ($this->poolFront as $promise)
        {
            $options = $promise->getOptions();
            $request = new \GuzzleHttp\Psr7\Request(
                    $promise->getMethod(), 
                    $promise->getUri(), 
                    (isset($options['headers']) ? $options['headers'] : []),
                    (isset($options['body']) ? $options['body'] : null)
            );
            unset($options['headers'], $options['body'], $options['version']);
            $request = $this->applyRequestOptions($request, $options);
            $requests[] = $request;
        }
        
        $front = $this->poolFront;
        $pool = new \GuzzleHttp\Pool($this->client, $requests, [
            'concurrency' => 200,
            'fulfilled' => function (\GuzzleHttp\Psr7\Response $response, $index) use($front) {
                $promise = $front[$index];
                $promise->message = $promise->processResponse($response);
                
                if ($promise->onFullfilled !== null)
                {
                    $fn = $promise->onFullfilled;
                    $fn($promise->message);
                }   
            },
            'rejected' => function (\GuzzleHttp\Exception\RequestException $reason, $index) {
                $promise = $front[$index];
                $dummyResponse = new \GuzzleHttp\Psr7\Response(401, array(), "", "1.1", $reason);
                if (WsConstants::$eventHandler != null)
                {
                    WsConstants::$eventHandler->onException($dummyResponse, $promise);
                }
            },
        ]);
        $promise = $pool->promise();
        $promise->wait();
    }
    
    private function applyRequestOptions(\Psr\Http\Message\RequestInterface $request, array &$options)
    {
        $modify = [];
        if (isset($options['form_params'])) {
            if (isset($options['multipart'])) {
                throw new \InvalidArgumentException('You cannot use '
                    . 'form_params and multipart at the same time. Use the '
                    . 'form_params option if you want to send application/'
                    . 'x-www-form-urlencoded requests, and the multipart '
                    . 'option to send multipart/form-data requests.');
            }
            $options['body'] = http_build_query($options['form_params'], null, '&');
            unset($options['form_params']);
            $options['_conditional']['Content-Type'] = 'application/x-www-form-urlencoded';
        }

        if (isset($options['multipart'])) {
            $elements = $options['multipart'];
            unset($options['multipart']);
            $options['body'] = new \GuzzleHttp\Psr7\MultipartStream($elements);
        }

        if (!empty($options['decode_content'])
            && $options['decode_content'] !== true
        ) {
            $modify['set_headers']['Accept-Encoding'] = $options['decode_content'];
        }

        if (isset($options['headers'])) {
            if (isset($modify['set_headers'])) {
                $modify['set_headers'] = $options['headers'] + $modify['set_headers'];
            } else {
                $modify['set_headers'] = $options['headers'];
            }
            unset($options['headers']);
        }

        if (isset($options['body'])) {
            if (is_array($options['body'])) {
                $this->invalidBody();
            }
            $modify['body'] = \GuzzleHttp\Psr7\stream_for($options['body']);
            unset($options['body']);
        }

        if (!empty($options['auth'])) {
            $value = $options['auth'];
            $type = is_array($value)
                ? (isset($value[2]) ? strtolower($value[2]) : 'basic')
                : $value;
            $config['auth'] = $value;
            switch (strtolower($type)) {
                case 'basic':
                    $modify['set_headers']['Authorization'] = 'Basic '
                        . base64_encode("$value[0]:$value[1]");
                    break;
                case 'digest':
                    // @todo: Do not rely on curl
                    $options['curl'][CURLOPT_HTTPAUTH] = CURLAUTH_DIGEST;
                    $options['curl'][CURLOPT_USERPWD] = "$value[0]:$value[1]";
                    break;
            }
        }

        if (isset($options['query'])) {
            $value = $options['query'];
            if (is_array($value)) {
                $value = http_build_query($value, null, '&', PHP_QUERY_RFC3986);
            }
            if (!is_string($value)) {
                throw new \InvalidArgumentException('query must be a string or array');
            }
            $modify['query'] = $value;
            unset($options['query']);
        }

        if (isset($options['json'])) {
            $modify['body'] = \GuzzleHttp\Psr7\stream_for(json_encode($options['json']));
            $options['_conditional']['Content-Type'] = 'application/json';
            unset($options['json']);
        }

        $request = \GuzzleHttp\Psr7\modify_request($request, $modify);
        if ($request->getBody() instanceof \GuzzleHttp\Psr7\MultipartStream) {
            // Use a multipart/form-data POST if a Content-Type is not set.
            $options['_conditional']['Content-Type'] = 'multipart/form-data; boundary='
                . $request->getBody()->getBoundary();
        }

        // Merge in conditional headers if they are not present.
        if (isset($options['_conditional'])) {
            // Build up the changes so it's in a single clone of the message.
            $modify = [];
            foreach ($options['_conditional'] as $k => $v) {
                if (!$request->hasHeader($k)) {
                    $modify['set_headers'][$k] = $v;
                }
            }
            $request = \GuzzleHttp\Psr7\modify_request($request, $modify);
            // Don't pass this internal value along to middleware/handlers.
            unset($options['_conditional']);
        }

        return $request;
    }
    
    public function appendToPool(promise\PlatformPromise $promise)
    {
        $this->poolFront[] = $promise;
    }

    public static function getImagePath($uri, $width = -1, $height = -1, $clearCache = false)
    {
        $url = WsConstants::$webservicesURL . $uri;
        if ($width !== -1 && $height !== -1)
        {
            $url .= '?height=' . $height . '&width=' . $width;
        }
        if ($clearCache)
        {
            $url .= '&' . time();
        }

        return $url;
    }
    
    public function wait()
    {
        $this->proccessInPool();
        $this->poolFront = [];
        
        if (WsConstants::$eventHandler != null)
        {
            WsConstants::$eventHandler->asyncUnwrapped();
        }
    }
    
    /*public function wait()
    {
        //\GuzzleHttp\Promise\unwrap($this->asyncCalls);
        \GuzzleHttp\Promise\all($this->asyncCalls)->wait();
        
        if (WsConstants::$eventHandler != null)
        {
            WsConstants::$eventHandler->asyncUnwrapped();
        }
        
        $this->asyncCalls=array();
    }*/
    
    function __destruct()
    {
        $this->asyncCalls=array();
    }
}
