<?php
$p = new Phar('shirtplatform.phar', FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::KEY_AS_FILENAME, 'shirtplatform.phar');
$p->startBuffering();

$p->buildFromDirectory('.', '$(.*)\.php$');

//Stop buffering write requests to the Phar archive, and save changes to disk
$p->stopBuffering();
echo "my.phar archive has been saved";

?>
