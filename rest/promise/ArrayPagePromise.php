<?php

namespace shirtplatform\rest\promise;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ArrayPagePromise
 *
 * @author admin
 */
class ArrayPagePromise extends JsonPromise
{
    
    private $className;
    public $parents = [];
    
    /**
     * 
     * @param type $method
     * @param type $uri
     * @param type $className
     * @param type $query
     * @param type $body
     * @param type $isAsync
     */
    public function __construct($method, $uri, $className, $query = [], $body = [], $isAsync = false)
    {
        parent::__construct($method, $uri, $query, ['body' => json_encode($body)], $isAsync);
        $this->className = $className;
        $this->message = [];
    }
    
    /**
     * 
     */
    protected function preProcess()
    {
        parent::preProcess();
        if (!isset($this->options['query']['size']) || $this->options['query']['size'] == null)
        {
            $this->options['query']['size'] = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            $this->options['query']['page'] = 0;
        }
    }
    
    /**
     * 
     * @param type $response
     * @return type
     */
    protected function &postProcess(&$response)
    {
        $className = $this->className;
        $response = parent::postProcess($response);
        
        if (!is_array($response) || !isset($response['records']))
        {
            \shirtplatform\constants\WsConstants::$eventHandler->onWrongReponse('Wrong response, no data.', $response, $this);
        }
        
        foreach($response['records'] as $wsItem)
        {
            $resultItem = new $className($wsItem);
            $this->message[$resultItem->getPrimaryKey()] = $resultItem;
        }
        return $this->message;
    }
    
}
