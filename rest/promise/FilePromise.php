<?php
namespace shirtplatform\rest\promise;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FilePromise
 *
 * @author Jan Maslik
 */
class FilePromise extends PlatformPromise
{
    public function __construct($method , $uri , &$data , $name = '' , $query = [], $isAsync = false)
    {
        parent::__construct($method, $uri, [], $isAsync);
        $this->options = [
            'multipart' => [[
                'name'     => 'uploadedFile',
                'Filename' =>  $name,
                'contents' => $data //fopen($data, 'r')
               ],
               [
                'name'     => 'name',
                'contents' => $name
               ], 
            ],
            'query' => $query,
            'headers' => [
                'Accept' => 'application/json',
                //'Connection' => 'close'
            ]
        ];
    }
    
    protected function &postProcess(&$response)
    {
        $result = json_decode($response->getBody()->getContents(),true );
        
        if (\shirtplatform\constants\WsConstants::$eventHandler != null)
        {
            \shirtplatform\constants\WsConstants::$eventHandler->postRequest($result , $this );
        }
        
        return $result;
    }
}