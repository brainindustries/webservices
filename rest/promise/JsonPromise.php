<?php
namespace shirtplatform\rest\promise;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use \shirtplatform\constants\WsConstants;
/**
 * Description of JsonPromise
 *
 * @author Jan Maslik
 */
class JsonPromise extends PlatformPromise
{
    public function __construct($method , $uri ,$query = array(), $options = array() , $isAsync = false)
    {
        parent::__construct($method, $uri, $options, $isAsync);
        $this->options['query'] = $query;
    }
    
    protected function preProcess()
    {
        parent::preProcess();
        \shirtplatform\parser\ArrayUtil::setIfEmpty($this->options, 'headers', array());
        \shirtplatform\parser\ArrayUtil::setIfEmpty($this->options['headers'], 'Content-Type', 'application/json' );
        \shirtplatform\parser\ArrayUtil::setIfEmpty($this->options['headers'], 'Accept', 'application/json' );
    }
    
    protected function &postProcess(&$response)
    {
        $result = json_decode($response->getBody()->getContents(),true );
        
        if (WsConstants::$eventHandler != null)
        {
            WsConstants::$eventHandler->postRequest($result , $this );
        }
        
        return $result;
    }
}
