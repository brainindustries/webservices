<?php
namespace shirtplatform\rest\promise;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use shirtplatform\exception\DebugException;
/**
 * Description of JsonItemPromise
 *
 * @author Jan Maslik
 */
class ItemPromise extends JsonPromise
{
    public $shopId;
    public $parents=array();
    private $className;
    
    
    public function __construct($method , $uri , $className , $query = array() ,  $json=array() , $isAsync = false)
    {
        parent::__construct($method, $uri,$query,[], $isAsync);
        $this->message = new $className() ;
        $this->className = $className;
        $this->options['json'] = $json;
    }
    
    protected function &postProcess(&$response)
    {
       $className = $this->className;
       $arrayResp = parent::postProcess($response);

       if (isset($arrayResp['pagedData']))
       {
            throw new DebugException("For paged data use getPageJson method!");
       }
       
       $wsEntity = \shirtplatform\parser\WsParse::getItem($arrayResp, $className::VAR_NAME  );
       
       $this->message->set($wsEntity);
       
       if(method_exists( $this->message , 'setShopId' ) )
       {
           $this->message->setShopId( $this->shopId );
       }
       
       if(method_exists( $this->message, 'setParents' ) )
       {
           $this->message->setParents( $this->parents );
       }
       
       return $this->message;
    }
    
    
}
