<?php
namespace shirtplatform\rest\promise;


/**
 *
 * @author Jan Maslik
 */
class CountPromise extends JsonPromise
{
    
    public function __construct($method , $uri , $query = array() ,  $json=array() , $isAsync = false)
    {
        parent::__construct($method, $uri,$query, [], $isAsync);
        $this->options['json'] = $json;
        $this->message = -1;
    }
    
    protected function preProcess()
    {
        parent::preProcess();
         
        \shirtplatform\parser\ArrayUtil::setIfEmpty($this->options[], 'query' , [] );
        $this->options['query']['size'] = 1;
        $this->options['query']['page'] = 0;
    }
    
    protected function &postProcess(&$response)
    {
       $response = parent::postProcess($response);
       $this->message = \shirtplatform\parser\WsParse::getTotalElements($response);

       return $this->message;
    }
    
}
