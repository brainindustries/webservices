<?php
namespace shirtplatform\rest\promise;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PagePromise
 *
 * @author Jan Maslik
 */
class PagePromise extends JsonPromise
{
    private $getAll= false;
    private $className;
    public $parents=array();
    public $shopId;
    
    /**
     * 
     * @param type $method
     * @param type $uri
     * @param type $className
     * @param type $query
     * @param type $json
     * @param type $isAsync
     */
    public function __construct($method , $uri , $className , $query = array() ,  $json=array() , $isAsync = false)
    {
        parent::__construct($method, $uri,$query, [], $isAsync);
        $this->className = $className;
        $this->message = array();
        $this->options['json'] = $json;
    }
    
    
    protected function preProcess()
    {
        parent::preProcess();

        if (!isset($this->options['query']['size']) || $this->options['query']['size'] == null)
        {
            $this->options['query']['size'] = \shirtplatform\constants\WsConstants::WS_DEFAULT_PAGE_SIZE;
            //paging not alowed if we want to load all
            $this->options['query']['page'] = 0;
            $this->getAll = true;
        }
    }
    
    
    protected function &postProcess(&$response)
    {

        $className = $this->className;
        $response = parent::postProcess($response);//json_decode($response->getBody()->getContents(),true );
        
        if (!isset($response['pagedData']['totalElements']) && \shirtplatform\constants\WsConstants::$eventHandler != null)
        {
            \shirtplatform\constants\WsConstants::$eventHandler->onWrongReponse("There is no page data!", $response , $this );
        }
        
        if(\shirtplatform\constants\WsConstants::$eventHandler == null && (!isset($response['pagedData']) || !isset($response['pagedData']['totalElements'])))
        {
            throw new \Exception("Returned Bad Data, eventHandler not set");
        }

        $total = intval($response['pagedData']['totalElements']);

        if ($this->getAll && $total > intval($this->options['query']['size']))
        {
            \shirtplatform\utils\log\Log::warn("Default limit is smaller than totalElements. Performance issue. uri:$this->uri", __CLASS__);
            $this->options['query']['size'] = ($total < 1) ? 1 : $total;
            $this->isAsync = false;
            $rest = \shirtplatform\rest\REST::getInstance();
            
            $rest->_call($this);
            
            return $this->message;
        }
        
        $wsEntities = \shirtplatform\parser\WsParse::getList($response, $className::VAR_NAME );
        $counter = 0;
        
        foreach($wsEntities as $wsEntity )
        {
            $resultItem = new $className($wsEntity);

            if (method_exists($resultItem, 'setShopId'))
            {
                $resultItem->setShopId($this->shopId);
            }

            if (method_exists($resultItem, 'setParents'))
            {
                $resultItem->setParents($this->parents);
            }
            
            if ( method_exists($resultItem,"getPrimaryKey") && $resultItem->getPrimaryKey() !== null)
            {
                $this->message[$resultItem->getPrimaryKey()] = $resultItem;
            } 
            else
            {
                $this->message['UNDEFINED-' . $counter++] = $resultItem; //this should not happend
            }
        }
        
        /*if (\shirtplatform\constants\WsConstants::$eventHandler != null)
        {
            \shirtplatform\constants\WsConstants::$eventHandler->postRequest($response , $this );
        }*/
        
        return $this->message;
    }
    
}
