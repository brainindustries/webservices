<?php
namespace shirtplatform\rest\promise;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use shirtplatform\constants\WsConstants;
/**
 * Description of PlatformPromise
 *
 * @author Jan Maslik
 */
class PlatformPromise
{
    public $promise;
    //private $format='';
    protected $uri;
    protected $method;
    protected $options;
    protected $isAsync = false;        
    
    /**
     * Parsed response
     */
    public $message = null;
    
    /**
     * Custom tracking data
     */
    public $data = []; 
    public $filterPromise = null; //maybe only in page
    
    public $onFullfilled = null;
            
            
    public function __construct($method , $uri , $options , $isAsync = false)
    {
        $this->method = $method;
        $this->uri = $uri;
        $this->options = $options;
        $this->isAsync = $isAsync;
    }
    
    protected function preProcess()
    {
        if($this->filterPromise != null )
        {
            \shirtplatform\parser\ArrayUtil::setIfEmpty( $this->options, 'query', array() );
             $this->options['query']['filterId']= $this->filterPromise->message;
        }
        
        $this->uri = str_replace(WsConstants::$webservicesURL, '' ,  $this->uri);
        
        if (\shirtplatform\utils\user\User::getSessionId() != null)
        {
            $this->uri .= ';jsessionid=' . \shirtplatform\utils\user\User::getSessionId();
        }

    }
    
    public function &processRequest(\GuzzleHttp\Client &$client)
    {
        $this->preProcess();
        
        if (WsConstants::$eventHandler != null)
        {
            WsConstants::$eventHandler->preRequest($this);
        }
        
        if ($this->isAsync() )
        {
            \shirtplatform\rest\REST::getInstance()->appendToPool($this);
            
            /*$this->promise = $client->requestAsync($this->method, $this->uri, $this->options)->then(
                function ($response)
                {
                    $this->message = $this->processResponse($response);
                    if ($this->onFullfilled !== null)
                    {
                        $fn = $this->onFullfilled;
                        $fn($this->message);
                    }
                }, 
                function ($reason)
                {
                    $dummyResponse = new \GuzzleHttp\Psr7\Response(401, array(), "", "1.1", $reason);
                    
                    if ( WsConstants::$eventHandler != null)
                    {
                        WsConstants::$eventHandler->onException($dummyResponse, $this);
                    }
                });*/
        } 
        else
        {
            $response = $client->request($this->method, $this->uri, $this->options);
            $this->message = $this->processResponse($response);
        }

        return $this;
    }
    
    public function processResponse(&$response)
    {
        if ($response->getStatusCode() != 200 && $response->getStatusCode() != 204 && WsConstants::$eventHandler != null)
        {
            WsConstants::$eventHandler->onException($response, $this);
        }
        
        return  $this->postProcess($response);
    }
    
    protected function &postProcess(&$response)
    {
        $result = $response->getBody()->getContents();
        
        if (WsConstants::$eventHandler != null)
        {
            WsConstants::$eventHandler->postRequest($result , $this );
        }
        
        return $result;
    }
    
    public function isAsync()
    {
        return $this->isAsync && !WsConstants::$DISABLE_ASYNC_CALLS ;
    }
    function getUri()
    {
        return $this->uri;
    }

    function getMethod()
    {
        return $this->method;
    }

    function getOptions()
    {
        return $this->options;
    }

    
    public function toArray()
    {
        return [
            'class' => get_class($this),
            'method' => $this->method,
            'uri' => $this->uri,
            'options' => $this->options ,
            'async' => $this->isAsync ,
            'filter' => ($this->filterPromise == null ? null : $this->filterPromise->toArray() )
        ];
    }
    
    public function onFullfilled(callable $fn = null)
    {
        $this->onFullfilled = $fn;
        return $this;
    }
    
}
