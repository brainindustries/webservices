<?php
namespace shirtplatform\rest\promise;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FilterPromise
 *
 * @author Jan Maslik
 */
class FilterPromise extends JsonPromise
{
    const filterUri = 'filters;';
    private $filterName;
    private $response; //debug purposes
    
    public function __construct($filterName , $json=array() ,$useDefault= true, $isAsync = false)
    {
        $uri = 'filters';
        if( $useDefault )
        {
            $uri .= ';setDefaults=true';
        }
        parent::__construct('POST', $uri,[],[], $isAsync);
        $this->filterName = $filterName;
        $this->options['json'] = $json;
        ///unset($this->options['headers']['Connection']);
    }
    
  
    protected function &postProcess(&$response)
    {
        $this->response = parent::postProcess($response);
        return $this->response[$this->filterName]['filterId'];
    }
    
    public function getFilterName()
    {
        return $this->filterName;
    }
    
    public function toArray()
    {
        return array_merge(parent::toArray(), 
                [
                    'filterName' => $this->filterName,
                    'response' => $this->response
                ]);
    }
}
