<?php

namespace shirtplatform\resource;

/**
 * Description of UploadResource
 *
 * @author Jan Maslik
 */
use \shirtplatform\rest\REST;
use \User;
use \shirtplatform\entity\motive\Motive;

class UploadResource
{

    /**
     * Upload product Swf.
     * 
     * @param string $sourcePath
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadProductSwf($sourcePath, $isAsync = false)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/upload/productSwf';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, '', [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload product bitmap.
     * 
     * @param string $sourcePath
     * @param boolean $isFileContent
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadProductBitmap($sourcePath, $isFileContent = FALSE, $isAsync = false)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/upload/productImage';

        $rest = \shirtplatform\rest\REST::getInstance();

        $content = ($isFileContent) ? $sourcePath : fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, '', [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload product cool photo.
     * 
     * @param string $sourcePath
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadCoolPhoto($sourcePath, $isAsync = false)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/upload/coolPhoto';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, '', [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload sticker.
     * 
     * @param string $sourcePath
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadSticker($sourcePath, $isAsync = false)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/upload/sticker';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, '', [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload color texture.
     * 
     * @param string $sourcePath
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadColorTexture($sourcePath, $isAsync = false)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/upload/colorTexture';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, '', [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload color texture group.
     * 
     * @param string $sourcePath
     * @return \shirtplatform\entity\account\ColorTextureGroup
     */
    public static function &uploadColorTextureGroup($sourcePath)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/upload/colorTextureGroup';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, '', [], false);
        $data = $rest->_call($promise);
        if (isset($data[\shirtplatform\entity\account\ColorTextureGroup::VAR_NAME]))
        {
            $group = new \shirtplatform\entity\account\ColorTextureGroup($data[\shirtplatform\entity\account\ColorTextureGroup::VAR_NAME]);
            return $group;
        }
        elseif (\shirtplatform\constants\WsConstants::$eventHandler != null)
        {
            \shirtplatform\constants\WsConstants::$eventHandler->onWrongReponse("Color texture group doesnt contains entity!", $data, $promise);
        }

        return null;
    }

    /**
     * Upload color swf.
     * 
     * @param string $sourcePath
     * @param string $name
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadColorSwf($sourcePath, $name, $isAsync = false)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/upload/colorSwf';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload motive image.
     * 
     * @param int $motiveId
     * @param string $sourcePath
     * @param string $name
     * @param int $shopId
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadMotiveImage($motiveId, $sourcePath, $name, $shopId = null, $isAsync = false)
    {
        $url = Motive::getUrl($shopId);
        $url .= '/' . $motiveId . '/bitmap';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload customized motive image.
     * 
     * @param int $motiveId
     * @param string $sourcePath
     * @param int $shopId
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadCustomizedMotiveImage($motiveId, $sourcePath, $shopId = null, $isAsync = false)
    {
        $url = Motive::getUrl($shopId);
        $url .= '/' . $motiveId . '/bitmapCustomize';

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $sourcePath, '', [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload motive vector.
     * 
     * @param int $motiveId
     * @param string $sourcePath
     * @param string $name
     * @param int $shopId
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadMotiveVector($motiveId, $sourcePath, $name, $shopId = null, $isAsync = false)
    {
        $url = Motive::getUrl($shopId);
        $url .= '/' . $motiveId . '/vectorUpload';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload motive reduced image.
     * 
     * @param int $motiveId
     * @param boolean $fileContent
     * @param int $shopId
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadMotiveReducedImage($motiveId, $fileContent, $shopId = null, $isAsync = false)
    {
        $url = Motive::getUrl($shopId);
        $url .= '/' . $motiveId . '/bitmap';

        $rest = \shirtplatform\rest\REST::getInstance();

        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $fileContent, 'reduced', [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload account logo image.
     * 
     * @param string $sourcePath
     * @param string $name
     * @param boolean $isFileContent
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadAccountLogo($sourcePath, $name, $isFileContent = FALSE, $isAsync = false)
    {
        $url = \shirtplatform\entity\account\Account::getUrl();
        $url .= '/' . \shirtplatform\utils\user\User::getAccountId() . '/uploadLogo';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = ($isFileContent) ? $sourcePath : fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload shop logo image.
     * 
     * @param string $sourcePath
     * @param string $name
     * @param boolean $isFileContent
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadShopLogo($sourcePath, $name, $shopId = null , $isFileContent = FALSE, $isAsync = false)
    {
        if($shopId == null )
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }
        
        $url = \shirtplatform\entity\account\Shop::getUrl();
        $url .= '/' . $shopId . '/uploadLogo';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = ($isFileContent) ? $sourcePath : fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, [], $isAsync);
        return $rest->_call($promise);
    }
    
    /**
     * Upload attribute item picture.
     * 
     * @param string $sourcePath
     * @param string $name
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadAttributeItemPicture($sourcePath, $name, $isAsync = false)
    {
        $url = \shirtplatform\entity\account\Account::getUrl();
        $url .= '/' . \shirtplatform\utils\user\User::getAccountId() . '/upload/attributeItemPhoto';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload logo image.
     * 
     * @param string $sourcePath
     * @param string $name
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadLogo($sourcePath, $name, $isAsync = false)
    {
        $url = \shirtplatform\entity\account\Account::getUrl();
        $url .= '/uploadLogo';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload template svg.
     * 
     * @param string $sourcePath
     * @param string $name
     * @param boolean $isAsync
     * @return array
     */
    public static function &uploadTemplateSvg($sourcePath, $name, $isAsync = false)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/upload/templateSvg';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, [], $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload creator config ttf.
     * 
     * @param string $sourcePath
     * @param string $name
     * @param int $shopId
     * @param string $fontName
     * @param string $fontWeight @see shirtplatform\entity\enumerator\Font
     * @param string $fontStyle @see shirtplatform\entity\enumerator\Font
     * @return array
     */
    public static function &uploadCreatorConfigTtf($sourcePath, $name, $shopId = null, $fontName = null, $fontWeight = null, $fontStyle = null, $isAsync = false)
    {
        if ($shopId == null)
        {
            $shopId = User::getShopId();
        }

        $arguments = array();

        if ($fontName != null)
        {
            $arguments['name'] = $fontName;
        }

        if ($fontWeight != null)
        {
            $arguments['weight'] = $fontWeight;
        }

        if ($fontStyle != null)
        {
            $arguments['style'] = $fontStyle;
        }

        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/shops/' . $shopId . '/configCreator/ttf';

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, $arguments, $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload text motive ttf.
     * 
     * @param int $fontId
     * @param string $sourcePath
     * @param string $name
     * @param string $fontName
     * @param string $fontWeight @see shirtplatform\entity\enumerator\Font
     * @param string $fontStyle @see shirtplatform\entity\enumerator\Font
     * @return array
     */
    public static function &uploadTextMotiveTtf($fontId, $sourcePath, $name, $fontName = null, $fontWeight = null, $fontStyle = null, $isAsync = false)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/fonts/' . $fontId;

        $arguments = array();

        if ($fontName != null)
        {
            $arguments['name'] = $fontName;
        }

        if ($fontWeight != null)
        {
            $arguments['weight'] = $fontWeight;
        }

        if ($fontStyle != null)
        {
            $arguments['style'] = $fontStyle;
        }

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, $arguments, $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload new text motive ttf.
     * 
     * @param string $sourcePath
     * @param string $name
     * @param string $fontName
     * @param string $fontWeight
     * @param string $fontStyle
     */
    public static function &uploadNewTextMotiveTtf($sourcePath, $name, $fontName, $fontWeight, $fontStyle, $isAsync = false)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/fonts/data';

        $arguments = array();
        $arguments['name'] = $fontName;
        $arguments['weight'] = $fontWeight;
        $arguments['style'] = $fontStyle;

        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, $arguments, $isAsync);
        return $rest->_call($promise);
    }

    /**
     * Upload print profile.
     * 
     * @param string $sourcePath
     * @param string $name
     * @return array
     */
    public static function &uploadPrintProfile($sourcePath, $name)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/upload/printProfile';
        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name);
        return $rest->_call($promise);
    }

    public static function &uploadUserMotive($sourcePath, $name, $shopId = null)
    {
        if ($shopId == null)
        {
            $shopId = \shirtplatform\utils\user\User::getShopId();
        }

        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/shops/' . $shopId . '/creator/userMotives/uploadOne';
        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name);
        $data = $rest->_call($promise);
        $res = new \shirtplatform\entity\creator\UserMotive($data['creator.Motive']);
        return $res;
    }
    
    /**
     * Upload shipping carrier logo.
     * 
     * @param type $sourcePath
     * @param type $moduleId
     * @param type $carrierId
     * @return \shirtplatform\entity\creator\UserMotive
     */
    public static function uploadShippingCarrierLogo($sourcePath, $name, $moduleId, $carrierId)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/shippingModules/'.$moduleId.'/carriers/'.$carrierId.'/logo';
        $rest = \shirtplatform\rest\REST::getInstance();
        $content = fopen($sourcePath, 'r');
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name);
        $data = $rest->_call($promise);
        return $data;
    }
    
    public static function uploadConfigCreatorResource($shopId, $resourceId, $content, $name)
    {
        $url = str_replace(
            ['{accountId}', '{shopId}'], 
            [\shirtplatform\utils\user\User::getAccountId(), $shopId], 
            'accounts/{accountId}/shops/{shopId}/configCreator/resource'
        );
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, $content, $name, ['name' => $resourceId]);
        $data = \shirtplatform\rest\REST::getInstance()->_call($promise);
        return $data;
    }
    
    public static function deleteConfigCreatorResource($shopId, $resourceId)
    {
        $url = str_replace(
            ['{accountId}', '{shopId}'], 
            [\shirtplatform\utils\user\User::getAccountId(), $shopId], 
            'accounts/{accountId}/shops/{shopId}/configCreator/resource'
        );
        $promise = new \shirtplatform\rest\promise\PlatformPromise('DELETE', $url, ['query' => ['name' => $resourceId]]);
        $data = \shirtplatform\rest\REST::getInstance()->_call($promise);
        return $data;
    }
    
    public static function uploadStockSupplierCsv($stockSupplierId, $sourcePath, $name)
    {
        $url = \shirtplatform\entity\account\Account::getUrl();
        $url .= '/' . \shirtplatform\utils\user\User::getAccountId() . '/stockSuppliers/' . $stockSupplierId . '/importCsv';

        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, fopen($sourcePath, 'r'), $name, [], FALSE);
        return $rest->_call($promise);
    }
    
    public static function uploadFactoryStockCsv($factoryId, $sourcePath, $name)
    {
        $url = 'stock/factories/' . $factoryId . '/importCsv';
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\FilePromise('POST', $url, fopen($sourcePath, 'r'), $name, [], FALSE);
        return $rest->_call($promise);
    }

}
