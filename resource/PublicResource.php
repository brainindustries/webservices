<?php

namespace shirtplatform\resource;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

use \LocalSettings;
use \shirtplatform\entity\_interface\TemplateSvgLayer;

/**
 * Description of PublicResource
 *
 * @author Jan Maslik
 */
class PublicResource
{

    private static $IMG_COUNTER = 1;

    /**
     * Get domain url path.
     * 
     * @return string
     */
    private static function getPrefixedDomainUrl()
    {
        $domain = str_replace(\shirtplatform\constants\WsConstants::IMG_REPLACEMENT, self::$IMG_COUNTER . \shirtplatform\constants\WsConstants::IMG_REPLACEMENT, \shirtplatform\constants\WsConstants::$webservicesURL);

        self::$IMG_COUNTER = (self::$IMG_COUNTER >= 5 ? 1 : self::$IMG_COUNTER + 1 );

        return $domain;
    }
    
    private static function getNewDomainUrl()
    {
        $domain = \shirtplatform\constants\WsConstants::$webservicesURL;
        $domain = str_replace('/webservices/rest/', '/image-product/rest/', $domain);
        return $domain;
    }

    /**
     * Get Swf image url.
     * 
     * @param int $id
     * @return string
     */
    public static function getSwfImageUrl($id)
    {
        return self::getNewDomainUrl() . 'public/products/swfImage/' . $id . '/image';
    }

    /**
     * Get Swf source url.
     * 
     * @param int $id
     * @return string
     */
    public static function getSwfSourceUrl($id)
    {
        return self::getPrefixedDomainUrl() . 'public/swfSource/' . $id;
    }

    /**
     * Get bitmap image url.
     * 
     * @param int $id
     * @return string
     */
    public static function getBitmapImageUrl($id)
    {
        return self::getPrefixedDomainUrl() . 'public/bitmapImage/' . $id;
    }

    /**
     * Get cool photo image url.
     * 
     * @param int $id
     * @return string
     */
    public static function getCoolPhotoImageUrl($id)
    {
        return self::getPrefixedDomainUrl() . 'public/coolPhoto/' . $id;
    }

    /**
     * Get product image url.
     * 
     * @param int $productId
     * @param int $aViewId
     * @param int $aColorId
     * @return string
     */
    public static function getProductImageUrl($productId, $aViewId, $aColorId)
    {
        return self::getPrefixedDomainUrl() . "public/product/{$productId}/assignedViews/{$aViewId}/assignedColors/{$aColorId}/image";
    }

    /**
     * Get product preview url.
     * 
     * @param int $productId
     * @param int $accountId
     * @param int $shopId
     * @return string
     */
    public static function getProductPreviewUrl($productId, $accountId, $shopId)
    {
        $url = self::getPrefixedDomainUrl() . 'accounts/' . $accountId . '/shops/' . $shopId . '/products/' . $productId . '/image';
        return $url;
    }

    /**
     * Get motive preview url.
     * 
     * @param int $motiveId
     * @param int $accountId
     * @param int $shopId
     * @return string
     */
    public static function getMotivePreviewUrl($motiveId, $uuid)
    {
        return self::getPrefixedDomainUrl() . 'public/motive/' . $uuid . '/' . $motiveId . '/preview';
    }

    /**
     * Get ordered product image url.
     * 
     * @param int $orderedProductId
     * @param int $aViewId
     * @param int $width
     * @param int $height
     * @param boolean $showError
     * @return string
     */
    public static function getOrderedProductImageUrl($orderedProductId, $uuid, $aViewId, $width = null, $height = null, $showError = null)
    {
        $url = self::getPrefixedDomainUrl() . "public/product/ordered/{$uuid}/{$orderedProductId}/assignedViews/{$aViewId}/image";

        $delimiter = '?';
        if ($width != NULL)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != NULL)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }
        if ($showError != null)
        {
            $url .= $delimiter . 'showError=' . (($showError) ? 'true' : 'false' );
            $delimiter = '&';
        }
        return $url;
    }

    /**
     * Get shared product image url.
     * 
     * @param int $sharedProductId
     * @param int $aViewId
     * @param int $width
     * @param int $height
     * @return string
     */
    public static function getSharedProductImageUrl($sharedProductId, $uuid, $aViewId, $width = null, $height = null)
    {
        $url = self::getPrefixedDomainUrl() . "public/product/shared/{$uuid}/{$sharedProductId}/assignedViews/{$aViewId}/image";

        $delimiter = '?';
        if ($width != NULL)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != NULL)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }

        return $url;
    }

    /**
     * Get shared product image url.
     * 
     * @param int $sharedProductId
     * @param int $aViewId
     * @param int $width
     * @param int $height
     * @return string
     */
    public static function getCollectionProductImageUrl($templateProductId, $aViewId,$aColorId = null,$showStickers = null, $width = null, $height = null )
    {
        $url = self::getPrefixedDomainUrl() . "public/product/template/{$templateProductId}/assignedViews/{$aViewId}/image";

        $delimiter = '?';
        if ($width != NULL)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != NULL)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }
        
        if ($aColorId != NULL)
        {
            $url .= $delimiter . 'assignedColorId=' . $aColorId;
            $delimiter = '&';
        }
        
        if ($showStickers != NULL)
        {
            $url .= $delimiter . 'showStickers=' . $showStickers;
            $delimiter = '&';
        }
        
        return $url;
    }
    
    /**
     * Get motive data image url.
     * 
     * @param int $motiveDataId
     * @param int $width
     * @param int $height
     * @return string
     */
    public static function getMotiveDataImageUrl($motiveId, $uuid, $width = NULL, $height = NULL)
    {
        $url = self::getPrefixedDomainUrl() . "public/motive/{$uuid}/{$motiveId}/preview";

        $delimiter = '?';
        if ($width != NULL)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != NULL)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }

        return $url;
    }
    
    /**
     * 
     * @param int $motiveDataId
     * @param int|null $width
     * @param int|null $height
     * @return string
     */
    public static function getMotiveDataOriginalUrl($motiveId, $uuid, $width = NULL, $height = NULL)
    {
        $url = self::getPrefixedDomainUrl() . "public/motive/{$uuid}/{$motiveId}/original";

        $delimiter = '?';
        if ($width != NULL)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != NULL)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }

        return $url;
    }

    /**
     * Get production motive data url.
     * 
     * @param int $motiveDataId
     * @return string
     */
    public static function getProductionMotiveDataUrl($motiveId, $uuid)
    {
        return self::getPrefixedDomainUrl() . "public/motive/{$uuid}/{$motiveId}/production";
    }

    /**
     * Get motive Swf source url.
     * 
     * @param int $motiveId
     * @return string
     */
    public static function getMotiveSwfSourceUrl($motiveId)
    {
        return self::getPrefixedDomainUrl() . "public/motive/{$motiveId}/swf";
    }
    
    /**
     * Get motive Svg source url.
     * 
     * @param int $motiveId
     * @return string
     */
    public static function getMotiveSvgSourceUrl($motiveId)
    {
        return self::getPrefixedDomainUrl() . "public/motive/{$motiveId}/svg";
    }

    /**
     * Get barcode url.
     * 
     * @param int $value
     * @param int $width
     * @param int $height
     * @param int $fontSize
     * @return string
     */
    public static function getBarcode($value, $width = NULL, $height = NULL, $fontSize = NULL)
    {
        $url = self::getPrefixedDomainUrl() . "public/barcode128?value=" . $value;
        if ($width != null)
        {
            $url .= '&width=' . $width;
        }

        if ($height != null)
        {
            $url .= '&height=' . $height;
        }
        if ($fontSize != null)
        {
            $url .= '&fontSize=' . $fontSize;
        }

        return $url;
    }

    /**
     * Get product color image url.
     * 
     * @param int $productColorId
     * @param int $width
     * @param int $height
     * @param int $version
     * @return string
     */
    public static function getProductColorImageUrl($productColorId, $width = NULL, $height = NULL, $version = NULL)
    {
        $url = self::getPrefixedDomainUrl() . "public/productColor/{$productColorId}/preview";
        $delimiter = '?';

        if ($width != null)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != null)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }
        if ($version != null)
        {
            $url .= $delimiter . 'version=' . $version;
            $delimiter = '&';
        }

        return $url;
    }

    /**
     * Get print color image url.
     * 
     * @param int $printColorId
     * @param int $width
     * @param int $height
     * @param int $version
     * @return string
     */
    public static function getPrintColorImageUrl($printColorId, $width = NULL, $height = NULL, $version = NULL)
    {
        //^^
        $url = self::getPrefixedDomainUrl() . "public/printColor/{$printColorId}/preview";
        if ($width != NULL && $height != NULL)
        {
            $url .= '?width=' . $width . '&height=' . $height;
        }
        elseif ($width != NULL && $height == NULL)
        {
            $url .= '?width=' . $width;
        }
        elseif ($height != NULL && $width == NULL)
        {
            $url .= '?height=' . $height;
        }

        if ($version != NULL)
        {
            if ($width != NULL || $height != NULL)
            {
                $url .= '&version=' . $version;
            }
            else
            {
                $url .= '?version=' . $version;
            }
        }

        return $url;
    }

    /**
     * Get print color texture url.
     * 
     * @param int $textureId
     * @param int $width
     * @param int $height
     * @return string
     */
    public static function getPrintColorTextureUrl($textureId, $width = NULL, $height = NULL)
    {
        $url = self::getPrefixedDomainUrl() . 'public/colorTecture/' . $textureId;
        if ($width != NULL && $height != NULL)
        {
            $url .= '?width=' . $width . '&height=' . $height;
        }
        elseif ($width != NULL && $height == NULL)
        {
            $url .= '?width=' . $width;
        }
        elseif ($height != NULL && $width == NULL)
        {
            $url .= '?height=' . $height;
        }

        return $url;
    }

    /**
     * Get account logo url.
     * 
     * @param int $accountId
     * @param int $width
     * @param int $height
     * @param int $version
     * @return string
     */
    public static function getAccountLogo($accountId, $width = NULL, $height = NULL, $version = NULL)
    {
        $url = self::getPrefixedDomainUrl() . "public/accounts/{$accountId}/logo";
        $delimiter = '?';

        if ($width != null)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != null)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }
        if ($version != null)
        {
            $url .= $delimiter . 'version=' . $version;
            $delimiter = '&';
        }

        return $url;
    }

    /**
     * Get account logo url.
     * 
     * @param int $accountId
     * @param int $width
     * @param int $height
     * @param int $version
     * @return string
     */
    public static function getShopLogo($shopId, $width = NULL, $height = NULL, $version = NULL)
    {
        $url = self::getPrefixedDomainUrl() . "public/shops/{$shopId}/logo";
        $delimiter = '?';

        if ($width != null)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != null)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }
        if ($version != null)
        {
            $url .= $delimiter . 'version=' . $version;
            $delimiter = '&';
        }

        return $url;
    }
    
    /**
     * Get attribute image url.
     * 
     * @param int $pictureId
     * @param int $width
     * @param int $height
     * @param int $version
     * @return string
     */
    public static function getAttributeImage($pictureId, $width = NULL, $height = NULL, $version = NULL)
    {
        $url = self::getPrefixedDomainUrl() . "public/attributeItemPicture/{$pictureId}/preview";
        $delimiter = '?';

        if ($width != null)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != null)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }
        if ($version != null)
        {
            $url .= $delimiter . 'version=' . $version;
            $delimiter = '&';
        }

        return $url;
    }

    /**
     * Get sticker image url.
     * 
     * @param int $stickerImageId
     * @param int $width
     * @param int $height
     * @param int $version
     * @return string
     */
    public static function getStickerImage($stickerImageId, $width = NULL, $height = NULL, $version = NULL)
    {
        $url = self::getPrefixedDomainUrl() . "public/stickerImage/{$stickerImageId}";
        $delimiter = '?';

        if ($width != null)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != null)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }
        if ($version != null)
        {
            $url .= $delimiter . 'version=' . $version;
            $delimiter = '&';
        }

        return $url;
    }

    /**
     * Get font preview url.
     * 
     * @param int $fontId
     * @param string $text
     * @param int $size
     * @param int $version
     * @return string
     */
    public static function getFontPreviewUrl($fontId, $text = NULL, $size = NULL, $version = NULL)
    {
        $url = self::getPrefixedDomainUrl() . "public/image/font/{$fontId}/preview";
        $delimiter = '?';

        if ($text != null)
        {
            $url .= $delimiter . 'text=' . $text;
            $delimiter = '&';
        }

        if ($size != null)
        {
            $url .= $delimiter . 'size=' . $size;
            $delimiter = '&';
        }

        if ($version != null)
        {
            $url .= $delimiter . 'version=' . $version;
            $delimiter = '&';
        }

        return $url;
    }

    /**
     * Get font data preview url.
     * 
     * @param int $fontDataId
     * @param string $text
     * @param int $size
     * @param int $version
     * @param int $color
     * @return string
     */
    public static function getFontDataPreviewUrl($fontDataId, $text = NULL, $size = NULL, $version = NULL, $color = NULL)
    {
        $url = self::getPrefixedDomainUrl() . "public/image/font/data/{$fontDataId}/preview";
        $delimiter = '?';

        if ($text != null)
        {
            $url .= $delimiter . 'text=' . $text;
            $delimiter = '&';
        }

        if ($size != null)
        {
            $url .= $delimiter . 'size=' . $size;
            $delimiter = '&';
        }

        if ($version != null)
        {
            $url .= $delimiter . 'version=' . $version;
            $delimiter = '&';
        }

        if ($color != null)
        {
            $url .= $delimiter . 'color=' . $color;
        }

        return $url;
    }

    /**
     * Get template Svg data url.
     * 
     * @param int $fontDataId
     * @return string
     */
    public static function getTemplateSvgDataUrl($fontDataId)
    {
        return self::getPrefixedDomainUrl() . "public/templateSvg/{$fontDataId}/svgData";
    }

    /**
     * Get template svg url.
     * 
     * @param int $fontDataId
     * @param \shirtplatform\entity\_interface\TemplateSvgLayer $layerColor
     * @param int $width
     * @param int $height
     * @param int $version
     * @return string
     */
    public static function getTemplateSvgUrl($fontDataId, TemplateSvgLayer $layerColor = null, $width = NULL, $height = NULL, $version = NULL)
    {
        $url = self::getPrefixedDomainUrl() . "public/templateSvg/{$fontDataId}/preview";
        $delimiter = '?';

        if ($width != null)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != null)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }
        if ($layerColor != null)
        {
            if ($layerColor->clip != null)
            {
                $url .= $delimiter . 'clip=' . $layerColor->clip;
                $delimiter = '&';
            }
            if ($layerColor->background != null)
            {
                $url .= $delimiter . 'background=' . $layerColor->background;
                $delimiter = '&';
            }
            if ($layerColor->bounds != null)
            {
                $url .= $delimiter . 'bounds=' . $layerColor->bounds;
                $delimiter = '&';
            }
        }

        if ($version != null)
        {
            $url .= $delimiter . 'version=' . $version;
            $delimiter = '&';
        }

        return $url;
    }

    /**
     * Get ordered product composition url.
     * 
     * @param int $orderedProductId
     * @param int $assignedViewId
     * @return string
     */
    public static function getOrderedProductCompositionUrl($orderedProductId, $uuid, $assignedViewId)
    {
        return self::getPrefixedDomainUrl() . "public/product/ordered/{$uuid}/{$orderedProductId}/assignedViews/{$assignedViewId}/imageComposition";
    }

    /**
     * Get print profile preview.
     * 
     * @param int $whitePorfileId
     * @param int $colourProfileId
     * @param boolean $advanced
     * @param boolean $blackCompensation
     * @return string
     */
    public static function getPrintProfilePreview($whitePorfileId, $colourProfileId, $advanced, $blackCompensation)
    {
        return self::getPrefixedDomainUrl() . "public/profilePreview?whitePorfileId={$whitePorfileId}&colourProfileId={$colourProfileId}&advanced={$advanced}&blackCompensation={$blackCompensation}";
    }
    
    /**
     * Get shipping carrier logo url.
     * 
     * @param type $accountId
     * @param type $moduleId
     * @param type $carrierId
     * @return type
     */
    public static function getShippingCarrierLogoUrl($accountId, $moduleId, $carrierId, $width, $height)
    {
        return self::getPrefixedDomainUrl() . 'accounts/'.$accountId.'/shippingModules/'.$moduleId.'/carriers/'.$carrierId.'/logo?width=' . $width . '&height=' . $height;
    }
    
    /**
     * 
     * @param type $accountId
     * @param type $shopId
     * @param type $motiveId
     * @param type $width
     * @param type $height
     * @return string
     */
    public static function getMotiveCleanUrl($accountId, $shopId, $motiveId, $width = null, $height = null)
    {
        $url = self::getPrefixedDomainUrl() . "accounts/{accountId}/shops/{shopId}/motives/{motiveId}/cleanImage";
        $url = str_replace(['{accountId}', '{shopId}', '{motiveId}'], [$accountId, $shopId, $motiveId], $url);
        
        $delimiter = '?';
        if ($width != NULL)
        {
            $url .= $delimiter . 'width=' . $width;
            $delimiter = '&';
        }
        if ($height != NULL)
        {
            $url .= $delimiter . 'height=' . $height;
            $delimiter = '&';
        }

        return $url;
    }
    
    public static function getConfigCreatorResourcePreviewUrl($accountId, $shopId, $resourceId, $v)
    {
        $url = self::getPrefixedDomainUrl() . 'accounts/{accountId}/shops/{shopId}/configCreator/resource/preview/{resourceId}?v=' . $v;
        return str_replace(['{accountId}', '{shopId}', '{resourceId}'], [$accountId, $shopId, $resourceId], $url);
    }

}
