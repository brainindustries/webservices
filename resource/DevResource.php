<?php

namespace shirtplatform\resource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DevResource
 *
 * @author Janci
 */
class DevResource
{

    public static function test($id)
    {
        $url = 'dev/' . $id;

        $rest = \shirtplatform\rest\REST::getInstance();

        //return $rest->getJson($url);
    }

    public static function evictAll()
    {
        $url = 'dev/' . \User::getAccountId() . 'evictCache';

        $rest = \shirtplatform\rest\REST::getInstance();

        // return $rest->getJson($url);
    }

    public static function reorderEntity( $entityTypeId , $accountId , $all = false )
    {
        $url = 'dev/accounts/'.\shirtplatform\utils\user\User::getAccountId().'/reorderEntity/'.$entityTypeId;
        $rest = \shirtplatform\rest\REST::getInstance();
        $query = [
            'account'=> $accountId , 
            'all' => ($all ? 'true' : 'false')
        ];
        $promise = new \shirtplatform\rest\promise\JsonPromise('GET', $url, $query, []);
        return $rest->_call($promise);
        
    }

}
