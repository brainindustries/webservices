<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\resource;

/**
 * Description of ProductionResource
 *
 * @author Jan Maslik
 */
class ProductionResource
{

    const SUMMARY_URL = 'accounts/{accountId}/production/summary';

    /**
     * Build url path for generation of production data
     * 
     * @param array $printTechnologyIds  (string of technology ids separated by comma) list of technologies which you want to generate. If null , returns all.
     * @param array $productIds (string of product ids separated by comma) list of products to generate  
     * @param array $orderIds (string of order ids separated by comma) list of order to generate  
     * @param string $summaryId label in file lame. Depends of technology output settings
     * @return string url to platform
     */
    public static function getProductionDataUrl($printTechnologyIds = null, $productIds = null, $orderIds = null, $summaryId = null, $langId = null, $ignoreUnprepared = FALSE)
    {
        $url = \shirtplatform\constants\WsConstants::$webservicesURL . 'accounts/{accountId}/productionData';
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $url);
        $url .= ';jsessionid=' . \shirtplatform\utils\user\User::getSessionId();

        $delimiter = '?';

        if ($printTechnologyIds != null)
        {
            $url .= $delimiter . 'technologies=' . $printTechnologyIds;
            $delimiter = '&';
        }

        if ($productIds != null)
        {
            $url .= $delimiter . 'products=' . $productIds;
            $delimiter = '&';
        }
        if ($orderIds != null)
        {
            $url .= $delimiter . 'orders=' . $orderIds;
            $delimiter = '&';
        }

        if ($summaryId != null)
        {
            $url .= $delimiter . 'summary=' . $summaryId;
            $delimiter = '&';
        }

        if ($langId != null)
        {
            $url .= $delimiter . 'langId=' . $langId;
            $delimiter = '&';
        }

        if ($ignoreUnprepared != FALSE)
        {
            $url .= $delimiter . 'ignoreUnprepared=true';
            $delimiter = '&';
        }

        return $url;
    }

    /**
     * Get production data content.
     * 
     * @param array $printTechnologyIds
     * @param array $productIds
     * @param array $orderIds
     * @param string $summaryId
     * @param int $langId
     * @return mixed
     */
    public static function getProductionData($printTechnologyIds = null, $productIds = null, $orderIds = null, $summaryId = null, $langId = null)
    {
        return file_get_contents(self::getProductionDataUrl($printTechnologyIds, $productIds, $orderIds, $summaryId, $langId));
    }

    /**
     * Get production pages count.
     * 
     * @param array $printTechnologyIds
     * @param array $productIds
     * @param array $orderIds
     * @param string $summaryId
     * @return int
     */
    public static function getProductionPageCount($printTechnologyIds = null, $productIds = null, $orderIds = null, $summaryId = null)
    {
        $url = \shirtplatform\constants\WsConstants::$webservicesURL . 'accounts/{accountId}/productionPages';
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $url);
        $url .= ';jsessionid=' . \shirtplatform\utils\user\User::getSessionId();

        $delimiter = '?';

        if ($printTechnologyIds != null)
        {
            $url .= $delimiter . 'technologies=' . $printTechnologyIds;
            $delimiter = '&';
        }

        if ($productIds != null)
        {
            $url .= $delimiter . 'products=' . $productIds;
            $delimiter = '&';
        }
        if ($orderIds != null)
        {
            $url .= $delimiter . 'orders=' . $orderIds;
            $delimiter = '&';
        }

        if ($summaryId != null)
        {
            $url .= $delimiter . 'summary=' . $summaryId;
            $delimiter = '&';
        }
        $xmlstring = file_get_contents($url);

        $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
        //$json = json_encode($xml);
        //$array = json_decode($json, TRUE);

        return $xml->value;
    }

    public static function countCharaters($orderIds, $technologies = null)
    {
        $url = \shirtplatform\constants\WsConstants::$webservicesURL . 'accounts/{accountId}/production/characters';
        $url = str_replace('{accountId}', \shirtplatform\utils\user\User::getAccountId(), $url);
        $url .= ';jsessionid=' . \shirtplatform\utils\user\User::getSessionId();

        $url .= '?orders=' . $orderIds;
        if($technologies != null )
        {
            $url .= '&technologies=' . $technologies;
        }
        $xmlstring = file_get_contents($url);

        $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
        //$json = json_encode($xml);
        //$array = json_decode($json, TRUE);

        return $xml->value;
    }

    
    /*
      public static  function createSummary( $orderIds )
      {
      $url = str_replace('{accountId}' , \shirtplatform\utils\user\User::getAccountId() , self::SUMMARY_URL );

      $rest = \shirtplatform\rest\REST::getInstance();
      $wsResponse = $rest->getPageJson( $url  , null, array('orders' => $orderIds )  );

      return $wsResponse;
      } */
}
