<?php

namespace shirtplatform\resource;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ShopConnectionResource {

    /**
     * Link shopify shop with platform shop
     * @param type $connector
     * @param int $shopId
     * @param String $shopName
     * @param int $langId
     * @param boolean $countryId
     * @return type
     */
    public static function connect($connector, $shopId, $shopName, $langId, $countryId, $newAccount)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/connectors/shops/' . $shopId;
        $query = [
            'connector' => $connector,
            'shopName' => $shopName,
            'langId' => $langId, 
            'countryId' => $countryId,
            'newAccount' => (int) $newAccount
        ];
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\JsonPromise('POST', $url, $query);
        return $rest->_call($promise);
    }
    
    /**
     * Synchronize base product.
     * @param int $shopId
     * @param int $collectionProductId
     * @param int $connectorId
     * @return type
     */
    public static function synchronizeBaseProduct($shopId, $collectionProductId, $connectorId, \shirtplatform\entity\connection\SyncProductUpdateOptions $updateOptions)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/shops/' . $shopId . '/connectors/' . $connectorId . '/products/' . $collectionProductId;
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_class($updateOptions), [], [\shirtplatform\entity\connection\SyncProductUpdateOptions::VAR_NAME => $updateOptions], FALSE);
        return $rest->_call($promise);
    }
    
    /**
     * Synchronize collection product.
     * @param int $shopId
     * @param int $collectionProductId
     * @param int $connectorId
     * @return type
     */
    public static function synchronizeCollectionProduct($shopId, $collectionProductId, $connectorId, \shirtplatform\entity\connection\SyncProductUpdateOptions $updateOptions)
    {
        $url = 'accounts/' . \shirtplatform\utils\user\User::getAccountId() . '/shops/' . $shopId . '/connectors/' . $connectorId . '/templateProducts/' . $collectionProductId;
        $rest = \shirtplatform\rest\REST::getInstance();
        $promise = new \shirtplatform\rest\promise\ItemPromise('POST', $url, get_class($updateOptions), [], [\shirtplatform\entity\connection\SyncProductUpdateOptions::VAR_NAME => $updateOptions], FALSE);
        return $rest->_call($promise);
    }
    
   
}
