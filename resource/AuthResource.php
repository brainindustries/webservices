<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace shirtplatform\resource;

/**
 * Description of AuthResource
 *
 * @author Jan Maslik
 */
class AuthResource
{

    /**
     * Try login and return result.
     * 
     * @param string $userName
     * @param string $password
     * @param int $accountId
     * @return boolean
     */
    public static function wsLogin($userName, $password, $accountId = null)
    {
        $rest = \shirtplatform\rest\REST::getInstance();
        try
        {
            $response = $rest->auth($userName, $password);

            if ($accountId != null && $response['authSessionDetails']['accountId'] != $accountId)
            {
                return false;
            }

            if (!isset($response['authSessionDetails']['sessionId']) || $response['authSessionDetails']['sessionId'] == null)
            {
                return false;
            }

            \shirtplatform\utils\user\User::setUsername($userName);
            \shirtplatform\utils\user\User::setSessionId($response['authSessionDetails']['sessionId']);
            \shirtplatform\utils\user\User::setAccountId($response['authSessionDetails']['accountId']);
            \shirtplatform\utils\user\User::setUserId($response['authSessionDetails']['userId']);
            return true;
        }
        catch (\Exception $e)
        {
            \shirtplatform\utils\user\User::setUsername(null);
            \shirtplatform\utils\user\User::setSessionId(null);
            \shirtplatform\utils\user\User::setAccountId(null);
            return false;
        }
    }

    /**
     * Check if user is logged.
     * 
     * @return boolean
     */
    public static function isLogged()
    {
        $isLogged = self::checkIsLogged();

        if (!$isLogged && \shirtplatform\constants\WsConstants::$DEVELOPER_MODE)
        {
            if (\shirtplatform\utils\user\User::getUsername() != null && \shirtplatform\utils\user\User::getPassword() != null)
            {
                \shirtplatform\utils\log\Log::trace("Re-login ... ", __CLASS__);
                $identity = new \UserIdentity(\shirtplatform\utils\user\User::getUsername(), \shirtplatform\utils\user\User::getPassword());

                if ($identity->authenticate())
                {
                    Yii::app()->user->login($identity);
                    \shirtplatform\utils\log\Log::trace("Re-login success! ", __CLASS__);
                    return true;
                }
            }
        }

        return $isLogged;
    }

    /**
     * Check if user is logged.
     * 
     * @return boolean
     */
    private static function checkIsLogged()
    {
        //\shirtplatform\utils\log\Log::info("checking isLogged , sessionId : ". \shirtplatform\utils\user\User::getSessionId()  , __CLASS__);

        if (\shirtplatform\utils\user\User::getSessionId() == null)
        {
            return false;
        }


        $rest = \shirtplatform\rest\REST::getInstance();
        try
        {
            $promise = new \shirtplatform\rest\promise\JsonPromise('GET', 'auth');
            $response = $rest->_call($promise);
        }
        catch (\Exception $ex)
        {
            
        }

        if (isset($response) && is_array($response) && isset($response["authSessionDetails"]))
        {
            \shirtplatform\utils\user\User::setAccountId($response["authSessionDetails"]['accountId']);
            \shirtplatform\utils\user\User::setUserId($response["authSessionDetails"]['userId']);
            \shirtplatform\utils\user\User::setSessionId($response["authSessionDetails"]['sessionId']);
            return true;
        }

        \shirtplatform\utils\user\User::unsetAll();

        //\shirtplatform\utils\log\Log::trace('Login failed!',__CLASS__);

        return false;
    }

    /**
     * Logout current logged user.
     */
    public static function logout()
    {
        \shirtplatform\utils\user\User::unsetAll();
    }

}
