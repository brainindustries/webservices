<?php

     spl_autoload_register(function ($className) {
        $ds = DIRECTORY_SEPARATOR;
        $className = str_replace('\\', $ds, $className);
        $file = "./{$ds}{$className}.php";
        //$file = str_replace('shirtplatform'.DIRECTORY_SEPARATOR, '', $file);
        if (is_readable($file)) require_once $file;
    });
    