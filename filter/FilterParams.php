<?php

namespace shirtplatform\filter;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FilterParams
 *
 * @author admin
 */
class FilterParams implements \ArrayAccess
{

    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }
    
    public function &offsetGet($offset)
    {
        return $this->$offset;
    }
    
    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }
    
    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }
    
    public function removeByProperty($exp, $property)
    {
        if(isset($this->$exp))
        {
            $newArray = [];
            foreach($this->$exp as $condArray)
            {
                if(isset($condArray['property']) && $condArray['property'] === $property)
                {
                    continue;
                }
                $newArray[] = $condArray;
            }
            $this->$exp = $newArray;
        }
    }

}
