<?php

namespace shirtplatform\filter;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WsParameters
 *
 * @author Jan Maslik
 */
use \shirtplatform\rest\REST;
use shirtplatform\filter\Filter;

class WsParameters
{

    public $appendDefaultFilter = true;
    public $page;
    public $size;
    public $filter = null;
    private $filtePromise;
    private $params = array();

    function __construct($page = 0, $size = null)
    {
        $this->page = $page;
        $this->size = $size;
    }

    /**
     * Set filter.
     * 
     * @param Filter $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    }

    /**
     * Set an root entity name.
     * 
     * @param string $entityName
     */
    public function setRootEntityName($entityName, $force = false)
    {
        if ((!isset($this->filter) && !$this->appendDefaultFilter) || (!isset($this->filter) && $force))
        {
            $this->filter = new Filter();
        }

        if ($this->filter != null)
        {
            $this->filter->setEntityName($entityName);
        }
    }

    /**
     * Get filter name.
     * 
     * @return string
     */
    public function getFilterName()
    {
        if ($this->filter != null)
        {
            return $this->filter->name;
        }

        return null;
    }

    /**
     * Get a promise instance.
     * 
     * @param boolean $isAsync
     * @return \shirtplatform\rest\promise\FilterPromise
     */
    public function getPromise($isAsync = false)
    {
        if (!isset($this->filter))
        {
            return null;
        }

        if ($this->filtePromise == null)
        {
            $isAsync = false; //disabled for now
            $this->filtePromise = new \shirtplatform\rest\promise\FilterPromise($this->filter->name, array($this->filter->name => $this->filter->params), $this->appendDefaultFilter, $isAsync);
        }

        return $this->filtePromise;
    }

    /**
     * Build filter params.
     * 
     * @return array
     */
    public function buildParams()
    {
        $result = $this->params;
        $result['page'] = $this->page;
        $result['size'] = $this->size;

        return $result;
    }
    
    /**
     * Build filter params for POST requests.
     * @return array
     */
    public function buildPostData()
    {
        if ($this->filter == null || $this->filter->name == null)
        {
            return [];
        }
        
        return [
            $this->filter->name => $this->filter->params
        ];
    }

    /**
     * Add an query array.
     * 
     * @param array $params
     */
    public function addQuery($params)
    {
        $this->params = array_merge($this->params, $params);
    }

    /**
     * Add an simple expression.
     * 
     * @param string $property
     * @param string $op
     * @param string|int|boolean $value
     */
    public function addSimpleExpression($property, $op, $value)
    {
        if ($this->filter == null)
        {
            $this->filter = new Filter();
        }

        $this->filter->addSimpleExpression($property, $op, $value);
    }

    /**
     * Add left join simple expression.
     * 
     * @param string $property
     * @param string $op
     * @param string|int|boolean $value
     */
    public function addLeftJoinSimpleExpression($property, $op, $value)
    {
        if ($this->filter == null)
        {
            $this->filter = new Filter();
        }

        $this->filter->addLeftJoinSimpleExpression($property, $op, $value);
    }

    /**
     * Add expression contain.
     * 
     * @param string $property
     * @param array $list
     * @param boolean $negate
     */
    public function addExpressionContain($property, $list, $negate = false)
    {
        if ($this->filter == null)
        {
            $this->filter = new \shirtplatform\filter\Filter();
        }

        $this->filter->addExpressionContain($property, $list, $negate);
    }

    public function &getFilter()
    {
        if ($this->filter == null)
        {
            $this->filter = new \shirtplatform\filter\Filter();
        }
        
        return $this->filter;
    }
    /**
     * Add left join contain expression.
     * 
     * @param string $property
     * @param array $list
     * @param boolean $negate
     */
    public function addLeftJoinExpressionContain($property, $list, $negate = false)
    {
        if ($this->filter == null)
        {
            $this->filter = new \shirtplatform\filter\Filter();
        }

        $this->filter->addLeftJoinExpressionContain($property, $list, $negate);
    }

    /**
     * Add is null expression.
     * 
     * @param string $property
     */
    public function addExpressionIsNull($property)
    {
        if ($this->filter == null)
        {
            $this->filter = new Filter();
        }

        $this->filter->addExpressionIsNull($property);
    }

    /**
     * Add order by sort.
     * 
     * @param string $property
     * @param string $direction
     */
    public function addOrderBy($property, $direction = 'asc')
    {
        if ($this->filter == null)
        {
            $this->filter = new Filter();
        }

        $this->filter->addOrderBy($property, $direction);
    }

    /**
     * Add sub related filter.
     * 
     * @param \shirtplatform\filter\Filter $filter
     */
    public function addRelatedFilter(Filter $filter)
    {
        if ($this->filter == null)
        {
            $this->filter = new Filter();
        }
        $this->filter->setRelatedFilter($filter);
    }
    
    public function getCloned()
    {
        $clone = new WsParameters();
        $clone->appendDefaultFilter = $this->appendDefaultFilter;
        $clone->page = $this->page;
        $clone->size = $this->size;
        $clone->filter = clone $this->filter;
        $clone->filter->params = clone $this->filter->params;
        $clone->params = $this->params;
        return $clone;
    }

    /**
     * 
     * @return \stdClass
     * @throws \Exception
     */
    public function buildJsonFilter()
    {
        if($this->filter == null)
        {
            return new \stdClass();
        }
        
        $criteria = new \stdClass();
        foreach($this->filter->params as $filterId => $filters) {
            if($filterId === 'leftJoins')
            {
                continue;
            }
            
            foreach($filters as $filter) {
                $filterObject = [];
                if(in_array($filterId, ['expressionSimple', 'expressionIsNull'])) {
                    $filterObject = $filter;
                }elseif($filterId === 'expressionContain') {
                    $filterObject = [
                        'property' => $filter['property'],
                        'negation' => $filter['negation'],
                        'list' => $filter['list']['String']
                    ];
                }else {
                    throw new \Exception('Non implemented filter "' . $filterId . '".');
                }
                $criteria->{$filterId}[] = $filterObject;
            }
        }
        
        $leftJoins = [];
        foreach($this->filter->params as $filterId => $filters) {
            if($filterId !== 'leftJoins')
            {
                continue;
            }
            $leftJoins = $filters;
        }
        
        $result = new \stdClass();
        $result->criteria = $criteria;
        $result->leftJoins = $leftJoins;
        return $result;
    }
    
}
