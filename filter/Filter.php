<?php

namespace shirtplatform\filter;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Filter
 *
 * @author Jan Maslik
 */
use shirtplatform\parser\ArrayUtil,
    \User,
    \DebugException;

class Filter
{

    const EXPRESSION_CONTAIN = 'expressionContain';
    const EXPRESSION_DISJUNCTION = 'expressionDisjunction';
    const EXPRESSION_SIMPLE = 'expressionSimple';
    const EXPRESSION_IS_NULL = 'expressionIsNull';
    const ORDER_BY = 'order';
    const EXPRESSION_CONJUNCTION = 'expressionConjunction';

    public $params = NULL;
    public $name = null;

    function __construct($entityName = '', $params = NULL)
    {
        if($params !== NULL)
        {
            $this->params = $params;
        }
        else
        {
            $this->params = new FilterParams();
        }
        $this->setEntityName($entityName);
    }

    /**
     * Set entity name.
     * 
     * @param string $entityName
     */
    public function setEntityName($entityName)
    {
        if (!empty($entityName))
        {
            $this->name = lcfirst($entityName::VAR_NAME) . 'Filter';
        }
    }

    /**
     * Set filter name.
     * 
     * @param string $filterName
     */
    public function setFilterName($filterName)
    {
        $this->name = $filterName;
    }

    /**
     * Set sub related filter.
     * 
     * @param \shirtplatform\filter\Filter $filter
     * @throws DebugException
     */
    public function setRelatedFilter(Filter $filter)
    {
        if ($filter == null)
        {
            throw new DebugException('Unable to set filter!', $filter);
        }

        $this->params[$filter->name] = $filter->params;
    }

    /**
     * Add order by sort.
     * 
     * @param string $property
     * @param string $direction
     */
    public function addOrderBy($property, $direction = 'asc')
    {
        if (!isset($this->params[self::ORDER_BY]))
        {
            $this->params[self::ORDER_BY] = array();
        }
        /*
        $this->params[self::ORDER_BY][] = array(
            'property' => $property,
            'direction' => $direction,
        );
         */
        
        $this->params[self::ORDER_BY] = array_merge($this->params[self::ORDER_BY], [array(
            'property' => $property,
            'direction' => $direction,
        )]);
    }

    /**
     * Add contain expression.
     * 
     * @param string $property
     * @param array $items
     * @param boolean $negate
     * @return void
     */
    public function addExpressionContain($property, $items, $negate = false)
    {
        if (!isset($this->params[self::EXPRESSION_CONTAIN]))
        {
            $this->params[self::EXPRESSION_CONTAIN] = array();
        }
        if (empty($items))
        {
            return;
        }

        /*        
         * $this->params[self::EXPRESSION_CONTAIN][] = array(
            'property' => $property,
            'negation' => $negate,
            'list' => array('String' => $items),
        );
        */   
        
        $this->params[self::EXPRESSION_CONTAIN] = array_merge($this->params[self::EXPRESSION_CONTAIN], [array(
            'property' => $property,
            'negation' => $negate,
            'list' => array('String' => $items),
        )]);
    }

    /**
     * Add left josin expression contain.
     * 
     * @param string $property
     * @param array $items
     * @param boolean $negate
     * @return void
     */
    public function addLeftJoinExpressionContain($property, $items, $negate = false)
    {
        if (empty($items))
        {
            return;
        }

        if (!isset($this->params['lJ' . self::EXPRESSION_CONTAIN]))
        {
            $this->params['lJ' . self::EXPRESSION_CONTAIN] = array();
        }
/*
        $this->params['lJ' . self::EXPRESSION_CONTAIN][] = array(
            'property' => $property,
            'negation' => $negate,
            'list' => array('String' => $items),
        );*/
        
        $this->params['lJ' . self::EXPRESSION_CONTAIN] = array_merge($this->params['lJ' . self::EXPRESSION_CONTAIN], [array(
            'property' => $property,
            'negation' => $negate,
            'list' => array('String' => $items),
        )]);
    }

    /**
     * Add expression disjunction.
     * 
     * @param array $criterions
     */
    public function addExpressionDisjunction($criterions)
    {
        if (!isset($this->params[self::EXPRESSION_DISJUNCTION]))
        {
            $this->params[self::EXPRESSION_DISJUNCTION] = array();
        }
        /*
          $t=array();
          $t[self::EXPRESSION_CONTAIN][] = array(
          'property' => 'id',
          'negation' => true,
          'list' => array( 'String' => array(9)),
          );
          $t[self::EXPRESSION_IS_NULL][] = array(
          'property' => 'id'
          );
         */
        //$this->params[self::EXPRESSION_DISJUNCTION][] = $criterions;
        
        $this->params[self::EXPRESSION_DISJUNCTION] = array_merge($this->params[self::EXPRESSION_DISJUNCTION], [$criterions]);
    }

    /**
     * Add expression conjunction.
     * 
     * @param array $criterions
     */
    public function addExpressionConjunction($criterions)
    {
        if (!isset($this->params[self::EXPRESSION_CONJUNCTION]))
        {
            $this->params[self::EXPRESSION_CONJUNCTION] = array();
        }
        //$this->params[self::EXPRESSION_CONJUNCTION][] = $criterions;
        
        $this->params[self::EXPRESSION_CONJUNCTION] = array_merge($this->params[self::EXPRESSION_CONJUNCTION], [$criterions]);
    }

    /**
     * Add is null expression.
     * 
     * @param string $property
     */
    public function addExpressionIsNull($property)
    {
        if (!isset($this->params[self::EXPRESSION_IS_NULL]))
        {
            $this->params[self::EXPRESSION_IS_NULL] = array();
        }
        /*
        $this->params[self::EXPRESSION_IS_NULL][] = array(
            'property' => $property,
        );*/
        
        $this->params[self::EXPRESSION_IS_NULL] = array_merge($this->params[self::EXPRESSION_IS_NULL], [array(
            'property' => $property,
        )]);
    }

    /**
     * Add simple expression.
     * 
     * @param string $property
     * @param string $op
     * @param string|int|boolean $value
     */
    public function addSimpleExpression($property, $op, $value)
    {
        if (!isset($this->params[self::EXPRESSION_SIMPLE]))
        {
            $this->params[self::EXPRESSION_SIMPLE] = array();
        }

        //$this->params[self::EXPRESSION_SIMPLE][] = self::createSimpleExpression($property, $op, $value);
        $this->params[self::EXPRESSION_SIMPLE] = array_merge($this->params[self::EXPRESSION_SIMPLE], [self::createSimpleExpression($property, $op, $value)]);
    }

    /**
     * 
     * @param type $property
     * @param type $op
     * @param type $value
     * @return type
     */
    public static function createSimpleExpression($property, $op, $value)
    {
        return array(
            'property' => $property,
            'op' => $op,
            'value' => $value,
        );
    }
    /**
     * Add left join simple expression.
     * 
     * @param string $property
     * @param string $op
     * @param string|int|boolean $value
     */
    public function addLeftJoinSimpleExpression($property, $op, $value)
    {
        if (!isset($this->params['lJ' . self::EXPRESSION_SIMPLE]))
        {
            $this->params['lJ' . self::EXPRESSION_SIMPLE] = array();
        }
        /*
        $this->params['lJ' . self::EXPRESSION_SIMPLE][] = array(
            'property' => $property,
            'op' => $op,
            'value' => $value,
        );*/
        
        $this->params['lJ' . self::EXPRESSION_SIMPLE] = array_merge($this->params['lJ' . self::EXPRESSION_SIMPLE], [array(
            'property' => $property,
            'op' => $op,
            'value' => $value,
        )]);
    }
    /**
     * 
     * @param type $property
     * @param type $negate
     */
    public function addLeftJoinIsNullExpression($property)
    {
        if (!isset($this->params['lJ' . self::EXPRESSION_IS_NULL]))
        {
            $this->params['lJ' . self::EXPRESSION_IS_NULL] = array();
        }
        /*
        $this->params['lJ' . self::EXPRESSION_IS_NULL][] = array(
            'property' => $property,
        );*/
        
        $this->params['lJ' . self::EXPRESSION_IS_NULL] = array_merge($this->params['lJ' . self::EXPRESSION_IS_NULL], [array(
            'property' => $property,
        )]);
    }
}
