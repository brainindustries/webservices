<?php
namespace shirtplatform\callback;

/**
 * Defines whats happend before + after  call and on exception
 *
 * @author Jan Maslik
 */
class EmptyCallback 
{
    /**
     * Callback after asynchronous calls were loaded
     */
    public function asyncUnwrapped()
    {
        
    }
    
    /**
     * Callback called before each request
     * 
     * @param \shirtplatform\rest\promise\PlatformPromise $promise
     */
    public function preRequest(\shirtplatform\rest\promise\PlatformPromise &$promise)
    {
        
    }
    
    /**
     * Callback called after each request
     * 
     * @param type $response
     * @param \shirtplatform\rest\promise\PlatformPromise $promise
     */
    public function postRequest(&$response ,\shirtplatform\rest\promise\PlatformPromise &$promise)
    {
        
    }
    
    /**
     * Callback called on exception from WS
     * 
     * @param type $exception
     * @param \shirtplatform\rest\promise\PlatformPromise $promise
     */
   public function onException(&$exception, \shirtplatform\rest\promise\PlatformPromise &$promise)
    {
        
    }
    
    /**
     * Callback called on unexpected response from WS
     * 
     * @param type $msg
     * @param type $response
     * @param \shirtplatform\rest\promise\PlatformPromise $promise
     */
    public function onWrongResponse(&$msg , &$response , \shirtplatform\rest\promise\PlatformPromise &$promise)
    {
         
    }
    
    /**
     * Callback called on exception in parsing phase of response
     * 
     * @param \Exception $exception
     * @param type $msg
     * @param \shirtplatform\rest\promise\PlatformPromise $promise
     */
    public function onPhpException(\Exception &$exception ,  &$msg , \shirtplatform\rest\promise\PlatformPromise &$promise )
    {
        
    } 
    
}
