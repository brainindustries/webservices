<?php
namespace shirtplatform\callback;
/**
 * Trace ws calls and store them to database
 * 
 * SQL DML here:

 * CREATE TABLE IF NOT EXISTS `ws_client_trace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session` char(32) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `method` varchar(4) NOT NULL,
  `uri` varchar(2048) NOT NULL,
  `params` varchar(2048) NOT NULL,
  `is_exception` tinyint(1) NOT NULL DEFAULT '0',
  `exception` varchar(2048) NOT NULL,
  `is_wrong_format` tinyint(1) NOT NULL DEFAULT '0',
  `response` varchar(2048) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `session` (`session`),
  KEY `is_wrong_format` (`is_wrong_format`),
  KEY `is_exception` (`is_exception`),
  KEY `uri` (`uri`(255)),
  KEY `order_id` (`order_id`),
  KEY `created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ; 
 *
 * @author Jan Maslik
 */
class DatabasePlatformCallback extends \shirtplatform\callback\EmptyCallback
{
    private $conn = null;
    public $STORE_REQUEST = false;
    public $STORE_PARAMS = false;
    public $STORE_ALL_REPONSE = false;
    public $STORE_EXCEPTION = false;
    public $STORE_UNEXPECTED_RESPONSE = false;
    
    public function asyncUnwrapped()
    {
        
    }
    
    public function __construct($host , $user , $password , $schema,$port)
    {
       $this->conn = new \mysqli($host, $user , $password, $schema , $port);
       $this->conn->autocommit(TRUE);
       
       if ($this->conn->connect_errno) 
       {
           //TODO LOG error
           $this->conn = null;
       }
    }
    
    public function preRequest(\shirtplatform\rest\promise\PlatformPromise &$promise)
    {
       //HINT filter requests by method (trace post/put/delete only ) 
       //HINT Ignore  generation of filter , auth , or other requests by uri pattern


       if( $this->conn != null && $this->STORE_REQUEST)
       {
           //TODO getOrderId from uri using regexp - slash pattern . Or using some intern function
           $orderId = '';
           $session = \shirtplatform\utils\user\User::getSessionId();
           if( $this->STORE_PARAMS )
           {
               $promise->data['id'] = $this->conn->query("insert into ws_client_trace (session,order_id,method,uri,params) values ('{$session}','{$orderId}','{$promise->getMethod()}','{$promise->getUri()}','".implode($promise->getOptions())."')");
           }
           else
           {
               $promise->data['id'] = $this->conn->query("insert into ws_client_trace (session,order_id,method,uri) values ('{$session}','{$orderId}','{$promise->getMethod()}','{$promise->getUri()}')");
           }
       }
    }
    
    private function insert(\shirtplatform\rest\promise\PlatformPromise &$promise)
    {
        //TODO getOrderId from uri using regexp - slash pattern . Or using some intern function
        $orderId = null;
        $session = \shirtplatform\utils\user\User::getSessionId();
        $promise->data['id'] = $this->conn->query("insert into ws_client_trace (session,order_id,method,uri) values ('{$session}','{$orderId}','{$promise->getMethod()}','{$promise->getUri()}')");
    }
    
    public function postRequest(&$response ,\shirtplatform\rest\promise\PlatformPromise &$promise)
    {
       if( $this->conn != null && $this->STORE_ALL_REPONSE)
       {
           if( isset( $promise->data['id'] ) )
           {
               $this->insert($promise);
           }
                   
           $this->conn->query("update ws_client_trace  set response = '{$response->getReasonPhrase()}' where id = ".$promise->data['id'] );
       }
    }
    
    public function onException(&$exception, \shirtplatform\rest\promise\PlatformPromise &$promise)
    {
       //HINT Ignore  unauthorized request

       if( $this->conn != null && $this->STORE_EXCEPTION)
       {
            if( isset( $promise->data['id'] ) )
           {
               $this->insert();
           }
                   
           $this->conn->query("update ws_client_trace  set is_exception = 1 , exception = '{$exception}' where id = ".$promise->data['id'] );
       }
    }
    
    public function onWrongResponse(&$msg , &$response , \shirtplatform\rest\promise\PlatformPromise &$promise)
    {
        if( $this->conn != null && $this->STORE_UNEXPECTED_RESPONSE)
       {
            if( isset( $promise->data['id'] ) )
           {
               $this->insert();
           }
                   
           $this->conn->query("update ws_client_trace  set is_wrong_format = 1 , response = '{$response}' where id = ".$promise->data['id'] );
       }    
    }
    
    function __destruct()
    {
       if( $this->conn != null )
       {
            $this->conn->close();       
       }        
    }
        
}
